# Elster – PicSter

Elster was aimed at an ambitious project towards a general semantic peer-to-peer anotation infrastructure.
It has been instantiated as a single application, PicSter, enabling to annotate collaboratively pictures.

This is unmaintained software.

This repository contains the source code of the last version of PicSter (in the [picster directory](picster)).
It has been extracted from gforge.inria.fr as it was part of another repository containing the [https://gforge.inria.fr/moex/swip](swip software).
Unfortunately, the svn history was not preserved.

Jérôme Euzenat, 2023
