package org.mindswap.store.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

import org.mindswap.datasource.impl.FileDataSource;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class FileStore extends StoreImpl {
    private Model mModel;
    private File mFile;

    public FileStore(File theFile) {
        super("File: "+theFile.getName(), new FileDataSource(theFile));
        mModel = ModelFactory.createDefaultModel();
        mFile = theFile;
    }

//    public void connect() {
//        try {
//            mModel.read(new FileInputStream(mFile),null);
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    public void disconnect() {
//    }
//
//    public Model getInstances() {
//        return mModel;
//    }
//
//    public boolean submit(Model theModel) {
//        mModel.add(theModel);
//        try {
//            mModel.write(new FileOutputStream(mFile));
//            return true;
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//            return false;
//        }
//    }
}
