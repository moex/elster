package org.mindswap.store.impl;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

import java.util.HashSet;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;

import com.hp.hpl.jena.rdql.Query;
import com.hp.hpl.jena.rdql.QueryResults;

import org.mindswap.datasource.DataSource;

import org.mindswap.datasource.exception.DataSourceException;
import org.mindswap.datasource.exception.QueryException;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class LocalMarkupStore extends StoreImpl {
    private MarkupModel mModel;

    public LocalMarkupStore(MarkupModel theModel) {
        super("Local", null);
        mModel = theModel;
        mDataSource = new MarkupDataSource();
    }

    private class MarkupDataSource implements DataSource
    {
        public Model asModel() {
            return mModel.getWorkingModel(null);
        }

        public boolean connect() { return true; }
        public boolean disconnect() { return true; }

        public boolean add(Model theModel) { return false; }
        public boolean add(Statement theStatement) { return false; }
        public boolean add(Resource theSubj, Property thePred, RDFNode theObj) { return false; }

        public boolean delete(Model theModel) { return false; }
        public boolean delete(Statement theStatement) { return false; }
        public boolean delete(Resource theSubj, Property thePred, RDFNode theObj) { return false; }

        public QueryResults query(Query theQuery) throws QueryException {
            throw new QueryException("Cannot query this datasource!");
        }

        public Resource getResource(String theId) {
            return null;
        }
    }
}
