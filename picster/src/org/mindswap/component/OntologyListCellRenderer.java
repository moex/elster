// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import java.awt.Component;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDFS;

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.NamespaceManager;

import fr.inrialpes.exmo.elster.picster.markup.*;
import org.mindswap.markup.media.*;

public class OntologyListCellRenderer extends DefaultListCellRenderer {

    public Component getListCellRendererComponent(JList list,Object value,int index,boolean isSelected, boolean cellHasFocus)
    {
        super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);
        if (value instanceof Resource)
            setText(displayName( (Resource) value));
        return this;
    }

    private String displayName(Resource resource) {
        if (MarkupModel.USE_LABELS && resource.hasProperty(RDFS.label) &&
            !resource.getProperty(RDFS.label).getString().startsWith("_:")) {
            // the startsWith condition is a hack to filter out the bullshit labels
            // contained on many of the anon instances on the mindswap and swint sites...
            return resource.getProperty(RDFS.label).getString();
        }
        else if (resource.isAnon()) {
            return "("+JenaUtils.getType(resource,true).getLocalName()+")";
        } else if (MarkupModel.getQNames()) {
            return JenaUtils.getQName(resource);
        } else {
            return resource.getLocalName();
        }
    }
}
