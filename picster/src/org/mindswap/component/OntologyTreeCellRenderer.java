// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import fr.inrialpes.exmo.elster.picster.markup.*;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDFS;

import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.ImageIcon;
import javax.swing.JTree;

import java.awt.Component;

import org.mindswap.utils.JenaUtils;

import java.util.Iterator;
import org.mindswap.markup.media.*;

public class OntologyTreeCellRenderer extends DefaultTreeCellRenderer {
    private static final ImageIcon rootIcon = new ImageIcon(OntologyTreeCellRenderer.class.getResource("images/" + "root.gif"));
    //private static final ImageIcon classIcon = new ImageIcon(OntologyTreeCellRenderer.class.getResource("images/" + "class.gif"));
    private static final ImageIcon ontIcon = new ImageIcon(OntologyTreeCellRenderer.class.getResource("images/" + "ont.gif"));

    public OntologyTreeCellRenderer() {
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        if (node.equals(tree.getModel().getRoot())) {
            setIcon(rootIcon);
        } else if (!node.isRoot() && node.getParent().equals(tree.getModel().getRoot())) {
            setIcon(ontIcon);
        } else {
            setIcon(null);
        }

        Object userObject = ((DefaultMutableTreeNode) value).getUserObject();

        if (node.getUserObject() instanceof Resource) {
            Resource res = (Resource)userObject;

            setText(displayName(res));

            String text = "Properties:\n\n";

            boolean empty = true;
            Iterator iter = JenaUtils.getDeclaredProps(res).iterator();
            while (iter.hasNext()) {
                Resource r = (Resource)iter.next();
                text += JenaUtils.getQName(r)+"\n";
                empty = false;
            }

            if (empty)
                setToolTipText("Properties:\n\nNONE");
            else setToolTipText(text);
        } else {
            setToolTipText(null);
        }

        return this;
    }

    private String displayName(Resource resource) {
        if (resource.isAnon()) {
            return "("+JenaUtils.getType(resource,true).getLocalName()+")";
        } else if (MarkupModel.getQNames()) {
            return JenaUtils.getQName(resource);
        } else {
            if (MarkupModel.USE_LABELS && resource.hasProperty(RDFS.label)) {
                return resource.getProperty(RDFS.label).getString();
            } else {
                return resource.getLocalName();
            }
        }
    }
}
