// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import java.awt.Component;
import java.awt.BorderLayout;

import java.net.URL;

import javax.swing.JTree;
import javax.swing.JToolTip;
import javax.swing.JScrollPane;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.event.TreeExpansionEvent;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;

import com.hp.hpl.jena.rdf.model.Resource;

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.Pair;

import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Iterator;

import javax.swing.JPanel;

public class OntologyTreeView extends JPanel implements OntologyView, TreeSelectionListener, TreeWillExpandListener {

    private JTree mTree;
    private DefaultTreeModel mTreeModel;
    private OntologyManagerComponent mManager;

    public OntologyTreeView() {
        initGUI();
        updateView();
    }

    /**
     * Sets the parent ontology manager
     * @param theManager OntologyManagerComponent
     */
    public void setManager(OntologyManagerComponent theManager)
    {
        mManager = theManager;
    }
    
    public OntologyManagerComponent getManager()  {
    	return this.mManager;
    }

    /**
     * Sets the current resource to selected in the current view
     * @param theResource Resource resource to select
     */
    public void select(Resource theResource)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) mTree.getLastSelectedPathComponent();

        if (node != null && node.getUserObject() instanceof Resource)
        {
            Resource aRes = (Resource)node.getUserObject();
            if (!theResource.equals(aRes))
                searchInTree((DefaultMutableTreeNode)mTreeModel.getRoot(),theResource.toString(),true);
        }
        else {        	
        	boolean f = searchInTree((DefaultMutableTreeNode)mTreeModel.getRoot(),theResource.toString(),true);        	
        	if(!f)  
        		expandNode((DefaultMutableTreeNode)mTreeModel.getRoot());
        }
    }

    /**
     * Returns the user-friendly name of this component
     * @return String
     */
    public String getName() {
        return "Class Tree";
    }

    /**
     * Returns the UI for the tree view in the form of a JTree
     * @return JTree Swing UI representation of the data
     */
    public JTree getTree(){
        return mTree;
    }
    /**
     * Returns the UI component that represents this object
     * @return Component - the representative UI Component
     */
    public Component getComponent() {
        return this;
    }

    private LinkedHashMap models;
    private LinkedHashMap getModels() {
        if (models == null) {
            models = new LinkedHashMap();
        }
        return models;
    }

    /**
     * Sets the list of models used to build this view, also updates the UI
     * @param models LinkedHashMap - the new list of models
     */
    public void setModels(LinkedHashMap models) {
        this.models = models;
        updateView();
    }
    
    public int getNoOfModels ()  {
    	return this.models.size();
    }

    private void initGUI() {
        mTree = new JTree() {
            public JToolTip createToolTip() {
                org.mindswap.component.MultiLineToolTip tt = new org.mindswap.component.MultiLineToolTip();
                tt.setComponent(this);
                return tt;
            }
        };

        mTree.setScrollsOnExpand(true);
        javax.swing.ToolTipManager.sharedInstance().registerComponent(mTree);
        mTreeModel = new DefaultTreeModel(new DefaultMutableTreeNode("All Ontologies"));
        mTree.setModel(mTreeModel);
        mTree.addTreeSelectionListener(this);
        mTree.addTreeWillExpandListener(this);

        mTree.setCellRenderer(new OntologyTreeCellRenderer());
        mTree.setDragEnabled(true);

        setLayout(new BorderLayout());
        add(new JScrollPane(mTree), BorderLayout.CENTER);
    }

    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) mTree.getLastSelectedPathComponent();
        if (node != null) {
            mTree.scrollPathToVisible(new TreePath(node.getPath()));
        }

         if (node != null && node.getUserObject() instanceof Resource) {
             mTree.scrollPathToVisible(new TreePath(node.getPath()));

             Resource aRes = (Resource)node.getUserObject();
             mManager.setSelectedResource(aRes);
         }
    }
    
    public void setTreeResource(Resource res)  {
    	mManager.setSelectedResource(res);
    }

    private boolean searchInTree(DefaultMutableTreeNode node, String term) {
        return searchInTree(node, term, false);
    }

    private boolean searchInTree(DefaultMutableTreeNode node,String term, boolean strict) {
        boolean found = false;
        for (int i = 0; !found && i < node.getChildCount(); i++) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(i);
             if (child.getUserObject() instanceof Resource) {
                 Resource res = (Resource)child.getUserObject();

                 boolean match = false;
                 if (strict)
                     match = res.toString().equals(term);
                 else match = res.getLocalName().toLowerCase().indexOf(term.toLowerCase()) != -1;

                 if (!res.isAnon() && match) {
                     TreePath thePath = new TreePath(child.getPath());
                     mTree.setSelectionPath(thePath);
                     mTree.scrollPathToVisible(thePath);
                     return true;
                 } else found = searchInTree(child, term, strict);
             } else found = searchInTree(child, term, strict);
        }
        return found;
    }

    /**
     * Search in the tree for the ontology with the given url
     * @param ontURL String ontology to search for
     * @return boolean true if the ontology is in the tree, false otherwise
     */
    private boolean isInTree(String ontURL)
    {
        boolean inTree = false;
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();
        for (int i = 0; i < rootNode.getChildCount(); i++)
        {
            Pair data = (Pair)((DefaultMutableTreeNode)rootNode.getChildAt(i)).getUserObject();
            if (data.mSecond.toString().equals(ontURL))
            {
                inTree = true;
                break;
            } // if
        } // for
        return inTree;
    } // isInTree
    
    public void setCurrentNode(OntClass c)  {
    	DefaultMutableTreeNode foo = new DefaultMutableTreeNode(c);
    	expandNode(foo);
    }

    /**
     * Refreshes the UI view of the data.
     */
    public void updateView() {
        boolean rebuild = false;
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();

        // are there other cases when we would rebuild the tree?
       // commented by arun -- if (rootNode.getChildCount() == 0 || getModels().isEmpty())
            rebuild = true;

        if (rebuild)
        {
            mTreeModel = new DefaultTreeModel(new DefaultMutableTreeNode("All Ontologies"));
            mTree.setModel(mTreeModel);
            rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();
            //expand();
        } // if

        LinkedHashMap models = getModels();
        Set keys = models.keySet();
        Iterator keyIter = keys.iterator();
        while (keyIter.hasNext()) {
            URL ontURL = (URL) keyIter.next();
            String url = ontURL.toString();

            if (isInTree(url))
                continue;

            OntModel currModel = (OntModel) models.get(ontURL);

            int theIndex = url.lastIndexOf("/");
            if (theIndex == -1)
                theIndex = url.lastIndexOf("\\");
            int endIndex = url.lastIndexOf(".");
            if (endIndex < theIndex)
                endIndex = -1;

            String name = null;
            if (endIndex == -1)
                name = url.substring(theIndex + 1);
            else name = url.substring(theIndex + 1, endIndex);

            DefaultMutableTreeNode node = new DefaultMutableTreeNode(new Pair(name + " Ontology",url));

            com.hp.hpl.jena.util.iterator.ExtendedIterator ei = currModel.listClasses();
            while (ei.hasNext()) {
                OntClass oc = (OntClass) ei.next();

                if (!JenaUtils.isClass(oc) || oc.isAnon() || JenaUtils.isLanguageTerm(oc))
                    continue;

                com.hp.hpl.jena.util.iterator.ExtendedIterator scIter = oc.listSuperClasses();

                if (!scIter.hasNext()) {
                    DefaultMutableTreeNode foo = new DefaultMutableTreeNode(oc);
                    mTreeModel.insertNodeInto(foo,node,node.getChildCount());
                } else {
                    boolean top = true;
                    while (scIter.hasNext()) {
                        OntClass c = (OntClass) scIter.next();
                        if (!c.isAnon() && !JenaUtils.isLanguageTerm(c)) {
                            top = false;
                            break;
                        }
                    }
                    if (top)
                    {
                        DefaultMutableTreeNode foo = new DefaultMutableTreeNode(oc);
                        mTreeModel.insertNodeInto(foo,node,node.getChildCount());
                    }
                }
                scIter.close();
            }
            mTreeModel.insertNodeInto(node, rootNode, rootNode.getChildCount());
            mTree.setSelectionPath(new TreePath(node.getPath()));
            mTree.makeVisible(new TreePath(node.getPath()));
        }

        mTree.repaint();
        //by arun
        this.expand();//arun end
    }
    
    
    public void updateView(String userId) {
        boolean rebuild = false;
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();

        // are there other cases when we would rebuild the tree?
       // commented by arun -- if (rootNode.getChildCount() == 0 || getModels().isEmpty())
            rebuild = true;

        if (rebuild)
        {
            mTreeModel = new DefaultTreeModel(new DefaultMutableTreeNode("All Ontologies"));
            mTree.setModel(mTreeModel);
            rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();
            //expand();
        } // if

        LinkedHashMap models = getModels();
        Set keys = models.keySet();
        Iterator keyIter = keys.iterator();
        while (keyIter.hasNext()) {
            URL ontURL = (URL) keyIter.next();
            String url = ontURL.toString();

            if (isInTree(url))
                continue;

            OntModel currModel = (OntModel) models.get(ontURL);

            int theIndex = url.lastIndexOf("/");
            if (theIndex == -1)
                theIndex = url.lastIndexOf("\\");
            int endIndex = url.lastIndexOf(".");
            if (endIndex < theIndex)
                endIndex = -1;

            String name = null;
            if (endIndex == -1)
                name = url.substring(theIndex + 1);
            else name = url.substring(theIndex + 1, endIndex);
            
            DefaultMutableTreeNode node = null;
            if(name.indexOf('_') > -1)
            	node = new DefaultMutableTreeNode(new Pair(name + " Ontology",url));
            else 
            	node = new DefaultMutableTreeNode(new Pair(userId + name + " Ontology",url));

            com.hp.hpl.jena.util.iterator.ExtendedIterator ei = currModel.listClasses();
            while (ei.hasNext()) {
                OntClass oc = (OntClass) ei.next();

                if (!JenaUtils.isClass(oc) || oc.isAnon() || JenaUtils.isLanguageTerm(oc))
                    continue;

                com.hp.hpl.jena.util.iterator.ExtendedIterator scIter = oc.listSuperClasses();

                if (!scIter.hasNext()) {
                    DefaultMutableTreeNode foo = new DefaultMutableTreeNode(oc);
                    mTreeModel.insertNodeInto(foo,node,node.getChildCount());
                } else {
                    boolean top = true;
                    while (scIter.hasNext()) {
                        OntClass c = (OntClass) scIter.next();
                        if (!c.isAnon() && !JenaUtils.isLanguageTerm(c)) {
                            top = false;
                            break;
                        }
                    }
                    if (top)
                    {
                        DefaultMutableTreeNode foo = new DefaultMutableTreeNode(oc);
                        mTreeModel.insertNodeInto(foo,node,node.getChildCount());
                    }
                }
                scIter.close();
            }
            mTreeModel.insertNodeInto(node, rootNode, rootNode.getChildCount());
            mTree.setSelectionPath(new TreePath(node.getPath()));
            mTree.makeVisible(new TreePath(node.getPath()));
        }

        mTree.repaint();
        //by arun
        this.expand();//arun end
    }
    
    
    
    
    
    
    
    

    /**
     * Looks up the given term in the tree, if it is found, the term will be highlighted
     * @param searchTerm String the search term
     */
    public void lookup(String searchTerm) {
       searchInTree( (DefaultMutableTreeNode) mTreeModel.getRoot(), searchTerm);
    } // lookup

    public Resource getSelectedResource() {
        return (Resource)((DefaultMutableTreeNode)mTree.getSelectionPath().getLastPathComponent()).getUserObject();
    }

    /**
     * Collapses all nodes in the tree
     */
    public void collapse()
    {
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();

        if (rootNode == null)
            return;

        for (int i = 0; i < rootNode.getChildCount(); i++)
        {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)rootNode.getChildAt(i);
            mTree.collapsePath(new TreePath(child.getPath()));
        } // for
    }

    /**
     * Collapses all the nodes in the tree except for the most recently added ontology branch
     * which is the last child of the root.
     */
    public void expand()
    {
        collapse();
        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)mTreeModel.getRoot();

        if (rootNode == null || rootNode.getChildCount() == 0)
            return;

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)rootNode.getChildAt(rootNode.getChildCount()-1);
        expandNode(node);
        mTree.scrollPathToVisible(new TreePath(node.getPath()));
    }

    /**
     * Expands a given node and all the nodes under it.
     * @param theNode DefaultMutableTreeNode the parent node to expand
     */
    private void expandNode(DefaultMutableTreeNode theNode)
    {
        mTree.expandPath(new TreePath(theNode.getPath()));
        for (int i = 0; i < theNode.getChildCount(); i++)
        {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode)theNode.getChildAt(i);
            mTree.expandPath(new TreePath(child.getPath()));
            expandNode(child);
        } // for
    }

    public void treeWillExpand(TreeExpansionEvent ev)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)ev.getPath().getLastPathComponent();

        // this fxn autoloads the lower branches on the fly when you expand them.
        if (node.isLeaf())
        {
            addChildrenToTree(node);
        }
        else // not a leaf
        {
            for (int i = 0; i < node.getChildCount(); i++)
            {
                DefaultMutableTreeNode tn = (DefaultMutableTreeNode)node.getChildAt(i);
                if (tn.isLeaf())
                {
                    addChildrenToTree(tn);
                    mTreeModel.nodeStructureChanged(tn);
                }
            }
        }
    }

    public void treeWillCollapse(TreeExpansionEvent ev)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)ev.getPath().getLastPathComponent();

        // do nothing
    }

    /**
     * Given a parent node, find its children and add them to the parent node
     * @param parent DefaultMutableTreeNode the parent node
     */
    private void addChildrenToTree(DefaultMutableTreeNode parent)
    {
        if (parent.getUserObject() instanceof OntClass)
        {
            OntClass oc = (OntClass)parent.getUserObject();

            if (oc.isAnon())
                return;

            com.hp.hpl.jena.util.iterator.ExtendedIterator ex = oc.listSubClasses(false);
            while (ex.hasNext()) {
                OntClass r = (OntClass) ex.next();
                if (!r.equals(oc) && !r.isAnon()) {
                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(r);
                    mTreeModel.insertNodeInto(node, parent, parent.getChildCount());
                }
            }
            ex.close();
        }
    }
} // OntologyTreeView
