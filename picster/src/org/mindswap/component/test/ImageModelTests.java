package org.mindswap.component.test;

import junit.framework.TestCase;
import junit.framework.Assert;
import junit.framework.TestSuite;

import org.mindswap.markup.media.SelectionRegion;
import org.mindswap.markup.media.ImageModel;
import org.mindswap.markup.media.MarkupImageModel;

import fr.inrialpes.exmo.elster.picster.markup.*;

import java.awt.Rectangle;

import java.net.URL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class ImageModelTests extends TestCase {

    // TODO: test getRegions() ??

    public static TestSuite suite() {
        return new TestSuite(ImageModelTests.class);
    }

    public void test()
    {
        Rectangle aRect1 = new Rectangle(0,0,100,100);
        SelectionRegion aRegion1 = SelectionRegion.createRectangleRegion(aRect1);

        Rectangle aRect2 = new Rectangle(398,498,378,229);
        SelectionRegion aRegion2 = SelectionRegion.createRectangleRegion(aRect2);

        Rectangle aRect3 = new Rectangle(283,834,118,239);
        SelectionRegion aRegion3 = SelectionRegion.createRectangleRegion(aRect3);

        ImageModel aModel = new MarkupImageModel(new MarkupModel());
        aModel.addRegion(aRegion1);
        aModel.addRegion(aRegion2);

        // test contains
        Assert.assertTrue(aModel.containsRegion(aRegion1));
        Assert.assertFalse(aModel.containsRegion(aRegion3));

        // test image label
        Assert.assertEquals(aModel.getMediaLabel(),"");

        aModel.setMediaLabel("a label");
        Assert.assertEquals(aModel.getMediaLabel(),"a label");

        try {
            // test image url
            URL aURL = new URL("http://www.example.org/foo.jpg");

            aModel.setImageURL(aURL);

            Assert.assertEquals(aModel.getImageURL(),aURL);
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }

        // test select
        aModel.selectRegion(aRegion1);
        Assert.assertTrue(aRegion1.isSelected());

        aModel.selectRegion(aRegion2);
        Assert.assertFalse(aRegion1.isSelected());
        Assert.assertTrue(aRegion2.isSelected());

        // test deselect
        aModel.deselectRegion(aRegion2);
        Assert.assertFalse(aRegion2.isSelected());

        aModel.deselectAllRegions();
        Assert.assertFalse(aRegion1.isSelected());
        Assert.assertFalse(aRegion2.isSelected());

        // test delete
        aModel.deleteRegion(aRegion2);
        Assert.assertFalse(aModel.containsRegion(aRegion2));
        Assert.assertTrue(aModel.containsRegion(aRegion1));

//        aModel.addRegion(aRegion2);
//        aModel.selectRegion(aRegion2);
//        aModel.deleteRegion();
//        Assert.assertFalse(aModel.containsRegion(aRegion2));
//        Assert.assertTrue(aModel.containsRegion(aRegion1));

        // test clear
        aModel.clearRegions();
        Assert.assertFalse(aModel.getRegions().hasNext());
    }

}
