package org.mindswap.component.test;

import junit.framework.Assert;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.mindswap.markup.SubClassResourceSortComparator;
import fr.inrialpes.exmo.elster.picster.markup.*;

import org.mindswap.markup.utils.ResourceComparator;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResIterator;

import java.util.Vector;

import java.io.File;

import org.mindswap.utils.JenaUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class SortTests extends TestCase {
    public static TestSuite suite() {
        return new TestSuite(SortTests.class);
    }

    public void test()
    {
        MarkupModel aModel = new MarkupModel();

        try {
            aModel.addOntology(new File("tests\\sort_test.owl").toURL());
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }

        Vector list = new Vector();

        ResIterator rIter = aModel.getMasterOntologyModel().listSubjects();
        while (rIter.hasNext())
        {
            Resource aRes = rIter.nextResource();
            list.add(aRes);
        }

        java.util.Collections.sort(list,new SubClassResourceSortComparator(aModel));

        Model temp = ModelFactory.createDefaultModel();
        Vector checkList = new Vector();
        checkList.add(temp.getResource(AllTests.BASE+"A"));
        checkList.add(temp.getResource(AllTests.BASE+"B"));
        checkList.add(temp.getResource(AllTests.BASE+"C"));
        checkList.add(temp.getResource(AllTests.BASE+"D"));
        checkList.add(temp.getResource(AllTests.BASE+"E"));

        for (int i = 0; i < list.size(); i++)
        {
            Resource aRes = (Resource)list.elementAt(i);
            Assert.assertEquals(aRes,checkList.elementAt(i));
        }
    }

    public void testSort()
    {
        Vector aList = new Vector();

        Model aModel = ModelFactory.createDefaultModel().read(new File("tests\\reasoner_test.owl").toURI().toString());

        ResIterator rIter = aModel.listSubjects();
        while (rIter.hasNext()) {
            aList.add(rIter.nextResource());
        }

        java.util.Collections.sort(aList, new ResourceComparator());

        for (int i = 0; i < aList.size(); i++)
        {
            Resource aRes = (Resource)aList.elementAt(i);
            if (i+1 < aList.size()-1)
            {
                Resource aCompare = (Resource)aList.elementAt(i+1);

                if (!aCompare.isAnon() && aRes.isAnon())
                    Assert.fail("bnode sorting error");

                if (aCompare.isAnon() && aRes.isAnon())
                {
                    String one = JenaUtils.getLabel(aRes).toLowerCase();
                    String two = JenaUtils.getLabel(aCompare).toLowerCase();

                    if (two != null && one == null)
                        Assert.fail("bnode sorting by label error");
                    else if (two == null && one == null)
                        continue;
                    else if (one.compareTo(two) > 0)
                        Assert.fail("Did not sort resources ("+one+","+two+") properly "+one.compareTo(two));
                }
                else if (!aRes.isAnon() && aCompare.isAnon())
                {
                    String label = JenaUtils.getLabel(aCompare);

                    // we'll assume this is the start of the bnodes in the list
                    // and say it sorted ok
                    if (label == null)
                        continue;
                    else label = label.toLowerCase();

                    if (aRes.getLocalName().toLowerCase().compareTo(label) > 0)
                        Assert.fail("Did not sort resources ("+aRes.getLocalName()+","+aCompare+" == '"+label+"') properly "+aRes.getLocalName().toLowerCase().compareTo(label));
                }

                if (aRes.getLocalName().toLowerCase().compareTo(aCompare.getLocalName().toLowerCase()) > 0)
                    Assert.fail("Did not sort resources ("+aRes.getLocalName()+","+aCompare.getLocalName()+") properly "+aRes.getLocalName().compareTo(aCompare.getLocalName()));
            }
        }
    }

}
