// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component.test;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

import org.mindswap.markup.MarkupOWLReasoner;

import java.io.File;

import java.util.Set;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;

import org.mindswap.utils.JenaUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */
public class MarkupOWLReasonerTests extends TestCase {

    private static Property[] PROP_LIST;


    private MarkupOWLReasoner mReasoner;

    public static TestSuite suite() {
        return new TestSuite(MarkupOWLReasonerTests.class);
    }

    protected void setUp() {
        mReasoner = new MarkupOWLReasoner();

        mReasoner.load(ModelFactory.createDefaultModel().read(new File("tests\\reasoner_test.owl").toURI().toString()));

        PROP_LIST = new Property[] {
            mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp1"),
            mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp2"),
            mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp3"),
            mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp4"),
            mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp1"),
            mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp2"),
            mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp3"),
            mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp4")
        };
    }

    public void testDeclaredProperties()
    {
        Resource aRes = mReasoner.getModel().getResource(AllTests.BASE+"A");

        Set aAnswer = JenaUtils.getDeclaredProps(aRes);
        Set aResult = mReasoner.getDeclaredProperties(aRes,false);

        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());

        aRes = mReasoner.getModel().getResource(AllTests.BASE+"C");

        aAnswer = JenaUtils.getDeclaredProps(aRes);
        aResult = mReasoner.getDeclaredProperties(aRes,false);

        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
    }

//    public void testRanges()
//    {
//        Resource aRes = mReasoner.getModel().getResource(AllTests.BASE+"A");
//        Property aProp1 = mReasoner.getModel().getProperty(AllTests.BASE+"aDataProp1");
//        Property aProp2 = mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp2");
//
//        Set aAnswer = JenaUtils.getRanges(aProp1,aRes);
//        Set aResult = mReasoner.getRanges(aProp1,aRes);
//
//        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
//
//        aAnswer = JenaUtils.getRanges(aProp2,aRes);
//        aResult = mReasoner.getRanges(aProp2,aRes);
//
//        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
//
//        aRes = mReasoner.getModel().getResource(AllTests.BASE+"C");
//        aProp1 = mReasoner.getModel().getProperty(AllTests.BASE+"aDataProp2");
//        aProp2 = mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp1");
//
//        aAnswer = JenaUtils.getRanges(aProp1,aRes);
//        aResult = mReasoner.getRanges(aProp1,aRes);
//
//        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
//
//        aAnswer = JenaUtils.getRanges(aProp2,aRes);
//        aResult = mReasoner.getRanges(aProp2,aRes);
//
//        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
//    }

    public void testSuperProperties()
    {
        for (int i = 0; i < PROP_LIST.length; i++)
        {
            Property aProperty = PROP_LIST[i];

            Set aAnswer = JenaUtils.getAllSuperPropertiesOf(aProperty,mReasoner.getModel());
            Set aResult = mReasoner.getSuperProperties(aProperty);

            Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
        }
   }

    public void testSubProperties()
    {
        for (int i = 0; i < PROP_LIST.length; i++)
        {
            Property aProperty = PROP_LIST[i];

            Set aAnswer = JenaUtils.getAllSubPropertiesOf(aProperty,mReasoner.getModel());
            Set aResult = mReasoner.getSubProperties(aProperty);

            Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
        }
    }
}
