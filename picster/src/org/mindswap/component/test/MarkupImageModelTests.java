package org.mindswap.component.test;

import junit.framework.Assert;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.mindswap.markup.media.MarkupImageModel;
import fr.inrialpes.exmo.elster.picster.markup.*;

import org.mindswap.markup.media.SelectionRegion;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import com.hp.hpl.jena.vocabulary.RDF;

import java.awt.Rectangle;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class MarkupImageModelTests extends TestCase {
    private static final String IMAGE_URL = "http://www.mindswap.org/foo.jpg";
    private static final String INST1 = "http://www.example.org/#inst1";
    private static final String INST2 = "http://www.example.org/#inst2";
    private static final String INST3 = "http://www.example.org/#inst3";
    private static final String INST4 = "http://www.example.org/#inst4";

    public static TestSuite suite() {
        return new TestSuite(MarkupImageModelTests.class);
    }

    public void test() {
        org.mindswap.multimedia.impl.Mapper.mapImplementations();

        MarkupModel aMarkupModel = new MarkupModel();
        MarkupImageModel aModel = new MarkupImageModel(aMarkupModel);

        Model aJenaModel = ModelFactory.createDefaultModel();

        Model aResultModel = ModelFactory.createDefaultModel();
        aResultModel.add(aResultModel.getResource(IMAGE_URL),RDF.type,aMarkupModel.PROFILE.Image());

        SelectionRegion aRegion1 = SelectionRegion.createRectangleRegion(new Rectangle(0,0,100,100));
        aRegion1.setName("aRegion1");
        SelectionRegion aRegion2 = SelectionRegion.createRectangleRegion(new Rectangle(0,0,130,200));
        aRegion2.setName("aRegion2");
        SelectionRegion aRegion3 = SelectionRegion.createRectangleRegion(new Rectangle(0,0,140,300));
        aRegion3.setName("aRegion3");
        SelectionRegion aRegion4 = SelectionRegion.createRectangleRegion(new Rectangle(0,0,150,400));
        aRegion4.setName("aRegion4");
        SelectionRegion aRegion5 = SelectionRegion.createRectangleRegion(new Rectangle(0,0,350,550));
        aRegion5.setName("aRegion5");

        try {
            aModel.setImageURL(new java.net.URL(IMAGE_URL));

            aModel.setMetadata(ModelFactory.createDefaultModel().read(new java.io.File("tests\\sort_test.owl").toURL().toString()));
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }

        aModel.addRegion(aRegion2);

        // add image depicts
        aModel.addDepicts(aJenaModel.getResource(INST1));

        // add region depicts
        aModel.addRegionDepicts(aRegion1,aJenaModel.getResource(INST2));

        // set a pending depiction, but clear it so it doesnt get persisted
        aModel.setPendingDepiction(aRegion2);
        aModel.clearPendingDepiction();

        // now lets set one and persist it
        aModel.setPendingDepiction(aRegion3);
        aModel.addPendingDepiction(aJenaModel.getResource(INST3));

        Assert.assertTrue(aModel.getDepictedInstances(aRegion3).hasNext());

        // set our current region and instance, these should still be
        // these values after we're done
        //aModel.setCurrentInstance(aJenaModel.getResource(INST4));
        aModel.setCurrentRegion(aRegion4);

        // test region selection
        aModel.selectRegion(aRegion2);
        Assert.assertTrue(aRegion2.isSelected());
        Assert.assertEquals(aModel.getCurrentRegion(),aRegion2);

        // test region deselect
        aModel.deselectRegion(aRegion2);
        Assert.assertNull(aModel.getCurrentRegion());
        Assert.assertFalse(aRegion2.isSelected());

        // test delete region
        aModel.addRegion(aRegion5);
        Assert.assertTrue(aModel.containsRegion(aRegion5));
        aModel.deleteRegion(aRegion5);
        Assert.assertFalse(aModel.containsRegion(aRegion5));

//        aModel.addRegion(aRegion5);
//        aModel.selectRegion(aRegion5);
//        aModel.deleteRegion();
//        Assert.assertFalse(aModel.containsRegion(aRegion5));

        // reselect region 4 after all the stuff we did with the current region
        aModel.selectRegion(aRegion4);

        //Assert.assertEquals(aModel.getCurrentInstance(),aJenaModel.getResource(INST4));
        Assert.assertEquals(aModel.getCurrentRegion(),aRegion4);

        Model temp = ModelFactory.createDefaultModel().read(new java.io.File("tests\\markup_image_model_out.owl").toURI().toString());

        Assert.assertTrue(aModel.asModel().isIsomorphicWith(temp));

        aModel.clearDepictions();

        Assert.assertFalse(aModel.getDepictedInstances().hasNext());
        Assert.assertFalse(aModel.getDepictedInstances(aRegion1).hasNext());
        Assert.assertFalse(aModel.getDepictedInstances(aRegion2).hasNext());
        Assert.assertFalse(aModel.getDepictedInstances(aRegion3).hasNext());
        Assert.assertFalse(aModel.getDepictedInstances(aRegion4).hasNext());

        aModel.clearAll();

       Assert.assertTrue(aModel.asModel().isIsomorphicWith(aResultModel));

        aModel.addDepicts(aJenaModel.getResource(INST1));
        aModel.removeDepicts(aJenaModel.getResource(INST1));
        Assert.assertFalse(aModel.getDepictedInstances().hasNext());

        aModel.addRegionDepicts(aRegion2,aJenaModel.getResource(INST2));
        aModel.removeRegionDepicts(aRegion2,aJenaModel.getResource(INST2));
        Assert.assertFalse(aModel.getDepictedInstances(aRegion2).hasNext());
    }
}
