// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component.test;

import junit.framework.TestSuite;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class AllTests {
    public static final String BASE = "http://www.mindswap.org/PhotoStuff/test.owl#";

    /*
    TODO:
       how to test the mouse stuff, like the tools
       also how to test gui things
     */

    // TODO: Need tests for these classes:

    // org.mindswap.markup.SimpleReasoner -- done?
    // org.mindswap.markup.MarkupModel -- in progress
    // org.mindswap.markup.MarkupImageModel  -- done?
    // org.mindswap.markup.ChangeTracker
    // org.mindswap.markup.MarkupReasoner -- done?
    // org.mindswap.markup.MarkupOWLReasoner -- done?
    // org.mindswap.markup.ResourceMoverControl
    // org.mindswap.markup.PelletReasoner -- done?
    // org.mindswap.markup.utils.ResourceComparator - done?
    // org.mindswap.markup UI elements ??

    // org.mindswap.component.ImageComponent  ?
    // org.mindswap.component.MoverControl
    // org.mindswap.component.AddressBar
    // org.mindswap.component -- ontology view stuff ??

    // org.mindswap.markup.utils.ResourceComparator

    // org.mindswap.utils.JenaUtils -- this is sort of tested w/ the reasoner stuff, but would be good it if had its own tests
    // other utils stuff??

    // org.mindswap.form ???

    public static TestSuite suite()
    {
        TestSuite aSuite = new TestSuite("All PS Tests");

        aSuite.addTest(SelectionRegionTests.suite());
        aSuite.addTest(SortTests.suite());

        aSuite.addTest(MarkupModelTests.suite());
        aSuite.addTest(ImageModelTests.suite());
        aSuite.addTest(MarkupImageModelTests.suite());

        aSuite.addTest(SimpleReasonerTests.suite());
        aSuite.addTest(MarkupOWLReasonerTests.suite());
        aSuite.addTest(PelletReasonerTests.suite());

        return aSuite;
    }
}
