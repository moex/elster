// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component.test;

import junit.framework.Assert;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;

import java.net.URL;

import org.mindswap.utils.Bookmark;
import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.BasicUtils;

import fr.inrialpes.exmo.elster.picster.markup.*;
import org.mindswap.markup.MarkupModelListener;
import org.mindswap.markup.MarkupModelEvent;
import org.mindswap.markup.media.MarkupImageModel;

import org.mindswap.store.impl.SiteStore;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import com.hp.hpl.jena.vocabulary.RDF;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MarkupModelTests extends TestCase implements MarkupModelListener {
    private static final String CREW_ONTOLOGY_URL = "http://semspace.mindswap.org/2004/ontologies/Crew-ont.owl";
    private static final String SHUTTLE_ONTOLOGY_URL = "http://semspace.mindswap.org/2004/ontologies/ShuttleMission-ont.owl";

    private static final String[] IMAGE_URLS = {
        "http://grin.hq.nasa.gov/IMAGES/SMALL/GPN-2000-001038.jpg",
        "http://www.themoderatevoice.com/files/joe-pizza.gif"
    };

    private Model mCrewModel;
    private Model mShuttleModel;

    private static MarkupModel mMarkupModel;

    private MarkupModelEvent mLastEvent;

    public MarkupModelTests() {
    }

    public static TestSuite suite() {
        return new TestSuite(MarkupModelTests.class);
    }

    protected void setUp()
    {
        mCrewModel = MarkupModel.createOntModel();
        mShuttleModel = MarkupModel.createOntModel();

        mCrewModel.read(CREW_ONTOLOGY_URL);
        mShuttleModel.read(SHUTTLE_ONTOLOGY_URL);

        getPopulatedMarkupModel();
    }

    // TODO: submit test
    // TODO: image upload test
    // getInstance(String)
    // Prefs stuff?
    // TODO: test specific add site stuff like addNewSiteStore
    // TODO: check triple tracking fxns
    //       imageDepictionAdded
    //       imageDepictionRemove
    //       propertyChanged
    //       commitChanges
    //       commitChangesFor
    //       cancelChanges
    //       cancelChangesFor
    // TODO: test loadInstances
    // TODO: test listAllInstances
    // TODO: test getWorkingModel, getRDF
    // TODO: test addPendingDepiction, clearPendingDepiction - these are kind of tested in MarkupImageModelTests
    // TODO: test addressChanged
    // TODO: test clear, clearAll
    // TODO: test deleteInstance
    // TODO: test deleteProperty
    // TODO: test get functions

    public void testBookmarks() {
        Bookmark aBookmark1 = new Bookmark(Bookmark.BOOKMARK_ONTOLOGY,"label","http://www.mindswap.org/#bookmark1");
        Bookmark aBookmark2 = new Bookmark(Bookmark.BOOKMARK_ONTOLOGY,"label2","http://www.mindswap.org/#bookmark2");

        HashSet aSet = new HashSet();
        aSet.add(aBookmark1);
        aSet.add(aBookmark2);

        MarkupModel aModel = new MarkupModel();
        aModel.addBookmark(aBookmark1.getLabel(),aBookmark1.getURL(),aBookmark1.getType());
        aModel.addBookmark(aBookmark2.getLabel(),aBookmark2.getURL(),aBookmark2.getType());

        Assert.assertTrue(aModel.listBookmarks().contains(aBookmark1));
        Assert.assertTrue(aModel.listBookmarks().contains(aBookmark2));

        aModel.removeBookmark(aBookmark1.getURL());
        aModel.removeBookmark(aBookmark2.getURL());

        Assert.assertFalse(aModel.listBookmarks().contains(aBookmark1));
        Assert.assertFalse(aModel.listBookmarks().contains(aBookmark2));
    }

    public void testPrefFunctions()
    {
        MarkupModel aModel = new MarkupModel();

        aModel.setAutoLoadImageMetadata(true);
        Assert.assertTrue(aModel.getAutoLoadImageMetadata());

        aModel.setAutoLoadImageMetadata(false);
        Assert.assertFalse(aModel.getAutoLoadImageMetadata());

//        aModel.setAutoLoadSubmissions(true);
//        Assert.assertTrue(aModel.getAutoLoadSubmissions());
//
//        aModel.setAutoLoadSubmissions(false);
//        Assert.assertFalse(aModel.getAutoLoadSubmissions());

        aModel.setQNames(true);
        Assert.assertTrue(aModel.getQNames());

        aModel.setQNames(false);
        Assert.assertFalse(aModel.getQNames());
    }

    public void testSetFunctions()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.addMarkupModelListener(this);

        //aModel.setConnectedStore(aStore);

        Resource aRes = ModelFactory.createDefaultModel().getResource(AllTests.BASE+"A");
        aModel.setCurrentInstance(aRes);

        Assert.assertEquals(aModel.getCurrentInstance(),aRes);

        try {
            LinkedHashMap aList = new LinkedHashMap();

            // TODO: populate the markup image models some
            aList.put(new URL(AllTests.BASE+"Foo"), new MarkupImageModel(aModel));
            aList.put(new URL(AllTests.BASE+"Bar"), new MarkupImageModel(aModel));

            aModel.setMediaModels(aList);
            Assert.assertEquals(aList,aModel.getMediaModels());
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }

//        mLastEvent = null;
//        aModel.setImageServerURL("http://www.mindswap.org/dav/images/");
//        Assert.assertEquals(aModel.getImageServerURL(),"http://www.mindswap.org/dav/images/");
//        checkEvent(aModel, MarkupModelEvent.PREFS_CHANGED);

        try {
            mLastEvent = null;
            URL aURL = new URL(AllTests.BASE+"anImage");
            aModel.setMediaURL(aURL);
            Assert.assertEquals(aModel.getMediaURL(),aURL);
            //checkEvent(aModel, MarkupModelEvent.IMAGE_CHANGED);
            checkEvent(aModel, MarkupModelEvent.MEDIA_SELECTED);
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }

        HashMap aMap = getInstanceMap();

        aModel.setInstances(aMap);
        Assert.assertEquals(aModel.getInstances(),aMap);
    }

    public void testSaveInstance()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.addMarkupModelListener(this);

        Model aJenaModel = ModelFactory.createDefaultModel();

        Resource aRes = aJenaModel.getResource(AllTests.BASE+"A");
        Resource aRes2 = aJenaModel.getResource(AllTests.BASE+"B");
        //Resource aRes3 = aJenaModel.getResource(AllTests.BASE+"C");
        //aRes2.addProperty(aJenaModel.getProperty(AllTests.BASE+"aProp"),aRes3);

        aModel.setCurrentInstance(aRes);
        aModel.saveInstance(aRes,true);
        Assert.assertNull(aModel.getCurrentInstance());
        Assert.assertTrue(aModel.getInstances().containsValue(aRes));
        Assert.assertEquals(JenaUtils.getLabel(aRes),aRes.getLocalName());

        mLastEvent = null;
        aModel.saveInstance(aRes2,false);
        Assert.assertTrue(aModel.getInstances().containsValue(aRes2));
        //Assert.assertTrue(aModel.getInstances().containsValue(aRes3));
        Assert.assertEquals(JenaUtils.getLabel(aRes2),aRes2.getLocalName());
        //Assert.assertEquals(JenaUtils.getLabel(aRes3),aRes3.getLocalName());
        checkEvent(aModel,MarkupModelEvent.MODEL_CHANGED);
    }

    public void testAddRemoveStore()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.addMarkupModelListener(this);

        SiteStore aSiteStore = new SiteStore("a label","param3","param4");

        mLastEvent = null;
        aModel.addStore(aSiteStore);
        Assert.assertTrue(aModel.getStores().contains(aSiteStore));
        checkEvent(aModel,MarkupModelEvent.PREFS_CHANGED);

        mLastEvent = null;
        aModel.removeStore(aSiteStore);
        Assert.assertFalse(aModel.getStores().contains(aSiteStore));
        checkEvent(aModel,MarkupModelEvent.PREFS_CHANGED);

        // TODO: check that the store was added and removed from prefs correctly
    }

    public void testMisc()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.addMarkupModelListener(this);

        mLastEvent = null;
        aModel.regionSelected();
        checkEvent(aModel,MarkupModelEvent.REGION_SELECTED);

        mLastEvent = null;
        aModel.formSelected();
        checkEvent(aModel,MarkupModelEvent.FORM_SELECTED);

        mLastEvent = null;
        aModel.formChanged();
        checkEvent(aModel,MarkupModelEvent.FORM_CHANGED);

        mLastEvent = null;
        aModel.mediaSelected();
        checkEvent(aModel,MarkupModelEvent.MEDIA_SELECTED);

        mLastEvent = null;
        aModel.modelChanged();
        checkEvent(aModel,MarkupModelEvent.MODEL_CHANGED);

        mLastEvent = null;
        aModel.modelChanged(MarkupModelEvent.FORM_CHANGED);
        checkEvent(aModel,MarkupModelEvent.FORM_CHANGED);

        aModel.removeMarkupModelListener(this);

        mLastEvent = null;
        aModel.regionSelected();
        Assert.assertNull(mLastEvent);
    }

    private HashMap getInstanceMap()
    {
        // TODO: populate w/ more instance data?
        HashMap aMap = new HashMap();

        Model aModel = ModelFactory.createDefaultModel();
        for (int i = 0; i < 10; i++)
        {
            Resource aRes = aModel.getResource(AllTests.BASE+"A"+i);

            aMap.put(aRes.getURI(),aRes);
        }

        return aMap;
    }

    public void testListInstances()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.setInstances(getInstanceMap());

        Set aList = new HashSet(aModel.getInstances().values());
        Set aAnswer = BasicUtils.collectElements(getInstanceMap().values().iterator());

        Assert.assertTrue(aList.containsAll(aAnswer) && aList.size() == aAnswer.size());
    }

    private void checkEvent(MarkupModel theModel, int theId)
    {
        Assert.assertNotNull(mLastEvent);
        Assert.assertEquals(mLastEvent.getID(),theId);
        Assert.assertEquals(mLastEvent.getSource(),theModel);
    }

    public void handleMarkupModelEvent(MarkupModelEvent theEvent)
    {
        mLastEvent = theEvent;
    }

    public void testAddOntology()
    {
        MarkupModel aModel = new MarkupModel();
        aModel.addMarkupModelListener(this);

        try {
            URL aURL = new URL(CREW_ONTOLOGY_URL);

            mLastEvent = null;
            aModel.addOntology(aURL);

            Assert.assertTrue(aModel.getMasterOntologyModel().isIsomorphicWith(mCrewModel));
            Assert.assertTrue(aModel.getReasoner().getModel().isIsomorphicWith(mCrewModel));

            Assert.assertTrue(aModel.getOntologyModels().containsKey(aURL));
            Assert.assertTrue(((Model)aModel.getOntologyModels().get(aURL)).isIsomorphicWith(mCrewModel));

            checkEvent(aModel,MarkupModelEvent.ONTOLOGY_LOADED);

            aModel.addOntology(new URL(SHUTTLE_ONTOLOGY_URL));

            Model aResultModel = MarkupModel.createOntModel();
            aResultModel.add(mCrewModel);
            aResultModel.add(mShuttleModel);

            Assert.assertTrue(aModel.getMasterOntologyModel().isIsomorphicWith(aResultModel));
            Assert.assertTrue(aModel.getReasoner().getModel().isIsomorphicWith(aResultModel));
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public void testCreateInstance()
    {
        MarkupModel aModel = new MarkupModel();

        String aTypeURI = CREW_ONTOLOGY_URL+"#MissionSpecialist";

        Resource aInst = aModel.createInstance(null,aTypeURI);

        Assert.assertTrue(aInst.isAnon());
        Assert.assertTrue(aInst.hasProperty(RDF.type));
        Assert.assertTrue(aInst.getProperty(RDF.type).getResource().equals(aInst.getModel().getResource(aTypeURI)));

        aInst = aModel.createInstance("http://www.example.org/#Foo",aTypeURI);

        Assert.assertTrue(!aInst.isAnon());
        Assert.assertTrue(aInst.hasProperty(RDF.type));
        Assert.assertTrue(aInst.getProperty(RDF.type).getResource().equals(aInst.getModel().getResource(aTypeURI)));
    }

    public void testFileToRemoteURL()
    {
        MarkupModel aModel = new MarkupModel();

        String aFileURL = "file:///home/mhgrove/images/birthday.jpg";
        String aBaseURL = "http://www.mindswap.org/dav/images/";

        try {
            String aRemoteURL = aModel.adjustURL(new URL(aFileURL), new URL(aBaseURL)).toString();
            Assert.assertEquals(aRemoteURL,aBaseURL+"birthday.jpg");
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public void testGetMasterOntologyModel()
    {
        MarkupModel aModel = getPopulatedMarkupModel();

        Model aResultModel = MarkupModel.createOntModel();
        aResultModel.add(mCrewModel);
        aResultModel.add(mShuttleModel);

        Assert.assertTrue(aResultModel.isIsomorphicWith(aModel.getMasterOntologyModel()));
    }

    public void testGetOntologyModels()
    {
        MarkupModel aModel = getPopulatedMarkupModel();

        try {
            Model crew = (Model)aModel.getOntologyModels().get(new URL(CREW_ONTOLOGY_URL));
            Model shuttle = (Model)aModel.getOntologyModels().get(new URL(SHUTTLE_ONTOLOGY_URL));

            Assert.assertTrue(crew.isIsomorphicWith(mCrewModel));
            Assert.assertTrue(shuttle.isIsomorphicWith(mShuttleModel));
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public void testGetImageModel()
    {
        MarkupModel aModel = getPopulatedMarkupModel();

        try {
            for (int i = 0; i < IMAGE_URLS.length; i++) {
                aModel.setMediaURL(new URL(IMAGE_URLS[i]));
                MediaModel aImgModel = aModel.getMediaModel(aModel.getMediaURL());
                Assert.assertEquals(aImgModel.getMediaURL(),new URL(IMAGE_URLS[i]));
            }
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    public void testGetImageModels()
    {
        MarkupModel aModel = getPopulatedMarkupModel();

        try {
            Assert.assertEquals(IMAGE_URLS.length,aModel.getMediaModels().size());

            LinkedHashMap aMap = aModel.getMediaModels();
            Iterator kIter = aMap.keySet().iterator();
            int i = 0;
            while (kIter.hasNext())
            {
                Object aKey = kIter.next();
                MediaModel aMediaModel = (MediaModel)aMap.get(aKey);
                Assert.assertEquals(aKey,new URL(IMAGE_URLS[i]));
                Assert.assertEquals(aMediaModel.getMediaURL(), new URL(IMAGE_URLS[i]));
                i++;
            }
        }
        catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    private MarkupModel getPopulatedMarkupModel()
    {
        if (mMarkupModel == null)
        {
            mMarkupModel = new MarkupModel();
            mMarkupModel.addMarkupModelListener(this);

            try {
                mMarkupModel.addOntology(new URL(CREW_ONTOLOGY_URL));
                mMarkupModel.addOntology(new URL(SHUTTLE_ONTOLOGY_URL));

                for (int i = 0; i < IMAGE_URLS.length; i++)
                {
                    mMarkupModel.setMediaURL(new URL(IMAGE_URLS[i]));
                    MediaModel aModel = mMarkupModel.getMediaModel(mMarkupModel.getMediaURL());
                    Assert.assertEquals(aModel.getMediaURL(),new URL(IMAGE_URLS[i]));
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return mMarkupModel;
    }
}
