// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component.test;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Assert;

import org.mindswap.markup.SimpleReasoner;
import fr.inrialpes.exmo.elster.picster.markup.*;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import com.hp.hpl.jena.vocabulary.RDFS;

import java.io.File;

import java.util.Set;
import java.util.HashSet;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class SimpleReasonerTests extends TestCase {

    public static TestSuite suite() {
        return new TestSuite(SimpleReasonerTests.class);
    }

    private SimpleReasoner mReasoner;

    protected void setUp()
    {
        mReasoner = new SimpleReasoner(new MarkupModel());

        mReasoner.addModel(ModelFactory.createDefaultModel().read(new File("tests\\reasoner_test.owl").toURI().toString()));
    }

    public void testListClasses()
    {
        Set aClasses = mReasoner.listClasses();

        Model aTempModel = mReasoner.getModel();
        Set aClassAnswer = new HashSet();
        // populate with the known answer
        aClassAnswer.add(aTempModel.getResource(AllTests.BASE+"A"));
        aClassAnswer.add(aTempModel.getResource(AllTests.BASE+"B"));
        aClassAnswer.add(aTempModel.getResource(AllTests.BASE+"C"));
        aClassAnswer.add(aTempModel.getResource(AllTests.BASE+"D"));
        aClassAnswer.add(aTempModel.getResource(AllTests.BASE+"E"));

        Assert.assertTrue(aClasses.containsAll(aClassAnswer) && aClassAnswer.size() == aClasses.size());
    }

    public void testDeclaredProperties()
    {
        Model aTempModel = mReasoner.getModel();
        Set aDeclaredAnswer = new HashSet();
        // populate with the known answer
        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"anObjectProp2"));
        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"aDatatypeProp1"));

        Set aDeclared = mReasoner.getDeclaredProperties(aTempModel.getResource(AllTests.BASE+"A"));

        Assert.assertTrue(aDeclared.containsAll(aDeclaredAnswer) && aDeclaredAnswer.size() == aDeclared.size());

        aDeclared = mReasoner.getDeclaredProperties(aTempModel.getResource(AllTests.BASE+"B"));
        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"aDatatypeProp2"));

        Assert.assertTrue(aDeclared.containsAll(aDeclaredAnswer) && aDeclaredAnswer.size() == aDeclared.size());

        aDeclared = mReasoner.getDeclaredProperties(aTempModel.getResource(AllTests.BASE+"E"));
        aDeclaredAnswer.clear();
        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"anObjectProp1"));

        Assert.assertTrue(aDeclared.containsAll(aDeclaredAnswer) && aDeclaredAnswer.size() == aDeclared.size());
    }

    public void testDeclaredRanges()
    {
        Model aTempModel = mReasoner.getModel();
        Set aDeclaredAnswer = new HashSet();
        // populate with the known answer

        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"D"));

        Set aDeclared = mReasoner.getDeclaredRanges(aTempModel.getProperty(AllTests.BASE+"anObjectProp2"));

        Assert.assertTrue(aDeclared.containsAll(aDeclaredAnswer) && aDeclaredAnswer.size() == aDeclared.size());

        aDeclaredAnswer.add(aTempModel.getProperty(AllTests.BASE+"C"));

        aDeclared = mReasoner.getRanges(aTempModel.getProperty(AllTests.BASE+"anObjectProp2"),aTempModel.getResource(AllTests.BASE+"A"));

        Assert.assertTrue(aDeclared.containsAll(aDeclaredAnswer) && aDeclaredAnswer.size() == aDeclared.size());
    }

    public void testPropertyFunctions()
    {
        Assert.assertTrue(mReasoner.isDatatypeProperty(mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp1")));
        Assert.assertFalse(mReasoner.isDatatypeProperty(mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp1")));

        Assert.assertTrue(mReasoner.isObjectProperty(mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp1")));
        Assert.assertFalse(mReasoner.isObjectProperty(mReasoner.getModel().getProperty(AllTests.BASE+"aDatatypeProp1")));

        Set aRangesAnswer = new HashSet();
        aRangesAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"C"));
        aRangesAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"D"));

        Set aRanges = mReasoner.getRanges(mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp2"),mReasoner.getModel().getProperty(AllTests.BASE+"A"));

        Assert.assertTrue(aRanges.containsAll(aRangesAnswer) && aRangesAnswer.size() == aRanges.size());
    }

    public void testClassFunctions()
    {
        Assert.assertTrue(mReasoner.isClass(mReasoner.getModel().getResource(AllTests.BASE+"E")));
        Assert.assertFalse(mReasoner.isClass(mReasoner.getModel().getProperty(AllTests.BASE+"anObjectProp1")));

        Assert.assertEquals(mReasoner.getType(mReasoner.getModel().getResource(AllTests.BASE+"Foo")),mReasoner.getModel().getResource(AllTests.BASE+"E"));
        Assert.assertEquals(mReasoner.getType(mReasoner.getModel().getResource(AllTests.BASE+"Bar")),mReasoner.getModel().getResource(AllTests.BASE+"D"));

        Set aTypes = mReasoner.getTypes(mReasoner.getModel().getResource(AllTests.BASE+"Baz"));
        Set aTypesAnswer = new HashSet();
        aTypesAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"D"));
        aTypesAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"E"));

        Assert.assertTrue(aTypes.containsAll(aTypesAnswer) && aTypesAnswer.size() == aTypes.size());

        Assert.assertTrue(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"B"),mReasoner.getModel().getResource(AllTests.BASE+"A")));
        Assert.assertFalse(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"A"),mReasoner.getModel().getResource(AllTests.BASE+"B")));

        Assert.assertTrue(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"C"),mReasoner.getModel().getResource(AllTests.BASE+"B")));
        Assert.assertTrue(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"C"),mReasoner.getModel().getResource(AllTests.BASE+"A")));
        Assert.assertFalse(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"A"),mReasoner.getModel().getResource(AllTests.BASE+"C")));
        Assert.assertFalse(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"B"),mReasoner.getModel().getResource(AllTests.BASE+"C")));

        Assert.assertFalse(mReasoner.isSubClassOf(mReasoner.getModel().getResource(AllTests.BASE+"D"),mReasoner.getModel().getResource(AllTests.BASE+"C")));

        Set aList = mReasoner.listIndividuals();
        Set aListAnswer = new HashSet();
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Foo"));
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Bar"));
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Baz"));
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp"));
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp-A"));
        aListAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp-B"));

        Assert.assertTrue(aList.containsAll(aListAnswer) && aListAnswer.size() == aList.size());
    }

    public void testInstancesWithType()
    {
        Set aAnswer = new HashSet();
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Foo"));
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Baz"));

        Set aResult = mReasoner.getInstancesWithType(mReasoner.getModel().getResource(AllTests.BASE+"E"));
System.err.println("results: ");
java.util.Iterator aIter = aResult.iterator();
        while (aIter.hasNext())
System.err.println(aIter.next());
        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());

        aAnswer.clear();
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp-A"));
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp-B"));
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp"));

        aResult = mReasoner.getInstancesWithType(mReasoner.getModel().getResource(AllTests.BASE+"A"));

        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());

        aAnswer.clear();
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp"));
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp-B"));

        aResult = mReasoner.getInstancesWithType(mReasoner.getModel().getResource(AllTests.BASE+"B"));

        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());

        aAnswer.clear();
        aAnswer.add(mReasoner.getModel().getResource(AllTests.BASE+"Temp"));

        aResult = mReasoner.getInstancesWithType(mReasoner.getModel().getResource(AllTests.BASE+"C"));

        Assert.assertTrue(aResult.containsAll(aAnswer) && aAnswer.size() == aResult.size());
    }
}
