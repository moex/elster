package org.mindswap.component.test;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import junit.framework.Assert;

import org.mindswap.markup.media.SelectionRegion;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Point;

import java.util.Vector;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */
public class SelectionRegionTests extends TestCase {

    /*
    TODO:
        how to check draw methods?
        need to have poly svg test
     */

    public void testDefaults() {
        Rectangle aRect = new Rectangle(0,0,100,100);
        SelectionRegion aRegion = SelectionRegion.createOvalRegion(aRect);

        // these should start false
        Assert.assertEquals(aRegion.isHover(),false);
        Assert.assertEquals(aRegion.isSelected(),false);

        // check setting the nam
        aRegion.setName("foo");
        Assert.assertEquals(aRegion.getName(),"foo");

        // check setting hover
        aRegion.setHover(true);
        Assert.assertEquals(aRegion.isHover(),true);

        // check setting selected
        aRegion.setSelected(true);
        Assert.assertEquals(aRegion.isSelected(),true);
    }

    public void testOvalRegion() {
        Rectangle aRect = new Rectangle(0,0,100,100);
        SelectionRegion aRegion = SelectionRegion.createOvalRegion(aRect);

        // make sure the shape and type is correct
        Assert.assertEquals(aRegion.getRect(),aRect);
        Assert.assertEquals(aRegion.getRegionShape(),aRect);
        Assert.assertEquals(aRegion.getType(),SelectionRegion.SHAPE_OVAL);
    }

    public void testFromSVG() {
        String aVal1 = "<svg xml:space=\"preserve\" width=\"202\" heigth=\"151\" viewBox=\"0 0 202 151\">  <image xlink:href=\"http://www-pao.ksc.nasa.gov/kscpao/chron/images/sts-27crew.jpg\" x=\"0\" y=\"0\"  width=\"202\" height=\"151\" /> <rect x=\"78.0\" y=\"85.0\" width=\"41.0\" height=\"52.0\" style=\"fill:none; stroke:yellow; stroke-width:1pt;\"/> </svg>";
        String aVal2 = "<svg xml:space=\"preserve\" width=\"1600\" heigth=\"1200\" viewBox=\"0 0 1600 1200\">  <image xlink:href=\"http://www.mindswap.org/2004/images/2005-04/BlewettMoonrise.jpg\" x=\"0\" y=\"0\"  width=\"1600\" height=\"1200\" /> <ellipse cx=\"376.5\" cy=\"450.0\" rx=\"46.5\" ry=\"26.0\" style=\"fill:none; stroke:yellow; stroke-width:1pt;\"/> </svg>";

        Rectangle aRect1 = new Rectangle(78,85,41,52);
        SelectionRegion aRegion1 = SelectionRegion.fromSVG(aVal1);

        Assert.assertEquals(aRect1, aRegion1.getRect());
        Assert.assertEquals(aRegion1.getRegionShape(),aRect1);
        Assert.assertEquals(aRegion1.getType(),SelectionRegion.SHAPE_RECT);

        Rectangle aRect2 = new Rectangle(376,450,46,26);
        SelectionRegion aRegion2 = SelectionRegion.fromSVG(aVal2);
        Assert.assertEquals(aRegion2.getRect(),aRect2);
        Assert.assertEquals(aRegion2.getRegionShape(),aRect2);
        Assert.assertEquals(aRegion2.getType(),SelectionRegion.SHAPE_OVAL);
    }

    public void testRectRegion() {
        Rectangle aRect = new Rectangle(0,0,150,200);
        SelectionRegion aRegion = SelectionRegion.createRectangleRegion(aRect);

        // make sure the shape and type is correct
        Assert.assertEquals(aRegion.getRect(),aRect);
        Assert.assertEquals(aRegion.getRegionShape(),aRect);
        Assert.assertEquals(aRegion.getType(),SelectionRegion.SHAPE_RECT);
    }

    public void testPolyRegion() {
        Vector aPoints = new Vector();
        aPoints.add(new Point(100,100));
        aPoints.add(new Point(150,100));
        aPoints.add(new Point(150,150));
        aPoints.add(new Point(100,150));

        SelectionRegion aRegion = SelectionRegion.createPolygonRegion(aPoints);

        int[] xpoints = new int[aPoints.size()];
        int[] ypoints = new int[aPoints.size()];
        for (int i = 0; i < aPoints.size(); i++) {
            Point p = (Point)aPoints.elementAt(i);
            xpoints[i] = p.x;
            ypoints[i] = p.y;
        }
        Polygon aPoly = new Polygon(xpoints,ypoints,aPoints.size());

        // make sure the shape and type is correct
        Assert.assertEquals(aPoly.npoints,((Polygon)aRegion.getRegionShape()).npoints);
        for (int i = 0; i < aPoly.npoints; i++)
        {
            Assert.assertEquals(aPoly.xpoints[i],((Polygon)aRegion.getRegionShape()).xpoints[i]);
            Assert.assertEquals(aPoly.ypoints[i],((Polygon)aRegion.getRegionShape()).ypoints[i]);
        }

        Assert.assertEquals(aRegion.getRect(),aPoly.getBounds());
        Assert.assertEquals(aRegion.getType(),SelectionRegion.SHAPE_POLY);
    }

    public static TestSuite suite() {
        return new TestSuite(SelectionRegionTests.class);
    }

}
