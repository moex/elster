// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;

import java.awt.Component;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;

import org.mindswap.utils.ui.TypeAheadList;

import org.mindswap.utils.JenaUtils;

import org.mindswap.markup.utils.ResourceComparator;
import java.util.Comparator;
import java.util.Collections;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.Resource;

import java.util.Vector;
import java.util.Iterator;

import org.mindswap.component.MultiLineToolTip;
import org.mindswap.markup.media.*;

public class OntologyListView extends JPanel implements OntologyView, ListSelectionListener
{
    private TypeAheadList mList;
    private ListModel mListModel;
    private OntModel model;
    private OntologyManagerComponent mManager;

    public OntologyListView() {
        initGUI();
    } // cons

    /**
     * Sets the parent ontology manager
     * @param theManager OntologyManagerComponent
     */
    public void setManager(OntologyManagerComponent theManager)
    {
        mManager = theManager;
    }

    /**
     * Sets the current resource to selected in the current view
     * @param theResource Resource resource to select
     */
    public void select(Resource theResource)
    {
        mList.setSelectedValue(theResource,true);
    }

    /**
     * Returns the name of this component for use in GUI display
     * @return String
     */
    public String getName() { return "Class List"; }

    /**
     * Returns the UI element that represents this object
     * @return Component
     */
    public Component getComponent() {
        return this;
    }

    /**
     * Returns the JList component this control wraps
     * @return Component the JList component
     */
    public Component getList(){
        return mList;
    }

    /**
     * Sets the model used to build this view, also updates the UI
     *
     * @param model LinkedHashMap - the new model
     */
    public void setModel(OntModel model) {
        this.model = model;
        updateView();
    }

    private void initGUI()
    {
        mList = new TypeAheadList() {
            public JToolTip createToolTip()
            {
                MultiLineToolTip tt = new MultiLineToolTip();
                tt.setComponent(this);
                return tt;
            }

            public String getToolTipText(java.awt.event.MouseEvent e)
            {
                Object node = mListModel.getElementAt(mList.locationToIndex(e.getPoint()));
                if (node instanceof Resource)
                {
                    Resource res = (Resource)node;

                    String text = "Properties:\n\n";

                    boolean empty = true;
                    Iterator iter = JenaUtils.getDeclaredProps(res).iterator();
                    while (iter.hasNext())
                    {
                        Resource r = (Resource)iter.next();
                        text += JenaUtils.getQName(r)+"\n";
                        empty = false;
                    }

                    if (empty)
                        return "Properties:\n\nNONE";
                    else return text;
                }
                else return "";
            }
        };

        mListModel = new DefaultListModel();
        mList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        mList.setModel(mListModel);
        mList.addListSelectionListener(this);
        mList.setDragEnabled(true);
        mList.setCellRenderer(new OntologyListCellRenderer());

        JPanel listPane = new JPanel();
        listPane.setLayout(new BorderLayout());

        listPane.add(new JScrollPane(mList),BorderLayout.CENTER);

        setLayout(new BorderLayout());
        add(listPane,BorderLayout.CENTER);
    } // initGUI

    public void valueChanged(ListSelectionEvent e)
    {
        if (mList.getSelectedIndex() != -1)
        {
            Resource selected = (Resource)mList.getSelectedValue();
            mManager.setSelectedResource(selected);
        }
    }

    private void searchInList(String searchTerm) { searchInList(searchTerm,false); }
    private void searchInList(String searchTerm, boolean strict)
    {
        for (int i = 0; i < mListModel.getSize(); i++)
        {
            Resource res = (Resource)mListModel.getElementAt(i);

            boolean match = false;

            if (strict)
                match = res.toString().equals(searchTerm);
            else match = res.getLocalName().toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;

            if (!res.isAnon() && match)
            {
                mList.setSelectedIndex(i);
                mList.ensureIndexIsVisible(i);
                break;
            }
        }
    }

    /**
     * Updates the UI for this component
     */
    public void updateView()
    {
        Vector v = new Vector();

        OntModel m = model;
        com.hp.hpl.jena.util.iterator.ExtendedIterator ei = m.listClasses();
        while (ei.hasNext()) {
            OntClass oc = (OntClass)ei.next();

            if (!JenaUtils.isClass(oc) || oc.isAnon() || JenaUtils.isLanguageTerm(oc))
                continue;

            v.addElement(oc);
        }
        ei.close();
        Comparator c = new ResourceComparator();
        Collections.sort(v, c);
        mList.setListData(v);
        mListModel = mList.getModel();
        mList.repaint();
    }

    public void lookup(String searchTerm)
    {
        searchInList(searchTerm);
    } // lookup

//    public Vector getAllResources() {
//        Vector v = new Vector();
//        com.hp.hpl.jena.util.iterator.ExtendedIterator ex = model.listClasses();
//        while (ex.hasNext()) {
//            OntClass oc = (OntClass)ex.next();
//            v.add(oc);
//        }
//        ex.close();
//        java.util.Collections.sort(v, new org.mindswap.utils.ResourceComparator());
//        return v;
//    }
}
