// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTabbedPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.BorderLayout;

import java.util.Vector;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 */
public class OntologyManagerComponent extends JPanel implements ActionListener {
    private static final String LOOKUP = "LOOKUP";
    private JTabbedPane mTabPane;
    private JTextField mLookupText;

    private Vector mViews;

    private Resource mSelectedResource;

    public OntologyManagerComponent() {
        initGUI();
        mViews = new Vector();
    }

    /**
     * Adds a new View to the component.  Each view shows up in its own tab
     * @param view OntologyView
     */
    public void addView(OntologyView view) {
        try {
            mTabPane.addTab(view.getName(),view.getComponent());
            mViews.addElement(view);
            view.setManager(this);
        } catch (Exception e) {
            System.err.println("invalid ontology view specified");
        }
    }

    public void setSelectedResource(Resource theResource)
    {
        mSelectedResource = theResource;

        for (int i = 0; i < mViews.size(); i++)
        {
            OntologyView aView = (OntologyView)mViews.elementAt(i);
            aView.select(theResource);
        }
    }

    private void initGUI() {
        setLayout(new GridBagLayout());

        mLookupText = new JTextField();
        mLookupText.addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        doLookup();
                    }
                }
            });
        JButton lookupButton = new JButton("Lookup");
        lookupButton.setActionCommand(LOOKUP);
        lookupButton.addActionListener(this);

        mTabPane = new JTabbedPane();

        add(mTabPane,new GridBagConstraints(0,0,1,100,1,100,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));

        JPanel lookupPanel = new JPanel();
        lookupPanel.setLayout(new BorderLayout());
        lookupPanel.add(lookupButton,BorderLayout.WEST);
        lookupPanel.add(mLookupText,BorderLayout.CENTER);
        add(lookupPanel,new GridBagConstraints(0,100,1,1,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
    }

    private void doLookup() {
        String searchTerm = mLookupText.getText();
        mLookupText.setText("");

        if (searchTerm == null || searchTerm.length() == 0) {
            return;
        }

        OntologyView view = (OntologyView)mTabPane.getSelectedComponent();
        view.lookup(searchTerm);
    }

    public void actionPerformed(ActionEvent ev) {
        String command = ev.getActionCommand();
        if (command.equals(LOOKUP)) {
            doLookup();
        }
    }

    /**
     * Updates the UI for this component
     */
    public void updateComponent() {
        for (int i = 0; i < mViews.size(); i++) {
            OntologyView view = (OntologyView)mViews.elementAt(i);
            view.updateView();
        }
    }
}
