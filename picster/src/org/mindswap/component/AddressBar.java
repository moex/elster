// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridBagLayout;
import java.awt.AWTEventMulticaster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Iterator;

import java.net.URL;
import java.net.MalformedURLException;
import org.mindswap.markup.media.*;

public class AddressBar extends JPanel implements ActionListener {
    private JComboBox comboBox;
    private JFileChooser fileChooser;
    private URL address;
    private Set addresses;
    transient ActionListener actionListener;
    private String actionCommand;

    public AddressBar() {
        initGUI();
    }

    /**
     * Sets the command name for the action event fired by this address bar.
     *
     * @param command String
     */
    public void setActionCommand(String command) {
        this.actionCommand = command;
    }

    /**
     * Returns the command name of the action event fired by this button.
     *
     * @return String
     */
    public String getActionCommand() {
        if (actionCommand==null) {
            actionCommand = "addressChanged";
        }
        return actionCommand;
    }

    /**
     * Returns the current address of the address bar
     * @return URL the current address
     */
    public URL getAddress() {
        return address;
    }

    /**
     * Updates the address bar with the new URL
     * @param url URL new address url
     */
    public void setAddress(URL url) {
        if (this.address!=url) { // only update if setting to different url
            this.address = url;
            String s = url.toString();
            addAddress(s);
            updateAddressBar(s);
        }
    }

    public synchronized void addActionListener(ActionListener l) {
        if (l == null) {
            return;
        }
        actionListener = AWTEventMulticaster.add(actionListener, l);
        // TODO: newEventsOnly = true;
    }

    public synchronized void removeActionListener(ActionListener l) {
        if (l == null) {
            return;
        }
        actionListener = AWTEventMulticaster.remove(actionListener, l);
    }


    private void initGUI() {
        JLabel addrLbl = new JLabel("Address: ");
        comboBox = new JComboBox();
        comboBox.addActionListener(this);
        comboBox.setEditable(true);
        comboBox.setToolTipText("Enter URI of resource to load");

        JButton addrBtn = new JButton("Browse Local");
        addrBtn.addActionListener(this);
        addrBtn.setActionCommand("BROWSE");

        setLayout(new GridBagLayout());
        add(addrLbl,
            new GridBagConstraints(0, 0, 1, 1, 0, 1,
                                   GridBagConstraints.WEST,
                                   GridBagConstraints.NONE,
                                   new Insets(0, 0, 0, 0), 0, 0));

        add(comboBox,
            new GridBagConstraints(1, 0, 1, 1, 1, 1,
                                   GridBagConstraints.CENTER,
                                   GridBagConstraints.HORIZONTAL,
                                   new Insets(0, 0, 0, 0), 0, 0));

        add(addrBtn,
            new GridBagConstraints(2, 0, 1, 1, 0, 1,
                                   GridBagConstraints.EAST,
                                   GridBagConstraints.NONE,
                                   new Insets(0, 0, 0, 0), 0, 0));

    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();

        if (e.getSource() == comboBox
            && e.getActionCommand().equals("comboBoxChanged")) {
            addressChanged();
        } else if (command.equals("BROWSE")) {
            browseLocal();
        }
    }

    /**
     * Adds a url to the list of addresses in the address bar
     * @param url String the address url to add
     */
    public void addAddress(String url) {
        Set addresses = getAddresses();
        if (addresses.contains(url)==false) {
            addresses.add(url);
            updateAddressBar(url);
        }
    }

    /**
     * Clears the address bar
     */
    public void clear() {
        addAddress("");
        updateAddressBar("");
    }

    /**
     * Returns whether or not the address bar contains the given address
     * @param url String the address url to search for
     * @return boolean true if the address is found false otherwise
     */
    public boolean containsAddress(String url) {
        return getAddresses().contains(url);
    }

    private void addressChanged() {
        String s = (String) comboBox.getSelectedItem();
        s = s.trim();
        if (s.equals("")==false) {
            try {
                URL url = new URL(s);
                setAddress(url);
                ActionEvent e = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, getActionCommand());
                actionListener.actionPerformed(e);
            } catch (MalformedURLException e) {
                System.err.println("'"+s+"'");
                JOptionPane.showMessageDialog(
                                              this,
                                              "Invalid URL entered",
                                              "Invalid URL Error",
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void updateAddressBar(String url) {
        Set addresses = getAddresses();
        DefaultComboBoxModel m = new DefaultComboBoxModel();

        Iterator iter = addresses.iterator();
        while (iter.hasNext()) {
            String s = iter.next().toString();
            m.addElement(s);
        }
        m.setSelectedItem(url);
        comboBox.setModel(m);
    }

    private void browseLocal() {
        JFileChooser fc = getFileChooser();
        fc.rescanCurrentDirectory();
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            String filePath = file.toURI().toString();
            try {
                URL aURL = new URL(filePath);
                addAddress(aURL.toExternalForm());
                addressChanged();
            }
            catch (Exception ex) { ex.printStackTrace(); }
        }
    }

    /**
     * Returns the list of addresses in the address bar
     * @return Set the list of addresses
     */
    public Set getAddresses() {
        if (addresses==null) {
            // NOTE: LinkedHashSet defines iteration ordering as the
            // order elements were instered initially into the set
            // (reinserts do not change the order).
            addresses = new LinkedHashSet();
        }
        return addresses;
    }

    private JFileChooser getFileChooser() {
        if (fileChooser==null) {
            fileChooser = new JFileChooser();
            File f = new java.io.File(System.getProperty("user.dir"));
            fileChooser.setCurrentDirectory(f);
        }
        return fileChooser;
    }
}
