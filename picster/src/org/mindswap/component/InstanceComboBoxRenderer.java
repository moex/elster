// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.ListCellRenderer;

import java.awt.Component;

import fr.inrialpes.exmo.elster.picster.markup.*;

import com.hp.hpl.jena.rdf.model.Resource;

import com.hp.hpl.jena.vocabulary.RDFS;

public class InstanceComboBoxRenderer extends JLabel implements ListCellRenderer {

    public InstanceComboBoxRenderer() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        if (value instanceof Resource) {
            Resource theInst = (Resource)value;
            if (MarkupModel.USE_LABELS || (theInst.isAnon() && theInst.hasProperty(RDFS.label) && !theInst.getProperty(RDFS.label).getObject().toString().equals("")))
                setText(theInst.getProperty(RDFS.label).getObject().toString());
            else {
                if (theInst.getLocalName() == null || theInst.getLocalName().equals(""))
                {
                    if (theInst.isAnon())
                        setText(value.toString());
                    else setText(theInst.getURI().substring(theInst.getURI().indexOf("#")+1));
                }
                else setText(theInst.getLocalName());
            }
        } else setText(value.toString());

        return this;
    }
}

