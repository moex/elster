// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import java.util.Vector;
import java.util.Enumeration;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;

import javax.swing.JToolTip;
import javax.swing.JComponent;

import javax.swing.plaf.metal.MetalToolTipUI;
import org.mindswap.markup.media.*;

public class MultiLineToolTipUI extends MetalToolTipUI {
    private String[] mLines;
    private int mMaxWidth = 0;

    public void paint(Graphics g, JComponent c) {
        Font f = g.getFont();
        FontMetrics fontMetrics = c.getFontMetrics(f);
        Dimension size = c.getSize();

        g.setColor(c.getBackground());
        g.fillRect(0, 0, size.width, size.height);
        g.setColor(c.getForeground());
        if (mLines != null) {
            for (int i = 0; i < mLines.length; i++) {
                String s = mLines[i];
                g.drawString(s, 3, (fontMetrics.getHeight()) * (i + 1));
            }
        }
    }

    public Dimension getPreferredSize(JComponent c) {
        Font f = c.getFont();
        FontMetrics fontMetrics = c.getFontMetrics(f);

        String tipText = ((JToolTip) c).getTipText();
        if (tipText == null) {
            tipText = "";
        }

        BufferedReader br = new BufferedReader(new StringReader(tipText));
        String line;
        int maxWidth = 0;
        Vector v = new Vector();

        try {
            while ((line = br.readLine()) != null) {
                int width = fontMetrics.stringWidth(line);
                maxWidth = (maxWidth < width) ? width : maxWidth;
                v.addElement(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        int lines = v.size();
        if (lines < 1) {
            mLines = null;
            lines = 1;
        } else {
            mLines = new String[lines];
            int i = 0;

            for (Enumeration e = v.elements(); e.hasMoreElements(); i++)
                mLines[i] = (String) e.nextElement();
        }

        int height = fontMetrics.getHeight() * lines;
        mMaxWidth = maxWidth;

        return new Dimension(maxWidth + 6, height + 4);
    }
}
