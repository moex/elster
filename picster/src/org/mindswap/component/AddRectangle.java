// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import java.awt.Point;

import java.awt.Rectangle;
import java.awt.Dimension;

import java.awt.event.MouseEvent;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import javax.swing.SwingUtilities;

import org.mindswap.markup.media.MarkupImageModel;
import org.mindswap.markup.media.ImageComponent;
import org.mindswap.markup.media.ImageModel;
import org.mindswap.markup.media.SelectionRegion;

// implementation of the rectangle region selection tool
public class AddRectangle implements Tool {

    private ImageComponent imageComponent;

    public AddRectangle(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
    }

    /**
     * Invoked when the mouse has been clicked on a component.
     *
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    private Point mouseDown, mouseUp;
    private SelectionRegion currentRegion;
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e))
            return;

        ImageModel imageModel = imageComponent.getImageModel();
        if (imageModel != null) {
            mouseDown = getPoint(e);
            Rectangle r = new Rectangle(mouseDown, new Dimension(0, 0));
            currentRegion = SelectionRegion.createRectangleRegion(r);
            imageModel.addRegion(currentRegion);
            ((MarkupImageModel)imageModel).getMarkupModel().modelChanged();
            imageComponent.repaint();
        }
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @param e MouseEvent
     */
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e))
            return;

        MarkupImageModel imageModel = (MarkupImageModel)imageComponent.getImageModel();
        mouseUp = getPoint(e);
        if (imageModel != null && currentRegion != null) {
            Rectangle r = (Rectangle) (currentRegion.getRegionShape());
            r.setLocation(new Point(Math.min(mouseDown.x, mouseUp.x),
                                    Math.min(mouseDown.y, mouseUp.y)));
            Dimension size = new Dimension(Math.abs((int) (mouseDown.x - mouseUp.x)),
                                           Math.abs((int) (mouseDown.y - mouseUp.y)));
            r.setSize(size);
            // delete degenerate regions
            if (size.getHeight()==0 || size.getWidth()==0) {
                imageModel.deleteRegion(currentRegion);
            } else {
                imageModel.selectRegion(currentRegion);
                imageModel.getMarkupModel().regionSelected();
            }
            imageComponent.repaint();
        }
    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e MouseEvent
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e MouseEvent
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button is pressed on a component and then dragged.
     * Mouse drag events will continue to be delivered to the component where
     * the first originated until the mouse button is released (regardless of
     * whether the mouse position is within the bounds of the component).
     *
     * @param e MouseEvent
     */
    public void mouseDragged(MouseEvent e) {
        Point mouse = getPoint(e);
        if (currentRegion != null) {
            Rectangle r = (Rectangle) (currentRegion.getRegionShape());
            r.setLocation(new Point(Math.min(mouseDown.x, mouse.x),
                                    Math.min(mouseDown.y, mouse.y)));
            r.setSize(new Dimension(Math.abs((int) (mouseDown.x - mouse.x)),
                                    Math.abs((int) (mouseDown.y - mouse.y))));
            this.imageComponent.repaint();
        }
    }

    /**
     * Invoked when the mouse button has been moved on a component (with no
     * buttons no down).
     *
     * @param e MouseEvent
     */
    public void mouseMoved(MouseEvent e) {
    }


    private Point getPoint(MouseEvent e) {
        AffineTransform c2i = this.imageComponent.getComponentToImage();
        Point p = new Point(e.getX(), e.getY());
        Point2D r = c2i.transform(p, null);
        return new Point((int)r.getX(), (int)r.getY());
    }
}
