// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.component;

import javax.swing.Scrollable;
import javax.swing.SwingConstants;

import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;

import java.awt.Rectangle;
import java.awt.Dimension;
import org.mindswap.markup.media.*;

public class ScrollableImageComponent extends ImageComponent
    implements Scrollable, MouseMotionListener {

    private Dimension mViewportSize;

    public ScrollableImageComponent() {
        super();
        setAutoscrolls(true);
        addMouseMotionListener(this);

        mViewportSize = new Dimension(320, 480);
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
        scrollRectToVisible(r);
        // repaint(); TODO: is this needed? It's not in the tutorial example --eik
    }

    private float zoomFactor = 1.0f;
    public float getZoomFactor() {
        return zoomFactor;
    }
    public void setZoomFactor(float zoomFactor) {
        this.zoomFactor = zoomFactor;
        revalidate();
    }

    public Dimension getPreferredSize() {
         Dimension d = getPreferredScrollableViewportSize();
         return new Dimension((int)(zoomFactor*d.width), (int)(zoomFactor*d.height));
    }

    public Dimension getPreferredScrollableViewportSize() {
        return mViewportSize;
    }

    public void setPreferredScrollableViewportSize(Dimension theSize) {
        mViewportSize = theSize;
    }

    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
      return 10;
    }

    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        if (orientation == SwingConstants.HORIZONTAL) {
            return visibleRect.width - 10;
        } else {
            return visibleRect.height - 10;
        }
    }

    public boolean getScrollableTracksViewportWidth() {
        return false;
    }
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }
}
