package org.mindswap.markup;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import org.mindswap.utils.BasicUtils;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.Razor;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */
public class NewStoreDialog extends JDialog implements ActionListener
{
    private static final String CMD_OK = "CMD_OK";
    private static final String CMD_CANCEL = "CMD_CANCEL";
    private static final String CMD_BROWSE = "CMD_BROWSE";

    private Razor mApp;
    private JTextField mURL;

    public NewStoreDialog(Razor theEditor)
    {
        super(theEditor,"Load New Store",true);

        mApp = theEditor;

        initGUI();
    }

    public void show() {
        BasicUtils.centerFrame(mApp,this);
        super.show();
    }

    public void setVisible(boolean theVis) {
        if (theVis)
            BasicUtils.centerFrame(mApp,this);
        super.setVisible(theVis);
    }

    public void actionPerformed(ActionEvent theEvent) {
        String aCommand = theEvent.getActionCommand();
        if (aCommand.equals(CMD_BROWSE))
            doBrowse();
        else if (aCommand.equals(CMD_OK))
            doOk();
        else if (aCommand.equals(CMD_CANCEL))
            doCancel();
    }

    private void initGUI()
    {
        JPanel aPanel = new JPanel();

        aPanel.setLayout(new GridBagLayout());

        mURL = new JTextField();

        JButton aBrowseButton = new JButton("Browse");
        aBrowseButton.setActionCommand(CMD_BROWSE);
        aBrowseButton.addActionListener(this);

        JButton aOkButton = new JButton("Ok");
        aOkButton.setActionCommand(CMD_OK);
        aOkButton.addActionListener(this);

        JButton aCancelButton = new JButton("Cancel");
        aCancelButton.setActionCommand(CMD_CANCEL);
        aCancelButton.addActionListener(this);

        aPanel.add(new JLabel("URL: "),new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
        aPanel.add(mURL,new GridBagConstraints(1, 0, 60, 1, 60, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 5, 0), 0, 0));
        aPanel.add(aBrowseButton,new GridBagConstraints(61, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        JPanel aButtonPanel = new JPanel();
        aButtonPanel.setLayout(new GridBagLayout());

        aButtonPanel.add(aOkButton,new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        aButtonPanel.add(aCancelButton,new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 5, 5, 0), 0, 0));

        //aPanel.add(aButtonPanel,new GridBagConstraints(0, 1, 7, 1, 7, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(aPanel,BorderLayout.NORTH);
        getContentPane().add(aButtonPanel,BorderLayout.SOUTH);

        setSize(300,100);
    }

    private void doOk() {
        hide();

        if (!BasicUtils.isValidURL(mURL.getText()))
        {
            JOptionPane.showMessageDialog(this,"You did not enter a valid URL.  Try again.", "Load error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        mApp.setCursorWaiting();
        mApp.repaint();

        if (!mApp.getMarkupModel().addNewStore(mURL.getText()))
            JOptionPane.showMessageDialog(this,"Invalid store description, could not load.", "Load error", JOptionPane.ERROR_MESSAGE);
        else {
            mApp.makeBookmarkMenu();
            mApp.getJMenuBar().revalidate();
            JOptionPane.showMessageDialog(this,"Store successfully loaded!", "Create New Store", JOptionPane.INFORMATION_MESSAGE);
        }

        mApp.resetCursor();

        mURL.setText("");
    }

    private void doCancel() {
        hide();
        mURL.setText("");
    }

    private void doBrowse()
    {
        JFileChooser fc = mApp.getFileChooser();
        fc.rescanCurrentDirectory();
        fc.setDialogTitle("Open Local Store Description");
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            mURL.setText(file.getAbsoluteFile().toURI().toString());
        }
    }
}
