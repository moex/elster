package org.mindswap.markup;

import java.awt.Dimension;
import java.awt.BorderLayout;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class RDFPanel extends JPanel implements ItemListener {

    private JTextArea textArea;
    private MarkupModel model;
    private JComboBox outputs;

    public RDFPanel(MarkupModel mm) {
        model = mm;

        textArea = new JTextArea();
        textArea.setEditable(false);
        textArea.setWrapStyleWord(false);
        textArea.setText(model.getRDF());

        textArea.setFont(new java.awt.Font("Courier",java.awt.Font.PLAIN,12));

        outputs = new JComboBox();
        outputs.addItem("RDF/XML");
        outputs.addItem("N3");

        outputs.addItemListener(this);

        JPanel boxPane = new JPanel();
        boxPane.setBorder(javax.swing.BorderFactory.createEmptyBorder(5,5,5,5));
        boxPane.add(new javax.swing.JLabel("Display type: "));
        boxPane.add(outputs);

        setLayout(new BorderLayout());
        add(boxPane,BorderLayout.PAGE_START);
        add(new JScrollPane(textArea),BorderLayout.CENTER);
    }

    /**
     * Updates the UI for this component
     */
    public void updateView() {
        int selectedIndex = outputs.getSelectedIndex();
        textArea.setText("");
        if (selectedIndex == 0) // RDF/XML
            textArea.setText(model.getRDF());
        else if (selectedIndex == 1) // n3
            textArea.setText(model.getN3());
        repaint();
    }

    public void itemStateChanged(ItemEvent e) {
        updateView();
    }

    public Dimension getPreferredSize() {
        return new Dimension(550, 400);
    }

}
