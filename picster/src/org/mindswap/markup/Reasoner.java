package org.mindswap.markup;

import java.util.Set;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;

/*
    We should extend this so all the brains of the tool
    are here (in the implementing classes actually) for calculating
    subclasses, ranges, etc.  then we can back it up with
    Pellet or other reasoners so we dont have to rely on the
    crappiness that as jena.  move "reasoning" code here as you see it!
*/

public interface Reasoner {

    public void addModel(Model m);
    public void removeModel(Model m);

    public Set getRanges(Property prop, Resource theClass);

    public Set getDeclaredProperties(Resource res);

    public Set listClasses();
    public Set listIndividuals();

    public boolean isDatatypeProperty(Property p);
    public boolean isObjectProperty(Property p);
    public boolean isClass(Resource res);

//    public int getMinCardinality(Property p);
//    public int getMaxCardinality(Property p);

    public boolean isSubClassOf(Resource child, Resource parent);

    public Resource getType(Resource theRes);
    public Set getTypes(Resource theRes);
}
