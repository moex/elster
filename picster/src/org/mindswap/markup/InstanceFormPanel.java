package org.mindswap.markup;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.hp.hpl.jena.vocabulary.RDFS;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Property;

import java.util.Iterator;
import java.util.Vector;
import java.util.Set;

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.BasicUtils;

import org.mindswap.form.FormControl;
import org.mindswap.form.FormModel;
import org.mindswap.form.Form;

import org.mindswap.form.event.FormEvent;
import org.mindswap.form.event.FormListener;
import org.mindswap.form.event.CollapsibleFormEvent;

import com.hp.hpl.jena.rdf.model.Resource;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class InstanceFormPanel extends JPanel implements FormListener {
    private MarkupModel mModel;
    private FormControl mFormControl;
    private MarkupFormModel mFormModel;
    private Resource mImage;

    public InstanceFormPanel(MarkupModel theModel) {
        mModel = theModel;
        mFormModel = new MarkupFormModel(theModel);
        mFormControl = new FormControl(mFormModel,FormControl.BUTTONS_OK_CANCEL);
        mFormControl.addFormListener(this);

        initGUI();
    }

    public void openImageForm(Resource theImage)
    {
        mFormControl.editCheck();

        mImage = theImage;
        rebuildUI();
        getMarkupModel().formSelected();
    }

    /**
     * Creates the UI representation of this component
     */
    private void initGUI() {
        setLayout(new BorderLayout());
        rebuildUI();
    }

    private void rebuildUI()
    {
        removeAll();
        if (mImage != null)
        {
            JPanel formPanel = new JPanel();
            formPanel.setLayout(new BorderLayout());

            formPanel.add(new ImageMetadataForm(getMarkupModel(),mImage).getControl(),BorderLayout.CENTER);
            JButton aButton = new JButton("Close");
            aButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent theEvent) {
                    formCanceled(null);
                    revalidate();
                    repaint();
                }
            });
            formPanel.add(aButton,BorderLayout.SOUTH);
            add(new JScrollPane(formPanel),BorderLayout.CENTER);
        }
        else add(mFormControl.getControl(),BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    /**
     * Updates this component
     */
    public void update() {
        mFormControl.update();
    }

    /**
     * Updates this component based on the new markup model
     */
    public void updateView() {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        // has instance changed
                        Resource formInstance = null;
                        if (mFormControl.getActiveForm() != null)
                            formInstance = mFormControl.getActiveForm().getInstance();
                        Resource currentInstance = mModel.getCurrentInstance();
                        if (currentInstance==null) {
                            if (mImage != null) {
                                closeImageForm();
                            }
                            // no current, but there is something in the form
                            // basically means we should close the form

                            if (formInstance!=null) {
                                if (mFormControl.isEditing()) {
                                    finishEdit(false);
                                }
                            }
                        } else {
                            if (mImage != null) {
                                closeImageForm();
                            }

                            if (formInstance!=currentInstance) {
                                if (formInstance!=null) {
                                    // switching instances, was editing something
                                    // but now editing something new.
                                    mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);
                                    mFormControl.editCheck();
                                    mFormControl.edit(currentInstance);
                                    mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
                                    getMarkupModel().formSelected();

                                } else {
                                    // editing a new inst, we werent editing anything before
                                    mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);
                                    mFormControl.edit(currentInstance);
                                    mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
                                    getMarkupModel().formSelected();
                                }
                            }
                        }
                        //mFormControl.update();
                        revalidate();
                        repaint();
                    }
                });


    }

    public void formPropertyAdded(FormEvent theEvent) {
        // ignore
    }

    public void formPropertyChanged(FormEvent theEvent) {
        mModel.propertyChanged(theEvent.getForm().getInstance(),theEvent.getProperty(),theEvent.getValue());
    }

    public void formPropertyEdited(FormEvent theEvent) {
        // nothing, this just means a value was added and we're being notified
        // of the new value, for now we can just ignore it
    }

    public void formPropertyDeleted(FormEvent theEvent) {
        RDFNode aNode = null;
        if (theEvent.getValue() instanceof RDFNode)
            aNode = (RDFNode)theEvent.getValue();
        else {
            //System.err.println("unidentifed value type: '"+theEvent.getValue().getClass()+"' for ("+theEvent.getValue()+") assuming literal type");
            aNode = theEvent.getProperty().getModel().createLiteral(theEvent.getValue());
        }
        mModel.deleteProperty(theEvent.getForm().getInstance(),theEvent.getProperty(),aNode);
    }

    /**
     * Called when a form is saved
     * @param aForm Form the form being saved
     * @param isTop boolean true if this is the top most form instance, false otherwise
     */
    public void formSaved(FormEvent theEvent) {
        if (mImage != null)
        {
            saveImageForm();
            return;
        }

        CollapsibleFormEvent aEvent = (CollapsibleFormEvent)theEvent;
        Form aForm = aEvent.getForm();
        boolean isTop = aEvent.isTop();

        Resource inst = aForm.getInstance();
// by arun
        //System.out.println("+++INSTANCE URI IS " + inst.getURI());
        //arun end
        if (isTop) {
            mModel.addPendingDepiction(inst);
            mModel.modelChanged(MarkupModelEvent.FORM_SAVED);
        }

        mModel.commitPendingChangesFor(inst);

        if (!inst.hasProperty(RDFS.label))
        {
            String name = inst.getURI();

            if (BasicUtils.isValidURL(name) || name.startsWith("http"))
                inst.addProperty(RDFS.label, JenaUtils.getLocalName(name).replace('_',' '));
        }

        mModel.saveInstance(inst,!mFormControl.isEditing());

        JOptionPane.showMessageDialog(this,"Data saved successfully!","Success",JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Called when a form has been canceled
     * @param aForm Form the form being canceled
     * @param isTop boolean true if this is the top most form instance, false otherwise
     */
    public void formCanceled(FormEvent theEvent) {
        if (mImage != null)
        {
            closeImageForm();
            mModel.modelChanged(MarkupModelEvent.FORM_CANCELED);
            return;
        }

        CollapsibleFormEvent aEvent = (CollapsibleFormEvent)theEvent;
        Form aForm = aEvent.getForm();
        boolean isTop = aEvent.isTop();

        Resource inst = aForm.getInstance();

        if (isTop) {
            mModel.clearPendingDepiction();
            mModel.setCurrentInstance(null);
            mModel.cancelPendingChanges();
            mModel.modelChanged(MarkupModelEvent.FORM_CANCELED);
        }
        else mModel.cancelPendingChangesFor(inst);

//        if (mModel.getInstance(inst.getURI()) == null) {
//            Resource curr = inst;
//            Iterator iter = mModel.getMediaModel().getRegionDepictions(curr);
//
//            // remove region depicts
//            while (iter.hasNext()) {
//                MediaRegion region = (MediaRegion) iter.next();
//                mModel.getMediaModel().removeRegionDepicts(region, curr);
//            }
//
//            // remove the image depicts
//            mModel.getMediaModel().removeDepicts(curr);
//        }
    }

    /**
     * Returns the markup model associated with this component
     * @return MarkupModel
     */
    public MarkupModel getMarkupModel() {
        return mModel;
    }

    /**
     * Finishes the editing of the current open form
     * @param save boolean if true, save the form, false cancel and do not save
     */
    private void finishEdit(boolean save) {
        mFormControl.finishEdit(save,true);
    }

    private void saveImageForm()
    {
        // the image form makes the changes to the metadata live, so
        // nothing here that needs to be saved
    }

    private void closeImageForm()
    {
        // not editing this image anymore
        mImage = null;

        // rebuild the ui
        rebuildUI();
    }

    private class MarkupFormModel implements FormModel
    {
        private MarkupModel mMarkupModel;

        public MarkupFormModel(MarkupModel theModel)
        {
            mMarkupModel = theModel;
        }

        // utility
        public String getBaseURL() {
            return mMarkupModel.getBaseURL();
        }

        // kb-like functions
        public Vector getAllClasses() {
            return new Vector(mMarkupModel.getAllClasses());
        }

        public Iterator getInstancesOfType(Resource theType) {
            return mMarkupModel.getInstancesWithType(theType);
        }

        public Resource createInstance(String id, String theTypeURI) {
            return mMarkupModel.createInstance(id,theTypeURI);
        }

        public Resource getInstance(String id) {
            return mMarkupModel.getInstance(id);
        }

        public Set getRanges(Property theProp, Resource theResource) {
            return mMarkupModel.getReasoner().getRanges(theProp,theResource);
        }

        public Resource getType(Resource theInst) {
            return JenaUtils.getType(theInst,true);
            //return mMarkupModel.getReasoner().getType(theInst);
        }

        public Set getTypes(Resource theInst) {
            //return JenaUtils.getTypes(theInst,true);
            return mMarkupModel.getReasoner().getTypes(theInst);
        }

        public Set getDeclaredProperties(Resource theInst) {
            return mMarkupModel.getReasoner().getDeclaredProperties(theInst);
            //return JenaUtils.getDeclaredProps(theInst,mMarkupModel.getMasterOntologyModel());
        }
    }
}
