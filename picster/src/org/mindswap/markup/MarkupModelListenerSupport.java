package org.mindswap.markup;

import java.util.Vector;

/**
 */
public class MarkupModelListenerSupport {
    private Vector listeners;

    public MarkupModelListenerSupport() {
    }

    /**
     *
     * @param listener MarkupModelListener
     */
    public synchronized void addMarkupModelListener(MarkupModelListener listener) {
        if (!listeners().contains(listener)) {
            listeners().addElement(listener);
        }
    }

    /**
     *
     * @param listener MarkupModelListener
     */
    public synchronized void removeMarkupModelListener(MarkupModelListener listener) {
        listeners().removeElement(listener);
    }

    /**
     *
     * @param event MarkupModelEvent
     */
    public void fireMarkupModelEvent(MarkupModelEvent event) {
        Vector v = listeners();
        for (int i = 0; i < v.size(); i++) {
            MarkupModelListener listener = (MarkupModelListener) v.elementAt(i);
            listener.handleMarkupModelEvent(event);
        }
    }

    private Vector listeners() {
        if (listeners == null) {
            listeners = new Vector();
        }
        return listeners;
    }

}




