package org.mindswap.markup;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.io.File;

import java.net.URL;

import org.mindswap.component.AddressBar;

import org.mindswap.utils.BasicUtils;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class LoadDialog extends JDialog implements ActionListener {
    private MarkupModel mModel;
    private Razor mApp;

    private JTextField mURL;

    private String type;

    private JFileChooser fc;

    /**
     * Creates a new Load Dialog
     * @param theModel MarkupModel - the model to get the preferences from
     */
    public LoadDialog(Razor theApp, String theType, File loc) {
        super( (javax.swing.JFrame)theApp, "Load " + theType, true);
        type = theType;
        mModel = theApp.getMarkupModel();
        initGUI();
        pack();
        mApp = theApp;
        if(loc == null){
            fc = new JFileChooser();
        }else{
            fc = new JFileChooser(loc);
        }
//by arun
	fc.setMultiSelectionEnabled(true);//arun ends
    } // cons

    public void show() {
        BasicUtils.centerFrame(mApp,this);
        super.show();
    }

    public void setVisible(boolean theVis) {
        if (theVis)
            BasicUtils.centerFrame(mApp,this);
        super.setVisible(theVis);
    }

    public String getType() {
        return type;
    }

    public JFileChooser getFileChooser() {
        return fc;
    }
    private void initGUI() {
        JPanel contentPane = new JPanel();
        setContentPane(contentPane);

        contentPane.setLayout(new GridBagLayout());
        contentPane.add(new javax.swing.JLabel("Enter URL for " + type + ":"), new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));

        mURL = new JTextField();
        mURL.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent theEvent) {
                if (theEvent.getKeyCode() == KeyEvent.VK_ENTER)
                    doOk();
            }
        });

        contentPane.add(mURL, new GridBagConstraints(1, 2, 4, 1, 4, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 10), 0, 0));

        JPanel instPane = new JPanel();
        instPane.setLayout(new GridBagLayout());

        JButton ok_button = new JButton("Ok");
        ok_button.setActionCommand("OK_URL");
        ok_button.addActionListener(this);

        JButton cancel = new JButton("Cancel");
        cancel.setActionCommand("CANCEL");
        cancel.addActionListener(this);

        JButton browse = new JButton("Browse");
        browse.setActionCommand("BROWSE");
        browse.addActionListener(this);

        contentPane.add(ok_button, new GridBagConstraints(1, 3, 2, 1, 2, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        contentPane.add(cancel, new GridBagConstraints(3, 3, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
        contentPane.add(browse, new GridBagConstraints(4, 3, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 5), 0, 0));
    } // initGUI

    private void doOk()
    {
    	File[] openFiles = fc.getSelectedFiles();
    	for (int  i = 0; i < openFiles.length; i++) {
			String filePath = openFiles[i].toURI().toString();
    	
    	
        //commented by arun String filePath = mURL.getText();
        if (filePath.equals("")) {
            // didn't enter a url to load from
            // this should throw some sort of msg box or something...
        } else{
            try {
                mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);

                // forgot the protocol, lets prepend it
                if (filePath.startsWith("www"))
                    filePath = "http://"+filePath;

                URL aURL = new URL(filePath);

                mApp.loadURL(aURL);
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),"Load Error!",JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
            finally {
                mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
            }
            
        }
    	}
    	setVisible(false);
    }
    
    public void doApprove()  {

    	File[] openFiles = fc.getSelectedFiles();
    	for (int  i = 0; i < openFiles.length; i++) {
			String filePath = openFiles[i].toURI().toString();
        try {
            mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);
            URL aURL = new URL(filePath);
            mApp.loadURL(aURL);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
        }
    	}
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK_URL")) {
            doOk();
        } else if (e.getActionCommand().equals("CANCEL")) { // else if
            setVisible(false);
        } else if (e.getActionCommand().equals("BROWSE")) {
            fc.rescanCurrentDirectory();
            if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            	doApprove();
            }
            setVisible(false);

        }
    } // actionPerformed
} // LoadDialog
