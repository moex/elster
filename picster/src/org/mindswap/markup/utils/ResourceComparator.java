package org.mindswap.markup.utils;

import java.util.Comparator;

import com.hp.hpl.jena.rdf.model.Resource;

import com.hp.hpl.jena.vocabulary.RDFS;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

// a comparator for sorting a collection of jena resources
// sorting is done on the resource's label and/or their friendly name
public class ResourceComparator implements Comparator {

  public int compare(Object a, Object b) {
      if (a!=null) {
          if(b!=null) {
              /*
                TODO: should also take into account whether or not to use
                the label of the resource etc.
              */
             if (a instanceof Resource && b instanceof Resource)
             {
                 Resource res1 = (Resource)a;
                 Resource res2 = (Resource)b;

                 String one = null;
                 String two = null;

                 if (res1.isAnon())
                 {
                     if (MarkupModel.USE_LABELS && res1.hasProperty(RDFS.label))
                         one = res1.getProperty(RDFS.label).getObject().toString().toLowerCase();
                     else return +1;
                 }
                 else one = res1.getLocalName().toLowerCase();

                 if (res2.isAnon())
                 {
                     if (fr.inrialpes.exmo.elster.picster.markup.MarkupModel.USE_LABELS && res2.hasProperty(RDFS.label))
                         two = res2.getProperty(RDFS.label).getObject().toString().toLowerCase();
                     else return -1;
                 }
                 else two = res2.getLocalName().toLowerCase();

                 if (fr.inrialpes.exmo.elster.picster.markup.MarkupModel.USE_LABELS && res1.hasProperty(RDFS.label))
                     one = res1.getProperty(RDFS.label).getObject().toString().toLowerCase();

                 if (fr.inrialpes.exmo.elster.picster.markup.MarkupModel.USE_LABELS && res2.hasProperty(RDFS.label))
                     two = res2.getProperty(RDFS.label).getObject().toString().toLowerCase();

                 return one.compareTo(two);
             }
             else return 0;
          } else {
              return +1;
          }
      } else {
          return -1;
      }

  }
}
