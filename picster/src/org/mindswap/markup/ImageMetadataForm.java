package org.mindswap.markup;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import com.hp.hpl.jena.vocabulary.RDF;

import java.util.TreeSet;
import java.util.Set;
import java.util.Iterator;

import org.mindswap.form.Form;
import org.mindswap.form.GenericFormModel;
import org.mindswap.form.DatatypeFormElement;

import org.mindswap.form.event.FormEvent;
import org.mindswap.form.event.FormListener;

import org.mindswap.utils.JenaUtils;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */
public class ImageMetadataForm extends Form {
    private static final String DC_URL = "http://purl.org/dc/elements/1.1/";

    private Model mDCModel;
    private MarkupModel mMarkupModel;

    public ImageMetadataForm(MarkupModel theModel, Resource theInstance)
    {
        super(new GenericFormModel(),theInstance);

        mMarkupModel = theModel;

        mDCModel = ModelFactory.createDefaultModel();
        mDCModel.read(DC_URL);

        addFormListener(new LiveUpdateFormListener());

        GenericFormModel fm = (GenericFormModel)getModel();
        fm.load(DC_URL);

        init();
    }

    protected void createFormElements() {
        getFormElements().clear();

        Set propList = new TreeSet(new org.mindswap.utils.ResourceComparator());

        ResIterator rIter = mDCModel.listSubjectsWithProperty(RDF.type,RDF.Property);

        while (rIter.hasNext())
        {
            Resource res = rIter.nextResource();
            Property aProp = mDCModel.getProperty(res.getURI());
            if (aProp != null && !propList.contains(aProp))
                propList.add(aProp);
        }
        rIter.close();

        StmtIterator sIter = getInstance().listProperties();
        while (sIter.hasNext())
        {
            Statement stmt = sIter.nextStatement();
            Property aProp = stmt.getPredicate();
            if (aProp != null && !propList.contains(aProp))
                propList.add(aProp);
        }
        sIter.close();

        Iterator iter = propList.iterator();
        while (iter.hasNext())
        {
            Property aProp = (Property)iter.next();
            if (aProp != null)
            {
                DatatypeFormElement dfe = new DatatypeFormElement(this,aProp);
                dfe.addFormElementListener(this);
                getFormElements().add(dfe);
            }
        }
    }

    private class LiveUpdateFormListener implements FormListener
    {
        public void formSaved(FormEvent ev) {
        }

        public void formCanceled(FormEvent ev) {
        }

        public void formPropertyAdded(FormEvent ev) {
            handleAddEvent(ev);
        }

        public void formPropertyDeleted(FormEvent ev) {
            handleDeleteEvent(ev);
        }

        public void formPropertyChanged(FormEvent ev) {
            handleDeleteEvent(ev);
        }

        public void formPropertyEdited(FormEvent ev) {
            handleAddEvent(ev);
        }

        private void handleDeleteEvent(FormEvent ev) {
            Resource subj = ev.getForm().getInstance();
            Property pred = ev.getProperty();
            RDFNode obj = JenaUtils.objectToRDFNode(ev.getValue());

            Model aModel = subj.getModel();
            Statement stmt = aModel.createStatement(subj,pred,obj);

            aModel.remove(stmt);

            mMarkupModel.modelChanged();
        }

        private void handleAddEvent(FormEvent ev) {
            Resource subj = ev.getForm().getInstance();
            Property pred = ev.getProperty();
            RDFNode obj = JenaUtils.objectToRDFNode(ev.getValue());

            Model aModel = subj.getModel();

            aModel.add(subj,pred,obj);

            mMarkupModel.modelChanged();
        }
    }
}
