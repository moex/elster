// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup;

import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Model;

import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import org.mindswap.pellet.jena.OWLReasoner;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class MarkupOWLReasoner extends OWLReasoner {
    public MarkupOWLReasoner() {
        super();
    }

    public Set getSuperProperties(Resource res) {
        Set aSet = toJenaSetOfSet(getKB().getSuperProperties(node2term(res)));
        aSet = flatten(aSet);

        cleanup(aSet);

        return aSet;
    }

    public Set getSuperProperties(Resource res, boolean direct) {
        Set aSet = toJenaSetOfSet(getKB().getSuperProperties(node2term(res),direct));
        aSet = flatten(aSet);

        cleanup(aSet);

        return aSet;
    }

    public Set getSubProperties(Resource res) {
        Set aSet = toJenaSetOfSet(getKB().getSubProperties(node2term(res)));
        aSet = flatten(aSet);
        cleanup(aSet);
        return aSet;
    }

    public Set getSubProperties(Resource res, boolean direct) {
        Set aSet = toJenaSetOfSet(getKB().getSubProperties(node2term(res),direct));
        aSet = flatten(aSet);
        cleanup(aSet);
        return aSet;
    }

    public Set getDeclaredProperties(Resource theRes, boolean direct) {
        HashSet props = new HashSet();

        Model theModel = getModel();
        theRes = theModel.getResource(theRes.toString());
        ResIterator rIter = theModel.listSubjectsWithProperty(RDFS.domain,theRes);
        while (rIter.hasNext())
            props.add(rIter.nextResource());
        rIter.close();

        if (!direct)
        {
            // see if we're a subclass of a restriction
            // then check that restriction to see if its on a property
            //Iterator sIter = getSuperClasses(theRes,true).iterator();

            Iterator sIter = collectRestrictionsOnClass(theRes,null).iterator();
            while (sIter.hasNext())
            {
                Resource child = (Resource)sIter.next();
                if (child.hasProperty(OWL.onProperty))
                {
                    // i think all that matters is that there is an onProperty restriction
                    // for the prop, i dont think it matters if its accompanied by a
                    // some or all values from, or a cardinality
                    Resource prop = child.getProperty(OWL.onProperty).getResource();
                    props.add(prop);
                }
            }

            // search all the parents for their props
            Set supers = getSuperClasses(theRes,true);
            sIter = supers.iterator();
            while (sIter.hasNext())
            {
                Set equivs = (Set)sIter.next();
                Iterator eIter = equivs.iterator();
                while (eIter.hasNext())
                {
                    Resource parent = (Resource) eIter.next();
                    if (!theRes.equals(parent) && !parent.isAnon() && !parent.toString().startsWith(RDFS.getURI())
                        && !parent.toString().startsWith(OWL.getURI()) && !parent.toString().startsWith(RDF.getURI()))
                    {
                        Set temp = getDeclaredProperties(parent,false);
                        props.addAll(temp);
                    }
                }
            }
        }

        return props;
    }

    public Set getRanges(Resource theProp, Resource onClass) {
        HashSet ranges = new HashSet();
        Set temp = new HashSet();

        try {
            temp = toJenaSet(getKB().getRanges(node2term(theProp)));
            ranges.addAll(temp);
        } // try
        catch (Exception ex)
        {
            System.err.println("rte: pellet getRanges glue code");
        }

        if (onClass != null)
        {
            ranges.addAll(getRangesFromRestrictions(onClass,theProp));
        }

        temp.clear();

        Iterator iter = ranges.iterator();
        while (iter.hasNext())
        {
            Resource aRange = (Resource)iter.next();
            Iterator sIter = getSubClasses(aRange).iterator();
            while (sIter.hasNext())
            {
                Set equivs = (Set)sIter.next();
                temp.addAll(equivs);
            }
        }
        ranges.addAll(temp);

        return ranges;
    }

    private Set collectRestrictionsOnClass(Resource theClass, Property thePredicate)
    {
        HashSet restrictions = new HashSet();

        if (thePredicate == null)
            thePredicate = RDFS.subClassOf;

        StmtIterator sIter = getModel().listStatements(theClass,thePredicate, (Resource)null);
        while (sIter.hasNext())
        {
            Resource sc = sIter.nextStatement().getResource();
            if (sc.hasProperty(RDF.type,OWL.Restriction))
                restrictions.add(sc);
        }
        sIter.close();
        return restrictions;
    }

    private Set getRangesFromRestrictions(Resource theClass, Resource theProp)
    {
        HashSet ranges = new HashSet();

        Iterator sIter = collectRestrictionsOnClass(theClass,null).iterator();
        while (sIter.hasNext())
        {
            Resource res = (Resource)sIter.next();

            if (res.getProperty(OWL.onProperty).getResource().equals(theProp))
            {
                // we'll treat some and all values from as the same thing in the code
                // photostuff doesnt need to make the distinction, at least not yet
                // doubtful we'll want error checking that sophisticated
                if (res.hasProperty(OWL.allValuesFrom))
                    ranges.add(res.getProperty(OWL.allValuesFrom).getObject());
                else if (res.hasProperty(OWL.someValuesFrom))
                    ranges.add(res.getProperty(OWL.someValuesFrom).getObject());
            }
        }

        // do we need to:
        // iterate over named super classes and check them for restrictions

        // iterate over the subclasses for each restriction defined range
        // and add them to the list
        HashSet tempRanges = new HashSet();
        Iterator rangeIter = ranges.iterator();
        while (rangeIter.hasNext())
        {
            Resource aRange = (Resource) rangeIter.next();
            sIter = getSubClasses(aRange,false).iterator();
            while (sIter.hasNext())
            {
                Set equivs = (Set)sIter.next();
                Iterator eIter = equivs.iterator();
                while (eIter.hasNext())
                {
                    Resource parent = (Resource)eIter.next();
                    tempRanges.add(parent);
                    tempRanges.addAll(getRanges(theProp,parent));
                }
            }
        }
        ranges.addAll(tempRanges);
        return ranges;
    }

    private Set flatten(Set theSet)
    {
        HashSet aResult = new HashSet();
        Iterator aIter = theSet.iterator();

        while (aIter.hasNext())
        {
            Set aSet = (Set)aIter.next();
            Iterator aValIter = aSet.iterator();
            while (aValIter.hasNext())
                aResult.add(aValIter.next());
        }

        return aResult;
    }

    private void cleanup(Set theSet)
    {
        theSet.remove(OWL.Thing);
        theSet.remove(OWL.Nothing);
    }
}
