package org.mindswap.markup;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.JPopupMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JDialog;
import javax.swing.KeyStroke;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSplitPane;
import javax.imageio.ImageWriter;
import javax.imageio.ImageReader;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import java.lang.Character;

import java.awt.image.BufferedImage;
import java.awt.Component;
import java.awt.Point;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import java.util.Enumeration;
import java.util.Vector;
import java.util.Properties;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;

import java.net.URL;
import java.net.URI;
import java.net.URLConnection;
import java.net.JarURLConnection;

import java.util.jar.JarFile;
import java.util.jar.JarEntry;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.mindswap.markup.media.MediaListPanel;
import org.mindswap.markup.media.MediaMarkupModel;
import org.mindswap.markup.media.MediaInfoPanel;
import org.mindswap.markup.media.MediaComponent;
import org.mindswap.markup.media.MediaImageComponent;
import org.mindswap.markup.media.MediaSearchPanel;
import org.mindswap.markup.media.MediaFilter;
import org.mindswap.markup.media.MediaModel;
import org.mindswap.markup.media.MarkupImageModel;

import org.mindswap.markup.MarkupModelListener;
import org.mindswap.markup.MarkupModelEvent;
import org.mindswap.markup.PreferencesDialog;

import org.mindswap.markup.plugin.PlugIn;

import org.mindswap.ocl.OCLFactory;
import org.mindswap.ocl.OCLModel;
import org.mindswap.ocl.OCLObject;

import org.mindswap.store.Store;

import org.mindswap.store.loader.StoreLoader;

import org.mindswap.component.OntologyTreeView;
import org.mindswap.component.AddressBar;

import org.mindswap.utils.BasicUtils;
import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.Bookmark;
import org.mindswap.markup.utils.QueryInterface;

import org.mindswap.multimedia.DigitalMedia;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.rdql.ResultBinding;
import com.hp.hpl.jena.rdql.Query;
import com.hp.hpl.jena.rdql.QueryEngine;
import com.hp.hpl.jena.rdql.QueryResults;
import net.jxta.picshare.PicShare;

import com.hp.hpl.jena.vocabulary.RDFS;

import org.mindswap.markup.plugin.OntoEditPlugIn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */
public class Razor extends JFrame implements MarkupModelListener, ActionListener, ItemListener {
    // TODO: maybe read these from a config file
    public static final String APP_URL = "http://www.mindswap.org/2003/PhotoStuff/";
    public static final String APP_NAME = "PicSter";
    public static final String APP_VERSION = "1.0.6 Beta";

    private static final int TYPE_MEDIA_IMAGE = 1;

    private static final String CMD_EXIT = "CMD_EXIT";
    private static final String CMD_QUIT = "CMD_QUIT";
    private static final String CMD_SHOW_LAUNCH_BAR = "CMD_SHOW_LAUNCH_BAR";
    private static final String CMD_VIEW_RDF = "CMD_VIEW_RDF";
    private static final String CMD_SHOW_HELP = "CMD_SHOW_HELP";
    private static final String CMD_SHOW_ABOUT = "CMD_SHOW_ABOUT";
    private static final String CMD_ADD_BOOKMARK = "CMD_ADD_BOOKMARK";
    private static final String CMD_REMOVE_BOOKMARK = "CMD_REMOVE_BOOKMARK";
    private static final String CMD_SHOW_PREFS = "CMD_SHOW_PREFS";
    private static final String CMD_SET_AUTOLOAD_METADATA = "CMD_SET_AUTOLOAD_METADATA";
    private static final String CMD_CLEAR = "CMD_CLEAR";
    private static final String CMD_LOAD_IMAGE = "CMD_LOAD_IMAGE";
    private static final String CMD_LOAD_ONTOLOGY = "CMD_LOAD_ONTOLOGY";
    //by arun
    private static final String CMD_SAVE_ONTOLOGY = "CMD_SAVE_ONTOLOGY";
    private static final String CMD_SAVE_ANNOTATION = "CMD_SAVE_ANNOTATION";
    private static final String CMD_LOAD_DEFAULT_ONTOLOGIES = "CMD_LOAD_DEFUALT_ONTOLOGIES";
    private static final String CMD_LOAD_IMAGES = "CMD_LOAD_IMAGES";
    //private static final String CMD_LOAD_INSTANCES = "CMD_LOAD_INSTANCES";
    private static final String CMD_LOAD_ANNOTATIONS = "CMD_LOAD_ANNOTATIONS";//arun ends
    private static final String CMD_ADD_STORE = "CMD_ADD_STORE";
    private static final String CMD_REMOVE_STORE = "CMD_REMOVE_STORE";
    private static final String CMD_ADD_PLUGIN = "CMD_ADD_PLUGIN";
    private static final String CMD_SPARQL_QUERY = "CMD_SPARQL_QUERY";
    private static final String CMD_CLEAR_ALL = "CMD_CLEAR_ALL";
    private static final String CMD_SUBMIT = "CMD_SUBMIT";
    private static final String CMD_SHOW_INST_LIST = "CMD_SHOW_INST_LIST";
    private static final String CMD_SHOW_QUERY_INTERFACE = "CMD_SHOW_QUERY_INTERFACE";
    
//  by arun
    private String editType;
    private Razor thisRazor = this;
    

    private MediaMarkupModel mModel;

    private MediaListPanel mMediaListPanel;
    private MediaInfoPanel mMediaInfoPanel;
    private MediaSearchPanel mMediaSearchPanel;
    private P2PQueryPanel mP2PQueryPanel;

    private JTabbedPane mSideInfoPanel;

    private JPanel mMediaComponentPanel;

    private OntologyTreeView mOntologyTree;

    private AddressBar mAddressBar;

    private InstanceFormPanel mFormPanel;

    private JComboBox mStoreList;
    private DefaultComboBoxModel mStoreModel;

    private JDialog mLaunchBarFrame;
    private JDialog mViewRDFDialog;
    private LoadDialog mLoadDialog;
    private PreferencesDialog mPreferencesDialog;
    private RDFPanel mRDFPanel;
    private InstanceList mInstList;

    private JMenu mBookmarkMenu;

    private JFileChooser mFileChooser;

    private Set mPlugIns = new HashSet();

    private int mMediaType = TYPE_MEDIA_IMAGE;

    private boolean mStoresUpdating = false;
    //by arun
    private File saveFile = null;
    private Properties prop;
    private String id;
    private String uriBase;
    public static String picsterHome;//arun ends
    public PicShare p;

    public Razor(PicShare ps) {
        
        super(APP_NAME+" v"+APP_VERSION);
        p = ps;
        org.mindswap.multimedia.impl.Mapper.mapImplementations();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent ev) {
                doExit();
            }
        });

        mModel = new MediaMarkupModel();
        mModel.addMarkupModelListener(this);

        mMediaListPanel = new MediaListPanel(mModel);
        mMediaInfoPanel = new MediaInfoPanel(mModel);
        
        //arun sarts
        prop = new Properties();
        try {
        FileInputStream fis = new FileInputStream(".elster.xml");
        prop.loadFromXML(fis);
        id = prop.getProperty("userID");
        uriBase = prop.getProperty("uriBase");
        picsterHome = prop.getProperty("PicSter_Home");
        } catch (Exception e)  {
        	System.err.print(" Properties file not found\n");
        }
        
        mModel.setUserId(id);
        mModel.setUriBase(uriBase);
        //mModel.loadInstances();//arun ends
                
        initMenu();
        initGUI();

	//by arun
        Properties imgUris = new Properties();
        Properties regUris = new Properties();
        try {
        FileInputStream fis1 = new FileInputStream("ImgUriToUrl.xml");
        imgUris.loadFromXML(fis1);
        FileInputStream fis2 = new FileInputStream("RegUriToUrl.xml");
        regUris.loadFromXML(fis2);        
        } catch (Exception e)  {
        	System.err.print(" Properties file not found\n");
        }
        
       
        HashMap img = new HashMap();
        HashMap reg = new HashMap();
        if(!img.equals(null))
            img.putAll(imgUris);
        if(!reg.equals(null))
            reg.putAll(regUris);
        MarkupImageModel.putUriMap(img);
        MarkupImageModel.putRegUriMap(reg);
        
	doLoadInstances(); 
	doLoadAnnotations();// arun ends

        mindswapInit();
    }

    private void initMenu()
    {
        JMenuBar aMenuBar = new JMenuBar();

        JMenu aFileMenu = new JMenu("File");
        aFileMenu.setMnemonic('F');

        JMenuItem aFileSave = makeMenuItem("Submit Markup...",CMD_SUBMIT,KeyStroke.getKeyStroke(KeyEvent.VK_M,KeyEvent.CTRL_MASK));
        aFileSave.setMnemonic('M');
        
        
        JMenuItem aOntologySave = makeMenuItem("Save Ontology",CMD_SAVE_ONTOLOGY,KeyStroke.getKeyStroke(KeyEvent.VK_S,KeyEvent.CTRL_MASK));
        aFileSave.setMnemonic('S');
	JMenuItem aAnnotationSave = makeMenuItem("Save Annotations",CMD_SAVE_ANNOTATION,KeyStroke.getKeyStroke(KeyEvent.VK_T,KeyEvent.CTRL_MASK));
        aFileSave.setMnemonic('T');
        JMenuItem aOntologiesLoad = makeMenuItem("Load default ontologies",CMD_LOAD_DEFAULT_ONTOLOGIES,KeyStroke.getKeyStroke(KeyEvent.VK_D,KeyEvent.CTRL_MASK));
        aFileSave.setMnemonic('D');

	JMenuItem aImagesLoad = makeMenuItem("Load Images from Reporitory",CMD_LOAD_IMAGES,KeyStroke.getKeyStroke(KeyEvent.VK_R,KeyEvent.CTRL_MASK));
        aFileSave.setMnemonic('R');
        aFileSave.setMnemonic('N');

        JMenuItem aFileClear = makeMenuItem("Clear",CMD_CLEAR,KeyStroke.getKeyStroke(KeyEvent.VK_E,KeyEvent.CTRL_MASK));
        aFileClear.setMnemonic('E');

	JMenuItem aFileClearAll = makeMenuItem("Clear All",CMD_CLEAR_ALL,KeyStroke.getKeyStroke(KeyEvent.VK_L,KeyEvent.CTRL_MASK));
        aFileClearAll.setMnemonic('L');

        JMenuItem aLoadOntology = makeMenuItem("Load Ontology...",CMD_LOAD_ONTOLOGY,KeyStroke.getKeyStroke(KeyEvent.VK_O,KeyEvent.CTRL_MASK));
        aLoadOntology.setMnemonic('O');

        JMenuItem aLoadImage = makeMenuItem("Load Image...",CMD_LOAD_IMAGE,KeyStroke.getKeyStroke(KeyEvent.VK_I,KeyEvent.CTRL_MASK));
        aLoadImage.setMnemonic('I');

        JMenuItem aFileExit = makeMenuItem("Exit",CMD_EXIT,KeyStroke.getKeyStroke(KeyEvent.VK_X,KeyEvent.CTRL_MASK));
        aFileExit.setMnemonic('x');

	JMenuItem aFileQuit = makeMenuItem("Quit without saving",CMD_QUIT,KeyStroke.getKeyStroke(KeyEvent.VK_Q,KeyEvent.CTRL_MASK));
        aFileExit.setMnemonic('Q');

        aFileMenu.add(aFileSave);
	aFileMenu.add(new JSeparator());
        aFileMenu.add(aLoadImage);
        aFileMenu.add(aLoadOntology);
	aFileMenu.add(new JSeparator());
	aFileMenu.add(aOntologiesLoad);
	aFileMenu.add(aImagesLoad);
	
	aFileMenu.add(new JSeparator());
        aFileMenu.add(aOntologySave);	
	aFileMenu.add(aAnnotationSave);
        
        aFileMenu.add(new JSeparator());
        aFileMenu.add(aFileClear);
        aFileMenu.add(aFileClearAll);
        aFileMenu.add(new JSeparator());
        aFileMenu.add(aFileExit);
	aFileMenu.add(aFileQuit);
        JMenu aOptionsMenu = new JMenu("Options");
        aOptionsMenu.setMnemonic('O');

        JMenuItem aPrefsItem = makeMenuItem("Preferences...",CMD_SHOW_PREFS,KeyStroke.getKeyStroke(KeyEvent.VK_P,KeyEvent.CTRL_MASK));
        aPrefsItem.setMnemonic('P');
        aOptionsMenu.add(aPrefsItem);
        JMenu aWindowsMenu = new JMenu("Windows");
        aWindowsMenu.setMnemonic('W');

        JMenuItem viewRDF = makeMenuItem("View RDF...",CMD_VIEW_RDF,KeyStroke.getKeyStroke(KeyEvent.VK_W,KeyEvent.CTRL_MASK));
        viewRDF.setMnemonic('W');

        JMenuItem launchBar = makeMenuItem("Show Launch Bar...",CMD_SHOW_LAUNCH_BAR,KeyStroke.getKeyStroke(KeyEvent.VK_L,KeyEvent.CTRL_MASK));
        launchBar.setMnemonic('L');

        JMenuItem instList = makeMenuItem("Show Instance List...",CMD_SHOW_INST_LIST,KeyStroke.getKeyStroke(KeyEvent.VK_N,KeyEvent.CTRL_MASK));
        instList.setMnemonic('I');

	JMenuItem qInterface = makeMenuItem("Show Query Interface...",CMD_SHOW_QUERY_INTERFACE,KeyStroke.getKeyStroke(KeyEvent.VK_Q,KeyEvent.CTRL_MASK));
        instList.setMnemonic('U');

        aWindowsMenu.add(viewRDF);
        aWindowsMenu.add(launchBar);
        aWindowsMenu.add(instList);
        aWindowsMenu.add(qInterface);

        mBookmarkMenu = new JMenu("Bookmarks");
        mBookmarkMenu.setMnemonic('B');

        JMenu aAdvancedMenu = new JMenu("Advanced");
        aAdvancedMenu.setMnemonic('A');

        JMenuItem loadStore = makeMenuItem("Add Store...",CMD_ADD_STORE,KeyStroke.getKeyStroke(KeyEvent.VK_F2,0));
        loadStore.setMnemonic('A');

        JMenuItem removeStore = makeMenuItem("Remove Store...",CMD_REMOVE_STORE,KeyStroke.getKeyStroke(KeyEvent.VK_F3,0));
        removeStore.setMnemonic('R');

        JMenuItem addPlugin = makeMenuItem("Add Plug-In...",CMD_ADD_PLUGIN,KeyStroke.getKeyStroke(KeyEvent.VK_F4,0));
        addPlugin.setMnemonic('P');
        //by arun
        JMenuItem sparqlQuery = makeMenuItem("SPARQL Query",CMD_SPARQL_QUERY,KeyStroke.getKeyStroke(KeyEvent.VK_F5,0));
        addPlugin.setMnemonic('S');

        aAdvancedMenu.add(loadStore);
        aAdvancedMenu.add(removeStore);
        aAdvancedMenu.add(addPlugin);
        aAdvancedMenu.add(sparqlQuery);//by arun

        JMenu aHelpMenu = new JMenu("Help");
        aHelpMenu.setMnemonic('H');

        JMenuItem aHelpItem = makeMenuItem("Help...",CMD_SHOW_HELP,KeyStroke.getKeyStroke(KeyEvent.VK_F1,0));
        JMenuItem aAboutItem = makeMenuItem("About...",CMD_SHOW_ABOUT);

        aHelpMenu.add(aHelpItem);
        aHelpMenu.add(new JSeparator());
        aHelpMenu.add(aAboutItem);

        aMenuBar.add(aFileMenu);
        aMenuBar.add(aOptionsMenu);
        aMenuBar.add(aWindowsMenu);
        aMenuBar.add(mBookmarkMenu);
        aMenuBar.add(aAdvancedMenu);
        aMenuBar.add(aHelpMenu);

        setJMenuBar(aMenuBar);

        makeBookmarkMenu();
    }

    //by arun
    
    public String getEditType()  {
    	return this.editType;
    }
    private void doShowPlugInPanel()
    {
        new PlugInDialog(this).show();
    }

    private void doShowQueryBox()
    {
        this.popupQuery();
    }
    
    /**
	 * Popup the Pellet Query panel
	 *
	 */
	public void popupQuery() {

		// popup the query interface
		JFrame frame = new JFrame("SPARQL Query Interface");
		QueryInterface qi = new QueryInterface(this, mModel, p);
		frame.getContentPane().add(qi);
		frame.setSize(500, 500);
		frame.setVisible(true);
		frame.toFront();
		qi.sparqlText.requestFocus();
	}

    public void loadPlugIn(String theURL)
    {
        PlugIn aPlugIn = null;

        try {
            OCLModel aModel = OCLFactory.createModel();
            aModel.register("http://www.mindswap.org/PhotoStuff#MarkupEditor",this);
            aModel.read(theURL);

            ResIterator rIter = aModel.listSubjects();
            while (rIter.hasNext())
            {
                Resource aRes = rIter.nextResource();
                if (aRes.canAs(OCLObject.class))
                {
                    OCLObject aObj = (OCLObject)aRes.as(OCLObject.class);
                    aPlugIn = (PlugIn)aObj.create();
                    break;
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        if (aPlugIn != null)
        {
            try {
                setCursorWaiting();
                aPlugIn.init();
                mPlugIns.add(aPlugIn);
                JOptionPane.showMessageDialog(this,"Plug In successfully loaded!","Load Plug-In",JOptionPane.INFORMATION_MESSAGE);
            }
            catch (Exception ex) {
                JOptionPane.showMessageDialog(this,"Plug In failed to load.","Load Plug-In",JOptionPane.ERROR_MESSAGE);
            }
            finally {
                resetCursor();
            }
        }
        else JOptionPane.showMessageDialog(this,"Plug In failed to load.","Load Plug-In",JOptionPane.ERROR_MESSAGE);
    }

    protected JFileChooser getFileChooser() {
        if (mFileChooser==null) {
            mFileChooser = new JFileChooser();
            File f = new File(System.getProperty("user.dir"));
            mFileChooser.setCurrentDirectory(f);
        }
        return mFileChooser;
    }

    protected JMenu makeBookmarkMenu()
    {
        mBookmarkMenu.removeAll();

        ActionListener aActionListener = new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                String url = ev.getActionCommand();

                try {
                    setCursorWaiting();
                    loadURL(new URL(url));
                }
                catch (Exception ex) {
                    JOptionPane.showMessageDialog(null,"There was an error while opening the bookmark","Error",JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace();
                }
                finally {
                    resetCursor();
                }
            }
        };

        JMenuItem add = makeMenuItem("Add Bookmark",CMD_ADD_BOOKMARK,KeyStroke.getKeyStroke(KeyEvent.VK_B,KeyEvent.CTRL_MASK));
        add.setMnemonic('A');

        JMenuItem remove = makeMenuItem("Remove Bookmark",CMD_REMOVE_BOOKMARK,KeyStroke.getKeyStroke(KeyEvent.VK_R,KeyEvent.CTRL_MASK));
        remove.setMnemonic('B');

        JMenu imageBookmarks = new JMenu("Images");
        JMenu ontologyBookmarks = new JMenu("Ontology");

        mBookmarkMenu.add(add);
        mBookmarkMenu.add(remove);
        mBookmarkMenu.add(imageBookmarks);
        mBookmarkMenu.add(ontologyBookmarks);

        Iterator iter = mModel.listBookmarks().iterator();
        while (iter.hasNext())
        {
            Bookmark b = (Bookmark)iter.next();

            JMenuItem item = new JMenuItem(b.getLabel());
            item.addActionListener(aActionListener);
            item.setActionCommand(b.getURL());

            if (b.getType() == Bookmark.BOOKMARK_IMAGE)
                imageBookmarks.add(item);
            else if (b.getType() == Bookmark.BOOKMARK_ONTOLOGY)
                ontologyBookmarks.add(item);
        }
        return mBookmarkMenu;
    }

    public void actionPerformed(ActionEvent theEvent)
    {
        String aCommand = theEvent.getActionCommand();

        if (aCommand.equals(getAddressBar().getActionCommand()))
            handleAddressBar();
        else if (aCommand.equals(CMD_EXIT))
            doExit();
	else if (aCommand.equals(CMD_QUIT))
            doQuit();
        else if (aCommand.equals(CMD_SET_AUTOLOAD_METADATA)) {
            JCheckBoxMenuItem jmi = (JCheckBoxMenuItem)theEvent.getSource();
            getMarkupModel().setAutoLoadImageMetadata(jmi.getState());
        }
        else if (aCommand.equals(CMD_LOAD_IMAGE))
            doLoadImage();
        //by arun
        else if (aCommand.equals(CMD_SAVE_ONTOLOGY))
            doSaveOntology(); 
	else if (aCommand.equals(CMD_SAVE_ANNOTATION))
            doSaveAnnotation();
	 else if (aCommand.equals(CMD_LOAD_DEFAULT_ONTOLOGIES))
            doLoadDefault("ontologies/");
	//  else if (aCommand.equals(CMD_LOAD_INSTANCES))
          //  doLoadInstances();
	  else if (aCommand.equals(CMD_LOAD_ANNOTATIONS))
            doLoadAnnotations();
	  else if (aCommand.equals(CMD_LOAD_IMAGES))
            doLoadDefault("images/");//arun ends
        else if (aCommand.equals(CMD_LOAD_ONTOLOGY))
            doLoadOntology();
        else if (aCommand.equals(CMD_CLEAR))
            doClear();
        else if (aCommand.equals(CMD_VIEW_RDF))
            doViewRDF();
        else if (aCommand.equals(CMD_SHOW_LAUNCH_BAR))
            doShowLaunchBar();
        else if (aCommand.equals(CMD_SHOW_ABOUT))
            doShowAbout();
        else if (aCommand.equals(CMD_SHOW_HELP))
            doShowHelp();
        else if (aCommand.equals(CMD_ADD_BOOKMARK))
            doAddBookmark();
        else if (aCommand.equals(CMD_REMOVE_BOOKMARK))
            doRemoveBookmark();
        else if (aCommand.equals(CMD_SHOW_PREFS))
            doShowPrefs();
        else if (aCommand.equals(CMD_ADD_STORE))
            doAddStore();
        else if (aCommand.equals(CMD_REMOVE_STORE))
            doRemoveStore();
        else if (aCommand.equals(CMD_ADD_PLUGIN))
            doShowPlugInPanel();
        else if (aCommand.equals(CMD_SPARQL_QUERY))
            doShowQueryBox();
	else if (aCommand.equals(CMD_CLEAR_ALL))
            doClearAll();
        else if (aCommand.equals(CMD_SUBMIT))
            doSubmit();
        else if (aCommand.equals(CMD_SHOW_INST_LIST))
            doShowInstanceList();
	else if (aCommand.equals(CMD_SHOW_QUERY_INTERFACE))
	    doShowQueryInterface();
    }

    private void doShowQueryInterface()  {
	    // TODO: add peers
	    // keyword search
	    // sparql text search
	    // add class search
    }

    private void doShowInstanceList()
    {
        BasicUtils.centerFrame(this,mInstList);
        mInstList.show();
    }

    private void doSubmit()
    {
        new SaveDialog(this).show();
    }

    private void doClearAll()
    {
        int option = JOptionPane.showConfirmDialog(this,"This will delete all ontologies, instances, images, depictions, and regions.  Would you like to proceed?","Confirm Clear All",JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION)
            getMarkupModel().clearAll();
    }

    private void doAddStore() {
        new NewStoreDialog(this).show();
    }

    private void doRemoveStore()
    {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());

        JComboBox siteList = new JComboBox(getMarkupModel().getStores());

        contentPane.add(new JLabel("Store to remove:"), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        contentPane.add(siteList, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        int option = JOptionPane.showConfirmDialog(this, contentPane, "Remove Store", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (option == JOptionPane.OK_OPTION)
            getMarkupModel().removeStore((Store)siteList.getSelectedItem());
    }

    private void doClear()
    {
        int option = JOptionPane.showConfirmDialog(this,"This will delete all new local instances and depictions. Would you like to proceed?","Confirm Clear Markup",JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION)
            getMarkupModel().clear();
    }

    private void doLoadImage() {
        getLoadDialog("Image").show();
	//by arun
	//mMediaListPanel.update();
	//mInstList.update();
	//mMediaInfoPanel.update();
        mModel.modelChanged();
    }
    
    //by arun
    private void doLoadInstances()  {
    	mModel.loadInstances();
    }

    private void doLoadAnnotations()  {
    	mModel.loadAnnotations();
    }

    private void doSaveAnnotation() {
//    	Model workingm = ModelFactory.createDefaultModel();
        //workingm = mModel.getWorkingModel(mModel.getCurrentStore().getImageURL());
        //Resource myRes = workingm.getResource(aRes.getURI());
        
        //StmtIterator sIter = myRes.listProperties();
    	////String saveFile = null;
    ////	boolean gotName = false;
        //System.out.println("In Save Annotation");
        
       //// LinkedHashMap mOntologyModels = mModel.getOntologyModels();
       //// Iterator iter = mModel.getOntologyModels().keySet().iterator();        
        //while (iter.hasNext())
       //// {
        	//System.out.println("In Model Iteration");
        	try  {
           // Object key = iter.next();
          //  OntModel aModel = (OntModel)mOntologyModels.get(key);
            Model aModel = mModel.getCurrentStore().asModel();
            Model mTemp = ModelFactory.createDefaultModel();
            Map ns = aModel.getNsPrefixMap();
            mTemp.setNsPrefixes(ns);
            //Resource r = aModel.getResource("http://exmo.infrialpes.fr/people/sharma/images/#coloseum.JPG");
            //if(r!=null)//System.out.println("I am there");
            StmtIterator stmts = aModel.listStatements();
            //int i = 0;
           // while(stmts.hasNext()) i = i+1;
            //System.out.println("No of statements = " + i);
            Vector images = new Vector();
            while(stmts.hasNext()) {
            	Statement st = stmts.nextStatement();
            	if(!st.getSubject().isAnon())  {
            		String subject = st.getSubject().getURI();
            		String localName = (String) MarkupImageModel.getUriMap().get(subject);
            	//	System.out.println("THIS IS LOCAL " + localName);
            		if(localName != null){
            			int le = localName.length();
            			if(le>1) {
            				String file = localName.substring(localName.lastIndexOf('/'), localName.length());
            				if (!images.contains(file))
            					images.add(file);
            			}
            		}
            	}
            }
            for(int i = 0; i < images.size(); i++)  {
            	StmtIterator stmt = aModel.listStatements();
                while(stmt.hasNext())  {
                  	Statement s = stmt.nextStatement();
                  	if(!s.getSubject().isAnon()) {
                  		String subject = s.getSubject().getURI();
                  		if(subject.indexOf((String)images.get(i)) != -1) {
                  			Property p = s.getPredicate();
                  			String pp = p.toString();
                  			if(!pp.contains("purl") && !pp.contains("desc"))
                  			mTemp.add(s);           	
                  		}
                  		else continue;
                  	}
                }
                String annoFileName = (String)images.get(i) + ".rdf";
                File annoDir = new File("annotations");
                if(!annoFileName.equals(""))  {
                	if (!annoDir.exists()) annoDir.mkdir();
                	File annoFile = new File("annotations/" + annoFileName);
                	OutputStream fileStream = new FileOutputStream(annoFile);
                	RDFWriter writer = mTemp.getWriter();
                	writer.write(mTemp, fileStream, null);
                }
                StmtIterator modelstmt = mTemp.listStatements();
                while(modelstmt.hasNext())
                	modelstmt.removeNext();
            }
            } catch(Exception e)  {
            	System.err.println("Error in writing Annotation file");
    			e.printStackTrace();
            }
      //// }
    }
    
    private void doSaveOntology()  {
    	LinkedHashMap models = mModel.getOntologyModels();
    	for (Iterator it=models.entrySet().iterator(); it.hasNext(); ) {
        	Map.Entry entry = (Map.Entry)it.next();
	        //URL key = (URL)entry.getKey();
        	OntModel value = (OntModel)entry.getValue();
	    boolean saved =  mModel.saveOntology(value);
	    if(saved) 
	    	System.out.println("Ontology Saved");
	    else
	    	System.err.println("Error in saving ontology");
    	}	    	
    }//arun ends

    private void doLoadOntology() {
        getLoadDialog("Ontology").show();
    }

    private LoadDialog getLoadDialog(String type) {
        if (mLoadDialog == null) {
            mLoadDialog = new LoadDialog(this, type, null);
        }
        else if (!mLoadDialog.getType().equals(type)) {
            mLoadDialog = new LoadDialog(this, type, mLoadDialog.getFileChooser().getCurrentDirectory());
        }
        return mLoadDialog;
    }

    private void doShowPrefs() {
        getPreferencesDialog().show();
    }

    private PreferencesDialog getPreferencesDialog() {
        if (mPreferencesDialog == null) {
            mPreferencesDialog = new PreferencesDialog(this);
        } // if
        return mPreferencesDialog;
    }

    public MediaMarkupModel getMarkupModel() {
        return mModel;
    }
    
    // by arun
    
    public void setMarkupModel(MediaMarkupModel mm)  {
    	this.mModel = mm;
    }

    private void doShowAbout()
    {
        JOptionPane.showMessageDialog(null,
                                      APP_NAME+" v "+APP_VERSION+"\n" +
                                      "EXMO TEAM\n" +
                                      "INRIA Rhone-Alpes\n" +
                                      "Based on PhotoStuff\n" +                                      "MINDSWAP Research Lab\n" +
                                      "The University of Maryland\n" +
                                      APP_URL,
                                      "About "+APP_NAME,
                                      JOptionPane.INFORMATION_MESSAGE );
    }

    private void doShowHelp()
    {
        String aHelpURL = APP_URL + "UserGuide.pdf";

        try {
            edu.stanford.ejalbert.BrowserLauncher.openURL(aHelpURL);
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(this,"There was an error while opening the help file.", "Open Page Error",JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }

    private void doViewRDF()
    {
        BasicUtils.centerFrame(null, getViewRDFDialog());
        getViewRDFDialog().show();
    }

    private JDialog getViewRDFDialog() {
        if (mViewRDFDialog == null) {
            mViewRDFDialog = new JDialog(this,"View RDF",false);
            mViewRDFDialog.getContentPane().add(getRDFPanel());
            mViewRDFDialog.pack();
        }
        return mViewRDFDialog;
    }

    private RDFPanel getRDFPanel() {
        if (mRDFPanel == null) {
            mRDFPanel = new RDFPanel(mModel);
        }
        return mRDFPanel;
    }

    private void doShowLaunchBar()
    {
        if (mLaunchBarFrame == null)
        {
            LaunchBar aLaunchBar = new LaunchBar();
            aLaunchBar.addLaunchBarListener(new LaunchBarListener() {
                public void urlLaunched(URL theURL) {
                    mLaunchBarFrame.setVisible(false);
                }
            });
            mLaunchBarFrame = new JDialog(this,"Launch Bar",true);
            mLaunchBarFrame.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
            JPanel pane = new JPanel();
            pane.setLayout(new BorderLayout());
            pane.add(aLaunchBar, BorderLayout.CENTER);
            mLaunchBarFrame.getContentPane().add(pane);
            mLaunchBarFrame.setSize(325, 75);
        }

        BasicUtils.centerFrame(this,mLaunchBarFrame);
        mLaunchBarFrame.show();
    }

    private void handleAddressBar()
    {
        URL theURL = getAddressBar().getAddress();

        loadURL(theURL);
    }

    public void openMediaForm(Resource theMediaResource)
    {
        mFormPanel.openImageForm(theMediaResource);
    }

    public void loadURL(URL theURL)
    {
        // 1) an ontology
        // 2) new media

        try {
            String addressString = theURL.toString();
	    //arun
	    //System.out.println("Reached loadURL with" + addressString); //arun ends
            URLConnection conn = theURL.openConnection();
            String contentType = conn.getContentType();

            if (contentType == null) {
                JOptionPane.showMessageDialog(this,"Address has no content type. Cannot load!","Load URL",JOptionPane.ERROR_MESSAGE);
            } else if (contentType.equals("text/xml") || contentType.equals("rdf/xml") || contentType.endsWith("rdf+xml") ||
                       addressString.endsWith("rdf") || addressString.endsWith("owl")) {
                // this is some ontology...lets load it into our model
            	// by arun
            	
            	//arun ends
	//	System.out.println("About to call addOntology");
                mModel.addOntology(theURL);

                if (!getAddressBar().containsAddress(theURL.toString()))
                    getAddressBar().addAddress(theURL.toString());
            } else if (contentType.startsWith("image/")) {
                // TODO: handle other media types
            	
            	if (theURL.toString().startsWith("http"))  {
            		// this is some global image
            		//TODO: copy it in a cache
            		//TODO: save the global URI in some file. Why??
            		try  {
            			String uri = theURL.toString();
            			String localImage = uri.substring(uri.lastIndexOf('/'), uri.length());
				File imageDir = new File("images");
				if(!imageDir.exists()) imageDir.mkdir();
            			FileImageOutputStream fileStream = new FileImageOutputStream(new File("images/" + localImage));
            			BufferedImage bi = ImageIO.read(theURL);
            			ImageIO.write(bi, "jpg", fileStream);           			           			
            			} catch(Exception e)  {
            				System.err.println("Error in storing the Image to local cache");
            				e.printStackTrace();
            			}
            	}

                // lets check to see if this is media in our current store
                Resource aRes = mModel.getCurrentStore().asModel().getResource(theURL.toString());
                
                if (aRes.listProperties().hasNext() && aRes.canAs(DigitalMedia.class))
                {
                	if (!mModel.getMediaURL().equals(theURL))
                        mModel.setMediaURL(theURL);
                }
                else
                {
                   
                	mModel.setMediaURL(theURL);//arun comment ends
                
                }

                // this is when an image is loaded
                // we'll want to ask the user if they want to extract and use
                // any metadata in the image
                boolean load = false;

                if (!mModel.getAutoLoadImageMetadata())
                {
                    int option = JOptionPane.showConfirmDialog(this,"Would you like to extract the metadata for this image?","Extract Image Metadata",JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.YES_OPTION)
                        load = true;
                }
                else load = true;

                if (load)
                {
                    //commented by arun 
                	MediaModel imgModel = mModel.getMediaModel(theURL);
                	////MediaModel imgModel = mModel.getMediaModel(u);
                    imgModel.extractMetadata();
                  mMediaInfoPanel.update();
                }
                mModel.mediaSelected();
            } else if (contentType.startsWith("text/html")) {
                JOptionPane.showMessageDialog(this,"You cannot load web pages into the editor!","Load URL",JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this,"Address has unknown content type: '" + contentType + "'","Load URL",JOptionPane.ERROR_MESSAGE);
            }

        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(this,"There was an error while opening the URL.\nMessage was: "+ex.getMessage(),"Load URL",JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }

    private void initGUI()
    {
        initStoreList();

        mInstList = new InstanceList(this);

        mMediaSearchPanel = new MediaSearchPanel(this);
	    mP2PQueryPanel = new P2PQueryPanel(this, p);

        mFormPanel = new InstanceFormPanel(mModel);

        JPanel aStorePanel = new JPanel();
        aStorePanel.setLayout(new BorderLayout());

        aStorePanel.add(new JLabel("Current Store: "),BorderLayout.WEST);
        aStorePanel.add(mStoreList,BorderLayout.CENTER);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        mMediaComponentPanel = new JPanel();
        mMediaComponentPanel.setLayout(new BorderLayout());

        mOntologyTree = new OntologyTreeView();
        mOntologyTree.setManager(new org.mindswap.component.OntologyManagerComponent());
        mOntologyTree.getTree().addMouseListener(new OntologyTreeMouseAdapter(this,mOntologyTree));

        mSideInfoPanel = new JTabbedPane();
        mSideInfoPanel.addTab("Media Info", mMediaInfoPanel);
        mSideInfoPanel.addTab("Class Tree", mOntologyTree);
        mSideInfoPanel.addTab("Instance Form",mFormPanel);

        JPanel aMainPanel = new JPanel();
        aMainPanel.setLayout(new GridBagLayout());

        //getContentPane().add(mMediaListPanel,new GridBagConstraints(0,0,2,1,1,3,GridBagConstraints.WEST,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));

        JPanel aTopPane = new JPanel();
        aTopPane.setLayout(new BorderLayout());
        aTopPane.add(getAddressBar(),BorderLayout.CENTER); //new GridBagConstraints(0,0,3,1,3,1,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
        aTopPane.add(aStorePanel,BorderLayout.EAST); //new GridBagConstraints(3,0,1,1,1,1,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0,0,0,0),0,0));

        aMainPanel.add(aTopPane,new GridBagConstraints(0,0,2,1,1,0,GridBagConstraints.EAST,GridBagConstraints.HORIZONTAL,new Insets(5,5,5,5),0,0));

        JSplitPane aSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,mMediaComponentPanel,mSideInfoPanel);
        aSplitPane.setDividerLocation(500);
        aMainPanel.add(aSplitPane,new GridBagConstraints(0,2,1,1,1,2,GridBagConstraints.WEST,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));

        aMainPanel.add(mMediaSearchPanel,new GridBagConstraints(0,3,1,1,1,0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));

        JSplitPane aPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT,mMediaListPanel,aMainPanel);
        aPanel.setDividerLocation(100);
        //aMainPanel.add(aSplitPane,new GridBagConstraints(0,2,1,1,1,2,GridBagConstraints.WEST,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
        
        JSplitPane combinedPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, mP2PQueryPanel, aPanel);
      //  getContentPane().setLayout(new GridLayout(1, 1));
        getContentPane().add(combinedPanel);

        setSize(1024,700);

        updateMediaComponent();
    }

    // TODO: when other media types arise, fix this hackery
    private MediaImageComponent mMediaImageComponent;
    public MediaComponent getMediaComponent() {
        if (mMediaImageComponent == null) {
            mMediaImageComponent = new MediaImageComponent(this);
        }
        return mMediaImageComponent;
    }

    public void selectClass(Resource theClass)
    {
        mOntologyTree.select(theClass);
        mSideInfoPanel.setSelectedComponent(mOntologyTree);
    }

    public OntologyTreeView getOntologyTree ()  {
	    return this.mOntologyTree;
    }

    private void updateMediaComponent()
    {
        // TODO: devine the media type and select the appropriate media component

        switch (mMediaType)
        {
            case TYPE_MEDIA_IMAGE:
            mMediaComponentPanel.removeAll();
            mMediaComponentPanel.add(getMediaComponent().getControl(),BorderLayout.CENTER);
            getMediaComponent().displayMedia(mModel.getMediaURL());
            break;
        }

        repaint();
    }

    public void handleMarkupModelEvent(MarkupModelEvent theEvent)
    {
//System.err.println("event recieved: "+theEvent.getID());
        getPreferencesDialog().updatePreferences();
        if (theEvent.getID() == MarkupModelEvent.ONTOLOGY_LOADED)
        //System.out.println("I AM CALLED");
       // int size = mOntologyTree.getNoOfModels();
        if (theEvent.getID() == MarkupModelEvent.ONTOLOGY_LOADED)
        //System.out.println(" ONT TREE CURRENT SIZE = " + size);
        mOntologyTree.setModels(getMarkupModel().getOntologyModels());

        switch (theEvent.getID())
        {
            case MarkupModelEvent.MODEL_CHANGED:
                getRDFPanel().updateView();
                mMediaInfoPanel.update();
                break;
            case MarkupModelEvent.CLEAR_ALL:
                mMediaListPanel.update();
                mOntologyTree.updateView();
                mMediaInfoPanel.update();
                mFormPanel.updateView();
                break;
            case MarkupModelEvent.PREFS_CHANGED:
                mOntologyTree.updateView();
                updateStoreList();
                makeBookmarkMenu();
                break;
            case MarkupModelEvent.REGION_SELECTED:
                getMediaComponent().update();
                mMediaInfoPanel.update();
                break;
            case MarkupModelEvent.FORM_SELECTED:
                mMediaInfoPanel.update();
                mSideInfoPanel.setSelectedComponent(mFormPanel);
                mFormPanel.requestFocus();
                break;
//            case MarkupModelEvent.FORM_UPDATED:
//                mMediaInfoPanel.update();
//                mFormPanel.update();
//                break;
            case MarkupModelEvent.FORM_CHANGED:
                mMediaInfoPanel.update();
                mFormPanel.updateView();
                break;
            case MarkupModelEvent.FORM_SAVED:
                mInstList.update();
                mSideInfoPanel.setSelectedComponent(mMediaInfoPanel);
                break;
            case MarkupModelEvent.FORM_CANCELED:
                mSideInfoPanel.setSelectedComponent(mMediaInfoPanel);
                break;
            case MarkupModelEvent.MEDIA_SELECTED:
                updateMediaComponent();
                mMediaInfoPanel.update();
                mMediaListPanel.update();
                mSideInfoPanel.setSelectedComponent(mMediaInfoPanel);

                if (mModel.getMediaURL() != null) {
                    if (!getAddressBar().containsAddress(mModel.getMediaURL().toString()))
                        getAddressBar().addAddress(mModel.getMediaURL().toString());
                }

                break;
            case MarkupModelEvent.START_WAIT_EVENT:
                setCursorWaiting();
                repaint();
                break;
            case MarkupModelEvent.END_WAIT_EVENT:
                resetCursor();
                repaint();
                break;
            case MarkupModelEvent.STORE_CHANGED:
            case MarkupModelEvent.CONNECTION_STATUS_CHANGED:
                mInstList.update();
                mFormPanel.update();
                mMediaListPanel.update();
                mMediaInfoPanel.update();
                updateStoreList();
                break;
                
            //by arun
  //          case MarkupModelEvent.ADDED_ENTITY:
            	//System.out.println("IN CASE*****");
    //        	mOntologyTree.setModels(mModel.getOntologyModels());
      //      	mOntologyTree.expand();
       //     	mSideInfoPanel.setSelectedComponent(mOntologyTree);
        //        getMediaComponent().update();
          //      mMediaSearchPanel.update();
            //    break;
            case MarkupModelEvent.ONTOLOGY_LOADED:
            	//by arun
            	mOntologyTree.updateView(); // arun end
                mOntologyTree.setModels(mModel.getOntologyModels());
                mOntologyTree.expand();
                mSideInfoPanel.setSelectedComponent(mOntologyTree);
                getMediaComponent().update();
                mMediaSearchPanel.update();
                
//              by arun
         //   	Collection values = mModel.getOntologyModels().values();
        	//	Iterator itr = values.iterator();
        		//int i  = 0;
 //       		while(itr.hasNext())  {  			
   //     			OntModel model = (OntModel) itr.next();
     //   			ExtendedIterator allClasses = model.listClasses();        			
       // 			while(allClasses.hasNext()) { 
        				////System.out.println(i); i = i+1;
        //				OntClass r = (OntClass) allClasses.next();
        				//System.out.println("URI IS " + r.getURI());
        	//		}
        			//System.out.println("\nTHE SIZE OF O_MODEL = " + model.size());
        		//} //arun end
                
                break;
            case MarkupModelEvent.MEDIA_CHANGED:
                getMediaComponent().update();
                mMediaInfoPanel.update();
                break;

            default:
                System.err.println("Markup model event: "+theEvent+" "+theEvent.getID());
                break;
        }
    }
    
    public void setSideInfoPanel(OntologyTreeView tree)  {
    	mSideInfoPanel.setSelectedComponent(tree);
    }
    public void setSearchPanelUpdate() {
    	mMediaSearchPanel.update();
    }

    private void doExit() {
	//added by arun
	doSaveAnnotation();
	doSaveOntology();//arun ends
        Iterator iter = mPlugIns.iterator();
        while (iter.hasNext())
        {
            PlugIn aPlugIn = (PlugIn)iter.next();
            aPlugIn.close();
        }

	//write the uri-url file for images
	HashMap imgUriToUrl = MarkupImageModel.getUriMap();
        HashMap regUriToUrl = MarkupImageModel.getRegUriMap();
        Properties p = new Properties();
        p.putAll(imgUriToUrl);
        Properties r = new Properties();
        r.putAll(regUriToUrl);
        try  {
            OutputStream fileStream = new FileOutputStream(new File("ImgUriToUrl.xml"));
            p.storeToXML(fileStream, "This file maps Image URIs to Image Location on the disc/web");
            OutputStream fileStream2 = new FileOutputStream(new File("RegUriToUrl.xml"));
            r.storeToXML(fileStream2, "This file maps Region URIs to Image Location on the disc/web");
        } catch(Exception e)  {
            System.err.println("Error writing the uri-url mappings");
        }
        mModel.exit();
    }

    private void doQuit() {	
        Iterator iter = mPlugIns.iterator();
        while (iter.hasNext())
        {
            PlugIn aPlugIn = (PlugIn)iter.next();
            aPlugIn.close();
        }

        mModel.exit();
    }

    public void itemStateChanged(ItemEvent theEvent) {
        if (!mStoresUpdating && theEvent.getStateChange() == ItemEvent.SELECTED)
        {
            Store aStore = (Store)mStoreList.getSelectedItem();

            setCursorWaiting();
            mModel.setCurrentStore(aStore);

            getMarkupModel().modelChanged(MarkupModelEvent.CONNECTION_STATUS_CHANGED);

            resetCursor();
        }
    }

    private void initStoreList()
    {
        mStoreModel = new DefaultComboBoxModel();

        mStoreList = new JComboBox();
        mStoreList.setModel(mStoreModel);

        updateStoreList();

        mStoreList.addItemListener(this);
    }

    private void updateStoreList() {
        mStoresUpdating = true;

        mStoreModel.removeAllElements();

        Vector aList = mModel.getStores();
        java.util.Collections.sort(aList,new java.util.Comparator() {
            public int compare(Object one, Object two) {
                return one.toString().compareTo(two.toString());
            }
        });

        Iterator aIter = aList.iterator();
        while (aIter.hasNext()) {
            Store aStore = (Store)aIter.next();
            mStoreModel.addElement(aStore);
        }

        mStoreList.setSelectedItem(mModel.getCurrentStore());

        mStoresUpdating = false;
    }

    private AddressBar getAddressBar() {
        if (mAddressBar==null) {
            mAddressBar = new AddressBar();
            mAddressBar.addActionListener(this);
        }
        return mAddressBar;
    }

    public void setCursorWaiting() {
        prevCursor = this.getCursor().getType();
        this.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
    }

    private int prevCursor = java.awt.Cursor.DEFAULT_CURSOR;
    public void resetCursor() {
        this.setCursor(java.awt.Cursor.getPredefinedCursor(prevCursor));
    }

    private JMenuItem makeMenuItem(String title, String cmd) {
        return makeMenuItem(title,cmd,null);
    }

    private JMenuItem makeMenuItem(String title, String cmd, KeyStroke accel) {
        JMenuItem jmi = new JMenuItem(title);
        jmi.addActionListener(this);
        jmi.setActionCommand(cmd);
        if (accel != null)
            jmi.setAccelerator(accel);
        return jmi;
    }

    private void doAddBookmark()
    {
        JComboBox jcb = new JComboBox(new String[] { "Image", "Ontology" } );
        JTextField label = new JTextField();
        JTextField url = new JTextField();
        GridLayout gl = new GridLayout(3, 2);
        gl.setVgap(3);
        JPanel panel = new JPanel(gl);
        panel.add(new JLabel("URL"));
        panel.add(url);
        panel.add(new JLabel("Label"));
        panel.add(label);
        panel.add(new JLabel("Select bookmark type"));
        panel.add(jcb);
        int option = JOptionPane.showConfirmDialog(null,panel,"Add Bookmark",JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);

        if (option == JOptionPane.OK_OPTION)
        {
            int type = -1;
            if (jcb.getSelectedItem().equals("Image"))
                type = Bookmark.BOOKMARK_IMAGE;
            else type = Bookmark.BOOKMARK_ONTOLOGY;

            mModel.addBookmark(label.getText(),url.getText(),type);

            makeBookmarkMenu();
            getJMenuBar().revalidate();
        }
    }

    private void doRemoveBookmark()
    {
        Set bookmarks = mModel.listBookmarks();
        Bookmark choice = (Bookmark)JOptionPane.showInputDialog(this,"Please select bookmark to remove","Remove Bookmark",JOptionPane.INFORMATION_MESSAGE,null,bookmarks.toArray(),null);
        if (choice != null)
        {
            mModel.removeBookmark(choice.getURL());
            makeBookmarkMenu();
            getJMenuBar().revalidate();
        }
    }

    public void applyFilter(MediaFilter theFilter) {
        mModel.setMediaURL(null);
        mMediaListPanel.setFilter(theFilter);
        mMediaListPanel.update();
    }

    public void addStore(Store theStore) {
        mModel.addStore(theStore);
    }

    private static Set listContents(URL theURL)
    {
        Set aContents = new HashSet();

        if (BasicUtils.isFileURL(theURL.toString()))
        {
            // xform the url back into a file path
            String aStr = theURL.toExternalForm().substring(theURL.toExternalForm().indexOf("file:/")+6);
            aStr = aStr.replaceAll("%20"," ").replace('/','\\');

            File aStoreDir = new File(aStr);

            if (aStoreDir.isDirectory())
            {
                File[] aDirList = aStoreDir.listFiles();
                for (int i = 0; i < aDirList.length; i++)
                {
                    File aFile = aDirList[i];

                    if (aFile.isDirectory())
                        continue;

                    if (!aFile.getAbsolutePath().toLowerCase().endsWith("rdf") && !aFile.getAbsolutePath().toLowerCase().endsWith("owl"))
                        continue;

                    try {
                        aContents.add(aFile.toURL());
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        else if (theURL.toExternalForm().startsWith("jar:")) {
            try {
                String aJarURLString = theURL.toExternalForm().substring(0,theURL.toExternalForm().indexOf("!/")+2);
                String aDirString = theURL.toExternalForm().substring(theURL.toExternalForm().indexOf("!/")+2);
                JarURLConnection aConn = (JarURLConnection)theURL.openConnection();
                if (aConn.getJarEntry().isDirectory())
                {
                    JarFile aJar = aConn.getJarFile();
                    Enumeration aEnum = aJar.entries();
                    while (aEnum.hasMoreElements())
                    {
                        JarEntry aEntry = (JarEntry)aEnum.nextElement();
                        if (aEntry.getName().startsWith(aDirString) && !aEntry.isDirectory()) {
                            aContents.add(new URL(aJarURLString+aEntry.getName()));
                        }
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else {
            System.err.println("cant list contents for: "+theURL);
        }

        return aContents;
    }

    private void mindswapInit() {
       
        getMarkupModel().addBookmark("Terrorism (New)","http://counterterror.mindswap.org/2005/terrorism.owl",Bookmark.BOOKMARK_ONTOLOGY);
        try {
            StoreLoader aLoader = new StoreLoader();
            aLoader.getRegistry().register("http://www.mindswap.org/PhotoStuff#MarkupModel",getMarkupModel());
            URL aLocation = getClass().getResource("../../../stores/");
            if (aLocation == null)
                aLocation = getClass().getResource("/stores/");

            Iterator aIter = listContents(aLocation).iterator();
            while (aIter.hasNext())
            {
                URL aURL = (URL)aIter.next();
                Store aStore = aLoader.getStore(URI.create(BasicUtils.encode(aURL).toExternalForm()));

                if (aStore != null)
                    getMarkupModel().addStore(aStore);
            }
        }
        catch (Exception ex) {
           //// ex.printStackTrace();
        }
    }
    
    //by arun
    public void doLoadDefault(String type)  {
    	try  {
            File ontDir = new File(type);
            if(!ontDir.exists()) ontDir.mkdir();
            if(ontDir.isDirectory())  {                	
            	String[] files = ontDir.list();
            	//System.out.println("NO OF FILES = " + files.length);
            	for(int i = 0; i<files.length; i++)  {
            		//System.out.println(files[i]);
            		if(!(new File(type + files[i])).isDirectory()) {
            			//System.out.println("FILE NOT DIR");
            			File ont = new File(type + files[i]);
            			String url = ont.getAbsolutePath();
            			//System.out.println(url);
            			String filePath = ont.toURI().toString();
            			URL aURL = new URL(filePath);
            			
            			        //mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);
            	                //URL aURL = new URL(url);
            	                //System.out.println("THE URI PASSED IS" + ont.getAbsolutePath());
            	                loadURL(aURL);
            	               // mModel.addOntology(new URL(ont.getAbsolutePath()));
            	            //    if (!getAddressBar().containsAddress(aURL.toString()))
            	              //      getAddressBar().addAddress(aURL.toString());
            			//loadURL(new URL(url));
            			//System.out.println("ONTO LOADED");
            		}
            	}
            }
            }  catch (Exception ex) {
                JOptionPane.showMessageDialog(this,"There was an error while opening the URL.\nMessage was: "+ex.getMessage(),"Load URL",JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        }

    private class OntologyTreeMouseAdapter extends MouseAdapter implements ActionListener
    {
        private OntologyTreeView mTree;
        private JPopupMenu mPopup;
        private Razor mApp;
        private JMenuItem aItem, aClass, aProperty;

        public OntologyTreeMouseAdapter(Razor theApp, OntologyTreeView theTree)
        {
            mApp = theApp;
            mTree = theTree;

            mPopup = new JPopupMenu();

            aItem = new JMenuItem("Create Instance");
            aItem.addActionListener(this);
            
            aClass = new JMenuItem("Create Sub Class");
            aClass.addActionListener(this);
            
            aProperty = new JMenuItem("Create Property");
            aProperty.addActionListener(this);

            mPopup.add(aItem);
            mPopup.add(aClass);
            mPopup.add(aProperty);
        }

        public void actionPerformed(ActionEvent theEvent)
        {
            Resource aRes = mTree.getSelectedResource();
            if (theEvent.getSource()== aItem)  {
            	//by arun
            	
            	//System.out.println("INSTANCE TO BE CREATED");
            	Collection values = mModel.getOntologyModels().values();
            	//System.out.println("IN RAZOR size - " + mModel.getOntologyModels().size());
        		Iterator itr = values.iterator();        		
        		while(itr.hasNext())  {  			
        			OntModel model = (OntModel) itr.next();
        			ExtendedIterator allClasses = model.listClasses();
        			//System.out.println("****These are the classes***");
        			while(allClasses.hasNext()) { 
        				////System.out.println(i); i = i+1;
        				OntClass r = (OntClass) allClasses.next();
        				//System.out.println("URI IS " + r.getURI());
        			}
        		} //arun end
            	 Resource aInst = mModel.createInstance(null, aRes.toString());
                 mModel.setCurrentInstance(aInst);
                 mModel.formChanged(); 	
            }
            else if (theEvent.getSource() == aClass)  {
            	//create a new Subclass of this resource
            	editType = "Class";
            	//loadPlugIn("ontoEdit_plugin.rdf");
            	OntoEditPlugIn op = new OntoEditPlugIn(thisRazor);
            	mModel.addMarkupModelListener(op);
            	op.setLocation(200, 200);
            	op.show();
            }
            
            else if (theEvent.getSource()== aProperty) {
            	//create a new property of this resource
            	editType = "Property";
            	OntoEditPlugIn op = new OntoEditPlugIn(thisRazor);
            	//op.init();
            	mModel.addMarkupModelListener(op);
            	op.setLocation(200, 200);
            	op.show();
            	//loadPlugIn("ontoEdit_plugin.rdf");
            }
           
        }

        public void mouseClicked(MouseEvent theEvent)
        {
            if (SwingUtilities.isRightMouseButton(theEvent)) {
                mTree.getTree().setSelectionPath(mTree.getTree().getClosestPathForLocation(theEvent.getX(),theEvent.getY()));
                Point aPoint = theEvent.getPoint();
                mPopup.show(mTree.getTree(),(int)aPoint.getX(),(int)aPoint.getY());
            }
        }
    }

}
