// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.RDFNode;

import org.mindswap.utils.JenaUtils;

/*
if something gets renamed, will that rename be reflected here?
  no, it wont, but is that a problem?  the triples will be a dead end, some data changes could get lost tho
*/

/**
 * <p>Title: ChangeTracker</p>
 * <p>Description: Keeps track of a list of triples that have been deleted from a model.</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */
public class ChangeTracker {

    private Model mChangeModel;
    private Model mPendingModel;

    public ChangeTracker() {
        mChangeModel = ModelFactory.createDefaultModel();
        mPendingModel = ModelFactory.createDefaultModel();
    }

    public void commitPendingChanges() {
        mChangeModel.add(mPendingModel);
        mPendingModel = ModelFactory.createDefaultModel();
    }

    public void cancelPendingChanges() {
        mPendingModel = ModelFactory.createDefaultModel();
    }

    public void clearChanges() {
        mChangeModel = ModelFactory.createDefaultModel();
        mPendingModel = ModelFactory.createDefaultModel();
    }

    public StmtIterator getChanges() {
        return mChangeModel.listStatements();
    }

    public void propertyChanged(Resource theRes, Property theProp, Object theValue, boolean isPending)
    {
        RDFNode object = JenaUtils.objectToRDFNode(theValue);

        Statement stmt = mChangeModel.createStatement(theRes, theProp, object);

        if (isPending)
            mPendingModel.add(stmt);
        else mChangeModel.add(stmt);
    }

    public void cancelPendingChangesFor(Resource theInst)
    {
        // get all triples from the pending model that have the isntance
        // as their subject, these are all the changes for the specified
        // instance, so to cancel pending changes, these are the triples
        // we'll want to delete
        StmtIterator sIter = mPendingModel.listStatements(theInst,null,(RDFNode)null);

        mPendingModel.remove(sIter);
    }

    public void commitPendingChangesFor(Resource theInst)
    {
        // get all triples from the pending model that have the isntance
        // as their subject, these are all the changes for the specified
        // instance, so to commit these pending changes, we want to pull
        // them from the pending model and move them to the change model
        StmtIterator sIter = mPendingModel.listStatements(theInst,null,(RDFNode)null);

        // temp storage for our changes
        Model aModel = ModelFactory.createDefaultModel();
        aModel.add(sIter);

        // remove the changes for this inst from the pending model
        mPendingModel.remove(aModel);

        // and add them to the change model
        mChangeModel.add(aModel);
    }

    public boolean containsChange(Resource theSubj, Property theProp, Object theValue)
    {
        RDFNode object = JenaUtils.objectToRDFNode(theValue);

        return mChangeModel.contains(theSubj,theProp,object);
    }

    public boolean removeChange(Resource theSubj, Property theProp, Object theValue)
    {
        RDFNode object = JenaUtils.objectToRDFNode(theValue);

        if (containsChange(theSubj,theProp,object))
        {
            Statement stmt = mChangeModel.createStatement(theSubj,theProp,object);
            mChangeModel.remove(stmt);
            return true;
        }
        else return false;
    }
}
