package org.mindswap.markup;

import java.util.Comparator;

import com.hp.hpl.jena.rdf.model.Resource;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class SubClassResourceSortComparator implements Comparator
{
    private MarkupModel mMarkupModel;
    public SubClassResourceSortComparator(MarkupModel model) {
        mMarkupModel = model;
    }

    public int compare(Object obj1, Object obj2) {
        Resource res1 = null;
        if (obj1 instanceof Resource)
            res1 = (Resource)obj1;
        else res1 = mMarkupModel.getMasterOntologyModel().getResource(obj1.toString());

        Resource res2 = null;
        if (obj2 instanceof Resource)
            res2 = (Resource)obj2;
        else res2 = mMarkupModel.getMasterOntologyModel().getResource(obj2.toString());

        if (res1.equals(res2))
            return 0;
        else if (mMarkupModel.getReasoner().isSubClassOf(res2,res1))
            return -1;

        else return 1;
    }
}
