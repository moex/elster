package org.mindswap.markup;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.Insets;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import java.net.URL;

import java.util.Vector;
import java.util.HashSet;
import java.util.Iterator;

import org.mindswap.utils.Pair;

import edu.stanford.ejalbert.BrowserLauncher;
import org.mindswap.markup.media.*;

public class LaunchBar extends JPanel implements ActionListener
{
    private static final Integer GOOGLE_IMAGE_SEARCH = new Integer(1);
    private static final Integer SWOOGLE_DOC_SEARCH = new Integer(2);
    private static final Integer SWOOGLE_TERMS_SEARCH = new Integer(3);
    private static final Integer SWOOGLE_CLASS_SEARCH = new Integer(4);
    private static final Integer SWOOGLE_PROPERTY_SEARCH = new Integer(5);

    private static final String SWOOGLE_URL = "http://swoogle.umbc.edu/";

    private JTextField mSearchText;
    private JComboBox mServiceComboBox;
    private HashSet mListeners;

    public LaunchBar()
    {
        mListeners = new HashSet();
        initGUI();
    }

    public void addLaunchBarListener(LaunchBarListener theListener) {
        if (!mListeners.contains(theListener))
            mListeners.add(theListener);
    }

    public void removeLaunchBarListener(LaunchBarListener theListener) {
        mListeners.remove(theListener);
    }

    private void notifyListeners(URL theURL)
    {
        Iterator aIter = mListeners.iterator();
        while (aIter.hasNext())
        {
            LaunchBarListener aListener = (LaunchBarListener)aIter.next();
            aListener.urlLaunched(theURL);
        }
    }

    public void actionPerformed(ActionEvent ev)
    {
        Pair selected = (Pair)mServiceComboBox.getSelectedItem();

        String searchString = mSearchText.getText();

        Integer id = (Integer)selected.mSecond;
        String url = null;

        if (id.equals(GOOGLE_IMAGE_SEARCH))
        {
            searchString = searchString.replace(' ','+');
            url = "http://images.google.com/images?q="+searchString+"&hl=en";
        }
        else if (id.equals(SWOOGLE_CLASS_SEARCH))
        {
            url = SWOOGLE_URL+"modules.php?name=Swoogle_Search&file=termSearch&searchString="+searchString+"&start=1&searchClass=1";
        }
        else if (id.equals(SWOOGLE_PROPERTY_SEARCH))
        {
            url = SWOOGLE_URL+"modules.php?name=Swoogle_Search&file=termSearch&searchString="+searchString+"&start=1&searchProperty=1";
        }
        else if (id.equals(SWOOGLE_DOC_SEARCH))
        {
            url = SWOOGLE_URL+"modules.php?searchString="+searchString+"&start=1&total=-1&searchParam=&name=Swoogle_Search&file=searchDB";
        }
        else if (id.equals(SWOOGLE_TERMS_SEARCH))
        {
            url = SWOOGLE_URL+"modules.php?name=Swoogle_Search&file=termSearch&searchString="+searchString+"&start=1&searchClass=1&searchProperty=1";
        }

        try {
            URL aURL = new URL(url);
            url = aURL.toString();
            BrowserLauncher.openURL(url);
            notifyListeners(aURL);
        } // try
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initGUI()
    {
        final JButton search = new JButton("Go");
        search.addActionListener(this);

        mSearchText = new JTextField();

        KeyAdapter keyAdapter = new KeyAdapter() {
            public void keyReleased(KeyEvent ev) {
                if (ev.getKeyCode() == KeyEvent.VK_ENTER)
                    search.doClick();
            }
        };

        addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent ev) {
                mSearchText.requestFocus();
            }
        });

        mSearchText.addKeyListener(keyAdapter);
        search.addKeyListener(keyAdapter);

        makeServiceComboBox();

        setLayout(new GridBagLayout());
        add(mServiceComboBox,new GridBagConstraints(0,0,1,1,0,1,GridBagConstraints.EAST,GridBagConstraints.HORIZONTAL,new Insets(5,5,5,3),0,0));
        add(mSearchText,new GridBagConstraints(1,0,114,1,40,1,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(5,3,5,3),0,0));

        add(search,new GridBagConstraints(115,0,1,1,.5,1,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(5,3,5,5),0,0));
    }

    private void makeServiceComboBox()
    {
        Vector list = new Vector();

        list.addElement(new Pair("Google Image Search",GOOGLE_IMAGE_SEARCH));
        list.addElement(new Pair("Swoogle Terms Search",SWOOGLE_TERMS_SEARCH));
        list.addElement(new Pair("Swoogle Document Search",SWOOGLE_DOC_SEARCH));
        list.addElement(new Pair("Swoogle Class Search",SWOOGLE_CLASS_SEARCH));
        list.addElement(new Pair("Swoogle Property Search",SWOOGLE_PROPERTY_SEARCH));

        mServiceComboBox = new JComboBox(list);

        java.awt.Font f = new java.awt.Font("MS Sans Serif",java.awt.Font.PLAIN,9);

        mServiceComboBox.setFont(f);
    }
}
