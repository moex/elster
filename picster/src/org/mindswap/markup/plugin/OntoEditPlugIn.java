package org.mindswap.markup.plugin;

import fr.inrialpes.exmo.elster.picster.markup.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.*;
import javax.swing.SpringLayout;
import java.awt.*;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.util.LinkedHashMap;

import org.mindswap.markup.MarkupModelEvent;


import org.mindswap.component.OntologyTreeView;
import org.mindswap.markup.plugin.PlugIn;
import org.mindswap.utils.BasicUtils;
import org.mindswap.markup.MarkupModelListener;
import org.mindswap.markup.media.MediaMarkupModel;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.vocabulary.XSD;

public class OntoEditPlugIn extends JFrame implements ActionListener, KeyListener, DocumentListener, MarkupModelListener {
	
	private Razor mEditor;
	private OntologyTreeView mTree;
	private String uri;
	private Resource lastSelectedResource;
	private OntModel oModel;
	private MediaMarkupModel mModel;
	
	Font tahoma = new Font("Tahoma", Font.PLAIN, 11);
	String type;
	JButton addBtn, cancelBtn;
	JTextField idFld, uriFld, labelFld;
	JTextArea commentArea;
	JComboBox headerBox, propType, parentBox, domainBox, rangeBox;
	String NEWLINE = System.getProperty("line.separator");
	LinkedHashMap mOntologies;
	OntModel model, prevModel, prev;
	
	public OntModel getModel()  {
		return model;
	}
	
	public OntoEditPlugIn(Razor theEditor)  {
		//System.out.println("reached constructor");
		mEditor = theEditor;
		type = mEditor.getEditType();
		mTree = mEditor.getOntologyTree();
		this.lastSelectedResource = mTree.getSelectedResource();
		prevModel = MarkupModel.createOntModel();
		prev = (OntModel) lastSelectedResource.getModel();
		StmtIterator sm = prev.listStatements();
		prevModel.add(sm);
		String resUri;
		resUri = ((OntClass)lastSelectedResource).getURI();
		//System.out.println("Selected resource is " + resUri);
		int i = resUri.indexOf("#");
		int j = resUri.indexOf("/");
		int k = resUri.lastIndexOf("/");		
		if (i > -1) uri = resUri.substring(0, i);
		else if (j > -1) uri = resUri.substring(0, k);		
		//oModel = (OntModel) this.lastSelectedResource.getModel();
		mModel = mEditor.getMarkupModel();
		oModel = MarkupModel.createOntModel();
		mOntologies = mModel.getOntologyModels();
		Collection values = mOntologies.values();
		Iterator itr = values.iterator();
		while(itr.hasNext())  {
			model = (OntModel) itr.next();
			if(model.containsResource(this.lastSelectedResource))  {
				//System.out.println("RESOURCE IS PRESENT");
				StmtIterator stmts = model.listStatements();
				oModel.add(stmts);
				//oModel = model;
			}
		}
		//String base = mModel.getBaseURL();
		//System.out.println("THIS IS BASE URL" + base);
		//String ar = mModel.getRDF();
		//System.out.println("THIS IS WHAT IS" + ar );
		//oModel = mModel.getMasterOntologyModel();
		//long size = oModel.size();
		//System.out.println("THIS IS ONT MODEL SIZE --" + size);
	//	prevModel = oModel;		
		//System.out.println("THIS IS TYPE   " + type);
		init();
	}
	public void init()  {
		try {
			//System.out.println("reached init");
			setupUI();
		}
		catch(Exception e)  {
			e.printStackTrace();
		}
	}
	
	private void setupUI() {
		JPanel mainPanel = new JPanel(new SpringLayout());		
		//JLabel headerLbl = new JLabel();
		//headerBox = new JComboBox();
		//headerBox.setFont(tahoma);
	//	headerLbl.setFont(tahoma);
		
		//if (type.equals("Class")) {
		//	headerLbl.setText("Adding OWL Class");
			//headerBox.addItem("Adding OWL Class");
			//headerBox.addItem("Adding OWL Property");
			//headerBox.addItem("Adding OWL Individual");
		//}
		//else if (type.equals("Property")) {
		//	headerLbl.setText("Adding OWL Property");
			//headerBox.addItem("Adding OWL Property");
			//headerBox.addItem("Adding OWL Class");
			//headerBox.addItem("Adding OWL Individual");
		//}
		//headerBox.addActionListener(this);
		
		//mainPanel.add(new JLabel(""));
		//mainPanel.add(headerBox);
		
		JLabel propLbl = new JLabel("Property Type:");
		propLbl.setFont(tahoma);
		propType = new JComboBox();
		propType.setFont(tahoma);
		propType.addItem("OWL Datatype Property");
		propType.addItem("OWL Object Property");
		propType.addActionListener(this);
		
		propLbl.setLabelFor(propType);
		if (type.equals("Property")) {
			mainPanel.add(propLbl);
			mainPanel.add(propType);
			propType.setSelectedIndex(0); // DEFAULT PROPERTY TYPE
		}	
		
		
		parentBox = new JComboBox();
		parentBox.setFont(tahoma);
		JLabel parentLbl = new JLabel();
		parentLbl.setFont(tahoma);
		if (type.equals("Class")) parentLbl.setText("subClass-of");
		else if (type.equals("Property")) parentLbl.setText("subProperty-of");
		parentLbl.setLabelFor(parentBox);
		mainPanel.add(parentLbl);
		mainPanel.add(parentBox);
		fillParentBox();

		if (type.equals("Property"))  {
			JLabel domain = new JLabel();
			domain.setText("Domain");
			domainBox = new JComboBox();			
			domainBox.setFont(tahoma);
			domain.setLabelFor(domainBox);
			mainPanel.add(domain);
			mainPanel.add(domainBox);
			fillDomainBox();
			
			JLabel range = new JLabel();
			range.setText("Range");
			rangeBox = new JComboBox();
			rangeBox.setFont(tahoma);
			range.setLabelFor(rangeBox);
			mainPanel.add(range);			
			mainPanel.add(rangeBox);
			//String prop_type = (String) propType.getSelectedItem();
			fillRangeBox();
		}
		
		JLabel idLbl = new JLabel("ID:");
		idLbl.setFont(tahoma);
		idFld = new JTextField();
		idFld.setFont(tahoma);
		idFld.getDocument().addDocumentListener(this);
		idFld.addKeyListener(this);
		idLbl.setLabelFor(idFld);
		mainPanel.add(idLbl);
		mainPanel.add(idFld);
		
		JLabel uriLbl = new JLabel("Logical URI:");
		uriLbl.setFont(tahoma);
		uriFld = new JTextField();
		uriFld.setFont(tahoma);
		uriFld.addKeyListener(this);
		uriFld.setText(uri +"/");
		uriLbl.setLabelFor(uriFld);
		mainPanel.add(uriLbl);
		mainPanel.add(uriFld);
		
		JLabel lbl = new JLabel("Label:");
		lbl.setFont(tahoma);
		labelFld = new JTextField("");
		labelFld.setFont(tahoma);
		labelFld.addKeyListener(this);
		lbl.setLabelFor(labelFld);
		mainPanel.add(lbl);
		mainPanel.add(labelFld);
		
		JLabel comm = new JLabel("Comment:");
		comm.setFont(tahoma);
		commentArea = new JTextArea();
		commentArea.setFont(tahoma);
		commentArea.setText(NEWLINE+NEWLINE+NEWLINE);
		commentArea.setCaretPosition(0);
		commentArea.addKeyListener(this);
		JScrollPane commentPane = new JScrollPane(commentArea);
		mainPanel.add(comm);
		comm.setLabelFor(commentPane);
		mainPanel.add(commentPane);		
		
		addBtn = new JButton("Apply");
		addBtn.setFont(tahoma);
		addBtn.setEnabled(false);
		addBtn.addActionListener(this);
		cancelBtn = new JButton("Close");
		cancelBtn.setFont(tahoma);
		cancelBtn.addActionListener(this);
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new GridLayout(1,2));
		btnPanel.add(addBtn);
		btnPanel.add(cancelBtn);
		mainPanel.add(new JLabel(""));
		mainPanel.add(btnPanel);
		
		int rows = 6;
		if (type.equals("Class")) rows = 6;
		if (type.equals("Property")) rows = 9;
		makeCompactGrid(mainPanel,
                rows, 2, //rows, cols
                9, 9,        //initX, initY
                6, 6);       //xPad, yPad
		Container content = getContentPane();
		content.setLayout(new BorderLayout());
		content.add(mainPanel, "Center");
		if (type.equals("Class")) {
			setSize(350, 260);
		}
		else if (type.equals("Property")){
			setSize(350, 350);
		}
		setResizable(true);
		setTitle("New Entity");
		//headerBox.setVisible(true);
		//System.out.println("INTI UI OVER");
	}
	
	private JPanel createRowPanel(String lblStr, Component comp) {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JLabel lbl = new JLabel(lblStr);
		lbl.setFont(tahoma);
		panel.add(lbl, "West");
		panel.add(comp, "Center");
		return panel;
	}
	
	private void redrawUI() {
		hide();
		getContentPane().removeAll();
		getContentPane().repaint();			
		try {
			setupUI();
			show();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private boolean isURI(String str) {		
		try {
			URI url = new URI(str);
			return true;
		}
		catch (Exception ex) {}
		return false;
	}
	
	public void changedUpdate(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
private void updateURI() {
		
		if ((!type.equals(("Ontology"))) && (idFld.getText()!=null)) {
			String uri = "";
			if (uriFld.getText()!=null) uri = uriFld.getText();
			if (uri.indexOf("#")>=0) uri = uri.substring(0, uri.indexOf("#"));
			else if (uri.indexOf("/")>=0) uri = uri.substring(0, uri.lastIndexOf("/"));
			else uri = "";
			uri+="/"+idFld.getText();
			uriFld.setText(uri);
		}
	}
	
	public void insertUpdate(DocumentEvent arg0) {
		updateURI();
	}

	public void removeUpdate(DocumentEvent arg0) {
		updateURI();
	}

	public void keyPressed(KeyEvent e) {
		
		if ((!type.equals("Ontology")) && 
				((e.getSource()==idFld)) || (e.getSource()==uriFld)) {
			addBtn.setEnabled(true);			
		}
		if ((type.equals("Ontology")) && (e.getSource()==uriFld)) {
			addBtn.setEnabled(true);			
		}
		
		if (e.getKeyCode()==10) {
			// enter key pressed
			if (addBtn.isEnabled()) {
				addBtn.doClick();
				if (type.equals("Ontology")) uriFld.requestFocus();
				else idFld.requestFocus();				
			}			
		}
	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void makeCompactGrid(Container parent, int rows, int cols, int initialX, int initialY, int xPad, int yPad) {
		SpringLayout layout;
		try {
			layout = (SpringLayout)parent.getLayout();
		} catch (ClassCastException exc) {
			System.err.println("The first argument to makeCompactGrid must use SpringLayout.");
			return;
		}

		//Align all cells in each column and make them the same width.
		Spring x = Spring.constant(initialX);
		for (int c = 0; c < cols; c++) {
			Spring width = Spring.constant(0);
			for (int r = 0; r < rows; r++) {
				width = Spring.max(width,
						getConstraintsForCell(r, c, parent, cols).
						getWidth());
			}
			for (int r = 0; r < rows; r++) {
				SpringLayout.Constraints constraints =
					getConstraintsForCell(r, c, parent, cols);
				constraints.setX(x);
				constraints.setWidth(width);
			}
			x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
		}
		
		//Align all cells in each row and make them the same height.
		Spring y = Spring.constant(initialY);
		for (int r = 0; r < rows; r++) {
			Spring height = Spring.constant(0);
			for (int c = 0; c < cols; c++) {
				height = Spring.max(height,
						getConstraintsForCell(r, c, parent, cols).
						getHeight());
			}
			for (int c = 0; c < cols; c++) {
				SpringLayout.Constraints constraints =
					getConstraintsForCell(r, c, parent, cols);
				constraints.setY(y);
				constraints.setHeight(height);
			}
			y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
		}
		
		//Set the parent's size.
		SpringLayout.Constraints pCons = layout.getConstraints(parent);
		pCons.setConstraint(SpringLayout.SOUTH, y);
		pCons.setConstraint(SpringLayout.EAST, x);
		
		//System.out.println("MAKE COMPACT GRID OVER");
	}
	
	private static SpringLayout.Constraints getConstraintsForCell(int row, int col, Container parent, int cols) {
		SpringLayout layout = (SpringLayout) parent.getLayout();
		Component c = parent.getComponent(row * cols + col);
		return layout.getConstraints(c);
	}
	
	public void fillDomainBox()  {
		try  {
			domainBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
			domainBox.removeActionListener(this);
			domainBox.removeAllItems();			
			ExtendedIterator allClasses =  oModel.listClasses();
			while(allClasses.hasNext())  {
				OntClass r = (OntClass) allClasses.next();					
				domainBox.addItem(r);
			}
		}  catch (Exception e)  {
			e.printStackTrace();
		}

		finally  {
			 	try {
				if (this.lastSelectedResource!=null) {
					domainBox.setSelectedItem(this.lastSelectedResource);			
				}			
			} catch(Exception e1)  {
				e1.printStackTrace();
			}
		}
	}

	public void fillRangeBox()  {
		String prop_type = (String) propType.getSelectedItem();
		if (prop_type.equals("OWL Datatype Property"))  {
			//range is xml-schema definitions
			rangeBox.addItem(XSD.anyURI);
			rangeBox.addItem(XSD.base64Binary);
			rangeBox.addItem(XSD.class);
			rangeBox.addItem(XSD.date);
			rangeBox.addItem(XSD.dateTime);
			rangeBox.addItem(XSD.decimal);
			rangeBox.addItem(XSD.duration);
			rangeBox.addItem(XSD.ENTITIES);
			rangeBox.addItem(XSD.ENTITY);
			rangeBox.addItem(XSD.gDay);
			rangeBox.addItem(XSD.gMonth);
			rangeBox.addItem(XSD.gYear);
			rangeBox.addItem(XSD.gYearMonth);
			rangeBox.addItem(XSD.ID);
			rangeBox.addItem(XSD.integer);
			rangeBox.addItem(XSD.Name);
			rangeBox.addItem(XSD.time);
			rangeBox.addItem(XSD.xstring);
			rangeBox.addItem(XSD.xboolean);
		}
		else  {
			//object property
			//range is an existing class TODO modify this to make it multiple classes
			try  {
				rangeBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
				rangeBox.removeActionListener(this);
				rangeBox.removeAllItems();			
				ExtendedIterator allClasses =  oModel.listClasses();
				while(allClasses.hasNext())  {
					OntClass r = (OntClass) allClasses.next();					
					rangeBox.addItem(r);
				}
			}  catch (Exception e)  {
				e.printStackTrace();
			}

			finally  {
				 	try {
				 		if (prop_type.equals("OWL Datatype Property"))
				 			rangeBox.setSelectedItem(XSD.xstring);
					//if (this.lastSelectedResource!=null) {
						//rangeBox.setSelectedItem(this.lastSelectedResource);			
				//	}			
				} catch(Exception e1)  {
					e1.printStackTrace();
				}
			}
			
		}
	}
	public void fillParentBox()  {
		try {
			parentBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
			parentBox.removeActionListener(this);
			//String base = mModel.getBaseURL();
			//System.out.println("THIS IS BASE URL--" + base);
			parentBox.removeAllItems();
			if (type.equals("Class")) {
				//ResIterator rItr = (ResIterator) oModel.listHierarchyRootClasses();
				//ResIterator rItr = (ResIterator) oModel.listClasses();
				//Resource top = rItr.nextResource();
				//OntClass topClass = (OntClass) top.as(OntClass.class);
				//OntClass thing = topClass.getSuperClass();
				//parentBox.addItem(thing);
			//	System.out.println("GETTING CLASSES");
				ExtendedIterator allClasses =  oModel.listClasses();
			//	System.out.println("GOT CLASSES");
				//int i = 0;
				while(allClasses.hasNext())  {
					//i = i+1;
					OntClass r = (OntClass) allClasses.next();					
					parentBox.addItem(r);
				}
				//System.out.println("NO OF CLASSESS = " + i);
			}
			else {
				parentBox.addItem("None");// TODO add parent property here
				ExtendedIterator allDataProp = oModel.listDatatypeProperties();
				while(allDataProp.hasNext())  {
					DatatypeProperty dpr = (DatatypeProperty)allDataProp.next();
					parentBox.addItem(dpr);
				}
				ExtendedIterator allObjProp = oModel.listObjectProperties();
				while(allObjProp.hasNext())  {
					ObjectProperty opr = (ObjectProperty)allObjProp.next();
					parentBox.addItem(opr);
				}
			}	
		} catch (Exception e)  {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (this.lastSelectedResource!=null) {
					//ExtendedIterator rItr = oModel.listClasses();
					//OntClass top = rItr.next();
					//OntClass topClass = (OntClass) top.as(OntClass.class);
					//OntClass thing = topClass.getSuperClass();
					//if (!this.lastSelectedResource.equals(thing)) 
						parentBox.setSelectedItem(this.lastSelectedResource);
					//else 
						//parentBox.setSelectedIndex(0);
				}			
			} catch(Exception e1)  {
				e1.printStackTrace();
			}
			//System.out.println("Finally Over");
		}
	 int count = parentBox.getItemCount();
	 //System.out.println("COUNT =    " + count);
	}
	
	public void close()  {
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==parentBox && e.getActionCommand().equals("comboBoxChanged")) {
			
			if (parentBox.getSelectedItem() instanceof DatatypeProperty) {
				propType.setSelectedIndex(0);				
			}
			else if (parentBox.getSelectedItem() instanceof ObjectProperty) {
				//******************************************
				//Changed for Econnections
				//*********************************************
				propType.setSelectedIndex(1);
				//************************************************				
			}			
			if (parentBox!=null) parentBox.setEnabled(true);
		}
		
		if (e.getSource()==propType && e.getActionCommand().equals("comboBoxChanged")) {
			if(propType.getSelectedIndex() == 1)  {
				//object property
				try  {
					rangeBox.removeAllItems();
					rangeBox.setFont(new Font("Tahoma", Font.PLAIN, 11));
					rangeBox.removeActionListener(this);
					rangeBox.removeAllItems();			
					ExtendedIterator allClasses =  oModel.listClasses();
					while(allClasses.hasNext())  {
						OntClass r = (OntClass) allClasses.next();					
						rangeBox.addItem(r);
					}
				}  catch (Exception ex)  {
					ex.printStackTrace();
				}
			}
			
			/*else  {
				rangeBox.removeAllItems();
				rangeBox.addItem(XSD.anyURI);
				rangeBox.addItem(XSD.base64Binary);
				rangeBox.addItem(XSD.class);
				rangeBox.addItem(XSD.date);
				rangeBox.addItem(XSD.dateTime);
				rangeBox.addItem(XSD.decimal);
				rangeBox.addItem(XSD.duration);
				rangeBox.addItem(XSD.ENTITIES);
				rangeBox.addItem(XSD.ENTITY);
				rangeBox.addItem(XSD.gDay);
				rangeBox.addItem(XSD.gMonth);
				rangeBox.addItem(XSD.gYear);
				rangeBox.addItem(XSD.gYearMonth);
				rangeBox.addItem(XSD.ID);
				rangeBox.addItem(XSD.integer);
				rangeBox.addItem(XSD.Name);
				rangeBox.addItem(XSD.time);
				rangeBox.addItem(XSD.xstring);
				rangeBox.addItem(XSD.xboolean);
			}*/
				// object property
			//System.out.println("Prop Type Changed");
			//String prop_type = (String) propType.getSelectedItem();
				//this.fillRangeBox(prop_type);
			//}
		}
		
		if (e.getSource()==addBtn) {
			try {				
				String uriStr = uriFld.getText();				
				if (uriStr.trim().equals("") || !(isURI(uriStr))) {
					JOptionPane.showMessageDialog(null, "A Valid Logical URI needs to be specified for the new OWL Ontology", "Creation Error!", JOptionPane.ERROR_MESSAGE);
					return;
				}				
				if (type.equals("Class")) {
					//System.out.println("CREATING NEW CLASS");
					OntClass newClass = oModel.createClass(uriStr);
					System.out.println("CLASS CREATED");
					addAnnotations(newClass);
					//System.out.println("ANNOTATIONS ADDED TO THE CLASS");
					//System.out.println("INDEX = " + parentBox.getSelectedIndex());
					if (parentBox.getSelectedIndex()!=0) {
						OntClass parentClass = (OntClass) parentBox.getSelectedItem();
						String puri = parentClass.getURI();
						//System.out.println("PARENT URI =  " + puri);
						parentClass.addSubClass(newClass);
						System.out.println("SUBCLASS added to the parent");
						//System.out.println("SUBCLASS URI = " + newClass.getURI());
					}
				}
				
				if (type.equals("Property"))  {
					if (propType.getSelectedIndex()==0) {
						// new datatype property
						DatatypeProperty newProp = oModel.createDatatypeProperty(uriStr);
						newProp.addDomain((OntClass)domainBox.getSelectedItem());
						newProp.addRange((Resource)rangeBox.getSelectedItem());
						addAnnotations(newProp);
						if (parentBox.getSelectedIndex()!=0) { 
							DatatypeProperty parentProp = (DatatypeProperty) parentBox.getSelectedItem();
							parentProp.addSubProperty(newProp);
						}
					}
					else  {
						// new Object property
						ObjectProperty newObjProp = oModel.createObjectProperty(uriStr);
						newObjProp.addDomain((OntClass)domainBox.getSelectedItem());
						newObjProp.addRange((OntClass)rangeBox.getSelectedItem());
						addAnnotations(newObjProp);
						if (parentBox.getSelectedIndex()!=0) {
							ObjectProperty parentObjProp = (ObjectProperty) parentBox.getSelectedItem();
							parentObjProp.addSubProperty(newObjProp);
						}
					}
				}
			//	model = oModel;
					setRazorModel(oModel);
					//System.out.println("GOING TO USE SWING WORKER");
					//mModel.getReasoner().addModel(oModel);
					//mModel.useSwingWorker(new MarkupModelEvent(mModel, MarkupModelEvent.ADDED_ENTITY), mTree, oModel, prevModel, lastSelectedResource );
					//System.out.println("SWING WORKER USE OVER");
					addBtn.setEnabled(false);
					idFld.setText("");
					labelFld.setText("");
					commentArea.setText("");
				
			} catch(Exception ex)  {
				ex.printStackTrace();
			}
		}
		
		if (e.getSource()==cancelBtn) {
			mModel.removeMarkupModelListener(this);
			dispose();
		}
		//this.mModel.modelChanged(MarkupModelEvent.ONTOLOGY_LOADED);
	}
	
	public void setRazorModel(OntModel oModel)  {
		LinkedHashMap revModels = mModel.getRevOntoModels();
		URL key = (URL)revModels.get(prev);
		LinkedHashMap models = mModel.getOntologyModels();
		models.remove(prev);
		oModel.setStrictMode(true);
		models.put(key, oModel);
		revModels.put(oModel, key);
		OntModel v = mModel.getMasterOntologyModel();
		 v = null;
		mModel.getReasoner().addModel(oModel);
		OntologyTreeView tree = mEditor.getOntologyTree();
		tree.setModels(mModel.getOntologyModels()); 
		tree.updateView(mModel.getUserId()+ "_");
		tree.select(lastSelectedResource);		
    	mEditor.setSideInfoPanel(tree);       
        mEditor.setSearchPanelUpdate();
		mEditor.getMarkupModel().modelChanged(MarkupModelEvent.ADDED_ENTITY);		
	}
		public void addAnnotations(OntResource r)  {
			String lbl = "", comment = "";
			if (labelFld.getText()!=null) lbl = labelFld.getText().trim();
			if (commentArea.getText()!=null) comment = commentArea.getText().trim();
			r.addComment(comment, "EN");
			r.addLabel(lbl, "EN");			
		}
		
		public void handleMarkupModelEvent(MarkupModelEvent event)  {
			if (event.getID() == MarkupModelEvent.ADDED_ENTITY)  {
				this.redrawUI();
			}
		}
}
