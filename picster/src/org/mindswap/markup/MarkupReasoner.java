// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup;

import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Iterator;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;

import com.hp.hpl.jena.ontology.OntModel;

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.BasicUtils;
import fr.inrialpes.exmo.elster.picster.markup.*;

public abstract class MarkupReasoner implements Reasoner {
    private MarkupModel mMarkupModel;
    private OntModel mModel;


    // this is a cache of the type info that we have already
    // calculated.  since the algo is slow, this helps performance
    // since we dont have to calc something we've already calc'ed.
    // TODO: it should be cleared any time the model is changed
    // however since that could result in incorrect and out of date
    // cached data.
    private HashMap mInstTypeInfo = new HashMap();
    private HashMap mResourceTypeInfo = new HashMap();

    public MarkupReasoner(MarkupModel model) {
        mMarkupModel = model;
        mModel = MarkupModel.createOntModel();
    }

    public Set getInstancesWithType(Resource theType)
    {
        Set filter = new HashSet();

        Set types = JenaUtils.getAllSubClassesOf(theType, true);

        types.add(theType);

        // get the instances from the stores in the markup model
        OntModel list = mMarkupModel.getLoadedInstances();

        // add the information from the reasoner KB
        list.add(getModel());

        Iterator iter = JenaUtils.listIndividuals(list);

        while (iter.hasNext()) {
            Resource inst = (Resource)iter.next();

            if (inst.isAnon() && MarkupModel.FILTER_ANON)
                continue;

            if (BasicUtils.containsAny(getTypes(inst), types) && !filter.contains(inst))
                filter.add(inst);
        }

        Set keys = mMarkupModel.getInstances().keySet();
        Iterator kIter = keys.iterator();
        while (kIter.hasNext()) {
            Object key = kIter.next();
            Resource inst = (Resource)mMarkupModel.getInstances().get(key);

            if (BasicUtils.containsAny(mMarkupModel.getReasoner().getTypes(inst), types) && !filter.contains(inst))
                filter.add(inst);
        }

        return filter;
    }

    public void addModel(Model m)
    {
        mModel.add(m);
        clearCachedData();
       // System.out.println("Model added");
    }

    public void removeModel(Model m)
    {
        mModel.remove(m);
        clearCachedData();
        //System.out.println("Model removed");
    }

    private void clearCachedData()
    {
        mInstTypeInfo.clear();
        mResourceTypeInfo.clear();
    }

    public OntModel getModel() {
        return mModel;
    }

    protected MarkupModel getMarkupModel() {
        return mMarkupModel;
    }
}
