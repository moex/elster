package org.mindswap.markup;

import org.mindswap.component.Tool;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Cursor;

import java.awt.event.MouseEvent;

import java.awt.geom.Point2D;
import java.awt.geom.AffineTransform;

import javax.swing.SwingUtilities;

import java.util.Iterator;
import java.util.Vector;

import org.mindswap.markup.media.SelectionRegion;
import org.mindswap.markup.media.ImageComponent;
import org.mindswap.markup.media.MarkupImageModel;
import org.mindswap.markup.media.MediaRegion;

public class Select implements Tool {

    private ImageComponent imageComponent;
    private Vector pressRegions;

    private int toolOp;
    private SelectionRegion opRegion;
    private Point anchor;

    private static final int NONE = -1;
    private static final int RESIZE_NE = 0;
    private static final int RESIZE_NW = 1;
    private static final int RESIZE_SE = 2;
    private static final int RESIZE_SW = 3;
    private static final int MOVE = 4;

    public Select(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
        toolOp = NONE;
        opRegion = null;
        anchor = null;
    }

    /**
     * Invoked when the mouse has been clicked on a component.
     *
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     *
     * @param e MouseEvent
     */
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e))
            return;

        Point p  = getPoint(e);
        pressRegions = new Vector();
        if (imageComponent.getImageModel() != null)
        {
            Iterator iter = imageComponent.getImageModel().getRegions();
            while (iter.hasNext()) {
                MediaRegion region = (MediaRegion)iter.next();
                if (region.contains(p)) {
                    // record the regions in which the press took place
                    // if the release is in the same region, its a click!
                    pressRegions.add(region);
                }
            }
        }
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @param e MouseEvent
     */
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e))
            return;

        Point p  = getPoint(e);
        MarkupImageModel imageModel = (MarkupImageModel)imageComponent.getImageModel();
        if (imageModel != null) {

            // we have two cases, a regular selection using the selection tool
            // or the modifcation of a region using an operation, resize or move

            // there's an op in progress
            if (toolOp != NONE) {

                if (toolOp == MOVE)
                    moveRegion(p);
                else resizeRegion(p);

                opRegion.setHover(false);
                imageModel.selectRegion(opRegion);
                imageModel.getMarkupModel().regionSelected();

                toolOp = NONE;
                opRegion = null;
                Cursor cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                imageComponent.setCursor(cursor);
            } else {
                Iterator iter = pressRegions.iterator();
                while (iter.hasNext()) {
                    MediaRegion region = (MediaRegion)iter.next();
                    if (region.contains(p)) {
                        // press and release in the same region, therefore a click!
                        if (e.isShiftDown()) {
                            imageModel.deselectRegion(region);
                        } else {
                            imageModel.selectRegion(region);
                            imageModel.getMarkupModel().regionSelected();
                        }
                    }
                }
            }
            pressRegions = null;
            this.imageComponent.repaint();
        }
    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e MouseEvent
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e MouseEvent
     */
    public void mouseExited(MouseEvent e) {
        updateHover(null);
    }

    /**
     * Invoked when a mouse button is pressed on a component and then dragged.
     * Mouse drag events will continue to be delivered to the component where
     * the first originated until the mouse button is released (regardless of
     * whether the mouse position is within the bounds of the component).
     *
     * @param e MouseEvent
     */
    public void mouseDragged(MouseEvent e) {
        Point p  = getPoint(e);

        if (toolOp != NONE) {
            if (toolOp == MOVE) {
                moveRegion(p);
            } else {
                resizeRegion(p);
            }
        } else updateHover(p);
    }

    /**
     * Invoked when the mouse button has been moved on a component (with no
     * buttons no down).
     *
     * @param e MouseEvent
     */
    public void mouseMoved(MouseEvent e) {
        Point p  = getPoint(e);

        Iterator iter = imageComponent.getImageModel().getRegions();
        Cursor cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);

        opRegion = null;
        toolOp = NONE;
        boolean needsDefault = true;
        while (iter.hasNext()) {
            SelectionRegion sr = (SelectionRegion) iter.next();

            // TODO: handle resizes and moves for polygon regions
            if (sr.getType() == SelectionRegion.SHAPE_POLY)
                continue;

            Rectangle rect = sr.getRect();
            Rectangle shape = (Rectangle)sr.getRegionShape();

            boolean nearTop = false;
            boolean nearLeft = false;
            boolean nearRight = false;
            boolean nearBottom = false;

            if (Math.abs(p.y-rect.y) <= 3)
                nearTop = true;
            if (Math.abs(p.x-rect.x) <= 3)
                nearLeft = true;
            if (Math.abs(p.y-(rect.y+rect.height)) <= 3)
                nearBottom = true;
            if (Math.abs(p.x-(rect.x+rect.width)) <= 3)
                nearRight = true;

            if (nearRight && nearTop) {
                // top right
                opRegion = sr;
                toolOp = RESIZE_NE;
                cursor = Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR);
                anchor = new Point(shape.x,shape.y+shape.height);
                imageComponent.setCursor(cursor);
                needsDefault = false;
            }
            else if (nearRight && nearBottom) {
                // bottom right
                opRegion = sr;
                toolOp = RESIZE_SE;
                cursor = Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR);
                anchor = new Point(shape.x,shape.y);
                imageComponent.setCursor(cursor);
                needsDefault = false;
            }
            else if (nearLeft && nearTop) {
                // top left
                opRegion = sr;
                toolOp = RESIZE_NW;
                cursor = Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR);
                anchor = new Point(shape.x+shape.width,shape.y+shape.height);
                imageComponent.setCursor(cursor);
                needsDefault = false;
            }
            else if (nearLeft && nearBottom) {
                // bottom left
                opRegion = sr;
                toolOp = RESIZE_SW;
                cursor = Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR);
                anchor = new Point(shape.x+shape.width,shape.y);
                imageComponent.setCursor(cursor);
                needsDefault = false;
            } else if (sr.contains(p)) {
                opRegion = sr;
                toolOp = MOVE;
                cursor = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
                anchor = p;
                imageComponent.setCursor(cursor);
                needsDefault = false;
            }
        }

        updateHover(p);

        if (needsDefault)
            imageComponent.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    private Point getPoint(MouseEvent e) {
        AffineTransform c2i = this.imageComponent.getComponentToImage();
        Point p = new Point(e.getX(), e.getY());
        Point2D r = c2i.transform(p, null);
        return new Point((int)r.getX(), (int)r.getY());
    }

    private void updateHover(Point p) {
        if (imageComponent.getImageModel() != null) {
            Iterator regions = imageComponent.getImageModel().getRegions();
            while (regions.hasNext()) {
                MediaRegion region = (MediaRegion) (regions.next());
                if (p!=null && region.contains(p)) {
                    region.setHover(true);
                } else {
                    region.setHover(false);
                }
            }
            this.imageComponent.repaint();
        }
    }

    private void moveRegion(Point p) {
        Rectangle oldRect = (Rectangle)opRegion.getRegionShape();
        int xdelta = p.x-anchor.x;
        int ydelta = p.y-anchor.y;
        oldRect.setLocation(oldRect.x+xdelta,oldRect.y+ydelta);
        anchor = p;
        imageComponent.repaint();
    }

    private void resizeRegion(Point p) {
        Rectangle oldRect = (Rectangle)opRegion.getRegionShape();

        Point location = null;
        if (p.x <= anchor.x && p.y <= anchor.y) {
            // nw quad
            location = new Point(p.x,p.y);
        } else if (p.x <= anchor.x && p.y >= anchor.y) {
            // sw quad
            location = new Point(p.x,anchor.y);
        } else if (p.x >= anchor.x && p.y <= anchor.y) {
            // ne quad
            location = new Point(anchor.x,p.y);
        } else if (p.x >= anchor.x && p.y >= anchor.y){
            // se quad
            location = anchor;
        }

        if (location != null) {
            oldRect.setLocation(location);
            oldRect.setSize(Math.abs(anchor.x - p.x),Math.abs(anchor.y - p.y));
            imageComponent.repaint();
        }
    }
}
