package org.mindswap.markup;


/**
 */
public interface MarkupModelListener {

    public void handleMarkupModelEvent(MarkupModelEvent event);

}
