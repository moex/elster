package org.mindswap.markup;

import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Vector;

import java.io.File;

import org.mindswap.store.Store;

import org.mindswap.utils.BasicUtils;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class SaveDialog extends JDialog implements ActionListener {
    private MarkupModel mModel;
    private Razor mApp;

    private JRadioButton mConsole;
    private JRadioButton mFile;
    private JRadioButton mSave;
    private JComboBox mLocation;
    private JButton mOkBtn;

    public SaveDialog(Razor theApp) {
        super( (JFrame)theApp, "Submit Markup...", true);
        mApp = theApp;
        mModel = mApp.getMarkupModel();
        initGUI();
        pack();
    } // cons

    public void show() {
        mOkBtn.requestFocus();
        BasicUtils.centerFrame(mApp,this);
        super.show();
    }

    public void setVisible(boolean theVis) {
        if (theVis) {
            mOkBtn.requestFocus();
            BasicUtils.centerFrame(mApp,this);
        }
        super.setVisible(theVis);
    }

    private void initGUI() {
        JPanel contentPane = new JPanel();
        setContentPane(contentPane);

        contentPane.setLayout(new GridBagLayout());

        ButtonGroup theGroup = new ButtonGroup();

        mConsole = new JRadioButton("to console", true);
        theGroup.add(mConsole);
        mFile = new JRadioButton("to file...", false);
        theGroup.add(mFile);

        mSave = new JRadioButton("to remote site:", false);
        theGroup.add(mSave);
        //mLocation = new JComboBox(mModel.getSubmitURLs());
        Vector aList = mModel.getStores();
        aList.remove(mModel.getLocalStore());
        mLocation = new JComboBox(aList);
        mLocation.setSelectedItem(mModel.getCurrentStore());

        contentPane.add(mConsole, new GridBagConstraints(0, 0, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        contentPane.add(mFile, new GridBagConstraints(0, 1, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        contentPane.add(mSave, new GridBagConstraints(0, 2, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        contentPane.add(mLocation, new GridBagConstraints(2, 2, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        mOkBtn = new JButton("OK");
        mOkBtn.addActionListener(this);
        mOkBtn.setActionCommand("OK");

        contentPane.add(mOkBtn, new GridBagConstraints(1, 4, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));

        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(this);
        cancel.setActionCommand("CANCEL");

        contentPane.add(cancel, new GridBagConstraints(2, 4, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 5, 5), 0, 0));
        mSave.setSelected(true);

        mOkBtn.requestFocus();
    } // initGUI

    private void saveToConsole() {
        System.out.println(mModel.getRDF());
    } // saveToConsole

    private JFileChooser mFileChooser = null;
    private JFileChooser getFileChooser() {
        if (mFileChooser == null) {
            mFileChooser = new JFileChooser();
            File f = new File(System.getProperty("user.dir"));
            mFileChooser.setCurrentDirectory(f);
        }

        return mFileChooser;
    }

    private void saveToFile() {
        JFileChooser chooser = getFileChooser();
        chooser.setCurrentDirectory(new java.io.File(System.getProperty("user.dir")));
        chooser.rescanCurrentDirectory();
        int returnVal = chooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            try {
                org.mindswap.utils.BasicUtils.saveStringToFile(mModel.getRDF(), path);
            } catch (Exception ex) {
                System.err.println("Error saving to file");
                JOptionPane.showMessageDialog(this, "Error while saving to file.", "Error", JOptionPane.ERROR_MESSAGE);
            } // ex
        } // if
    } // saveToFile

    private void sendRDF(Store theDB) {
        try {
            setVisible(false);
            mModel.submitRDF(theDB);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Error trying to submit: '" + ex + "'"
                                          , "Error", JOptionPane.ERROR_MESSAGE);
        }
    } // sendRDF

    /**
     * Gets the current selected save location
     * @return String - the string representation of the URL of the current save location
     */
    public Store getCurrentSaveLocation() {
        Store selected = (Store) mLocation.getSelectedItem();

        return selected;
    }

    /**
     * Informs this component to update and redraw itself
     */
    public void updateView() {
        initGUI();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            if (mConsole.isSelected())
                saveToConsole();
            else if (mFile.isSelected())
                saveToFile();
            else if (mSave.isSelected()) {
                Store aDB = getCurrentSaveLocation();
                if (aDB != null) {
                    int choice = JOptionPane.showConfirmDialog(this, "Submit RDF to: \"" + aDB.getName() + "\"?", "Confirm Submit RDF", JOptionPane.YES_NO_OPTION);
                    if (choice == JOptionPane.YES_OPTION) {
                        setVisible(false);
                        sendRDF(aDB);
                    } // if
                } else { // if
                    String msg = "No Remote Site Selected";
                    String title = "Save Error";
                    int type = JOptionPane.ERROR_MESSAGE;
                    JOptionPane.showMessageDialog(this, msg, title, type);
                }
            } // else if
        }
        setVisible(false);
    } // actionPerformed
} // SaveDialog
