package org.mindswap.markup;

import java.net.URL;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public interface LaunchBarListener {
    public void urlLaunched(URL theURL);
}
