package org.mindswap.markup;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import javax.swing.JToolBar;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.ButtonGroup;

import java.awt.Insets;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.AWTEventMulticaster;
import org.mindswap.markup.media.*;

public class MarkupToolBar extends JToolBar implements ActionListener {
    // tool bar commands
    public static final String DRAW_RECT_CMD = "DRAW_RECT";
    public static final String DRAW_OVAL_CMD = "DRAW_OVAL";
    public static final String DRAW_POLY_CMD = "DRAW_POLY";
    public static final String SELECT_CMD = "SELECT";
    public static final String ZOOM_OUT_CMD = "ZOOM_OUT";
    public static final String ZOOM_IN_CMD = "ZOOM_IN";

    // tool bar icons
    public static final String SELECT_ARROW_IMG = "select_arrow_icon.png";
    public static final String DRAW_RECT_IMG = "draw_rect_icon.png";
    public static final String DRAW_OVAL_IMG = "draw_oval_icon.png";
    public static final String DRAW_POLY_IMG = "draw_poly_icon.png";
    public static final String ZOOM_OUT_IMG = "zoom_out_icon.png";
    public static final String ZOOM_IN_IMG = "zoom_in_icon.png";

    // tool bar down icons
    public static final String SELECT_ARROW_IMG_DOWN = "select_arrow_icon_down.png";
    public static final String DRAW_RECT_IMG_DOWN = "draw_rect_icon_down.png";
    public static final String DRAW_OVAL_IMG_DOWN = "draw_oval_icon_down.png";
    public static final String DRAW_POLY_IMG_DOWN = "draw_poly_icon_down.png";
    public static final String ZOOM_IN_IMG_DOWN = "zoom_in_icon_down.png";
    public static final String ZOOM_OUT_IMG_DOWN = "zoom_out_icon_down.png";

    // tool tips
    public static final String SELECT_TOOLTIP = "Select";
    public static final String DRAW_RECT_TOOLTIP = "Draw Rectangle Region";
    public static final String DRAW_OVAL_TOOLTIP = "Draw Oval Region";
    public static final String DRAW_POLYGON_TOOLTIP = "Draw Polygon Region";
    public static final String ZOOM_IN_TOOLTIP = "Zoom In";
    public static final String ZOOM_OUT_TOOLTIP = "Zoom Out";
    public static final String ADD_REGION_TOOLTIP = "Add Region";

    private ButtonGroup buttonGroup;

    public MarkupToolBar() {
        setFloatable(true);

        buttonGroup = new ButtonGroup();

        addButton(SELECT_ARROW_IMG, SELECT_ARROW_IMG_DOWN, SELECT_CMD, SELECT_TOOLTIP);
        addButton(DRAW_RECT_IMG, DRAW_RECT_IMG_DOWN, DRAW_RECT_CMD, DRAW_RECT_TOOLTIP);
        addButton(DRAW_OVAL_IMG, DRAW_OVAL_IMG_DOWN, DRAW_OVAL_CMD, DRAW_OVAL_TOOLTIP);
        addButton(DRAW_POLY_IMG, DRAW_POLY_IMG_DOWN, DRAW_POLY_CMD, DRAW_POLYGON_TOOLTIP);
        addButton(ZOOM_IN_IMG, ZOOM_IN_IMG_DOWN, ZOOM_IN_CMD, ZOOM_IN_TOOLTIP);
        addButton(ZOOM_OUT_IMG, ZOOM_OUT_IMG_DOWN, ZOOM_OUT_CMD, ZOOM_OUT_TOOLTIP);
    }

    private void addButton(String rollover, String selected,
                           String actionCommand, String toolTip) {

        ImageIcon upImg = new ImageIcon(getClass().getResource("images/" + rollover));
        ImageIcon downImg = new ImageIcon(getClass().getResource("images/" + selected));

        JToggleButton button = new JToggleButton(upImg);
        button.setRolloverIcon(downImg);
        button.setSelectedIcon(downImg);
        button.addActionListener(this);
        button.setActionCommand(actionCommand);
        button.setToolTipText(toolTip);
        button.setMargin(new Insets(0,0,0,0));

        button.setFocusable(false);

        buttonGroup.add(button);
        add(button);
    }

    public void actionPerformed(ActionEvent ev)
    {
      buttonGroup.setSelected(((javax.swing.AbstractButton)ev.getSource()).getModel(),true);
      actionListener.actionPerformed(ev);
    } // actionPerformed

    transient ActionListener actionListener;
    public synchronized void addActionListener(ActionListener l) {
        if (l == null) {
            return;
        }
        actionListener = AWTEventMulticaster.add(actionListener, l);
        // TODO: newEventsOnly = true;
    }

    public synchronized void removeActionListener(ActionListener l) {
        if (l == null) {
            return;
        }
        actionListener = AWTEventMulticaster.remove(actionListener, l);
    }

}
