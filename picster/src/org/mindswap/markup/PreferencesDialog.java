package org.mindswap.markup;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.GridLayout;
import java.awt.Dimension;

import org.mindswap.utils.Pair;
import org.mindswap.utils.BasicUtils;
import org.mindswap.markup.media.*;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class PreferencesDialog extends JDialog implements ActionListener {
    private static final String CMD_OK = "CMD_OK";

    private MarkupModel mModel;
    private Razor mApp;

//    private JTextField mImageServerURL;

    private JCheckBox mQnameCheckbox;
    private JCheckBox mAutoLoadMetadata;
    private JCheckBox mUploadLocalImages;

    /**
     * Creates a new Preferences Dialog
     * @param theModel MarkupModel - the model to get the preferences from
     */
    public PreferencesDialog(Razor theApp) {
        super(theApp, "Preferences", true);
        mApp = theApp;
        mModel = theApp.getMarkupModel();
        initGUI();
        pack();
        updatePreferences();
    } // cons

    public void show() {
        BasicUtils.centerFrame(mApp,this);
        super.show();
    }

    public void setVisible(boolean theVis) {
        if (theVis)
            BasicUtils.centerFrame(mApp,this);
        super.setVisible(theVis);
    }

   /**
     * Updates the values of the preferences from the model
     */
    public void updatePreferences() {
//        mImageServerURL.setText(mModel.getImageServerURL());

        boolean current = mModel.getQNames();
        if (current!=mQnameCheckbox.isSelected()) {
            mQnameCheckbox.setSelected(current);
        }

        current = mModel.getAutoLoadImageMetadata();
        if (current != mAutoLoadMetadata.isSelected()) {
            mAutoLoadMetadata.setSelected(current);
        }
    } // updatePreferences

    private void initGUI() {
        JPanel contentPane = new JPanel();
        setContentPane(contentPane);

        mQnameCheckbox = new JCheckBox();

        mAutoLoadMetadata = new JCheckBox();

        // NOTE: need to leave setSelected here in addition to
        // updatePreferences method else java crashes most of the
        // time.
        boolean current = mModel.getQNames();
        if (current!=mQnameCheckbox.isSelected()) {
            mQnameCheckbox.setSelected(current);
        }

        current = mModel.getAutoLoadImageMetadata();
        if (current != mAutoLoadMetadata.isSelected()) {
            mAutoLoadMetadata.setSelected(current);
        }

        mQnameCheckbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                mModel.setQNames(mQnameCheckbox.isSelected());
                mModel.modelChanged(MarkupModelEvent.PREFS_CHANGED);
            }
        });

        mAutoLoadMetadata.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                mModel.setAutoLoadImageMetadata(mAutoLoadMetadata.isSelected());
                mModel.modelChanged(MarkupModelEvent.PREFS_CHANGED);
            }
        });

        contentPane.setLayout(new GridBagLayout());

        contentPane.add(new JLabel("Use QNames in UI"), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        contentPane.add(mQnameCheckbox, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

        contentPane.add(new JLabel("Auto-load Image Metadata"), new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));
        contentPane.add(mAutoLoadMetadata, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 0, 0, 0), 0, 0));

//        contentPane.add(new JLabel("Image server URL"), new GridBagConstraints(0, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 5, 0, 5), 0, 0));

//        mImageServerURL = new JTextField();
//        mImageServerURL.setSize(200,25);
//        mImageServerURL.setPreferredSize(new Dimension(200,25));
//        mImageServerURL.addFocusListener(new FocusAdapter() {
//            public void focusLost(FocusEvent e) {
//                if (!e.isTemporary()) {
//                    mModel.setImageServerURL(mImageServerURL.getText());
//                }
//            }
//        });
//        contentPane.add(mImageServerURL, new GridBagConstraints(1, 2, 4, 1, 4, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 10), 0, 0));

        JButton ok = new JButton("Ok");
        ok.setActionCommand(CMD_OK);
        ok.addActionListener(this);

        contentPane.add(ok, new GridBagConstraints(3, 5, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));
    } // initGUI

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(CMD_OK)) { // else if
            setVisible(false);
        }
    } // actionPerformed
} // PreferencesDialog
