package org.mindswap.markup;

import org.mindswap.markup.media.ImageComponent;
import fr.inrialpes.exmo.elster.picster.markup.*;

import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.DropTargetEvent;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.awt.Point;
import java.awt.BorderLayout;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.io.IOException;
import java.io.File;

import java.awt.geom.Point2D;
import java.awt.geom.AffineTransform;

import java.util.Iterator;
import java.util.Vector;

import com.hp.hpl.jena.rdf.model.Resource;
import org.mindswap.markup.media.*;

public class MarkupImageDropTargetListener implements DropTargetListener {

    private ImageComponent imageComponent;
    private MarkupModel mModel;
    private Razor mApp;

    public MarkupImageDropTargetListener(Razor theApp, ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
        mModel = theApp.getMarkupModel();
        mApp = theApp;
    }

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde){
        Point p = getPoint(dtde.getLocation());

        // no valid icon or tranformation for the image component
        // so lets just ignore the drop
        if (p == null)
            return;

        if (imageComponent.getImageModel() != null) {
            Iterator iter = imageComponent.getImageModel().getRegions();
            while (iter.hasNext()) {
                MediaRegion region = (MediaRegion)iter.next();
                if (region.contains(p)) {
                    region.setHover(true);
                } else {
                    region.setHover(false);
                }
            }
            this.imageComponent.repaint();
        }
    }

    public void dropActionChanged(DropTargetDragEvent dtde){
    }

    public void dragExit(DropTargetEvent dte){
    }

    public void drop(DropTargetDropEvent dtde) {
        Point p = getPoint(dtde.getLocation());

        Transferable t = dtde.getTransferable();
        Vector match = new Vector();
        String s = null;
        boolean wasFileDrop = false;
        try {
            if (t.isDataFlavorSupported(DataFlavor.stringFlavor))
            {
                s = (String) (t.getTransferData(DataFlavor.stringFlavor));
            } // if
            else if (dtde.getTransferable().isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                wasFileDrop = true;
                dtde.acceptDrop(java.awt.dnd.DnDConstants.ACTION_COPY_OR_MOVE);
                java.util.List list = (java.util.List)dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                if (list.size() > 0)
                {
                    File f = (File)list.get(0);
                    mApp.loadURL(f.toURL());
                    dtde.dropComplete(true);
                    return;
                } // if
            }
        } catch (UnsupportedFlavorException ufe) {
            throw new RuntimeException("NYI",ufe);
        } catch (IOException ioe) {
            // TODO: better error message
            throw new RuntimeException("NYI",ioe);
        }

        if (imageComponent.getImageModel().getImageURL() == null && !wasFileDrop)
        {
            JOptionPane.showMessageDialog(imageComponent,"Cannot drop to an empty image canvas", "Drop Error",JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (s != null && imageComponent.getImageModel() != null && !s.equals("")) {
            Iterator iter = imageComponent.getImageModel().getRegions();
            while (iter.hasNext()) {
                MediaRegion region = (MediaRegion)iter.next();
                if (region.contains(p)) {
                    match.add(region);
                }
                region.setHover(false);
                region.setSelected(false);
            }
            MediaModel imageModel = (MediaModel)imageComponent.getImageModel();

            // aInst is the name of an existing instance, so if we're dropping
            // an existing instance, we'll be merely adding a depicts assertion.
            // aNewInst is for when a class is dragged in and we have to create
            // an instance of that class and open it in the form for editing by the user
            Resource aInst = mModel.getInstance(s);
            Resource aNewInst = mModel.createInstance(null,s);

            if (match.size() == 0 && imageModel != null) {
                imageModel.setCurrentRegion(null);
                if (aInst != null)
                    imageModel.addDepicts(aInst);
                else imageModel.setPendingDepiction();
            }
            else if (match.size() == 1 && imageModel != null) {
                MediaRegion region = (MediaRegion) (match.elementAt(0));
                imageModel.setCurrentRegion(region);
                region.setSelected(true);
                region.setHover(false);

                if (aInst != null)
                    imageModel.addRegionDepicts(region,aInst);
                else imageModel.setPendingDepiction(region);
            }
            else {
                JComboBox jcb = new JComboBox(match);
                JPanel pane = new JPanel();
                pane.setLayout(new BorderLayout());
                pane.add(new JLabel("Multiple regions match drop location.\nPlease select a region from the box below"),BorderLayout.NORTH);
                pane.add(jcb,BorderLayout.CENTER);
                int option = JOptionPane.showConfirmDialog(null,pane,"Drop to multiple regions.",JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
                if (option == JOptionPane.OK_OPTION) {
                    MediaRegion region = (MediaRegion)jcb.getSelectedItem();
                    region.setSelected(true);
                    region.setHover(false);

                    if (aInst != null)
                        imageModel.addRegionDepicts(region,aInst);
                    else imageModel.setPendingDepiction(region);
                }
            }

            // the drop was of a class and we created a new instance, so lets
            // open it up for editing
            if (aInst == null) {
                mModel.setCurrentInstance(aNewInst);
                mModel.formChanged();
            }

            mModel.modelChanged();
        }

        this.imageComponent.repaint();
        dtde.dropComplete(true);
    }

    private Point getPoint(Point p) {
        AffineTransform c2i = this.imageComponent.getComponentToImage();
        if (c2i == null)
            return null;
        Point2D r = c2i.transform(p, null);
        return new Point((int)r.getX(), (int)r.getY());
    }

}
