package org.mindswap.markup;

import org.mindswap.component.Tool;

import org.mindswap.markup.media.ImageComponent;
import org.mindswap.markup.media.SelectionRegion;
import org.mindswap.markup.media.ImageModel;
import org.mindswap.markup.media.MarkupImageModel;

import java.awt.event.MouseEvent;

import java.awt.Point;
import java.awt.Polygon;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import java.util.Vector;

import javax.swing.SwingUtilities;

public class PolygonTool implements Tool {
    private ImageComponent imageComponent;
    private Vector pointList;
    private SelectionRegion currentRegion;

    public PolygonTool(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
        pointList = new Vector();
    }

    public Vector getPointList() {
        return pointList;
    }

    /**
     * Invoked when the mouse has been clicked on a component.
     *
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e))
            return;

        ImageModel imgModel = imageComponent.getImageModel();
        // single click
        if (e.getClickCount() == 1) {
            pointList.addElement(getPoint(e));

            if (pointList.size() == 1) {
                // just created this thing
                currentRegion = SelectionRegion.createPolygonRegion(pointList);

                if (imgModel != null) {
                    imgModel.addRegion(currentRegion);
                    imgModel.selectRegion(currentRegion);
                    ((MarkupImageModel)imageComponent.getImageModel()).getMarkupModel().modelChanged();
                }
            } else {
                // add a point
                Polygon p = (Polygon)currentRegion.getRegionShape();
                Point aPoint = (Point)pointList.lastElement();
                p.addPoint(aPoint.x,aPoint.y);
            }
            imageComponent.repaint();
        }
        else {
            // you need at least three points to make a polygon
            if (pointList.size() >= 3) {
                // double click, submit polygon region
                if (imgModel != null) {
                    imgModel.selectRegion(currentRegion);
                }
            } else if (imgModel != null && pointList.size() < 3)
                imgModel.deleteRegion(currentRegion);

            pointList.clear();
        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     *
     * @param e MouseEvent
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @param e MouseEvent
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e MouseEvent
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e MouseEvent
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button is pressed on a component and then dragged.
     * Mouse drag events will continue to be delivered to the component where
     * the first originated until the mouse button is released (regardless of
     * whether the mouse position is within the bounds of the component).
     *
     * @param e MouseEvent
     */
    public void mouseDragged(MouseEvent e) {
    }

    /**
     * Invoked when the mouse button has been moved on a component (with no
     * buttons no down).
     *
     * @param e MouseEvent
     */
    public void mouseMoved(MouseEvent e) {
    }


    private Point getPoint(MouseEvent e) {
        AffineTransform c2i = this.imageComponent.getComponentToImage();
        Point p = new Point(e.getX(), e.getY());
        Point2D r = c2i.transform(p, null);
        return new Point((int)r.getX(), (int)r.getY());
    }

}
