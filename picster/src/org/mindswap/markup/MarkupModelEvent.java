package org.mindswap.markup;

import fr.inrialpes.exmo.elster.picster.markup.*;


public class MarkupModelEvent {

    private MarkupModel source;
    private int id;

    /**
     * The ID for the event where the model was changed
     */
    public static final int MODEL_CHANGED = 1;
    public static final int FORM_CHANGED = 2;

    public static final int FORM_SELECTED = 3;
    public static final int REGION_SELECTED = 5;

    public static final int ONTOLOGY_LOADED = 6;
    public static final int MEDIA_CHANGED = 7;

    public static final int START_WAIT_EVENT = 8;
    public static final int END_WAIT_EVENT = 9;

    public static final int PREFS_CHANGED = 10;
    public static final int INSTANCES_LOADED = 11;

    //public static final int FORM_UPDATED = 12;
    public static final int CLEAR_ALL = 13;

    public static final int CONNECTION_STATUS_CHANGED = 14;

    // new razor stuff
    public static final int MEDIA_SELECTED = 15;
    public static final int STORE_CHANGED = 16;
    public static final int FORM_SAVED = 17;
    public static final int FORM_CANCELED = 18;
    
    // new entity stuff
    public static final int ADDED_ENTITY = 19;

    public MarkupModelEvent(MarkupModel source, int id) {
        this.source = source;
        this.id = id;
    }

    /**
     * Returns the source of the event
     * @return MarkupModel - the model from which the event originated
     */
    public MarkupModel getSource() {
        return source;
    }

    /**
     * Returns the event type
     * @return int - the event type
     */
    public int getID() {
        return id;
    }

}
