// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup;

import java.util.Vector;
import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.Property;

import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.OWL;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;

import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import org.mindswap.utils.JenaUtils;
import fr.inrialpes.exmo.elster.picster.markup.*;

public class SimpleReasoner extends MarkupReasoner {

    // this is a cache of the type info that we have already
    // calculated.  since the algo is slow, this helps performance
    // since we dont have to calc something we've already calc'ed.
    // TODO: it should be cleared any time the model is changed
    // however since that could result in incorrect and out of date
    // cached data.
    private HashMap mPropRangeInfo = new HashMap();
    private Set mAllClasses = null;

    public SimpleReasoner(MarkupModel theModel) {
        super(theModel);
    }

    public void addModel(Model m)
    {
        super.addModel(m);
        clear();
    }

    public void removeModel(Model m)
    {
        super.removeModel(m);
        clear();
    }

    private void clear()
    {
        mPropRangeInfo = new HashMap();
        mAllClasses = null;
    }

    public Set getRanges(Property aProp, Resource theClass) {
        if (mPropRangeInfo.containsKey(aProp))
            return (Set)mPropRangeInfo.get(aProp);

        Set ranges = JenaUtils.getRanges(aProp,theClass,getModel());

        mPropRangeInfo.put(aProp,ranges);
        return ranges;
    }

    public Set getDeclaredRanges(Property prop) {
        return JenaUtils.getDeclaredRanges(prop,getModel());
    }

    public Set listClasses()
    {
        if (mAllClasses == null)
        {
            mAllClasses = new HashSet();

            ExtendedIterator ex = getModel().listClasses();
            while (ex.hasNext())
            {
                OntClass oc = (OntClass)ex.next();
                if (!JenaUtils.isClass(oc) || oc.isAnon() || JenaUtils.isLanguageTerm(oc))
                    continue;
                else mAllClasses.add(oc);
            }
            ex.close();
        }

        return mAllClasses;
    }

    public Set listIndividuals()
    {
        Set classes = new HashSet();

        Iterator iter = JenaUtils.listIndividuals(getModel());
        while (iter.hasNext())
            classes.add(iter.next());

        return classes;
    }

    public Set getDeclaredProperties(Resource aRes) {
        return JenaUtils.getDeclaredProps(aRes,getModel());
    }

    public boolean isDatatypeProperty(Property prop) {
        return JenaUtils.isDatatypeProperty(prop);
    }

    public boolean isObjectProperty(Property prop) {
        return JenaUtils.isObjectProperty(prop);
    }

    public boolean isClass(Resource res) {
        return JenaUtils.isClass(res);
    }

//    public int getMaxCardinality(Property p) {
//        return JenaUtils.findMaxCardinality(p);
//    }
//
//    public int getMinCardinality(Property p) {
//        return JenaUtils.findMinCardinality(p);
//    }

    public boolean isSubClassOf(Resource child, Resource parent) {
        return JenaUtils.isSubClassOf(child,parent);
    }

    public Resource getType(Resource theRes) {
        return JenaUtils.getType(theRes,true);
    }

    public Set getTypes(Resource theRes) {
System.err.println("simple reasoner get types");
        return JenaUtils.getTypes(theRes,true);
    }
}
