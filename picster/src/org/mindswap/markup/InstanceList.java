package org.mindswap.markup;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.DefaultListCellRenderer;

import java.awt.Component;
import java.awt.BorderLayout;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;

import com.hp.hpl.jena.ontology.OntModel;

import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import java.util.Vector;
import java.util.Iterator;

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.ResourceComparator;

import org.mindswap.markup.media.MediaSearchPanel;
import fr.inrialpes.exmo.elster.picster.markup.*;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class InstanceList extends JDialog {
    private static final ImageIcon mInstIcon = new ImageIcon(MediaSearchPanel.class.getResource("images/" + "inst.png"));

    private Razor mApp;

    private JList mList;
    private DefaultListModel mListModel;

    public InstanceList(Razor theApp) {
        super(theApp,"Instance List",false);

        mApp = theApp;

        initGUI();
    }

    private void initGUI()
    {
        mListModel = new DefaultListModel();

        mList = new org.mindswap.utils.ui.TypeAheadList();
        mList.setModel(mListModel);
        mList.setCellRenderer(new ResourceListCellRenderer());
        mList.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent theEvent) {
                switch (theEvent.getKeyCode()) {
                    case KeyEvent.VK_ENTER:
                        if (mList.getSelectedIndex() != -1)
                        {
                            Resource aInst = (Resource)mList.getSelectedValue();
                            hide();
                            mApp.getMarkupModel().setCurrentInstance(aInst);
                            mApp.getMarkupModel().formChanged();
                            mApp.getMarkupModel().formSelected();
                        }
                        break;
                }
            }
        });
        mList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent theEvent)
            {
                if (theEvent.getClickCount() == 2)
                {
                    int aIndex = mList.locationToIndex(theEvent.getPoint());
                    Resource aInst = (Resource)mList.getModel().getElementAt(aIndex);

                    mList.setSelectedIndex(aIndex);

                    hide();

                    mApp.getMarkupModel().setCurrentInstance(aInst);
                    mApp.getMarkupModel().formChanged();
                    mApp.getMarkupModel().formSelected();
                }
            }
        });
        mList.setDragEnabled(true);

        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(new JScrollPane(mList),BorderLayout.CENTER);

        setSize(320,200);
    }

    public void update()
    {
        mListModel.clear();

        Vector aInstList = new Vector();

//        OntModel aModel = MarkupModel.createOntModel();
//        aModel.add(mApp.getMarkupModel().getCurrentStore().getInstances());
//long iStart = System.currentTimeMillis();
//        ExtendedIterator eIter = aModel.listIndividuals();
//long iEnd = System.currentTimeMillis();
//System.err.println("time to find ind's: "+(iEnd-iStart));
//        while (eIter.hasNext())
//        {
//            Resource aInd = (Resource)eIter.next();
//
//            if (MarkupModel.isDigitalMedia(aInd) || aInd.isAnon())
//                continue;
//
//            if (!aInstList.contains(aInd))
//                aInstList.add(aInd);
//        }
//        eIter.close();

        try {

            Model aStoreModel = mApp.getMarkupModel().getCurrentStore().asModel();

            Model aLocalModel = mApp.getMarkupModel().getLocalStore().asModel();

            Iterator aIter = JenaUtils.listIndividuals(aStoreModel);

            while (aIter.hasNext())
            {
                Resource aRes = (Resource)aIter.next();

                if (MarkupModel.isDigitalMedia(aRes) || aRes.isAnon())
                    continue;

                if (!aInstList.contains(aRes))
                    aInstList.add(aRes);
            }

            aIter = JenaUtils.listIndividuals(aLocalModel);
            while (aIter.hasNext())
            {
                Resource aRes = (Resource)aIter.next();

                if (MarkupModel.isDigitalMedia(aRes) || aRes.isAnon())
                    continue;

                if (!aInstList.contains(aRes))
                    aInstList.add(aRes);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        java.util.Collections.sort(aInstList,new ResourceComparator());

        for (int i = 0; i < aInstList.size(); i++)
        {
            Resource aRes = (Resource)aInstList.elementAt(i);
            mListModel.addElement(aRes);
        }
    }

    private class ResourceListCellRenderer extends DefaultListCellRenderer
    {
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);

            if (value == null)
                return this;

            Resource aRes = (Resource)value;

            setIcon(mInstIcon);

            String aLabel = JenaUtils.getLabel(aRes);
            if (aLabel == null)
                aLabel = aRes.getLocalName();

            setText(aLabel);

            return this;
        }
    }
}
