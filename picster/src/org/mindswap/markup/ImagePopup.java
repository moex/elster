package org.mindswap.markup;

import fr.inrialpes.exmo.elster.picster.markup.*;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JMenuItem;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseMotionListener;
import java.awt.Rectangle;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

import java.awt.geom.Point2D;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Window;
import java.awt.GraphicsConfiguration;


import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.rdf.model.Model;

import com.hp.hpl.jena.ontology.OntModel;

import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Collections;
import java.util.Vector;

import org.mindswap.markup.media.ImageComponent;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;

import org.mindswap.utils.JenaUtils;

import org.mindswap.markup.utils.ResourceComparator;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */
public class ImagePopup
{
    private static final String CMD_LOAD_IMG = "CMD_LOAD_IMG";
    private static final String CMD_DELETE_IMG = "CMD_DELETE_IMG";
    private static final String CMD_VIEW_METADATA = "CMD_VIEW_METADATA";

    private JPopupMenu mPopup;
    private MarkupModel mMarkupModel;
    private ImageComponent mImageComponent;
    private LoadDialog mLoadDialog;
    private Point mPopupClick;
    private Razor mApp;

    public ImagePopup(Razor theApp, ImageComponent theImageComponent)
    {
        mApp = theApp;
        mMarkupModel = theApp.getMarkupModel();
        mImageComponent = theImageComponent;        
        mPopup = new JPopupMenu();   
    
        //mPopup.setAutoscrolls(true);
        mPopup.addMouseMotionListener((new PopupMouseListener()).doScroll());
        mImageComponent.addMouseListener(new PopupMouseListener());        
        populateMenu();
    }
        
        
    public void update() {
        populateMenu();
    }

    private void populateMenu()
    {
        mPopup.removeAll();

        ActionListener aListener = new ImagePopupListener();

        JMenuItem aLoadImageItem = createMenuItem("Load Image",CMD_LOAD_IMG, aListener);
        JMenuItem aDeleteImageItem = createMenuItem("Delete Image",CMD_DELETE_IMG, aListener);
        JMenuItem aViewMetadata = createMenuItem("View Metadata",CMD_VIEW_METADATA, aListener);
        JMenuItem aAddDepicts = createDepictsMenu();
        
        

        mPopup.add(aLoadImageItem);
        mPopup.add(aDeleteImageItem);
        mPopup.add(aViewMetadata);

        if (mMarkupModel.getOntologyModels().size() >= 0 && mMarkupModel.getMediaURL() != null)
        {
            mPopup.add(new JSeparator());
            mPopup.add(aAddDepicts);
        }
    }

    private JMenu createDepictsMenu()
    {
        JMenu aMenu = new JMenu("Add Depicts");
       GridLayout menuGrid = new GridLayout(25,4);   
        //aMenu.getPopupMenu().setLayout(menuGrid);
        aMenu.setAutoscrolls(true);        
        
        aMenu.addMouseMotionListener((new PopupMouseListener()).doScroll());
        
        LinkedHashMap aOntList = mMarkupModel.getOntologyModels();
        Iterator kIter = aOntList.keySet().iterator();
        ImagePopupListener aListener = new ImagePopupListener();
        while (kIter.hasNext())
        {
            Object key = kIter.next();
            OntModel aModel = (OntModel)aOntList.get(key);

            String url = key.toString();
            int theIndex = url.lastIndexOf("/");
            if (theIndex == -1)
                theIndex = url.lastIndexOf("\\");
            int endIndex = url.lastIndexOf(".");
            if (endIndex < theIndex)
                endIndex = -1;

            String name = null;
            if (endIndex == -1)
                name = url.substring(theIndex + 1);
            else name = url.substring(theIndex + 1, endIndex);

            JMenu aOntMenu = new JMenu(name+" Ontology");
            //GridLayout menuGrid = new GridLayout(25,4);   
            aOntMenu.getPopupMenu().setLayout(menuGrid);
            aOntMenu.setAutoscrolls(true);           
            
            aOntMenu.addMouseMotionListener((new PopupMouseListener()).doScroll());
            
            // collect all the classes for this ont
            ExtendedIterator eIter = aModel.listClasses();
            Vector aClassList = new Vector();
            while (eIter.hasNext())
            {
                Resource aClass = (Resource)eIter.next();
                if (aClass.isAnon())
                    continue;
                else aClassList.add(aClass);
            }
            eIter.close();

            // sort the class list for the ontology
            Collections.sort(aClassList,new ResourceComparator());

            // now iterate over them and create the menu list
            Iterator iter = aClassList.iterator();
            while (iter.hasNext())
            {
                Resource aClass = (Resource)iter.next();

                if (aClass.isAnon())
                    continue;

                String aLabel = aClass.getLocalName();
                if (JenaUtils.getLabel(aClass) != null)
                    aLabel = JenaUtils.getLabel(aClass);
                
                OntClass currentClass = (OntClass)aClass;
                //ExtendedIterator subClasses = currentClass.listSubClasses(true);
                if(!currentClass.hasSubClass())  {
                	JMenuItem jmi = createMenuItem(aLabel,aClass.getURI(),aListener);
                	jmi.setAutoscrolls(true);
                	
                	jmi.addMouseMotionListener((new PopupMouseListener()).doScroll());
                	
                	aOntMenu.add(jmi);
                }                
                else  {
                	JMenu aSubClassMenu = getSubMenu(currentClass, aLabel, aListener);
                	aSubClassMenu.getPopupMenu().setLayout(menuGrid);
                	aSubClassMenu.setAutoscrolls(true);                	
                	aOntMenu.add(aSubClassMenu);                	
                }
            }
            aMenu.add(aOntMenu);
        }        
        return aMenu;
    }
    
    private JMenu getSubMenu(OntClass oc, String aLabel, ActionListener aListener)  {
    	ExtendedIterator subClasses = oc.listSubClasses(true);
    	JMenu aSubClassMenu = new JMenu(aLabel);
    	
    	
    	GridLayout menuGrid = new GridLayout(25,4);
    	aSubClassMenu.getPopupMenu().setLayout(menuGrid);
    	aSubClassMenu.setAutoscrolls(true);
    	
    	aSubClassMenu.addMouseMotionListener((new PopupMouseListener()).doScroll());
    	
    	JMenuItem jmi = createMenuItem(aLabel,oc.getURI(),aListener);                    
        aSubClassMenu.add(jmi);                	
    	Vector aSubClassList = new Vector();
        while (subClasses.hasNext())
        {
            Resource aSubClass = (Resource)subClasses.next();
            if (oc.isAnon())
                continue;
            else aSubClassList.add(aSubClass);
        }
        subClasses.close();
        
        Collections.sort(aSubClassList,new ResourceComparator());

        // now iterate over them and create the menu list
        Iterator iter = aSubClassList.iterator();
        while (iter.hasNext())
        {
            Resource aClass = (Resource)iter.next();

            if (aClass.isAnon())
                continue;

            String aClassLabel = aClass.getLocalName();
            if (JenaUtils.getLabel(aClass) != null)
                aClassLabel = JenaUtils.getLabel(aClass);
            
            OntClass currentClass = (OntClass)aClass;
            //ExtendedIterator subCurrentClasses = currentClass.listSubClasses(true);
            if(!currentClass.hasSubClass())  {
            	JMenuItem jmii = createMenuItem(aClassLabel,aClass.getURI(),aListener);            	
            	jmii.setAutoscrolls(true);
            	
            	jmii.addMouseMotionListener((new PopupMouseListener()).doScroll());
            	
            	aSubClassMenu.add(jmii);
            }                
            else  {
            	JMenu aSubSubClassMenu = getSubMenu(currentClass, aClassLabel, aListener);
            	aSubSubClassMenu.getPopupMenu().setLayout(menuGrid);
            	aSubClassMenu.add(aSubSubClassMenu);                	
            }
        }
        
        return aSubClassMenu;
    }

    private JMenuItem createMenuItem(String theLabel, String theCommand, ActionListener theListener)
    {
        JMenuItem jmi = new JMenuItem(theLabel);
        jmi.setActionCommand(theCommand);
        jmi.addActionListener(theListener);
        return jmi;
    }

    private LoadDialog getLoadDialog(String type)
    {
        if (mLoadDialog == null)
            mLoadDialog = new LoadDialog(mApp, type, null);
        else mLoadDialog = new LoadDialog(mApp, type, mLoadDialog.getFileChooser().getCurrentDirectory());

        return mLoadDialog;
    }

    private void doLoadImage()
    {
        JDialog dialog = getLoadDialog("Image");
        dialog.setVisible(true);
    }

    private void doDeleteImage()
    {
        if (mMarkupModel.getMediaURL() == null)
        {
            JOptionPane.showMessageDialog(mImageComponent,"No image to delete!");
            return;
        }

        int confirm = JOptionPane.showConfirmDialog(mImageComponent,"Delete this Image?", "Delete",JOptionPane.YES_NO_OPTION);
        if (confirm == JOptionPane.YES_OPTION)
        {
            try {
                mMarkupModel.removeMediaModel(mMarkupModel.getMediaURL());
                mMarkupModel.setMediaURL(null);
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void doViewMetadata()
    {
        if (mMarkupModel.getMediaURL() == null)
        {
            JOptionPane.showMessageDialog(mImageComponent,"No current image!");
            return;
        }
        else
        {
//            Model aModel = mMarkupModel.getMediaModel().getMetadata();
//            String aMediaURL = mMarkupModel.getMediaModel().getMediaURL().toExternalForm();

            Model aModel = mImageComponent.getImageModel().getMetadata();
            String aMediaURL = mImageComponent.getImageModel().getMediaURL().toExternalForm();

            Resource aRes = aModel.getResource(aMediaURL);

            mApp.openMediaForm(aRes);
            return;
        }
    }

    private class ImagePopupListener implements ActionListener
    {
        public void actionPerformed(ActionEvent theEvent)
        {
            String theCommand = theEvent.getActionCommand();
            if (theCommand == null || theCommand.equals(""))
            {
                // nothing to identify what kind of event this is, let just ignore it for now
            }
            else if (theCommand.equals(CMD_LOAD_IMG)) {
                doLoadImage();
            }
            else if (theCommand.equals(CMD_DELETE_IMG)) {
                doDeleteImage();
            }
            else if (theCommand.equals(CMD_VIEW_METADATA)) {
                doViewMetadata();
            }
            else {
                // this shouldnt happen, but if it does
                // at least it wont cause an exception
                if (mPopupClick == null)
                    return;

                if (mImageComponent.getComponentToImage() == null)
                {
                    System.err.println("cannot find image/component transform, cannot do depicts");
                    return;
                }

                Point2D r = mImageComponent.getComponentToImage().transform(mPopupClick,null);
                mPopupClick = new Point((int)r.getX(), (int)r.getY());

                Vector match = new Vector();
                if (mImageComponent.getImageModel() != null) {
                    Iterator iter = mImageComponent.getImageModel().getRegions();
                    while (iter.hasNext()) {
                        MediaRegion region = (MediaRegion)iter.next();
                        if (region.contains(mPopupClick)) {
                            match.add(region);
                        }
                        region.setHover(false);
                        region.setSelected(false);
                    }

                    MarkupImageModel imageModel = (MarkupImageModel)mImageComponent.getImageModel();
                    Resource instance = imageModel.getMarkupModel().createInstance(null,theCommand);

                    // wow, wtf did we do wrong if this is the case =x
                    if (instance == null)
                        return;

                    if (match.size() == 0 && imageModel != null) {
                        imageModel.setPendingDepiction();
                        mMarkupModel.setCurrentInstance(instance);
                        //imageModel.setCurrentRegion(null);
                        mMarkupModel.setCurrentRegion(null);
                        mMarkupModel.modelChanged();
                    } else if (match.size() == 1 && imageModel != null) {
                        MediaRegion region = (MediaRegion) (match.elementAt(0));
                        mMarkupModel.setCurrentRegion(region);
                        //imageModel.setCurrentRegion(region);
                        //region.setSelected(true);
                        region.setHover(false);
                        // this region depicts this instance
                        imageModel.setPendingDepiction(region);
                        mMarkupModel.setCurrentInstance(instance);
                        mMarkupModel.modelChanged();
                    } else {
                        JComboBox jcb = new JComboBox(match);
                        JPanel pane = new JPanel();
                        pane.setLayout(new BorderLayout());
                        pane.add(new JLabel("Multiple regions match drop location.\nPlease select a region from the box below"),BorderLayout.NORTH);
                        pane.add(jcb,BorderLayout.CENTER);
                        int option = JOptionPane.showConfirmDialog(null,pane,"Drop to multiple regions.",JOptionPane.PLAIN_MESSAGE,JOptionPane.OK_CANCEL_OPTION);
                        if (option == JOptionPane.OK_OPTION) {
                            MediaRegion region = (MediaRegion)jcb.getSelectedItem();
                            //region.setSelected(true);
                            region.setHover(false);
                            imageModel.setPendingDepiction(region);
                            mMarkupModel.setCurrentRegion(region);
                            mMarkupModel.setCurrentInstance(instance);
                            mMarkupModel.modelChanged();
                        }
                    }
                    mMarkupModel.formChanged();
                }
            }
        }
    }
    
    private class PopupMouseMotionListener extends MouseAdapter  {
    	
    }

    private class PopupMouseListener extends MouseAdapter
    {
        public PopupMouseListener() {
        }
        
        public MouseMotionListener doScroll()  {
        	MouseMotionListener doScrollRectToVisible = new MouseMotionAdapter() {
        	     public void mouseDragged(MouseEvent e) {
        	        Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
        	        ((JPanel)e.getSource()).scrollRectToVisible(r);
        	    }
        	 };
        	 return doScrollRectToVisible;
        }

        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                mPopupClick = e.getPoint();
                populateMenu();
                mPopup.show(e.getComponent(),e.getX(), e.getY());
                mPopup.setAutoscrolls(true);
            }
        }
    }
}
