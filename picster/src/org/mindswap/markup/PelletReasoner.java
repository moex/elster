// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Literal;

import com.hp.hpl.jena.vocabulary.OWL;

import org.mindswap.utils.JenaUtils;
import fr.inrialpes.exmo.elster.picster.markup.*;

/**
 *  How do you remove a model from the reasoner?
 *  How do you get bnodes from fxns like getSubClasses(...) ??
 *
 *  Pellet has a master jena model, might be able to remove the master model
 *  from MarkupModel and use this...
 */

public class PelletReasoner extends MarkupReasoner {
    public static boolean DEBUG = false;

    private MarkupOWLReasoner mReasoner;

    public PelletReasoner(MarkupModel model) {
        super(model);
        mReasoner = new MarkupOWLReasoner();
    }

    public void addModel(Model m) {
        super.addModel(m);
        mReasoner.load(m);
    }

    public void removeModel(Model m) {
        super.removeModel(m);
        //mReasoner.clear();
        mReasoner.remove(m);
    }

    private void cleanup(Set theSet)
    {
        // OWL.Nothing/OWL.Thing shows up in a lot of places
        // we dont neccessarily want it appearing because its a subclass of
        // every class and now that we use pellet, we get it returned in our
        // results...leaving it there will do nothing but confuse the user
        if (theSet.contains(OWL.Nothing))
            theSet.remove(OWL.Nothing);

        if (theSet.contains(OWL.Thing))
            theSet.remove(OWL.Thing);
    }

    public Set getRanges(Property prop, Resource theClass) {
        Set ranges = mReasoner.getRanges(prop,theClass);
        cleanup(ranges);
        return ranges;
    }

    public boolean isSubClassOf(Resource child, Resource parent) {
        return mReasoner.isSubclassOf(child,parent);
    }

    public Set getDeclaredProperties(Resource theRes) {
        return mReasoner.getDeclaredProperties(theRes,false);
    }

    public Set listClasses() {
        // TODO: do we need to xform resources in the set to OntClasses or will
        // that be handled in pellet?
        // we do!  Pellet doesnt use the OntModel so everything is set in stone
        // as a resource...fortunately we have an extra copy of the master jena
        // model from which we can get the OntClass for each class that comes back
        // from pellet
        Set aSet = new HashSet();
        Set classes = mReasoner.getClasses();
        Iterator iter = classes.iterator();

        while (iter.hasNext())
        {
            Object obj = iter.next();
            Resource res = (Resource)obj;
            Resource ontClass = getMarkupModel().getMasterOntologyModel().getOntClass(res.toString());
            if (ontClass == null)
            {
                if (DEBUG)
                    System.err.println("error finding class in reasoner");
                aSet.add(res);
            }
            else aSet.add(ontClass);
        }
        return aSet;
    }

    public Set listIndividuals() {
        HashSet list = new HashSet();
        Set instances = mReasoner.getIndividuals();
        Iterator iter = instances.iterator();
        while (iter.hasNext())
        {
            Resource res = (Resource) iter.next();
            list.add(res);
        }
        return list;
    }

    public boolean isDatatypeProperty(Property p) {
        return mReasoner.isDatatypeProperty(p);
    }

    public boolean isObjectProperty(Property p) {
        return mReasoner.isObjectProperty(p);
    }

    public boolean isClass(Resource res) {
        return mReasoner.isClass(res);
    }

    public int getMinCardinality(Property p) {
        return jenaGetMinCardinality(mReasoner.getModel(),p);
    }

    public int getMaxCardinality(Property p) {
        return jenaGetMaxCardinality(mReasoner.getModel(),p);
    }

    // Jena Utils

    public static final int NO_CARDINALITY = -1;

    private int jenaGetMinCardinality(Model m, Property p)
    {
        int cardinality = NO_CARDINALITY;

        // TODO: what if there are multiple cardinalities placed on a property?
        StmtIterator sIter = m.listStatements(p,OWL.minCardinality,(Literal)null);
        if (sIter.hasNext()) {
            cardinality = sIter.nextStatement().getInt();
        } else {
            // find all restrictions on this property
            // do we need to know which class the restriction is for?
            m.listStatements(null,OWL.onProperty,p);
        }
        sIter.close();

        return cardinality;
    }

    private int jenaGetMaxCardinality(Model m, Property p)
    {
        int cardinality = NO_CARDINALITY;

        // TODO: what if there are multiple cardinalities placed on a property?
        StmtIterator sIter = m.listStatements(p,OWL.maxCardinality,(Literal)null);
        if (sIter.hasNext()) {
            cardinality = sIter.nextStatement().getInt();
        } else {
            // find all restrictions on this property
            // do we need to know which class the restriction is for?
            m.listStatements(null,OWL.onProperty,p);
        }
        sIter.close();

        return cardinality;
    }

    public Resource getType(Resource theRes)
    {
        return JenaUtils.getType(theRes,true);

        //return mReasoner.getType(theRes,true);
    }

    public Set getTypes(Resource theRes)
    {
        return JenaUtils.getTypes(theRes,true);

//        HashSet aTypeSet = new HashSet();
//
//        Set types = mReasoner.getTypes(theRes);
//
//        Iterator outerIter = types.iterator();
//        while (outerIter.hasNext())
//        {
//            Set equivSet = (Set)outerIter.next();
//            Iterator eIter = equivSet.iterator();
//            while (eIter.hasNext())
//            {
//                Resource aType = (Resource)eIter.next();
//                aTypeSet.add(aType);
//            }
//        }
//
//        cleanup(aTypeSet);
//
//        return aTypeSet;
    }
}
