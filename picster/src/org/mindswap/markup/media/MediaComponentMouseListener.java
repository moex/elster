package org.mindswap.markup.media;

import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.Point;

import java.awt.geom.Point2D;
import java.awt.geom.AffineTransform;

import java.util.Iterator;

import org.mindswap.utils.JenaUtils;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaComponentMouseListener extends MouseMotionAdapter implements MouseListener
{
    private MediaImageComponent mMediaComponent;
    private MediaRegion mCurrRegion;

    public MediaComponentMouseListener(MediaImageComponent theMediaComponent)
    {
        mMediaComponent = theMediaComponent;
    }

    public void mouseMoved(MouseEvent theEvent)
    {
        if (mMediaComponent.getMediaModel() != null && (mMediaComponent.getMediaMarkupModel().getMediaURL() != null && mMediaComponent.getMediaMarkupModel().getMediaURL().equals(mMediaComponent.getMediaModel().getMediaURL())))
        {
            MediaRegion aRegion = regionAtPoint(theEvent.getPoint());

            MediaModel aMediaModel = (MediaModel)mMediaComponent.getMediaModel();

            if (mCurrRegion != null)
                mCurrRegion.setHover(false);

            if (aRegion != null)
                aRegion.setHover(true);

            mCurrRegion = aRegion;

            String aTipText = "";

            if (aRegion != null) {
                aTipText += "Region: "+aRegion.getName()+"\n";
                aTipText += "Depictions: ";

                Iterator aIter = aMediaModel.getDepictedInstances(aRegion);
                if (!aIter.hasNext())
                    aTipText += "NONE";
                else aTipText += "\n";

                while (aIter.hasNext())
                {
                    Resource aDepictedInstance = (Resource)aIter.next();
                    if (JenaUtils.getLabel(aDepictedInstance) != null)
                        aTipText += JenaUtils.getLabel(aDepictedInstance)+"\n";
                    else aTipText += aDepictedInstance.getLocalName()+"\n";
                }
            }
            else
            {
                if (mMediaComponent.getMediaModel() != null && mMediaComponent.getMediaModel().getMediaURL() != null) {
                    aTipText += "Image: ";
                    if (mMediaComponent.getMediaModel().getMediaLabel().equals(""))
                        aTipText += mMediaComponent.getMediaModel().getMediaURL().getFile().substring(mMediaComponent.getMediaModel().getMediaURL().getFile().lastIndexOf("/")+1)+"\n";
                    else aTipText += mMediaComponent.getMediaModel().getMediaLabel()+"\n";

                    aTipText += "Depictions: ";

                    Iterator aIter = aMediaModel.getDepictedInstances();

                    if (aIter.hasNext())
                        aTipText += "\n";
                    else aTipText += "NONE";

                    while (aIter.hasNext())
                    {
                        Resource aDepictedInstance = (Resource)aIter.next();
                        if (JenaUtils.getLabel(aDepictedInstance) != null)
                            aTipText += JenaUtils.getLabel(aDepictedInstance)+"\n";
                        else aTipText += aDepictedInstance.getLocalName()+"\n";
                    }
                }
            }

            if (!aTipText.equals(""))
                mMediaComponent.getImageComponent().setToolTipText(aTipText);
            else mMediaComponent.getImageComponent().setToolTipText(null);

            mMediaComponent.getControl().repaint();
        }
        else mMediaComponent.getImageComponent().setToolTipText(null);

        mMediaComponent.getControl().repaint();
    }

    private MediaRegion regionAtPoint(Point thePoint)
    {
        if (mMediaComponent.getMediaModel() != null)
        {
            Point p  = getPoint(thePoint);

            Iterator iter = mMediaComponent.getMediaModel().getRegions();
            while (iter.hasNext()) {
                MediaRegion region = (MediaRegion)iter.next();
                if (region.contains(p)) {
                    return region;
                }
            }
        }

        return null;
    }

    private Point getPoint(Point p) {
        AffineTransform c2i = mMediaComponent.getImageComponent().getComponentToImage();

        if (c2i == null)
            return p;

        Point2D r = c2i.transform(p, null);
        return new Point((int)r.getX(), (int)r.getY());
    }

    public void mouseClicked(MouseEvent theEvent) {
        if (javax.swing.SwingUtilities.isLeftMouseButton(theEvent) && mMediaComponent.getMediaModel() != null)
        {
            Point aPoint = getPoint(theEvent.getPoint());

            Iterator aIter = mMediaComponent.getMediaModel().getRegions();
            while (aIter.hasNext())
            {
                MediaRegion aRegion = (MediaRegion)aIter.next();
                if (aRegion.contains(aPoint))
                {
                    mMediaComponent.getMediaModel().setCurrentRegion(aRegion);
                    break;
                }
            }
        }
    }

    public void mousePressed(MouseEvent theEvent) {
    }

    public void mouseReleased(MouseEvent theEvent) {
    }

    public void mouseEntered(MouseEvent theEvent) {
        mMediaComponent.getControl().repaint();
    }

    public void mouseExited(MouseEvent theEvent) {
        mMediaComponent.getControl().repaint();
    }
}
