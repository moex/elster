package org.mindswap.markup.media;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

import org.mindswap.markup.MarkupModelEvent;

import org.mindswap.store.Store;

import org.mindswap.store.impl.LocalMarkupStore;

import org.mindswap.utils.JenaUtils;

import java.net.URL;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Model;

import com.hp.hpl.jena.vocabulary.RDF;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaMarkupModel extends MarkupModel {
//    private HashMap mMediaModels;

    public MediaMarkupModel() {
        super();

        //mMediaModels = new HashMap();
    }

    public Set getCurrentMedia() {
        // TODO: check all media types, not just image

        Set aRemoteList = new LinkedHashSet();

        if (getCurrentStore() != null && !getCurrentStore().equals(getLocalStore()))
        {
            try {
                Model aModel = getCurrentStore().asModel();

                ResIterator rIter = aModel.listSubjectsWithProperty(RDF.type,MarkupModel.PROFILE.Image());
                while (rIter.hasNext())
                {
                    Resource aRes = (Resource)rIter.nextResource();
                    aRemoteList.add(aRes);
                }

                rIter.close();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        Set aLocalList = new LinkedHashSet();

        try {
            ResIterator rIter = getLocalStore().asModel().listSubjectsWithProperty(RDF.type,MarkupModel.PROFILE.Image());
            while (rIter.hasNext())
            {
                Resource aRes = (Resource)rIter.nextResource();
                if (!aRemoteList.contains(aRes))
                    aLocalList.add(aRes);
            }
            rIter.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        Set aList = new LinkedHashSet();
        aList.addAll(aLocalList);
        aList.addAll(aRemoteList);

        return aList;
    }



//    public void setInstanceM(Resource theInstance) {
//        mInstance = theInstance;
//
//        if (mInstance != null && (getMediaURL() == null || !doesDepict(getModelForMedia(getMediaURL()),theInstance)))
//        {
//            // if this instance isnt depicted by the current media
//            // find the media that it is depicted by and make that the current media
//            Iterator aIter = getCurrentMedia().iterator();
//            while (aIter.hasNext())
//            {
//                Resource aRes = (Resource)aIter.next();
//                try {
//                    URL aURL = new URL(aRes.getURI());
//
//                    MarkupImageModel aTempModel = getModelForMedia(aURL);
//
//                    if (doesDepict(aTempModel,theInstance))
//                    {
//                        // wont call setMediaURL here cause that set the instance to null
//                        mMediaURL = aURL;
//                        mRegion = getRegionDepiction(aTempModel,theInstance);
//                        break;
//                    }
//                }
//                catch (Exception ex) {
//                }
//            }
//        }
//
//        // TODO: fire event instead
//    }

//    public Resource getInstanceM() {
//        return mInstance;
//    }

    private boolean doesDepict(MediaModel theModel, Resource theInstance)
    {
        Iterator aIter = theModel.getDepictedInstances();
        while (aIter.hasNext())
            if (aIter.next().equals(theInstance))
                return true;

        Iterator rIter = theModel.getRegions();
        while (rIter.hasNext())
        {
            MediaRegion aRegion = (MediaRegion)rIter.next();
            aIter = theModel.getDepictedInstances(aRegion);
            while (aIter.hasNext())
                if (aIter.next().equals(theInstance))
                    return true;
        }

        return false;
    }

    private MediaRegion getRegionDepiction(MediaModel theModel, Resource theInstance)
    {
        Iterator rIter = theModel.getRegions();
        while (rIter.hasNext())
        {
            MediaRegion aRegion = (MediaRegion)rIter.next();
            Iterator aIter = theModel.getDepictedInstances(aRegion);
            while (aIter.hasNext())
                if (aIter.next().equals(theInstance))
                    return aRegion;
        }

        return null;
    }

//    public MediaModel getModelForMedia(URL theURL) {
//        if (hasMediaModel(theURL))
//            return getMediaModel(theURL);
//        else
//        {
//            MediaModel aModel = buildModelForMedia(theURL);
//            getMediaModels().put(theURL,aModel);
//            return aModel;
//        }
//    }

    public MediaModel getMediaModel(URL url) {
        LinkedHashMap models = getMediaModels();
        MediaModel mediaModel = (MediaModel)models.get(url);
        if (mediaModel==null) {
            mediaModel = buildModelForMedia(url);
        }
        return mediaModel;
    }

    private MediaModel buildModelForMedia(URL theURL)
    {
//        if (hasMediaModel(theURL))
//            return getMediaModel(theURL);

        // TODO: use code from image check in MarkupModel instead of duping it here?
        // TODO: add these image model's back into PS?
        // TODO: use media model factory
        MediaModel aModel = MediaModel.create(this, theURL);

        // TODO: must add the model here otherwise we get an infinite loop when it
        // tries to populate the model, calls to addRegion and such call modelChanged
        // which will call this function again, which will repeat the process forever
        // other option is turning off eventing until after model is created...
        LinkedHashMap models = getMediaModels();
        models.put(theURL, aModel);

        if (getCurrentStore() != null && theURL != null)
        {
            try {
                Model aJenaModel = getCurrentStore().asModel();

                // TODO: check for all media types, not just image
                Resource aImgRes = aJenaModel.getResource(theURL.toString());

                if (aImgRes != null)
                {
                    StmtIterator sIter = aImgRes.listProperties();

                    while (sIter.hasNext())
                    {
                        Statement stmt = sIter.nextStatement();

                        if (stmt.getPredicate().equals(MarkupModel.PROFILE.hasRegion()))
                        {
                            Resource aRegionRes = stmt.getResource();
                            MediaRegion aRegion = createRegionFromResource(aRegionRes);

                            if (!aModel.containsRegion(aRegion))
                            {
                                aModel.addRegion(aRegion);
                                populateRegionDepicts(aModel,aRegionRes,aRegion);
                            }
                        }
                    }
                    sIter.close();

                    populateImageDepicts(aModel,aImgRes);

                    mImportModel.add(aModel.asModel());
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return aModel;
    }

    private void populateRegionDepicts(MediaModel theImageModel, Resource theRegionRes, MediaRegion theRegion)
    {
        // TODO: depicts stuff for image models
        StmtIterator depictsIter = theRegionRes.listProperties(MarkupModel.PROFILE.depicts());

        while (depictsIter.hasNext())
        {
            Statement depictStmt = depictsIter.nextStatement();
            Resource depicts = depictStmt.getResource();

            // TODO: grab instance from store?
            Resource inst = depicts;

            if (inst != null)
                theImageModel.addRegionDepicts(theRegion,inst);
        }
        depictsIter.close();
    }

    private void populateImageDepicts(MediaModel theModel, Resource theImageResource)
    {
        StmtIterator depictsIter = theImageResource.listProperties(MarkupModel.PROFILE.depicts());
        while (depictsIter.hasNext())
        {
            Statement depictStmt = depictsIter.nextStatement();
            Resource depicts = depictStmt.getResource();

            // TODO: get instance from store??
            Resource inst = depicts;

            if (inst != null)
                theModel.addDepicts(inst);
        }
        depictsIter.close();
    }

    private MediaRegion createRegionFromResource(Resource theRegionRes)
    {
        String svg = theRegionRes.getProperty(MarkupModel.PROFILE.svgOutline()).getString();

        // TODO: create the region here based on the type of the media it belongs to
        SelectionRegion aRegion = SelectionRegion.fromSVG(svg);

        String aLabel = JenaUtils.getLabel(theRegionRes);

        if (aLabel != null)
            aRegion.setName(aLabel);
        return aRegion;
    }
}
