package org.mindswap.markup.media.impl;

import java.net.URL;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class ImageMediaIconImpl extends MediaIconImpl {
    private Image mIcon;
    private URL mURL;
    private boolean mNeedsLoad = true;

    // TODO: for now just pass in the url, eventually will want to send a model
    public ImageMediaIconImpl(URL theURL) {
        mURL = theURL;
    }

    public synchronized Image getIcon() {
        if (mIcon == null && mNeedsLoad) {
            mNeedsLoad = false;
            Thread aLoadThread = new Thread() {
                public void run() {
                    Image aIcon = new ImageIcon(mURL).getImage().getScaledInstance(90,90,Image.SCALE_FAST);
                    mIcon = aIcon;
                    getControl().repaint();
                }
            };
            aLoadThread.setDaemon(true);
            aLoadThread.setPriority(Thread.MIN_PRIORITY);
            aLoadThread.start();
        }

        return mIcon;
    }

    public URL getMediaURL() {
        return mURL;
    }
}
