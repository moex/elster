package org.mindswap.markup.media.impl;

import org.mindswap.markup.media.MediaIcon;
import org.mindswap.markup.media.MediaIconListener;

import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.SwingUtilities;

import java.awt.Rectangle;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.Dimension;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.HashSet;
import java.util.Iterator;
import org.mindswap.markup.media.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public abstract class MediaIconImpl extends JPanel implements MediaIcon {
    private boolean mSelected;
    private HashSet mListeners;

    public MediaIconImpl() {
        mSelected = false;
        mListeners = new HashSet();

        initGUI();
    }

    public JPanel getControl() {
        return this;
    }

    public void addMediaIconListener(MediaIconListener theListener) {
        if (!mListeners.contains(theListener))
            mListeners.add(theListener);
    }

    public void removeMediaIconListener(MediaIconListener theListener) {
        mListeners.remove(theListener);
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean theSelection) {
        mSelected = theSelection;

        if (mSelected)
            setBackground(java.awt.Color.yellow);
        else setBackground(java.awt.Color.lightGray);

        revalidate();
        repaint();
    }

    public Dimension getPreferredSize() {
        return new Dimension(100,100);
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        // TODO: get better way to grab icon? get from thumbnail script to reduce d/l time?
//        if (mIcon == null)
//        {
//            ImageIcon aIcon = (ImageIcon)mModel.getModelForMedia(mURL).getIcon();
//            //ImageIcon aIcon = new ImageIcon(mURL);
//
//            Image aImg = aIcon.getImage().getScaledInstance(90,90,Image.SCALE_FAST);
//
//            mIcon = new ImageIcon(aImg);
//        }

        if (getIcon() == null || (getIcon().getWidth(this) == -1 || getIcon().getHeight(this) == -1)) {
            g.setColor(java.awt.Color.black);
            g.fillRect(5,5,(int)getPreferredSize().getWidth()-10,(int)getPreferredSize().getHeight()-10);
        }
        else g.drawImage(getIcon(),5,5,this);

        // TODO: have a custom label that paints like a drop shadow, or paint a drop
        // shadow on this maybe...
    }

    private void initGUI()
    {
        BorderFactory.createEmptyBorder(5,5,5,5);

        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent theEvent) {
                if (SwingUtilities.isLeftMouseButton(theEvent))
                {
                    Iterator aIter = mListeners.iterator();
                    while (aIter.hasNext())
                    {
                        MediaIconListener theListener = (MediaIconListener)aIter.next();
                        theListener.mediaSelected(getMediaURL(), theEvent.isControlDown());
                    }
                }
            }
        });
    }
}
