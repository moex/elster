package org.mindswap.markup.media;

import java.io.Serializable;

import java.util.Random;

import java.awt.Graphics2D;
import java.awt.Point;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public abstract class MediaRegion implements Serializable {
    private static final Random RANDOM = new Random();

    private String mName;
    private boolean mSelected;
    private boolean mHover;

    public MediaRegion() {
        mName = "region"+RANDOM.nextInt(10000);
        mSelected = false;
        mHover = false;
    }

    /**
     * Returns the name of this region
     * @return String
     */
    public String getName() {
        return mName;
    }

    /**
     * Sets the name of this region
     * @param s String - the new name for the region
     */
    public void setName(String s) {
        mName = s;
    }

    /**
     * Returns true of the region is currently selected, false otherwise
     * @return boolean
     */
    public boolean isSelected() {
        return mSelected;
    }
    /**
     * Sets the selection status of this region
     * @param theSelected boolean - true for selecting the region, false for deselecting it
     */
    public void setSelected(boolean theSelected) {
        mSelected = theSelected;
    }

    /**
     * Returns whether or not the mouse is hovering over the shape
     * @return boolean true if the mouse is hovering, false otherwise
     */
    public boolean isHover() {
        return mHover;
    }
    /**
     * Sets the hover status of the region
     * @param theHover boolean - true if the mouse is over the region, false if it is not
     */
    public void setHover(boolean theHover) {
        mHover = theHover;
    }

    /**
     * Returns the string representation of the region
     * @return String - the name of the region
     */
    public String toString() {
        return getName();
    }

    public abstract void draw(Graphics2D theGraphics);
    public abstract void fill(Graphics2D theGraphics);
    public abstract boolean contains(Point thePoint);
}
