// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup.media;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Polygon;
import java.awt.Point;

import java.util.Vector;
import java.util.StringTokenizer;
import java.util.Random;
import org.mindswap.component.*;

/**
 * The SelectionRegion class, represents a region of an image
 *
 * @author Michael Grove
 * @version 1.0
 */
public class SelectionRegion extends MediaRegion {
    public static final int SHAPE_OVAL = 0;
    public static final int SHAPE_RECT = 1;
    public static final int SHAPE_POLY = 2;

    private int mType;
    private Object mShape;

    private SelectionRegion(int type) {
        super();
        mType = type;
    }

    /**
     * Creates a rectangular region using the specified parameter
     * @param r Rectangle - the bounds of the rectangular region
     * @return SelectionRegion - the new region
     */
    public static SelectionRegion createRectangleRegion(Rectangle r) {
        SelectionRegion sr = new SelectionRegion(SHAPE_RECT);
        sr.mShape = r;
        return sr;
    }

    /**
     * Creates an oval region using the x,y and width and height of the supplied rectangle
     * @param r Rectangle - the bounds of the oval region
     * @return SelectionRegion - the new oval region
     */
    public static SelectionRegion createOvalRegion(Rectangle r) {
        SelectionRegion sr = new SelectionRegion(SHAPE_OVAL);
        sr.mShape = r;
        return sr;
    }

    /**
     * Creates a polygon region using the list of Points
     * @param pointList Vector - a list of Point objects that specify the vertices of the polygon
     * @return SelectionRegion - the new polygon region
     */
    public static SelectionRegion createPolygonRegion(Vector pointList) {
        int[] xpoints = new int[pointList.size()];
        int[] ypoints = new int[pointList.size()];
        for (int i = 0; i < pointList.size(); i++) {
            Point p = (Point)pointList.elementAt(i);
            xpoints[i] = p.x;
            ypoints[i] = p.y;
        }
        SelectionRegion sr = new SelectionRegion(SHAPE_POLY);
        sr.mShape = new Polygon(xpoints,ypoints,pointList.size());
        return sr;
    }

    /**
     * Returns the type of this region, either rectangular, oval or polygon.
     * @return int
     */
    public int getType() {
        return mType;
    }

    /**
     * Returns the shape of the region.  Rectangle is used for both a rectangular region
     * and the oval region.  A Polygon object is used for the polygon region.
     * @return Object - The shape, either a Rectangle or a Polygon
     */
    public Object getRegionShape() {
        return mShape;
    }

    /**
     * Draw this region using the specified graphics context
     * @param g Graphics2D - the context in which to draw the region
     */
    public void draw(Graphics2D g) {
        if (getType() == SHAPE_RECT) {
            Rectangle r = (Rectangle)mShape;
            g.draw(r);
        } else if (getType() == SHAPE_OVAL) {
            Rectangle r = (Rectangle)mShape;
            g.drawOval(r.x,r.y,r.width,r.height);
        } else if (getType() == SHAPE_POLY) {
            Polygon p = (Polygon)mShape;
            g.drawPolygon(p);
        }
    }

    /**
     * Fill this region using the specified graphics context
     * @param g Graphics2D - the context in which to fill the region
     */
    public void fill(Graphics2D g) {
        if (getType() == SHAPE_RECT) {
            Rectangle r = (Rectangle)mShape;
            g.fill(r);
        } else if (getType() == SHAPE_OVAL) {
            Rectangle r = (Rectangle)mShape;
            g.fillOval(r.x,r.y,r.width,r.height);
        } else if (getType() == SHAPE_POLY) {
            Polygon p = (Polygon)mShape;
            g.fillPolygon(p);
        }
    }

    /**
     * Tests whether or not a given point lies inside this particular region
     * @param pt Point - the point to test
     * @return boolean - true if the point lies within the bounds of the region, false otherwise
     */
    public boolean contains(Point pt) {
        if (getType() == SHAPE_RECT || getType() == SHAPE_OVAL) {
            Rectangle r = (Rectangle)mShape;
            return r.contains(pt);
        } else if (getType() == SHAPE_POLY) {
            Polygon p = (Polygon)mShape;
            return p.contains(pt);
        } else return false;
    }

    /**
     * Determines whether or not two regions are equal
     * @param o Object - the object to compare to
     * @return boolean - true if the regions are equal, false otherwise
     */
    public boolean equals(Object o) {
        if (o instanceof SelectionRegion) {
            SelectionRegion sr = (SelectionRegion)o;
            return sr.getType() == getType() && mShape.equals(sr.mShape);
        } else {
            return false;
        }
    }

    /**
     * Returns the bounds of the shape
     * @return Rectangle - the smallest rectangle that can hold the entire shape
     */
    public Rectangle getRect() {
        if (getType() == SHAPE_RECT || getType() == SHAPE_OVAL)
            return (Rectangle)mShape;
        else if (getType() == SHAPE_POLY) {
            Polygon p = (Polygon)mShape;
            return p.getBounds();
        }
        return null;
    }

    public static SelectionRegion fromSVG(String svg)
    {
        final String DELIM = "\"";
        final String X_KEY = "x="+DELIM;
        final String Y_KEY = "y="+DELIM;
        final String W_KEY = "width="+DELIM;
        final String H_KEY = "height="+DELIM;
        final String CX_KEY = "cx="+DELIM;
        final String CY_KEY = "cy="+DELIM;
        final String RX_KEY = "rx="+DELIM;
        final String RY_KEY = "ry="+DELIM;
        final String POINTS_KEY = "points="+DELIM;

        // scan to <image tag
        // after img tag />
        // will be rect/poly/oval right? (rect/ellipse/poly)

        SelectionRegion region = null;

        svg = svg.replaceAll("&lt;","<");
        int index = svg.indexOf("<image");
        index = svg.indexOf("/>",index);

        if (svg.indexOf("rect",index) != -1)
        {
            index = svg.indexOf("rect",index)+4;

            int xStart = svg.indexOf(X_KEY,index)+X_KEY.length();

            String xStr = svg.substring(xStart,svg.indexOf(DELIM,xStart));

            int yStart = svg.indexOf(Y_KEY,index)+Y_KEY.length();
            String yStr = svg.substring(yStart,svg.indexOf(DELIM,yStart));

            int widthStart = svg.indexOf(W_KEY,index)+W_KEY.length();
            String widthStr = svg.substring(widthStart,svg.indexOf(DELIM,widthStart));

            int heightStart = svg.indexOf(H_KEY,index)+H_KEY.length();
            String heightStr = svg.substring(heightStart,svg.indexOf(DELIM,heightStart));

            double x = Double.parseDouble(xStr.trim());
            double y = Double.parseDouble(yStr.trim());
            double w = Double.parseDouble(widthStr.trim());
            double h = Double.parseDouble(heightStr.trim());

            region = createRectangleRegion(new Rectangle((int)x,(int)y,(int)w,(int)h));
        }
        else if (svg.indexOf("ellipse",index) != -1)
        {
            index = svg.indexOf("ellipse",index)+7;

            int cxStart = svg.indexOf(CX_KEY,index)+CX_KEY.length();
            String cxStr = svg.substring(cxStart,svg.indexOf(DELIM,cxStart));

            int cyStart = svg.indexOf(CY_KEY,index)+CY_KEY.length();
            String cyStr = svg.substring(cyStart,svg.indexOf(DELIM,cyStart));

            int rxStart = svg.indexOf(RX_KEY,index)+RX_KEY.length();
            String rxStr = svg.substring(rxStart,svg.indexOf(DELIM,rxStart));

            int ryStart = svg.indexOf(RY_KEY,index)+RY_KEY.length();
            String ryStr = svg.substring(ryStart,svg.indexOf(DELIM,ryStart));

            double x = Double.parseDouble(cxStr.trim());
            double y = Double.parseDouble(cyStr.trim());
            double w = Double.parseDouble(rxStr.trim());
            double h = Double.parseDouble(ryStr.trim());

            region = createOvalRegion(new Rectangle((int)x,(int)y,(int)w,(int)h));
        }
        else if (svg.indexOf("polygon",index) != -1)
        {
            index = svg.indexOf("polygon",index)+7;

            int pointsStart = svg.indexOf(POINTS_KEY,index)+POINTS_KEY.length();
            String pointsStr = svg.substring(pointsStart,svg.indexOf(DELIM,pointsStart));

            StringTokenizer st = new StringTokenizer(pointsStr," ",false);
            while (st.hasMoreTokens())
            {
                Vector pointList = new Vector();

                String ptStr = st.nextToken();
                StringTokenizer ptToke = new StringTokenizer(ptStr,",",false);
                String xStr = ptToke.nextToken();
                String yStr = ptToke.nextToken();

                double x = Double.parseDouble(xStr);
                double y = Double.parseDouble(yStr);

                Point aPoint = new Point((int)x,(int)y);
                pointList.add(aPoint);
                region = createPolygonRegion(pointList);
            }
        }

        return region;
    }

}
