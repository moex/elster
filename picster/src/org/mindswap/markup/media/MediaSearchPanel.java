package org.mindswap.markup.media;

import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JDialog;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Component;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import java.util.Vector;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;

import java.net.URL;

import com.hp.hpl.jena.rdf.model.Resource;

import fr.inrialpes.exmo.elster.picster.markup.Razor;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;
import org.mindswap.markup.MarkupModelEvent;
import fr.inrialpes.exmo.elster.picster.markup.P2PQueryPanel;

import org.mindswap.utils.JenaUtils;
import fr.inrialpes.exmo.elster.jxta.*;
import fr.inrialpes.exmo.elster.picster.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaSearchPanel extends JPanel implements ActionListener, ItemListener
{
    public static boolean DEBUG = false;

    private static final ImageIcon mMediaIcon = new ImageIcon(MediaSearchPanel.class.getResource("images/" + "media.png"));
    private static final ImageIcon mInstIcon = new ImageIcon(MediaSearchPanel.class.getResource("images/" + "inst.png"));
    private static final ImageIcon mClassIcon = new ImageIcon(MediaSearchPanel.class.getResource("images/" + "class.gif"));

    private static final String CMD_SEARCH = "CMD_SEARCH";

    private static final String OPTION_ALL = "<All>";
    private static final String OPTION_CONCEPT = "<Concept>";

    private MediaMarkupModel mModel;
    private Razor mApp;

    private JTextField mSearchText;

    private JDialog mSearchDialog;

    private boolean mMatchCase = false;

    private JComboBox mOptionCombo;
    private JComboBox mConceptCombo;

    private JCheckBox mCaseCheckBox;

    private JPanel mSearchCriteriaPanel;

    public MediaSearchPanel(Razor theApp)
    {
        mApp = theApp;
        mModel = theApp.getMarkupModel();

        initGUI();
    }

    public void update()
    {
        buildCriteriaPanel();
    }

    private void initGUI()
    {
        setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        setLayout(new BorderLayout());

        mConceptCombo = new JComboBox();
        mConceptCombo.setRenderer(new ResourceListCellRenderer());

        final JButton aSearchBtn = new JButton("Query");
        aSearchBtn.setActionCommand(CMD_SEARCH);
        aSearchBtn.addActionListener(this);

        mSearchText = new JTextField();
        mSearchText.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent theEvent) {
                if (theEvent.getKeyCode() == KeyEvent.VK_ENTER)
                    aSearchBtn.doClick();
            }
        });

        mSearchCriteriaPanel = new JPanel();
        mSearchCriteriaPanel.setLayout(new BorderLayout());

        mOptionCombo = new JComboBox();
        mOptionCombo.addItem(OPTION_ALL);
        mOptionCombo.addItem(OPTION_CONCEPT);
        mOptionCombo.addItemListener(this);

        mCaseCheckBox = new JCheckBox("Match Case",false);

        JPanel aLeftPanel = new JPanel();
        aLeftPanel.setLayout(new BorderLayout());
      //  aLeftPanel.add(new JLabel("Search: "),BorderLayout.WEST);
       // aLeftPanel.add(mOptionCombo,BorderLayout.EAST);

        JPanel aRightPanel = new JPanel();
        aRightPanel.setLayout(new BorderLayout());
        aRightPanel.add(aSearchBtn,BorderLayout.WEST);
       // aRightPanel.add(mCaseCheckBox,BorderLayout.EAST);

        add(aRightPanel,BorderLayout.EAST);
       // add(mSearchCriteriaPanel,BorderLayout.CENTER);

       // add(aLeftPanel,BorderLayout.WEST);

        buildCriteriaPanel();
    }

    private void buildCriteriaPanel()
    {
        mSearchCriteriaPanel.removeAll();

        if (mOptionCombo.getSelectedItem().equals(OPTION_CONCEPT))
        {
            mCaseCheckBox.setEnabled(false);

            mConceptCombo.removeAllItems();

            Vector aList = new Vector(mModel.getAllClasses());
            //java.util.Collections.sort(aList, new org.mindswap.markup.utils.ResourceComparator());
            Iterator aIter = aList.iterator();
            while (aIter.hasNext())
                mConceptCombo.addItem(aIter.next());

            mSearchCriteriaPanel.add(mConceptCombo,BorderLayout.CENTER);
        }
        else
        {
            mSearchCriteriaPanel.add(mSearchText,BorderLayout.CENTER);
            mCaseCheckBox.setEnabled(true);
        }

        revalidate();
        repaint();
    }

    public void actionPerformed(ActionEvent theEvent)
    {
        String aCommand = theEvent.getActionCommand();

        if (aCommand.equals(CMD_SEARCH))
        {
            //TODO: Start JXTA
            
            Picster ps = mApp.p;
            JFrame frame = new JFrame("Query Interface");
            P2PQueryPanel p2p = new P2PQueryPanel(mApp, ps);
            frame.getContentPane().add(p2p);
            frame.setSize(800, 700);
            frame.setVisible(true);
            frame.toFront();            
        }
    }

    private void doSearch(String theSearchString, Resource theConcept)
    {
        long start = System.currentTimeMillis();

        Set aResults = new HashSet();

        String aQuery = "";

        try {
            com.hp.hpl.jena.rdf.model.Model aModel = mModel.getCurrentStore().asModel();

            aQuery = "select ?uri where { ";

            aQuery += " ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?aType . ";

            if (theConcept != null)
            {
                aQuery += " OPTIONAL { ?uri <"+MarkupModel.PROFILE.depicts()+"> ?x . ?x <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?aDepictsType . } . ";
                aQuery += " FILTER (?aType = <"+theConcept+"> || ?aDepictsType = <"+theConcept+"> ) . ";
            }
            else
            {
                aQuery += " OPTIONAL  { ?uri <http://www.w3.org/2000/01/rdf-schema#label> ?label . } ";
                aQuery += " FILTER (regex(str(?uri), \""+theSearchString+"\", \"i\") || regex(?label, \""+theSearchString+"\", \"i\") ) . ";
            }

            aQuery += " FILTER (!isBlank(?uri)) . ";

            aQuery += "}";

            mModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);

            com.hp.hpl.jena.query.Query aQ = com.hp.hpl.jena.query.QueryFactory.create(aQuery);

            com.hp.hpl.jena.query.QueryExecution aQE = com.hp.hpl.jena.query.QueryExecutionFactory.create(aQ,aModel);
System.err.println("Executing query: "+aQuery);

            com.hp.hpl.jena.query.ResultSet aResultSet = aQE.execSelect();

            while (aResultSet.hasNext())
            {
                com.hp.hpl.jena.query.QuerySolution aSolution = aResultSet.nextSolution();
                Resource aRes = (Resource)aSolution.get("uri");
                aResults.add(aRes);
            }
            aQE.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,"There was an error while searching!", "Search Error",JOptionPane.ERROR_MESSAGE);
            mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
            return;
        }

        long end = System.currentTimeMillis();

        if (DEBUG) {
            System.err.println("Search time: "+(end-start));
            System.err.println("searched for: "+theSearchString);
            System.err.println("returned "+aResults.size()+" results");
        }

        mModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);

        showResults(aQuery, aResults);
    }

    private void showResults(String theQuery, Set theResults)
    {
        if (theResults.isEmpty())
        {
            if (mSearchDialog != null)
                mSearchDialog.hide();

            JOptionPane.showMessageDialog(this,"No Results Found.","Search Results",JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            if (mSearchDialog != null) {
                mSearchDialog.hide();
                mSearchDialog.dispose();
                mSearchDialog = null;
            }

            mSearchDialog = new JDialog(mApp,"Search Results",false);
            mSearchDialog.getContentPane().setLayout(new BorderLayout());
            mSearchDialog.getContentPane().add(new SearchResults(theQuery,theResults),BorderLayout.CENTER);
            mSearchDialog.repaint();
            mSearchDialog.pack();
            mSearchDialog.show();
        }
    }

    private class SearchResults extends JPanel implements DragGestureListener
    {
        private Set mResults;
        private String mQuery;
        private JList mList;
        private DragSource mDragSource;

        public SearchResults(String theQuery, Set theResults)
        {
            mQuery = theQuery;
            mResults = theResults;
            initGUI();
        }

        public void dragGestureRecognized(DragGestureEvent dge)
        {
            // this enables dragging on the search results but only for instances
            // if something is media, we send an invalid drag event that shows the invalid cursor
            // and doesnt fire the dnd operation, or so it seems ;)
            int aIndex = mList.locationToIndex(dge.getDragOrigin());
            if (aIndex != mList.getSelectedIndex())
                mList.setSelectedIndex(aIndex);

            Resource aRes = (Resource)mList.getSelectedValue();

            if (MarkupModel.isDigitalMedia(aRes))
                mDragSource.startDrag(dge, DragSource.DefaultLinkNoDrop, new java.awt.datatransfer.StringSelection(""), null);
            else mDragSource.startDrag(dge, DragSource.DefaultMoveDrop, new java.awt.datatransfer.StringSelection(aRes.toString()), null);
        } // dragGestureRecognized

        private void initGUI()
        {
            mList = new JList(mResults.toArray());
            //mList.setDragEnabled(true);

            mDragSource = new DragSource();
            mDragSource.createDefaultDragGestureRecognizer(mList, DnDConstants.ACTION_MOVE, this);

            mList.setCellRenderer(new ResourceListCellRenderer());
            mList.addKeyListener(new KeyAdapter() {
                public void keyReleased(KeyEvent theEvent) {
                    if (theEvent.getKeyCode() == KeyEvent.VK_ENTER)
                    {
                        Resource aValue = (Resource)mList.getSelectedValue();

                       if (aValue != null)
                           open(aValue);
                    }
                }
            });

            mList.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent theEvent)
                {
                    if (theEvent.getClickCount() == 2)
                    {
                        int aIndex = mList.locationToIndex(theEvent.getPoint());
                        Resource aRes = (Resource)mList.getModel().getElementAt(aIndex);
                        open(aRes);
                    }
                }
            });

            JButton aButton = new JButton("Use as Media Filter");
            aButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent theEvent) {
                    mApp.applyFilter(new MediaFilterImpl(mModel,mQuery,mResults));
                }
            });

            setLayout(new BorderLayout());

            add(new JScrollPane(mList),BorderLayout.CENTER);
            add(aButton, BorderLayout.SOUTH);
        }
    }

    private void open(Resource theRes)
    {
        if (MarkupModel.isDigitalMedia(theRes))
        {
            try {
                mModel.setMediaURL(new URL(theRes.getURI()));
            }
            catch (Exception ex) {
            }
        }
        else if (JenaUtils.isIndividual(theRes))
        {
            // this is an instance, select that
            // basically want to find the media that it belongs to and open that media
            mModel.setCurrentInstance(theRes);

            mModel.formChanged();
            mModel.formSelected();
        }
        else
        {
            mApp.selectClass(theRes);
        }

        mSearchDialog.hide();
    }

    public void itemStateChanged(ItemEvent theEvent)
    {
        buildCriteriaPanel();
    }

    private class ResourceListCellRenderer extends DefaultListCellRenderer
    {
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);

            if (value == null)
                return this;

            Resource aRes = (Resource)value;

            // TODO: test for all media types
            if (JenaUtils.isType(aRes,MarkupModel.PROFILE.Image())) {
                setIcon(mMediaIcon);
            }
            else if (JenaUtils.isClass(aRes)) {
                setIcon(mClassIcon);
            }
            else {
                // otherwise just assume its an inst
                setIcon(mInstIcon);
            }

            String aLabel = JenaUtils.getLabel(aRes);
            if (aLabel == null)
                aLabel = aRes.getLocalName();

            setText(aLabel);

            return this;
        }
    }
}
