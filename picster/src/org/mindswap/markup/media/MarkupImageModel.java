// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup.media;

import java.net.URL;

import java.util.HashSet;
import java.util.Iterator;
import java.io.FileInputStream;
import java.io.File;

import java.awt.Rectangle;
import java.awt.Polygon;

import javax.swing.Icon;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.RDF;

import org.mindswap.multimedia.Region;
import org.mindswap.multimedia.Image;
import org.mindswap.multimedia.MultimediaModel;
import org.mindswap.multimedia.MultimediaFactory;

import fr.inrialpes.exmo.elster.picster.markup.*;
import org.mindswap.markup.MarkupModelEvent;

import org.mindswap.utils.ImageMetadataExtractor;

import org.mindswap.vocabulary.ImageRegions;
import org.mindswap.utils.JenaUtils;
import java.util.HashMap;

public class MarkupImageModel extends ImageModel {

    private MarkupModel markupModel;
    private static HashMap map = new HashMap();
    private static HashMap regMap = new HashMap();
    public MarkupImageModel(MarkupModel markupModel) {
        this.markupModel = markupModel;
    }

    public String getType() {
        return "image";
    }

    public void extractMetadata()
    {
    	//by arun
    	String surl = getMediaURL().toExternalForm();
    	String imageUri;
    	if(!surl.startsWith("http"))  {
    		String base = getMarkupModel().getBaseURL();
    		imageUri = base + "images/" + surl.substring(surl.lastIndexOf('/')+1, surl.length()); 
    	}
    	else imageUri = getMediaURL().toExternalForm();
    	//arun ends
    	//next changed by arun
        Model aModel = ImageMetadataExtractor.extract(imageUri, getMediaURL().toExternalForm());
        try  {
	    String fileName =  surl.substring(surl.lastIndexOf('/'), surl.length()) + ".rdf";
	    File anno = new File("annotations/" + fileName);
	    if(anno.exists())  {
	    	//System.out.println("ADDING ANNOTATIONS");
	    FileInputStream in = new FileInputStream(anno);
	    aModel.read(in, "");
	    StmtIterator allStatements = aModel.listStatements();
	    while(allStatements.hasNext()) {
	    	Statement s = allStatements.nextStatement();
          	if(!s.getSubject().isAnon()) {
          		Resource subject = s.getSubject();
          		if(subject.hasProperty(MarkupModel.PROFILE.svgOutline())) {
          			String svg = subject.getProperty(MarkupModel.PROFILE.svgOutline()).getString();
          			SelectionRegion aRegion = SelectionRegion.fromSVG(svg);
          	        String aLabel = JenaUtils.getLabel(subject);
          	        if (aLabel != null) {
          	            aRegion.setName(aLabel);
          	        //add this region to the media model          	        
          	            addRegion(aRegion);
          	        	//System.out.println("Region added");
          	        }
          	        markupModel.modelChanged(MarkupModelEvent.REGION_SELECTED);
          	        
          	      if(subject.hasProperty(MarkupModel.PROFILE.depicts())) {  
          	    	  Statement st = subject.getProperty(MarkupModel.PROFILE.depicts());
          	    	  Resource instance = (Resource) st.getObject();          	  
          	    	  if(instance != null)
        				addRegionDepicts(aRegion, instance);
          	    	  markupModel.modelChanged(MarkupModelEvent.FORM_SAVED);
          	      }
          		}
          		
          		if(subject.hasProperty(MarkupModel.PROFILE.depicts()) && !subject.hasProperty(MarkupModel.PROFILE.svgOutline())) {  
            	    	  Statement st = subject.getProperty(MarkupModel.PROFILE.depicts());
            	    	  Resource instance = (Resource) st.getObject();          	  
            	    	  if(instance != null)
            	    		  this.addDepicts(instance);
            	    	  markupModel.modelChanged(MarkupModelEvent.FORM_SAVED);
            	}          		
          	}
//
	    }
	    }
        } catch(Exception e)  {
        	System.err.println("Error in reading the annotations");
        	e.printStackTrace();
        }
        getMetadata().add(aModel);
    }

    private void storeURL(String url, String uri)  {
    	//System.out.println("ValueStored in hash");
    	map.put(uri, url);
    }

    private void storeRegURL(String url, String uri)  {
    	//System.out.println("ValueStored in hash");
    	regMap.put(uri, url);
    }
    
    private boolean existsURL(String uri)  {
    	return map.containsKey(uri);
    }

    private boolean existsRegURL(String uri)  {
    	return regMap.containsKey(uri);
    }
    
   public static HashMap getUriMap(){return map;}

   public static HashMap getRegUriMap(){return regMap;}
   
   public static void putUriMap(HashMap myImgmap)  {
       map = myImgmap;
   }
   
   public static void putRegUriMap(HashMap myregMap)  {
       regMap = myregMap;
   }

    private Image assertImage(MultimediaModel model) {
        Image image = null;
        URL url = getImageURL();
        if (url!=null) {

            //url = markupModel.adjustURL(url);
            //by arun
        	String surl = url.toString();
        	String imageUri;
        	if(!surl.startsWith("http"))  {
        		String base = getMarkupModel().getBaseURL();
        		imageUri = base + "images/" + surl.substring(surl.lastIndexOf('/')+1, surl.length()); 
        	}
        	else imageUri = url.toString();
            image = model.createImage(imageUri);
            if(!existsURL(imageUri))
            storeURL(surl, imageUri);
            //Image i_modi = (Image)model.createResource(imageUri);
            //StmtIterator stmt = image.listProperties();
            //while(stmt.hasNext()) {
            	///Statement s = stmt.nextStatement();
            ///}
            
            if (!getMediaLabel().equals("")) {
                Statement s = model.createStatement(image, RDFS.label, getMediaLabel());
                model.add(s);
            }

            Iterator iter = getDepictedInstances();

            while (iter.hasNext()) {
                Resource inst = (Resource)iter.next();
                image.addDepicts(inst);
            }
        }
        return image;
    }

    private Region assertRegion(MultimediaModel model, SelectionRegion selectionRegion, Image image) {
    	//by arun
    	URL url = getImageURL();
    	String surl = url.toString();
    	String imagename = surl.substring(surl.lastIndexOf('/')+1, surl.length());
    	String baseurl = getMarkupModel().getBaseURL();
    	String regionURL = baseurl + "images/" + imagename + "/" + selectionRegion.getName();
        Region region = model.createRegion(regionURL);

	 if(!existsRegURL(regionURL))
            storeRegURL(surl, regionURL);
	
        image.addRegion(region);
        region.addSVGOutline(getSVG(selectionRegion));
        model.add(model.createStatement(region,RDFS.label,selectionRegion.getName()));

        switch (selectionRegion.getType())
        {
            case SelectionRegion.SHAPE_RECT:
            {
                String coords = selectionRegion.getRect().x+","+selectionRegion.getRect().y;
                coords += " "+(selectionRegion.getRect().x+selectionRegion.getRect().width)+","+(selectionRegion.getRect().y+selectionRegion.getRect().height);
                model.add(region,RDF.type,ImageRegions.Rectangle);
                model.add(region,ImageRegions.coords,coords);
                break;
            }
            case SelectionRegion.SHAPE_OVAL:
            {
                String coords = selectionRegion.getRect().x+","+selectionRegion.getRect().y;
                coords += " "+(selectionRegion.getRect().x+selectionRegion.getRect().width)+","+(selectionRegion.getRect().y+selectionRegion.getRect().height);

                model.add(region,RDF.type,ImageRegions.Ellipse);
                model.add(region,ImageRegions.coords,coords);
                break;
            }
            case SelectionRegion.SHAPE_POLY:
            {
                String coords = "";
                Polygon aPoly = (Polygon)selectionRegion.getRegionShape();
                for (int i = 0; i < aPoly.npoints; i++)
                    coords += aPoly.xpoints[i]+","+aPoly.ypoints[i]+" ";
                model.add(region,RDF.type,ImageRegions.Polygon);
                model.add(region,ImageRegions.coords,coords.trim());
                break;
            }
        }

        HashSet aSet = (HashSet)getRegionDepictsTable().get(selectionRegion);
        if (aSet != null) // this region depicts something!
        {
            Iterator iter = aSet.iterator();
            while (iter.hasNext()) {
                Resource inst = (Resource)iter.next();
                region.addDepicts(inst);
            }
        }

        return region;
    }

    /**
     * Returns the RDF representation of this model return inside a Jena model
     * @return Model
     */
    public Model asModel() {
        MultimediaModel model = MultimediaFactory.createMultimediaModel();

        model.add(getMetadata());

        Image image = assertImage(model);
        Iterator regions = getRegions();

        while (regions.hasNext()) {
            SelectionRegion region = (SelectionRegion) (regions.next());
            assertRegion(model, region, image);
        }

        return model;
    }

    /**
     * Returns the svg representation of a given region
     * @param region SelectionRegion the region
     * @return String the svg for the specified region
     */
    private String getSVG(SelectionRegion region) {
      StringBuffer svgStr = new StringBuffer();
      URL url = getImageURL();

      int width = 0;
      int height = 0;
      if (icon != null)
      {
          width = getIcon().getIconWidth();
          height = getIcon().getIconHeight();
      }

      svgStr.append("<svg xmlns:xlink='http://www.w3.org/1999/xlink/' xml:space=\"preserve\" width=\"" + width + "\" heigth=\"" + height +
                    "\" viewBox=\"0 0 " +  width + " " +  height + "\">" + "  ");

      // markupModel.adjustURL(url)
      svgStr.append("<image xlink:href=\"" + url + "\" x=\"0\" y=\"0\" width=\"" + width + "\" height=\"" + height + "\" /> ");

      switch (region.getType()) {
        case SelectionRegion.SHAPE_RECT:
            Rectangle r = (Rectangle)region.getRegionShape();
            svgStr.append("<rect x=\"" + r.getX() + "\" y=\"" + r.getY() +
                          "\" width=\"" + r.getWidth()+"\" height=\""+r.getHeight());
            break;
        case SelectionRegion.SHAPE_OVAL:
            Rectangle rr = (Rectangle)region.getRegionShape();
            double cx = rr.getX() + rr.getWidth()/2;
            double cy = rr.getY() + rr.getHeight()/2;
            svgStr.append("<ellipse cx=\"" + cx + "\" cy=\"" + cy +
                          "\" rx=\"" + rr.getWidth()/2 + "\" ry=\"" + rr.getHeight()/2);
          break;
        case SelectionRegion.SHAPE_POLY:
            Polygon p = (Polygon)region.getRegionShape();
            StringBuffer ptsStr = new StringBuffer();
            int[] x = p.xpoints;
            int[] y = p.ypoints;
            for (int i = 0; i < p.npoints; i++) {
                ptsStr.append(x[i] + "," + y[i] + (i == (p.npoints-1)?"":", "));
            }
            svgStr.append("<polygon points=\"" + ptsStr.toString());
            break;
         // TODO: add default case
      }

      svgStr.append("\" style=\"fill:none; stroke:yellow; stroke-width:1pt;\"/> ");
      svgStr.append("</svg>");

      return svgStr.toString();
    }

    /**
     * Return the markup model associated with this ImageModel
     * @return MarkupModel
     */
    public MarkupModel getMarkupModel() {
        return markupModel;
    }

    private void modelChanged() {
        getMarkupModel().modelChanged();
    }

    public void addRegion(MediaRegion region) {
        super.addRegion(region);
        //modelChanged();
    }

    public void addRegionDepicts(MediaRegion theRegion, Resource theInstance)
    {
        super.addRegionDepicts(theRegion,theInstance);
        getMarkupModel().mediaRegionDepictionAdded(getMediaURL(),theInstance,theRegion);
        //modelChanged();
    }

    public void addDepicts(Resource instance) {
        super.addDepicts(instance);
        getMarkupModel().mediaDepictionAdded(getMediaURL(),instance);
        //modelChanged();
    }

    public void removeDepicts(Resource instance) {
        super.removeDepicts(instance);
        getMarkupModel().mediaDepictionRemoved(getMediaURL(),instance);
        modelChanged();
    }

    public void removeRegionDepicts(MediaRegion region, Resource instance) {
        super.removeRegionDepicts(region,instance);
        getMarkupModel().mediaRegionDepictionRemoved(getMediaURL(),instance,region);
        modelChanged();
    }

    public void deselectRegion(MediaRegion region) {
        super.deselectRegion(region);
        modelChanged();
    }

    public void selectRegion(MediaRegion toSelect) {
        super.selectRegion(toSelect);
        getMarkupModel().setCurrentRegion(toSelect);
        //modelChanged();
    }

//    public void deleteRegion() {
//        super.deleteRegion();
//        modelChanged();
//    }

    public void deleteRegion(MediaRegion theRegion) {
        super.deleteRegion(theRegion);
        modelChanged();

        if (getMarkupModel().getCurrentRegion() != null && getMarkupModel().getCurrentRegion().equals(theRegion))
            getMarkupModel().setCurrentRegion(null);
        else getMarkupModel().modelChanged(MarkupModelEvent.MEDIA_CHANGED);
    }
}
