package org.mindswap.markup.media;

import java.net.URL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public interface MediaFilter {
    public boolean accept(URL theURL);

    public String getName();
    public void setName(String theName);

    public String getQuery();

    public void update();
}
