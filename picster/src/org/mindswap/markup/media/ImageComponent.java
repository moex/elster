// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup.media;

import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.AlphaComposite;

import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;

import javax.swing.JComponent;
import javax.swing.Icon;
import javax.swing.BorderFactory;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Observer;
import java.util.Observable;


/**
 * This component displays an image
 */
public class ImageComponent extends JComponent implements Observer {

    public ImageComponent() {
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        setFocusable(true);
    }

    private ImageModel imageModel;
    public ImageModel getImageModel() {
        return imageModel;
    }

    public void setImageModel(ImageModel imageModel) {
        if (this.imageModel!=null) {
            imageModel.deleteObserver(this);
        }
        this.imageModel = imageModel;
        imageModel.addObserver(this);

        repaint();
    }

    public void update(Observable o, Object arg) {
        repaint();
    }

    private Icon currentIcon;
    private Icon getIcon() {
        Icon icon = null;
        ImageModel imageModel = getImageModel();
        if (imageModel != null) {
            icon = getImageModel().getIcon();
            if (icon!=null && icon!=currentIcon) {
                currentIcon = icon;
                invalidate();
                repaint();
            }
        }
        return icon;
    }

    private Iterator getRegions() {
        Iterator regions;
        ImageModel imageModel = getImageModel();
        if (imageModel!=null) {
            regions = imageModel.getRegions();
        } else {
            regions = emptyIterator;
        }
        return regions;
    }

    public Dimension getPreferredSize() {
        Icon icon = getIcon();
        if (icon != null) {
            Insets insets = getInsets();
            float zoomFactor = getZoomFactor();
            zoomFactor = 1f;
            return new Dimension((int)(icon.getIconWidth() * zoomFactor
                                       + insets.left
                                       + insets.right),
                                 (int)(icon.getIconHeight() * zoomFactor
                                       + insets.top
                                       + insets.bottom));
        } else {
            return new Dimension(0, 0);
        }
    }


    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    private AffineTransform imageToComponent;
    private float scaleFactor;
    public AffineTransform getImageToComponent() {
        // TODO: calculate imageToComponent in reshape!?!!
        imageToComponent = null; //TODO:
        if (imageToComponent==null) {
            Icon icon = getIcon();

            // no current icon, no way to get the transformation
            if (icon == null)
                return null;

            float zoomFactor = getZoomFactor();
            Insets insets = getInsets();
            Dimension size = getSize();
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();

            Dimension actualSize = new Dimension(size.width - (insets.left + insets.right), size.height - (insets.top + insets.bottom));

            float horizontalScaleFactor = actualSize.width / (float)iconWidth;
            float verticalScaleFactor = actualSize.height / (float)iconHeight;
            scaleFactor = Math.min(horizontalScaleFactor, verticalScaleFactor);

            float xOffset = (actualSize.width - (iconWidth * scaleFactor)) / 2f;
            float yOffset = (actualSize.height - (iconHeight * scaleFactor)) / 2f;

            imageToComponent = new AffineTransform();
            imageToComponent.translate(insets.left + xOffset, insets.top + yOffset);
            imageToComponent.scale(scaleFactor, scaleFactor);

            // zoom about center of image
            imageToComponent.translate(iconWidth/2f, iconHeight/2f);
            imageToComponent.scale(zoomFactor, zoomFactor);
            imageToComponent.translate(-iconWidth/2f, -iconHeight/2f);
        }
        return imageToComponent;
    }

    private AffineTransform componentToImage;
    public AffineTransform getComponentToImage() {
        componentToImage = null; // TODO:
        if (componentToImage==null) {
            try {
                AffineTransform xform = getImageToComponent();
                if (xform != null)
                    componentToImage = xform.createInverse();
            } catch (NoninvertibleTransformException e) {
                System.err.println("TODO: fix me");
                componentToImage = null;
            }
        }
        return componentToImage;
    }

    float last = 1f;
    protected void paintComponent(Graphics g) {
        if (isOpaque()) { //paint background
            g.setColor(getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        Icon icon = getIcon();

        if (icon != null) {
            float current = getZoomFactor();
            if (last!=current) {
                last = current;
                revalidate();
                repaint();
            }
            AffineTransform imageToComponent = getImageToComponent();
            int iconWidth = icon.getIconWidth();
            int iconHeight = icon.getIconHeight();

            Graphics2D g2d = (Graphics2D)g.create();

            g2d.transform(imageToComponent);

            Rectangle iconRect = new Rectangle();
            Rectangle clipRect = new Rectangle();
            g.getClipBounds(clipRect);
            iconRect.setBounds(0, 0, iconWidth, iconHeight);
            if (iconRect.intersects(clipRect)) {
                icon.paintIcon(this, g2d, 0, 0);
            }

            if (iconWidth == -1 || iconHeight == -1) {
                g.setColor(java.awt.Color.black);
                g.setFont(new java.awt.Font("Times",java.awt.Font.PLAIN,16));
                String aStr = "Loading...";
                java.awt.FontMetrics fm = this.getFontMetrics(g.getFont());
                g.drawString(aStr,(getWidth()/2)-fm.stringWidth(aStr),getHeight()/2 - fm.getHeight());
                return;
            }

            float width = 2 / scaleFactor;
            g2d.setStroke(new BasicStroke(width));

            Iterator regions;
            g2d.setColor(Color.blue);
            g2d.setColor(Color.black);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
            regions = getRegions();
            while (regions.hasNext()) {
                MediaRegion region = (MediaRegion) (regions.next());
                if (region.isSelected()==true) {
                    g2d.setColor(Color.white);
                } else {
                    g2d.setColor(Color.black);
                }
                region.fill(g2d); // draw the inside of the region
                if (region.isHover()) {
                    g2d.setColor(Color.yellow);
                    region.fill(g2d);
                }
            }
            g2d.setColor(Color.green);
            g2d.setComposite(AlphaComposite.Clear);
            regions = getRegions();
            while (regions.hasNext()) {
                MediaRegion region = (MediaRegion) (regions.next());
                // draw the outline of the region
                // diff outlines for each state?
                region.draw(g2d);
            }

            g2d.dispose();
        }
    }

    private float zoomFactor = 1.0f;
    /**
     * Returns the current image zoom factor
     * @return float
     */
    public float getZoomFactor() {
        return zoomFactor;
    }
    /**
     * Sets the current image zoom factor
     * @param zoomFactor float
     */
    public void setZoomFactor(float zoomFactor) {
        this.zoomFactor = zoomFactor;
    }

    private static class EmptyIterator implements Iterator {

        EmptyIterator() {
        }

        public boolean hasNext() {
            return false;
        }

        public Object next() {
            throw new NoSuchElementException("Empty Iterator");
        }

        public void remove() {
            throw new IllegalStateException("Empty Iterator");
        }

    }
    private static EmptyIterator emptyIterator = new EmptyIterator();


}
