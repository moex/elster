package org.mindswap.markup.media;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

import java.net.URL;

import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaFilterImpl implements MediaFilter {
    private Set mFilterSet;
    private String mQuery;
    private String mName;
    private MarkupModel mModel;

    public MediaFilterImpl(MarkupModel theModel, String theQuery) {
        this(theModel,theQuery, new HashSet());
    }

    public MediaFilterImpl(MarkupModel theModel, String theQuery, Set theFilterSet) {
        mModel = theModel;
        mQuery = theQuery;
        mFilterSet = theFilterSet;
        mName = null;
    }

    public boolean accept(URL theURL) {
        Iterator aIter = mFilterSet.iterator();
        while (aIter.hasNext())
        {
            Resource aRes = (Resource)aIter.next();
            if (!aRes.isAnon() && aRes.getURI().equals(theURL.toString()))
                return true;
        }
        return false;
    }

    public String getName() {
        return mName;
    }

    public void setName(String theName) {
        mName = theName;
    }

    public void update()
    {
        try {
            // TODO: un-dupe this code, a similar snippet is used in the search panel
            com.hp.hpl.jena.query.Query aQ = com.hp.hpl.jena.query.QueryFactory.create(mQuery);

            // TODO: issue this query against the db instead of doing it locally when the capability is there
            com.hp.hpl.jena.query.QueryExecution aQE = com.hp.hpl.jena.query.QueryExecutionFactory.create(aQ,mModel.getCurrentStore().asModel());

            com.hp.hpl.jena.query.ResultSet aResultSet = aQE.execSelect();

            mFilterSet.clear();
            while (aResultSet.hasNext())
            {
                com.hp.hpl.jena.query.QuerySolution aSolution = aResultSet.nextSolution();
                Resource aRes = (Resource)aSolution.get("uri");
                mFilterSet.add(aRes);
            }
            aQE.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String toString() {
        return getName();
    }

    public String getQuery() {
        return mQuery;
    }
}
