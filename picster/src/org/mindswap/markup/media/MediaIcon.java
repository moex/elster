package org.mindswap.markup.media;

import javax.swing.JPanel;

import java.awt.Image;

import java.net.URL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Mindswap (http://www.mindswap.org) Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public interface MediaIcon {
    public void addMediaIconListener(MediaIconListener theListener);
    public void removeMediaIconListener(MediaIconListener theListener);

    public boolean isSelected();
    public void setSelected(boolean theSelection);

    public Image getIcon();
    public URL getMediaURL();

    public JPanel getControl();
}
