package org.mindswap.markup.media;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.DefaultListModel;
import javax.swing.DefaultListCellRenderer;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Component;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;

import java.util.Iterator;

import com.hp.hpl.jena.rdf.model.Resource;

import org.mindswap.utils.JenaUtils;

import org.mindswap.markup.MarkupModelEvent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaInfoPanel extends JPanel implements ListSelectionListener {
    private static final String IMAGE = "Image";

    private MediaMarkupModel mModel;

    private DefaultListModel mRegionListModel;
    private JList mMediaRegionList;

    private JList mMediaDepictsList;
    private DefaultListModel mDepictsListModel;

    private MediaModel mMediaModel;
    private Resource mMediaInst;

    private boolean mIsUpdating = false;

    public MediaInfoPanel(MediaMarkupModel theModel) {
        mModel = theModel;

        initGUI();
    }

    public void update()
    {
        mIsUpdating = true;

        if (mModel.getCurrentStore() != null)
        {
            if (mModel.getMediaURL() != null)
            {
                removeAll();

                setLayout(new GridBagLayout());

                add(new JScrollPane(mMediaRegionList), new GridBagConstraints(0,0,1,1,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(10,5,10,5),0,0));
                add(new JScrollPane(mMediaDepictsList),new GridBagConstraints(1,0,1,1,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(10,5,10,5),0,0));

                boolean aImageDepicts = mMediaRegionList.getSelectedIndex() == -1 ? false : mMediaRegionList.getSelectedValue().equals(IMAGE);

                mRegionListModel.clear();
                mDepictsListModel.clear();

                try {
                    mMediaInst = mModel.getCurrentStore().asModel().getResource(mModel.getMediaURL().toString());
                    //by arun
                    //System.out.println("MEDIA URL = " + mModel.getMediaURL().toString());
                    //System.out.println("mMedia INSTANCE URI = " + mMediaInst.getURI());
                    //System.out.println("mMedia INSTANCE LocalName = " + mMediaInst.getLocalName());
                    //arun ends
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }

                mMediaModel = mModel.getMediaModel(mModel.getMediaURL());

                mRegionListModel.addElement(IMAGE);
                if (aImageDepicts)
                {
                    mMediaRegionList.setSelectedValue(IMAGE,true);
                    Iterator aIter = mMediaModel.getDepictedInstances();
                    while (aIter.hasNext())
                    {
                        Resource aInst = (Resource)aIter.next();
                        mDepictsListModel.addElement(aInst);
                        //by arun
                        //System.out.println("IMAGE DEPICTS");                        
                        //System.out.println("aa INSTANCE URI = " + aInst.getURI());
                        //System.out.println("aa INSTANCE LocalName = " + aInst.getLocalName());
                        //arun Ends
                    }
                }

                Iterator aIter = mMediaModel.getRegions();
                while (aIter.hasNext())
                {
                    MediaRegion aRegion = (MediaRegion)aIter.next();

                    mRegionListModel.addElement(aRegion);

                    if (aRegion.equals(mModel.getCurrentRegion()))
                    {
                        mMediaRegionList.setSelectedValue(aRegion,true);

                        Iterator dIter = mMediaModel.getDepictedInstances(aRegion);
                        while (dIter.hasNext())
                        {
                            Resource aInst = (Resource)dIter.next();
                            mDepictsListModel.addElement(aInst);
//                          by arun
                            //System.out.println("GET REGIONS");                            
                            //System.out.println("a INSTANCE URI = " + aInst.getURI());
                            //System.out.println("a INSTANCE LocalName = " + aInst.getLocalName());
                            //arun Ends
                        }
                    }
                }

                String aLabel = JenaUtils.getLabel(mMediaInst);
                if (aLabel == null)
                    aLabel = mMediaInst.getLocalName();

                setBorder(BorderFactory.createTitledBorder(aLabel));
            }
            else if (mModel.getMediaURL() == null)
            {
                removeAll();

                setBorder(BorderFactory.createEtchedBorder());
                setLayout(new BorderLayout());
                add(new JLabel("No Current Media"),BorderLayout.CENTER);
            }
        }

        revalidate();
        repaint();

        mIsUpdating = false;
    }

    private void initGUI()
    {
        Resource aDefaultCellValue = com.hp.hpl.jena.rdf.model.ModelFactory.createDefaultModel().getResource("FirstName_LastName");

        mRegionListModel = new DefaultListModel();

        mMediaRegionList = new JList();
        mMediaRegionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mMediaRegionList.setPrototypeCellValue(aDefaultCellValue);
        mMediaRegionList.setModel(mRegionListModel);
        mMediaRegionList.addListSelectionListener(this);
        mMediaRegionList.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent theEvent) {
                switch (theEvent.getKeyCode())
                {
                    case KeyEvent.VK_DELETE:
                    case KeyEvent.VK_BACK_SPACE:
                        if (mMediaRegionList.getSelectedIndex() != -1)
                        {
                            if (!mMediaRegionList.getSelectedValue().equals(IMAGE))
                            {
                                int option = JOptionPane.showConfirmDialog(mMediaRegionList,"Delete this region?", "Delete Region", JOptionPane.YES_NO_OPTION);
                                if (option == JOptionPane.OK_OPTION)
                                {
                                    MediaRegion aRegion = (MediaRegion)mMediaRegionList.getSelectedValue();
                                    mMediaModel.deleteRegion(aRegion);
                                }
                            }
                        }
                        break;
                }
            }
        });

        mDepictsListModel = new DefaultListModel();

        mMediaDepictsList = new JList();
        mMediaDepictsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mMediaDepictsList.setPrototypeCellValue(aDefaultCellValue);
        mMediaDepictsList.addListSelectionListener(this);
        mMediaDepictsList.setModel(mDepictsListModel);
        mMediaDepictsList.setCellRenderer(new ResourceListCellRenderer());
        mMediaDepictsList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent theEvent) {
                if (theEvent.getClickCount() == 2) {
                    Resource aRes = (Resource)mDepictsListModel.elementAt(mMediaDepictsList.locationToIndex(theEvent.getPoint()));
                    Resource bRes = (Resource) mModel.getInstances().get(aRes.getURI());
                    mModel.setCurrentInstance(bRes);                    
                    mModel.formChanged();
                    mModel.formSelected();
                }
            }
        });
        mMediaDepictsList.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent theEvent) {
                switch (theEvent.getKeyCode())
                {
                    case KeyEvent.VK_DELETE:
                    case KeyEvent.VK_BACK_SPACE:
                        if (mMediaDepictsList.getSelectedIndex() != -1)
                        {
                            Resource aInst = (Resource)mMediaDepictsList.getSelectedValue();

                            int option = JOptionPane.showConfirmDialog(mMediaDepictsList,"Delete this depiction?", "Delete Depiction", JOptionPane.YES_NO_OPTION);
                            if (option == JOptionPane.OK_OPTION)
                            {
                                if (mMediaRegionList.getSelectedValue().equals(IMAGE))
                                    mMediaModel.removeDepicts(aInst);
                                else mMediaModel.removeRegionDepicts((MediaRegion)mMediaRegionList.getSelectedValue(),aInst);
                            }
                        }
                        break;
                    case KeyEvent.VK_ENTER:
                        Resource aRes = (Resource)mMediaDepictsList.getSelectedValue();
                        mModel.setCurrentInstance(aRes);
                        mModel.formChanged();
                        break;
                }
            }
        });

        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new BorderLayout());
        add(new JLabel("No Current Media"),BorderLayout.CENTER);
    }

    public void valueChanged(ListSelectionEvent theEvent)
    {
        if (mIsUpdating)
            return;

        if (theEvent.getSource().equals(mMediaRegionList))
        {
            Object aSelectedValue = mMediaRegionList.getSelectedValue();

            if (aSelectedValue != null) {
                if (aSelectedValue.equals(IMAGE)) {
                    //if (mModel.getCurrentRegion() != null) {
                        mModel.setCurrentRegion(null);
                    //}
                }
                else {
                    MediaRegion aRegion = (MediaRegion)aSelectedValue;
                    mMediaModel.selectRegion(aRegion);
                    //mModel.regionSelected();
                    mModel.setCurrentRegion(aRegion);
                }

                mMediaRegionList.requestFocus();
                //mModel.modelChanged(MarkupModelEvent.MEDIA_CHANGED);
            }

            revalidate();
            repaint();
        }
        else if (theEvent.getSource().equals(mMediaDepictsList))
        {
            Object aSelectedValue = mMediaDepictsList.getSelectedValue();
        }
    }

    private class ResourceListCellRenderer extends DefaultListCellRenderer
    {
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
        {
            super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);

            Resource aRes = (Resource)value;

            String aLabel = JenaUtils.getLabel(aRes);
            if (aLabel == null)
                aLabel = aRes.getLocalName();

            setText(aLabel);

            return this;
        }
    }
}
