package org.mindswap.markup.media;

import javax.swing.JSeparator;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import java.awt.Point;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.HashSet;

import java.io.IOException;

import java.net.URL;

import org.mindswap.store.Store;

import org.mindswap.markup.MarkupModelEvent;

import org.mindswap.utils.BasicUtils;

import com.hp.hpl.jena.rdf.model.Resource;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaListPanel extends JPanel implements MediaIconListener, ActionListener {
    private static final String CMD_REMOVE_FILTER = "CMD_REMOVE_FILTER";
    private static final String CMD_SAVE_FILTER = "CMD_SAVE_FILTER";
    private static final String CMD_APPLY_FILTER = "CMD_APPLY_FILTER";
    private static final String CMD_DELETE_FILTER = "CMD_DELETE_FILTER";
    private static final String CMD_SELECT_ALL = "CMD_SELECT_ALL";

    private MediaMarkupModel mMarkupModel;

    private LinkedHashMap mIconMap;

    private Store mStore;

    private LinkedHashMap mSelectedIconMap;
    //private MediaIcon mCurrIcon;

    private MediaFilter mFilter;

    private JPanel mPanel;

    private JPopupMenu mPopup;

    public MediaListPanel(MediaMarkupModel theModel) {
        mMarkupModel = theModel;

        mIconMap = new LinkedHashMap();

        mStore = mMarkupModel.getCurrentStore();

        mSelectedIconMap = new LinkedHashMap();

        initGUI();
    }

    private void initGUI()
    {
        mPanel = new JPanel();
        mPanel.addMouseListener(new IconMouseAdapter());

        setBorder(BorderFactory.createEtchedBorder());

        setLayout(new BorderLayout());
        addMouseListener(new IconMouseAdapter());

        mStore = null;

        update();

        new DropTarget(this, new MediaListDropTargetListener(this));

        mPopup = new JPopupMenu();
    }

    private void updatePopup()
    {
        mPopup.removeAll();

        JMenu aApplyItem = new JMenu("Apply Filter");
        Iterator aIter = mMarkupModel.getFilters();
        while (aIter.hasNext())
        {
            MediaFilter aFilter = (MediaFilter)aIter.next();

            JMenuItem aFilterItem = new JMenuItem(aFilter.getName());
            aFilterItem.addActionListener(this);
            aFilterItem.setActionCommand(CMD_APPLY_FILTER+"-"+aFilter.getName());

            aApplyItem.add(aFilterItem);
        }

        JMenuItem aRemoveItem = new JMenuItem("Remove Current Filter");
        aRemoveItem.addActionListener(this);
        aRemoveItem.setActionCommand(CMD_REMOVE_FILTER);
        aRemoveItem.setEnabled(mFilter != null);

        JMenuItem aSaveItem = new JMenuItem("Save Filter...");
        aSaveItem.addActionListener(this);
        aSaveItem.setActionCommand(CMD_SAVE_FILTER);
        aSaveItem.setEnabled(mFilter != null);

        JMenuItem aDeleteItem = new JMenuItem("Delete Filter...");
        aDeleteItem.addActionListener(this);
        aDeleteItem.setActionCommand(CMD_DELETE_FILTER);

        JMenuItem aSelectAllItem = new JMenuItem("Select All");
        aSelectAllItem.addActionListener(this);
        aSelectAllItem.setActionCommand(CMD_SELECT_ALL);

        mPopup.add(aSelectAllItem);
        mPopup.add(new JSeparator());
        mPopup.add(aApplyItem);
        mPopup.add(aSaveItem);
        mPopup.add(aRemoveItem);
        mPopup.add(aDeleteItem);
    }

    public void actionPerformed(ActionEvent theEvent) {
        String aCommand = theEvent.getActionCommand();
        if (aCommand.equals(CMD_REMOVE_FILTER))
        {
            mFilter = null;
            update();
        }
        else if (aCommand.equals(CMD_SAVE_FILTER)) {
            doSaveFilter();
        }
        else if (aCommand.startsWith(CMD_APPLY_FILTER)) {
            doApplyFilter(aCommand.substring(aCommand.indexOf("-")+1));
        }
        else if (aCommand.equals(CMD_DELETE_FILTER)) {
            doDeleteFilter();
        }
        else if (aCommand.equals(CMD_SELECT_ALL)) {
            doSelectAll();
        }
    }

    private void doDeleteFilter() {
        Iterator fIter = mMarkupModel.getFilters();

        if (!fIter.hasNext())
        {
            JOptionPane.showMessageDialog(this,"No Filters to delete!","Delete Filter",JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());

        JComboBox filterList = new JComboBox(new java.util.Vector(BasicUtils.collectElements(fIter)));

        contentPane.add(new JLabel("Filter to remove:"), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        contentPane.add(filterList, new GridBagConstraints(1, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

        int option = JOptionPane.showConfirmDialog(this, contentPane, "Remove Filter", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (option == JOptionPane.OK_OPTION)
            mMarkupModel.removeFilter((MediaFilter)filterList.getSelectedItem());
    }

    private void doSelectAll()
    {
        deselectIcons();

        Set aMediaList = mMarkupModel.getCurrentMedia();

        Iterator aIter = aMediaList.iterator();
        while (aIter.hasNext())
        {
            Resource aRes = (Resource)aIter.next();
            try {
                URL aURL = new URL((String)MarkupImageModel.getUriMap().get(aRes.getURI()));

                if (mFilter != null && !mFilter.accept(aURL)) {
                    continue;
                }

                if (!mIconMap.containsKey(aURL))
                {
                    // TODO: need some factory method or something for when its not just images
                    MediaIcon aIcon = new org.mindswap.markup.media.impl.ImageMediaIconImpl(aURL);
                    aIcon.addMediaIconListener(this);
                    aIcon.getControl().addMouseListener(new IconMouseAdapter());
                    mIconMap.put(aURL,aIcon);
                }

                MediaIcon aIcon = (MediaIcon)mIconMap.get(aURL);
                selectIcon(aIcon,false);
                mMarkupModel.addMediaURL(aIcon.getMediaURL());
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void doSaveFilter() {
        if (mFilter.getName() == null) {
            String aName = JOptionPane.showInputDialog(this,"Enter a name for this filter", "Save Filter",JOptionPane.PLAIN_MESSAGE);
            if (aName != null) {
                mFilter.setName(aName);
            }
            else return;
        }

        if (!hasFilter(mFilter.getName()))
            mMarkupModel.addFilter(mFilter);
        else {
            JOptionPane.showMessageDialog(this,"Filter with that name already exists, cannot save","Save Filter",JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean hasFilter(String theName)
    {
        Iterator aIter = mMarkupModel.getFilters();
        while (aIter.hasNext())
        {
            MediaFilter aFilter = (MediaFilter)aIter.next();
            if (aFilter.getName().equals(theName))
                return true;
        }
        return false;
    }

    private void doApplyFilter(String theFilterName) {
        Iterator aIter = mMarkupModel.getFilters();
        while (aIter.hasNext())
        {
            MediaFilter aFilter = (MediaFilter)aIter.next();
            if (aFilter.getName().equals(theFilterName)) {
                mMarkupModel.modelChanged(MarkupModelEvent.START_WAIT_EVENT);
                aFilter.update();
                mFilter = aFilter;
                update();
                mMarkupModel.modelChanged(MarkupModelEvent.END_WAIT_EVENT);
            }
        }
    }

    public void update()
    {
        mStore = mMarkupModel.getCurrentStore();
        updateIconList();

        if (mMarkupModel.getMediaURL() != null)// && (mCurrIcon == null || !mCurrIcon.getMediaURL().equals(mMarkupModel.getMediaURL())))
        {
            MediaIcon aIcon = (MediaIcon)mIconMap.get(mMarkupModel.getMediaURL());
            selectIcon(aIcon,true);
        }
        else deselectIcons();

        // TODO: dont scroll if icon didnt change
        if (mSelectedIconMap.size() > 0)
        {
            MediaIcon aIcon = (MediaIcon)mSelectedIconMap.values().iterator().next();

            Rectangle aRect = aIcon.getControl().getBounds();

            //aRect.setLocation((int)(aRect.getX()-(mCurrIcon.getControl().getWidth()*3)),(int)aRect.getY());

            // TODO: rely on java instead of working around their possible bug
            // compute the visible rect for the scrollable area by hand, their rect is incorrect
            // TODO: center this in the visible rect
            Rectangle aViewRect = new Rectangle((int)mPanel.getVisibleRect().getX(),0,(int)this.getVisibleRect().getWidth(),(int)this.getVisibleRect().getHeight());

            if (!aViewRect.intersects(aRect))
                mPanel.scrollRectToVisible(aRect);
        }

        revalidate();
        repaint();
    }

    private void deselectIcons()
    {
        Iterator aIter = mSelectedIconMap.values().iterator();
        while (aIter.hasNext())
        {
            MediaIcon aIcon = (MediaIcon)aIter.next();
            aIcon.setSelected(false);
        }
    }

    public void mediaSelected(URL theURL, boolean theIsControlDown) {
        if (mIconMap.containsKey(theURL)) {
            MediaIcon aIcon = (MediaIcon)mIconMap.get(theURL);

            if (!theIsControlDown)
            {
                selectIcon(aIcon,true);

                mMarkupModel.setMediaURL(theURL);
            }
            else
            {
                if (aIcon.isSelected())
                {
                    // clicking an already selected icon, ie deselect
                    mSelectedIconMap.remove(aIcon.getMediaURL());
                    aIcon.setSelected(false);

                    mMarkupModel.removeMediaURL(aIcon.getMediaURL());

                    if (mSelectedIconMap.size() == 0)
                        mMarkupModel.setMediaURL(null);
                }
                else
                {
                    selectIcon(aIcon,false);

                    mMarkupModel.addMediaURL(aIcon.getMediaURL());

                    //if (mSelectedIconMap.size() > 1)
                        //mMarkupModel.setMediaURL(null);
                }
            }
        }
    }

    private void selectIcon(MediaIcon theIcon, boolean deselectPrevious)
    {
        if (theIcon == null)
            return;

        if (deselectPrevious) {
            deselectIcons();
            mSelectedIconMap.clear();
        }

        mSelectedIconMap.put(theIcon.getMediaURL(),theIcon);
        theIcon.setSelected(true);
    }

    private void updateIconList()
    {
        if (mStore != null)
        {
            removeAll();
            Set aMediaList = mMarkupModel.getCurrentMedia();

            if (aMediaList.size() == 0)
            {
                setLayout(new BorderLayout());
                add(new JLabel("No Media Found"),BorderLayout.CENTER);

                revalidate();
                repaint();
                return;
            }

            //JPanel aPanel = new JPanel();
            //mPanel = new JPanel();
            mPanel.removeAll();
            mPanel.setLayout(new GridBagLayout());

            int aIndex = 0;
            Iterator aIter = aMediaList.iterator();
            while (aIter.hasNext())
            {
                Resource aRes = (Resource)aIter.next();
                try {
                    URL aURL = new URL((String)MarkupImageModel.getUriMap().get(aRes.getURI()));

                    if (mFilter != null && !mFilter.accept(aURL)) {
                        continue;
                    }

                    if (!mIconMap.containsKey(aURL))
                    {
                        // TODO: need some factory method or something for when its not just images
                        MediaIcon aIcon = new org.mindswap.markup.media.impl.ImageMediaIconImpl(aURL);
                        aIcon.addMediaIconListener(this);
                        aIcon.getControl().addMouseListener(new IconMouseAdapter());
                        mIconMap.put(aURL,aIcon);
                    }

                    MediaIcon aIcon = (MediaIcon)mIconMap.get(aURL);
                    mPanel.add(aIcon.getControl(),new GridBagConstraints(aIndex,0,1,1,0,1,GridBagConstraints.WEST,GridBagConstraints.NONE,new Insets(0,0,0,0),0,0));
                    aIndex++;
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            setLayout(new BorderLayout());
            add(new JScrollPane(mPanel));
        }
        else
        {
            removeAll();
            mStore = null;
            setLayout(new BorderLayout());
            add(new JLabel("No Current Store"),BorderLayout.CENTER);
        }
    }

    public void setFilter(MediaFilter theFilter) {
        mFilter = theFilter;
    }

    private class MediaListDropTargetListener extends DropTargetAdapter
    {
        private MediaListPanel mMediaList;
        public MediaListDropTargetListener(MediaListPanel theMediaList)
        {
            mMediaList = theMediaList;
        }

        public void drop(DropTargetDropEvent dtde){
            Transferable t = dtde.getTransferable();
            String s = null;
            try {
                s = (String) (t.getTransferData(DataFlavor.stringFlavor));
            } catch (UnsupportedFlavorException ufe) {
                throw new RuntimeException("NYI");
            } catch (IOException ioe) {
                throw new RuntimeException("NYI");
            }

            if (s != null && !s.equals(""))
            {
                int aX = (int)mMediaList.getVisibleRect().getX() + (int)dtde.getLocation().getX();
                int aY = (int)dtde.getLocation().getY();

                try {
                    // ok this crazy access line is to get around the nesting of the UI components
                    // top level component is the jscrollpane, next is the viewport, next is the jpanel
                    // that wraps the list, and last is the media icon's.  we want to find which media
                    // icon the point corresponds with.
                    //JComponent aComponent = (JComponent)((JComponent)((JComponent)mMediaList.getComponent(0)).getComponent(0)).getComponent(0).getComponentAt(aX,aY);
                    JComponent aComponent = (JComponent)javax.swing.SwingUtilities.getDeepestComponentAt(mMediaList,aX,aY);

                    // ok, so now that we have the ui for the icon where the drop took place
                    // lets find the corresponding icon for that UI element
                    Iterator aIter = mIconMap.values().iterator();
                    while (aIter.hasNext())
                    {
                        MediaIcon aIcon = (MediaIcon)aIter.next();
                        if (aIcon.getControl().equals(aComponent))
                        {
                            if (mMarkupModel.getInstance(s) != null) {
                                // this is already an instance we know about, lets just create the depiction
                                Resource aInst = mMarkupModel.getInstance(s);

                                // dropping to selected media, do depicts for all selected media
                                if (mSelectedIconMap.containsKey(aIcon.getMediaURL()))
                                {
                                    Iterator aIconIter = mSelectedIconMap.values().iterator();
                                    while (aIconIter.hasNext())
                                    {
                                        MediaIcon aSelectedIcon = (MediaIcon)aIconIter.next();

                                        MediaModel aMediaModel = mMarkupModel.getMediaModel(aSelectedIcon.getMediaURL());
                                        aMediaModel.addDepicts(aInst);
                                    }
                                }
                                else
                                {
                                    // dropping to media that is not currently selected
                                    // so lets deselect everything else, select this and specify the depicts
                                    selectIcon(aIcon,true);
                                    mMarkupModel.setMediaURL(aIcon.getMediaURL());

                                    MediaModel aMediaModel = mMarkupModel.getMediaModel(aIcon.getMediaURL());
                                    aMediaModel.addDepicts(aInst);
                                }
                            }
                            else {
                                // drag was a class of some type, lets create an instance of it
                                // set the pending depictions and open the new instance in the form
                                Resource aRes = mMarkupModel.createInstance(null,s);

                                // multiple icons are selected and this is one of them
                                // specify the depicts for all selected media
                                if (mSelectedIconMap.containsKey(aIcon.getMediaURL()))
                                {
                                    Iterator aIconIter = mSelectedIconMap.values().iterator();
                                    while (aIconIter.hasNext())
                                    {
                                        MediaIcon aSelectedIcon = (MediaIcon)aIconIter.next();
                                        MediaModel aMediaModel = mMarkupModel.getMediaModel(aSelectedIcon.getMediaURL());

                                        aMediaModel.setPendingDepiction();
                                        aMediaModel.setCurrentRegion(null);
                                    }
                                }
                                else
                                {
                                    // dropping to media that is not currently selected
                                    // so lets deselect everything else, select this and specify the depicts
                                    selectIcon(aIcon,true);
                                    mMarkupModel.setMediaURL(aIcon.getMediaURL());

                                    MediaModel aMediaModel = mMarkupModel.getMediaModel(aIcon.getMediaURL());

                                    aMediaModel.setPendingDepiction();
                                    aMediaModel.setCurrentRegion(null);
                                }

                                mMarkupModel.setCurrentRegion(null);
                                mMarkupModel.setCurrentInstance(aRes);

                                mMarkupModel.formChanged();
                            }

                            dtde.acceptDrop(java.awt.dnd.DnDConstants.ACTION_COPY_OR_MOVE);
                            mMarkupModel.modelChanged();
                        }
                    }
                }
                catch (Exception ex) {
                    // drop took place outside of an icon most likely
                    dtde.rejectDrop();
                }
            }
            else {
                dtde.rejectDrop();
            }

            dtde.dropComplete(true);
        }
    }

    private class IconMouseAdapter extends MouseAdapter
    {
        public void mouseClicked(MouseEvent theEvent) {
            if (SwingUtilities.isRightMouseButton(theEvent)) {
                updatePopup();
                //((JComponent)theEvent.getSource()).getBounds().getX()
                mPopup.show((JComponent)theEvent.getSource(),(int)theEvent.getX(),theEvent.getY());
            }
        }
    }
}
