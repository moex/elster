// The MIT License
//
// Copyright (c) 2004 Michael Grove, Daniel Krech, Chris Halaschek
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package org.mindswap.markup.media;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.Image;

import java.net.URL;

public abstract class ImageModel extends MediaModel {

    // consts for the different image tools
    public static final int SELECT = 0;
    public static final int RECTANGLE_SELECT = 1;
    public static final int OVAL_TOOL = 2;
    public static final int POLYGON_TOOL = 3;

    public ImageModel() {
        super();
    }

    /**
     * Returns the URL of the image this model represents
     * @return URL
     */
    public URL getImageURL() {
        return getMediaURL();
    }
    /**
     * Sets the URL of the image this model represents
     * @param theURL URL
     */
    public void setImageURL(URL theURL) {
        setMediaURL(theURL);
        // TODO: if you change the url, shouldnt you change the icon?
    }

    protected ImageIcon icon;
    /**
     * Returns the ImageIcon this model represents
     * @return Icon
     */
    public ImageIcon getIcon() {
        if (icon == null) {
            URL url = getImageURL();
            if (url!=null) {
                icon = new ImageIcon(url);
                icon = new ImageIcon();

                Thread aLoadThread = new Thread() {
                    public void run() {
                        Image aImage = java.awt.Toolkit.getDefaultToolkit().createImage(getImageURL());

                        icon.setImage(aImage);
                    }
                };
                aLoadThread.setPriority(Thread.MIN_PRIORITY);
                aLoadThread.setDaemon(true);
                javax.swing.SwingUtilities.invokeLater(aLoadThread);
            }
        }
        return icon;
    }

    protected void setIcon(ImageIcon icon) {
        this.icon = icon;
    }
}
