package org.mindswap.markup.media;

import org.mindswap.component.ScrollableImageComponent;

import org.mindswap.markup.MarkupToolBar;
import org.mindswap.markup.DefaultImageListener;
import org.mindswap.markup.MarkupImageDropTargetListener;
import org.mindswap.markup.ImagePopup;
import fr.inrialpes.exmo.elster.picster.markup.Razor;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JToolTip;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import java.awt.dnd.DropTarget;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseMotionAdapter;

import java.net.URL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public class MediaImageComponent implements MediaComponent, ActionListener {
    private MediaMarkupModel mModel;
    private MarkupToolBar mToolBar;
    private DefaultImageListener mListener;
    private ScrollableImageComponent mImageComponent;
    private ImagePopup mImageComponentPopup;

    private JPanel mControl;

    public MediaImageComponent(Razor theApp) {
        super();

        mModel = theApp.getMarkupModel();

        initToolBar();
        initGUI();

        new DropTarget(mImageComponent, new MarkupImageDropTargetListener(theApp,mImageComponent));

        mImageComponentPopup = new ImagePopup(theApp,mImageComponent);
    }

    public void update()
    {
        mImageComponentPopup.update();

        mImageComponent.revalidate();
        mImageComponent.repaint();

        mControl.revalidate();
        mControl.repaint();
    }

    private void initGUI()
    {
        mControl = new JPanel();
        mControl.setLayout(new BorderLayout());
        mControl.setBorder(BorderFactory.createEtchedBorder());

        Thread aThread = new Thread() {
            public void run() {
                while (true) {
                    mControl.repaint();
                    try {
                        Thread.sleep(500);
                    }
                    catch (Exception ex)  {
                        ex.printStackTrace();
                    }
                }
            }
        };
        aThread.setPriority(Thread.MIN_PRIORITY);
        aThread.setDaemon(true);
        aThread.start();

        mImageComponent = new ScrollableImageComponent() {
            public JToolTip createToolTip() {
                org.mindswap.component.MultiLineToolTip tt = new org.mindswap.component.MultiLineToolTip();
                tt.setComponent(this);
                return tt;
            }
        };
        
        MouseMotionListener doScrollRectToVisible = new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
            	//System.out.println("IN MOUSE GRAGGED");
               Rectangle r = new Rectangle(e.getX(), e.getY(), 1, 1);
               ((JComponent)e.getSource()).scrollRectToVisible(r);
           }
        };

        mImageComponent.setPreferredScrollableViewportSize(new Dimension(320, 280));

        // this handles the tool tip stuff
        MediaComponentMouseListener aListener = new MediaComponentMouseListener(this);
        mImageComponent.addMouseMotionListener(aListener);
        mImageComponent.addMouseListener(aListener);
        
        mImageComponent.setAutoscrolls(true);
        mImageComponent.addMouseMotionListener(doScrollRectToVisible);

        // this listener handles the tool stuff for drawing regions and such
        mListener = new DefaultImageListener(mImageComponent);

        mImageComponent.addKeyListener(mListener);
        mImageComponent.addMouseListener(mListener);
        mImageComponent.addMouseMotionListener(mListener);

        mControl.add(mToolBar,BorderLayout.NORTH);
        mControl.add(mImageComponent,BorderLayout.CENTER);
    }

    private void initToolBar() {
        mToolBar = new MarkupToolBar();
        mToolBar.addActionListener(this);
    }

    public MediaMarkupModel getMediaMarkupModel() {
        return mModel;
    }

    public JComponent getControl() {
        return mControl;
    }

    public void displayMedia(URL theMediaURL) {
        // TODO: should we insert a check to make sure that the current media
        // model is really an image model, or will this only get called when its appropriate (up to App?)
        mImageComponent.setImageModel((ImageModel)mModel.getMediaModel(theMediaURL));

        mImageComponent.getImageModel().getIcon();

//        if (mImageComponent.getImageModel().getIcon() != null)
//            System.err.println(mImageComponent.getImageModel().getIcon().getIconWidth());
//
//        while (mImageComponent.getImageModel().getIcon() != null && mImageComponent.getImageModel().getIcon().getIconWidth() == -1)
//        {
//            try { Thread.sleep(1000); } catch (Exception ex) { ex.printStackTrace(); }
//            System.err.println("waiting "+mImageComponent.getImageModel().getIcon().getIconWidth());
//        }
    }

    public void actionPerformed(ActionEvent theEvent)
    {
        String aCommand = theEvent.getActionCommand();

        if (aCommand.equals(MarkupToolBar.SELECT_CMD)) {
            mListener.setMode(ImageModel.SELECT);
            mImageComponent.requestFocus();
        } else if (aCommand.equals(MarkupToolBar.DRAW_RECT_CMD)) {
            mListener.setMode(ImageModel.RECTANGLE_SELECT);
            mImageComponent.requestFocus();
        } else if (aCommand.equals(MarkupToolBar.DRAW_OVAL_CMD)) {
            mListener.setMode(ImageModel.OVAL_TOOL);
            mImageComponent.requestFocus();
        } else if (aCommand.equals(MarkupToolBar.DRAW_POLY_CMD)) {
            mListener.setMode(ImageModel.POLYGON_TOOL);
            mImageComponent.requestFocus();
        } else if (aCommand.equals(MarkupToolBar.ZOOM_IN_CMD)) {
            float zoomFactor = mImageComponent.getZoomFactor();
            mImageComponent.setZoomFactor(1.2f*zoomFactor);
            mImageComponent.requestFocus();
            mImageComponent.repaint();
        } else if (aCommand.equals(MarkupToolBar.ZOOM_OUT_CMD)) {
            float zoomFactor = mImageComponent.getZoomFactor();
            mImageComponent.setZoomFactor(0.8f*zoomFactor);
            mImageComponent.requestFocus();
            mImageComponent.repaint();
        }
    }

    public MediaModel getMediaModel() {
        return mImageComponent.getImageModel();
    }

    public ImageComponent getImageComponent() {
        return mImageComponent;
    }
}
