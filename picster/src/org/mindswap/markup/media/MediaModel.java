package org.mindswap.markup.media;

import java.io.Serializable;

import java.util.Observable;
import java.util.Vector;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.HashSet;
import java.util.Set;

import java.net.URL;

import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import fr.inrialpes.exmo.elster.picster.markup.MarkupModel;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public abstract class MediaModel extends Observable implements Serializable {
    private URL mMediaURL;
    private Vector mRegions;
    private String mLabel;
    private MediaRegion mCurrentRegion;
    private LinkedHashMap mRegionDepictsTable;
    private boolean imageDepiction;
    private MediaRegion regionDepiction;
    private Set depictions;
    private Model mMetadataModel;

    public MediaModel() {
        regionDepiction = null;
        imageDepiction = false;
        mMetadataModel = null;
    }

    public URL getMediaURL() {
        return mMediaURL;
    }

    public void setMediaURL(URL theURL) {
        mMediaURL = theURL;
    }

    /**
     * Clears the list of regions associated with this model
     */
    public void clearRegions() {
        if (mRegions != null)
            mRegions.clear();
    }

    /**
     * Returns an iterator over all the regions in this model
     * @return Iterator
     */
    public Iterator getRegions() {
        if (mRegions == null) {
            mRegions = new Vector();
        }
        return mRegions.iterator();
    }

    /**
     * Adds a new region
     * @param region MediaRegion
     */
    public void addRegion(MediaRegion region) {
        if (mRegions == null) {
            mRegions = new Vector();
        }

        if (!mRegions.contains(region))
            mRegions.add(region);
    }

    /**
     * Deletes the specified region
     * @param region MediaRegion - the region to delete
     */
    public void deleteRegion(MediaRegion region) {
        if (mRegions != null) {
            mRegions.remove(region);

            if (region.equals(getCurrentRegion()))
                setCurrentRegion(null);

            getRegionDepictsTable().remove(region);
        } else {
            throw new RuntimeException("Trying to delete region from model with no regions");
        }
    }

    /**
     * Returns the label of this image model
     * @return String - the label
     */
    public String getMediaLabel() {
        if (mLabel == null)
            mLabel = "";
        return mLabel;
    }
    /**
     * Sets the label for the image model
     * @param s String - the new label
     */
    public void setMediaLabel(String s) {
        s = s.replaceAll("%20"," ");
        mLabel = s;
    }

    /**
     * Deletes the currently selected region, or nothing if there is no current region
     */
//    public void deleteRegion() {
//        Iterator iter = getRegions();
//        while (iter.hasNext()) {
//            MediaRegion region = (MediaRegion)iter.next();
//            if (region.isSelected()) {
//                iter.remove();
//                break;
//            }
//        }
//
//        if (!containsRegion(getCurrentRegion()))
//            setCurrentRegion(null);
//
//        // TODO: shouldnt the depicts be deleted?
//    }

    public Iterator getSelectedRegions() {
        // TODO: write test case for this function
        Vector aList = new Vector();
        Iterator aIter = getRegions();
        while (aIter.hasNext())
        {
            MediaRegion aRegion = (MediaRegion)aIter.next();
            if (aRegion.isSelected())
                aList.add(aRegion);
        }

        return aList.iterator();
    }

    /**
     * Returns true if the parameter is in the models list of regions, false otherwise
     * @param sr MediaRegion
     * @return boolean
     */
    public boolean containsRegion(MediaRegion sr) {
        Iterator iter = getRegions();
        while (iter.hasNext()) {
            MediaRegion region = (MediaRegion)iter.next();
            if (region.equals(sr))
                return true;
        }
        return false;
    }

    /**
     * Set the parameter region to the currently selected region.
     * @param toSelect MediaRegion - region to select
     */
    public void selectRegion(MediaRegion toSelect) {
        deselectAllRegions();

        if (toSelect != null && containsRegion(toSelect)) {
            toSelect.setSelected(true);
            toSelect.setHover(false);
            notifyObservers();
        }

        setCurrentRegion(toSelect);
    }

    /**
     * Deselect this region
     * @param region MediaRegion
     */
    public void deselectRegion(MediaRegion region) {
        if (region.isSelected() && containsRegion(region))
        {
            region.setSelected(false);
            setCurrentRegion(null);
        }
    }

    /**
     * Deselect all regions
     */
    public void deselectAllRegions() {
        Iterator iter = getRegions();
        while (iter.hasNext()) {
            MediaRegion region = (MediaRegion)iter.next();
            region.setSelected(false);
        }
    }

    public void clearAll() {
        this.clearRegions();
        this.clearDepictions();
        setCurrentRegion(null);
        mMetadataModel = null;
    }

    public void clearDepictions()
    {
        mRegionDepictsTable = null;
        this.depictions = null;
    }

    /**
     * Returns the currently selected region
     * @return MediaRegion
     */
    public MediaRegion getCurrentRegion() {
        return mCurrentRegion;
    }

    /**
     * Sets the currently selected region
     * @param sr MediaRegion the new current region
     */
    public void setCurrentRegion(MediaRegion sr) {
        mCurrentRegion = sr;
    }


    /**
     * Returns all the depicted instances for a given region
     * @param region MediaRegion the region
     * @return Iterator the list of all instances depicted by this region
     */
    public Iterator getDepictedInstances(MediaRegion region) {
        HashSet aSet = (HashSet)getRegionDepictsTable().get(region);
        if (aSet == null) {
            aSet = new HashSet();
            getRegionDepictsTable().put(region,aSet);
        }
        return aSet.iterator();
    }

    protected LinkedHashMap getRegionDepictsTable() {
        if (mRegionDepictsTable==null) {
            mRegionDepictsTable = new LinkedHashMap();
        }
        return mRegionDepictsTable;
    }

    /**
     * Associates an instance with a region.
     * @param region MediaRegion
     * @param instance Instance
     */
    public void addRegionDepicts(MediaRegion region, Resource instance) {
        addRegion(region);

        HashSet aSet = (HashSet)getRegionDepictsTable().get(region);
        if (aSet == null) {
            aSet = new HashSet();
            getRegionDepictsTable().put(region,aSet);
        }
        aSet.add(instance);
    }

    public Iterator getRegionDepictions(Resource inst)
    {
        HashSet regions = new HashSet();
        Iterator iter = getRegionDepictsTable().keySet().iterator();
        while (iter.hasNext())
        {
            MediaRegion reg = (MediaRegion)iter.next();
            HashSet depicts = (HashSet)getRegionDepictsTable().get(reg);
            if (depicts.contains(inst))
                regions.add(reg);
        }
        return regions.iterator();
    }

    /**
     * Unassociates an instance with a region
     * @param region MediaRegion
     * @param instance Instance
     */
    public void removeRegionDepicts(MediaRegion region, Resource instance) {
        if (mRegionDepictsTable == null) {
            //System.err.println("should not see this, no way to undepict!");
            return;
        }

        HashSet aSet = (HashSet) getRegionDepictsTable().get(region);
        if (aSet == null) {
            // nothing to do, this assertion was never made in the first place
        }
        else {
            aSet.remove(instance);
        }
    }

    /**
     * Unassociates an instance with an image
     * @param instance Instance
     */
    public void removeDepicts(Resource instance) {
        if (depictions == null) {
            //System.err.println("should not see this, no way to undepict!");
            return;
        }

        depictions.remove(instance);
    }

    /**
     * Currently Set via drag gesture to image or image region see:
     * MarkupImageDropTargetListener. Is used to make the depiction
     * assertion when user his okay on the instance panel or set to
     * null if the user hits cancel.
     *
     * Is either the image
     */
    public void setPendingDepiction() {
        imageDepiction = true;
        regionDepiction = null; // Can't have both an image and a region pending a depiction.
    }

    public void setPendingDepiction(MediaRegion r) {
        imageDepiction = false; // Can't have both an image and a region pending a depiction.
        regionDepiction = r;
    }

    public void clearPendingDepiction() {
        imageDepiction = false;
        regionDepiction = null;
    }

    public void addPendingDepiction(Resource instance) {
        if (imageDepiction) {
            addDepicts(instance);
        } else {
            if (regionDepiction!=null) {
                addRegionDepicts(regionDepiction, instance);
            }
        }
    }

    public void addDepicts(Resource instance) {
        if (depictions==null) {
            depictions = new HashSet();
        }
        depictions.add(instance);
    }

    /**
     * Get depicted instances for the image itself.
     *
     * @return Iterator
     */
    public Iterator getDepictedInstances() {
        if (depictions==null) {
            depictions = new HashSet();
        }
        return depictions.iterator();
    }

    public Model getMetadata() {
        if (mMetadataModel == null)
            mMetadataModel = ModelFactory.createDefaultModel();

        return mMetadataModel;
    }

    public void setMetadata(Model theModel) {
        mMetadataModel = theModel;
    }

    public static MediaModel create(MarkupModel theModel, URL theURL)
    {
        // TODO: devine the media type from URL and create the appropriate type model

        MarkupImageModel imageModel = new MarkupImageModel(theModel);
        // the url can be null when the program first starts out
        // this results in a "degenerate" image model.  The model is fine
        // other than the fact it has a null url (make sure you test for this)
        // but this degenerate model is useful for being the placeholder model
        // when there is no active image.
        imageModel.setMediaURL(theURL);

        return imageModel;
    }

//    private Resource currentInstance;
//
//    /**
//     * Returns the current instance for this image model
//     * @return Resource the current instance
//     */
//    public Resource getCurrentInstance() {
//        return currentInstance;
//    }
//
//    /**
//     * Sets the current instance for the image model
//     * @param inst Resource the new current instance
//     */
//    public void setCurrentInstance(Resource inst) {
//        currentInstance = inst;
//    }

    public abstract Model asModel();
    public abstract void extractMetadata();
    public abstract String getType();
}
