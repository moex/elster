package org.mindswap.markup.media;

import org.mindswap.markup.media.MediaMarkupModel;

import javax.swing.JComponent;

import java.net.URL;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Mindswap (http://www.mindswap.org)</p>
 * @author Michael Grove
 * @version 1.0
 */

public interface MediaComponent {

    public MediaMarkupModel getMediaMarkupModel();

    public MediaModel getMediaModel();

    public JComponent getControl();

    public void displayMedia(URL theMediaURL);

    public void update();
}
