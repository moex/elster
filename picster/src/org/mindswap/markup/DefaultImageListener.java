package org.mindswap.markup;

import javax.swing.JOptionPane;

import org.mindswap.markup.media.ImageComponent;
import org.mindswap.markup.media.ImageModel;
import org.mindswap.markup.media.MediaRegion;
import org.mindswap.markup.media.MarkupImageModel;

import org.mindswap.component.Tool;
import org.mindswap.component.DefaultTool;
import org.mindswap.component.AddRectangle;
import org.mindswap.component.OvalTool;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

public class DefaultImageListener implements MouseListener, MouseMotionListener, KeyListener {

    private ImageComponent imageComponent;
    private Tool tool;

    public DefaultImageListener(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
    }

    private Tool noTool;
    private Tool getNoTool() {
        if (noTool==null) {
            noTool = new DefaultTool(imageComponent);
        }
        return noTool;
    }

    private Tool rectangleSelect;
    private Tool getRectangleSelect() {
        if (rectangleSelect==null) {
            rectangleSelect = new AddRectangle(imageComponent);
        }
        return rectangleSelect;
    }

    private PolygonTool polygonTool;
    private PolygonTool getPolygonTool() {
        if (polygonTool == null) {
            polygonTool = new PolygonTool(imageComponent);
        }
        return polygonTool;
    }

    private Tool ovalTool;
    private Tool getOvalTool() {
        if (ovalTool == null) {
            ovalTool = new OvalTool(imageComponent);
        }
        return ovalTool;
    }

    private Tool select;
    private Tool getSelect() {
        if (select==null) {
            select = new Select(imageComponent);
        }
        return select;
    }

    private int mode = 0;
    /**
     * Returns the current drawing mode
     * @return int
     */
    public int getMode() {
        return mode;
    }
    /**
     * Sets the current drawing mode
     * @param mode int
     */
    public void setMode(int mode) {
        this.mode = mode;
        getPolygonTool().getPointList().clear();
    }

    private Tool getTool() {
        ImageModel imageModel = this.imageComponent.getImageModel();
        if (imageModel!=null && imageModel.getImageURL()!=null) {
            int mode = getMode();
            if (mode==ImageModel.SELECT) {
                tool = getSelect();
            } else if (mode == ImageModel.RECTANGLE_SELECT) {
                tool = getRectangleSelect();
            } else if (mode == ImageModel.OVAL_TOOL) {
                tool = getOvalTool();
            } else if (mode == ImageModel.POLYGON_TOOL) {
                tool = getPolygonTool();
            } else {
                tool = getRectangleSelect();
            }
        } else {
            tool = getNoTool();
        }
        return tool;
    }

    public void mouseClicked(MouseEvent e) {
        getTool().mouseClicked(e);
    }

    public void mousePressed(MouseEvent e) {
        getTool().mousePressed(e);
        imageComponent.requestFocus();
    }

    public void mouseReleased(MouseEvent e) {
        getTool().mouseReleased(e);
    }

    public void mouseEntered(MouseEvent e) {
        getTool().mouseEntered(e);
    }

    public void mouseExited(MouseEvent e) {
        getTool().mouseExited(e);
    }

    public void mouseDragged(MouseEvent e) {
        getTool().mouseDragged(e);
    }

    public void mouseMoved(MouseEvent e) {
        getTool().mouseMoved(e);
    }

    public void keyPressed(KeyEvent ev) {
        if (ev.getKeyCode() == KeyEvent.VK_DELETE || ev.getKeyCode() == KeyEvent.VK_BACK_SPACE)
        {
            if (imageComponent.getImageModel() != null && imageComponent.getImageModel().getSelectedRegions().hasNext())
            {
                int option = JOptionPane.showConfirmDialog(imageComponent,"Delete this region?", "Delete Region", JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.OK_OPTION)
                {
                    //((MarkupImageModel)imageComponent.getImageModel()).getMarkupModel().deleteRegion(imageComponent.getImageModel(),(MediaRegion)imageComponent.getImageModel().getSelectedRegions().next());
                    imageComponent.getImageModel().deleteRegion((MediaRegion)imageComponent.getImageModel().getSelectedRegions().next());
                    imageComponent.repaint();
                }
            }
        }
    }

    public void keyReleased(KeyEvent ev) {
    }

    public void keyTyped(KeyEvent ev) {
    }
}
