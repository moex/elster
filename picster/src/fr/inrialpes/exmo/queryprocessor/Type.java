/*
 * Type.java
 *
 * Created on March 20, 2006, 11:20 AM
 *
 * 
 */

package fr.inrialpes.exmo.queryprocessor;

/**
 *
 * @author Arun Sharma
 */
public class Type {
    
    public static int SELECT = 1;
    public static int ASK = 2;
    public static int CONSTRUCT = 3;
    public static int DESCRIBE = 4;
    
    /** Creates a new instance of Type */
    public Type() {
    }
    
}
