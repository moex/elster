package fr.inrialpes.exmo.elster.picster;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.NumberFormat;
import java.text.DateFormat;
import java.util.*; 
import javax.swing.*;
import javax.swing.border.*;
import java.net.URL;


//import fr.inrialpes.exmo.elster.jxta.*;

import fr.inrialpes.exmo.queryprocessor.impl.ResultImpl;
import fr.inrialpes.exmo.elster.picster.PicsterQueryProcessor;
import fr.inrialpes.exmo.queryprocessor.*;
import fr.inrialpes.exmo.elster.picster.markup.*;
import fr.inrialpes.exmo.elster.QueryFromPeer;
import org.mindswap.markup.media.MarkupImageModel;
import org.mindswap.markup.SimpleReasoner;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSetFormatter;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntClass;

import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.rdf.model.Property;

import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.RDF;
import fr.inrialpes.exmo.elster.picster.markup.P2PQueryPanel;


public class Picster extends JPanel {

    // Picster version number.
    public final static String version = "2.00";

    P2PInterface      p2p;          // Class encapsulating the peer-2-peer networking protocols.
    Razor aApp;

    // Known peers running Picster in the current group.
    // The PeerId is the key, and the peer name is the value.
    Hashtable knownPeers = new Hashtable(50);
    Hashtable revKnownPeers = new Hashtable(50);

    // Known rendezvous connections.
    // The PeerId is the key, and last known rdv status is the value,
    // as an Integer object.
    Hashtable knownRdvs = new Hashtable(10);
    String fileSaveLoc = "";

    final static String rdvMsgStr      = "Rendezvous connections: ";

    // Peer Group tab button text.
    final static String changeGroupStr = "Change group";
    final static String newGroupStr    = "Create new group";
    final static String refreshStr     = "Refresh";

    // A collection of all the files we've sent or received during this run.
    // The vector will contain FilePackage objects.
    //
    Vector filePackages;
    Vector receivedFilePackages;
    int    currFilePkg = 0;

    // temp
    int chatMsgNum;

    public static void main(String s[]) {
        
        System.out.println("\nPicSter v" + version);
        System.out.println(DateFormat.getDateTimeInstance().format(new Date()));
        System.out.println();

        Picster picShare = new Picster();
        picShare.init();
    }
    
    /**
     *  Initialize the p2p system, and create the UI.
     */
    protected void init() {

        initPicster();
    }

    Picster thisPicster = this;    
    
    public void initPicster()  {
        System.setProperty("CLASSLOADER","org.mindswap.ocl.ClassLoaderFactory");
       try {
            String platform_native = javax.swing.UIManager.getSystemLookAndFeelClassName();
            javax.swing.UIManager.setLookAndFeel(platform_native);
        } catch (Exception e) {
            System.out.println("Unable to set look and feel");
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
           public void run() {
            JFrame.setDefaultLookAndFeelDecorated(true);
	    aApp = new Razor(thisPicster);
             aApp.show();
           }
        });
    }
   

    /** Send out the file.
    void sendFile(FilePackage pkg) {
	    System.out.println("*****************In send File");
        pkg.transType = pkg.SEND;
        pkg.fileSent = true;        
    }


    public void displayFile(FilePackage pkg)  {
	    System.out.println("\n\n\n\nAbout to display the file");
            System.out.println("File path = " + pkg.filename);
    }
     */

    /** Quit the program.
     */
    public void exitProgramCommand() {
        System.exit(0);
    }


    public Hashtable getKnownPeers()  {
	    return knownPeers;
    }

    public Hashtable getRevKnownPeers()  {
	    return revKnownPeers;
    }
    
    /** perform search operation
     * @param query     the SPARQL query string
     * @param peers     table of peers to be searched (peerId, peerName)
    public void performSearch(String query, Hashtable peers)  {
        jxtaCast.sendChatMsg(query, peers);
    }
     */
    
    public void queryListener(QueryFromPeer qo) {
	System.out.println("Reach query listener");        
        Set aResults = new HashSet();        
        String senderId = qo.getSenderId();
	System.out.println("processing query from = " + qo.getSenderName());
        String query = qo.getQuery();
        try  {
              PicsterQueryProcessor pqp = new PicsterQueryProcessor(aApp.getMarkupModel().getCurrentStore().asModel());
              //ResultImpl res = (ResultImpl) pqp.query(query);
              //ResultSet aResultSet = pqp.performQuery();
              //aResults = pqp.getResourceUrisAsSet(aResultSet);
              aResults =  pqp.query(query, false);
              processResults(aResults, senderId, qo.getSenderName(), pqp);
        } catch(Exception e)  {
                    e.printStackTrace();
        }
    }
    
    public void processResults(Set mResults, String peerId, String peerName, PicsterQueryProcessor pqp)  {
        //Vector imgUrls = new Vector();
        Vector depictionUris = new Vector();
        Iterator itr = mResults.iterator();
        String caption = "";
        while(itr.hasNext())  {
            String uri = (String) itr.next();
            if(isInstanceUri(uri))  {
                caption = caption + pqp.getAnnotationforUri(uri) + "<result>";
            }
            else
                depictionUris.add(uri);	   
        }
        caption = caption + "<imageUris>";
        String imgs = "";
        for(int j = 0; j < depictionUris.size(); j++)  {
            String mediaUri = (String)depictionUris.get(j);
            String annoFileName = "";
            if(mediaUri.contains("region"))  {
             String temp = mediaUri.substring(0, mediaUri.indexOf("region")-1);
             annoFileName = temp.substring(temp.lastIndexOf('/')+1, temp.length() );
             }
             else
               annoFileName = mediaUri.substring(mediaUri.lastIndexOf('/')+1, mediaUri.length());
            String resUri = aApp.getMarkupModel().getBaseURL() + "images/" + annoFileName;
            imgs = imgs + resUri + "<img>";
        }
        caption = caption + imgs;
        System.out.println("\n\n\n\n\n\n\n\n\n about to send the answers as \n\n" + caption);
        //jxtaCast.sendChatMsgRes(caption, peerId, peerName);
    }
    
    boolean isInstanceUri(String uri)  {
        if(!uri.contains("images"))
            return true;
        else
            return false;
    }
    public void processReceivedAnswers(String senderId, String senderName, String answers)  {
        String[] temp = answers.split("<imageUris");
        String resultS = temp[0];
        String images = temp[1];
        
        String[] ans = resultS.split("<result>");
        for(int j = 0; j < ans.length; j ++ )  {
            String imgs = ans[j];
            if(!imgs.equals("")) {
                String[] imgAnno = imgs.split("<image>");            
                for(int i = 0; i < imgAnno.length; i++)  {
                    if(!imgAnno[i].equals(""))
                        writeAnno(imgAnno[i], senderId, senderName);
                   // results.putAll(map);
                }                
             }
        }
 
        HashMap results = new HashMap();
        String[] mUris = images.split("<img>");
        for(int k = 0; k < mUris.length; k++) {
              System.out.println("******Adding uri******" + mUris[k]);
              results.put(mUris[k], senderName + ":");
              P2PQueryPanel.addResults(results);
        }
    }

    public void writeAnno(String anno, String senderId, String senderName)  {
        System.out.println("\n\n\n\n processing anno\n" + anno);
        HashMap map = new HashMap();
        String aAnno = "";
        String instance = "";
        String annFile = "";
        String instanceFile = "";
        String[] strs = anno.split("<divider>");
        if(strs.length == 4)  {
            annFile = strs[0];
            aAnno = strs[1];
            instanceFile = strs[2];
            instance = strs[3];
        }
        else  {
            System.out.println("I got --" + strs.length + "elements");
            for(int j = 0; j < strs.length; j++)  {
                System.out.println("strs[" + j + "] = ----" + strs[j]);
            }
        }
        StringReader aReader = new StringReader(aAnno);
        StringReader iReader = new StringReader(instance);
                
        OntModel m = aApp.getMarkupModel().createOntModel();
        OntModel m1 = aApp.getMarkupModel().createOntModel();
         
        m.read(aReader, "");
        m1.read(iReader, "");
        
       ResIterator resItr = m1.getBaseModel().listSubjects();
        while(resItr.hasNext())  {
            Resource res = resItr.nextResource();
            String uri = res.getURI();
            map.put(senderName + ": " + uri, annFile);
        }
        //loadAnnotation(annFile);
        //loadInstances(instanceFile);
        //return map;
    }
    
    public String adjustPath(String url)  {
        String loc = "";
        loc = url.replace('\\', '/'); 
        int i = loc.indexOf('/');
        loc = loc.substring(i+1);
        System.out.println("String affter coversion = " + loc);
        return loc;
    }
    /** Add the specified peer to our list of known peers.
     *
     *  @param peerId    Peer Id string.
     *  @param peerName  Peer name.
     *  @param updateUI  If true, update the peer display.
     *
     *  @return true if the new peer was added.  Returns false if the
     *           peer was already in the list.
     */
    public boolean addKnownPeer(String peerId, String peerName, boolean updateUI) {

        if (!knownPeers.containsKey(peerId)) {
            knownPeers.put(peerId, peerName);
	    revKnownPeers.put(peerName, peerId);
	    System.out.println("\n\n\n\n\n\n******Added peer with name == " + peerName + "       and ID  == " + peerId);
            return true;
        }

        return false;
    }


    // Sort the vector, according to the string representations of its objects.
    // Sorts in ascending order, case insensitive.
    public static void sort(Vector vec) {

        synchronized (vec) {

            // Ye olde bubble sort.
            Object a;
            Object b;
            String aStr;
            String bStr;
            for (int i = 0; i < vec.size() - 1; i++) {
                for (int j = i + 1; j < vec.size(); j++) {

                    a = vec.elementAt(i);
                    b = vec.elementAt(j);
                    aStr = a.toString().toLowerCase();
                    bStr = b.toString().toLowerCase();
                    if (aStr.compareTo(bStr) > 0) {
                        vec.setElementAt(b, i);
                        vec.setElementAt(a, j);
                    }
                }
            }
        }
    }

}
