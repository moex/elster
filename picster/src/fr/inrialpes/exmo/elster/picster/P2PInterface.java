package fr.inrialpes.exmo.elster.picster;


import java.util.*;

import net.jxta.discovery.*;
import net.jxta.protocol.PeerGroupAdvertisement;
import net.jxta.protocol.PeerAdvertisement;
import net.jxta.peergroup.PeerGroup;


public interface P2PInterface {

    /** Return my own peer name. */
    public String getMyPeerName();


    /** Return advertisement for the default (initial) peer group. */
    public PeerGroupAdvertisement getDefaultAdv();


    /** Return advertisement for my peer. */
    public PeerAdvertisement getMyPeerAdv();


    /** Return the default (initial) peer group. */
    public PeerGroup getDefaultGroup();


    public void discoverGroups(String targetPeerId, DiscoveryListener discoListener);


    public void discoverPeers(String targetPeerId,
                              PeerGroupAdvertisement group,
                              DiscoveryListener discoListener);


    public void discoverAdvertisements(String targetPeerId,
                                       PeerGroupAdvertisement group,
                                       DiscoveryListener discoListener,
                                       String attribute,
                                       String value);


    public Enumeration getKnownGroups();

    public Enumeration getKnownPeers(PeerGroupAdvertisement group);

    public Enumeration getKnownAdvertisements(PeerGroupAdvertisement group,
                                              String attribute,
                                              String value);

    public PeerGroup joinPeerGroup(PeerGroupAdvertisement groupAdv, boolean beRendezvous);


    public PeerGroup createNewGroup(String groupName, String description, boolean beRendezvous);

    public boolean isRendezvous(PeerAdvertisement peerAdv);
}
