package fr.inrialpes.exmo.elster.picster;


import javax.swing.*;
import java.util.*;
import java.io.IOException;

import net.jxta.discovery.*;
import net.jxta.document.MimeMediaType;
import net.jxta.exception.PeerGroupException;
import net.jxta.membership.Authenticator;
import net.jxta.membership.MembershipService;
import net.jxta.peer.PeerID;
import net.jxta.peergroup.*;
import net.jxta.protocol.*;
import net.jxta.rendezvous.*;

import net.jxta.credential.AuthenticationCredential;
import net.jxta.credential.Credential;
import net.jxta.document.Advertisement;
import net.jxta.document.StructuredDocument;
import net.jxta.document.StructuredTextDocument;


public class PicsterP2PInterface implements P2PInterface {

    protected PeerGroup rootGroup;
    protected Vector joinedGroups;  // Holds PeerGroup objects that we've joined.


    /** Constructor - Starts JXTA.
     *
     *  @param rdvListener - can be null if you don't want rdv event notification.
     */
    public PicsterP2PInterface(RendezvousListener rdvListener) {
        System.out.println("Reached JXTAP2P");
        // Start JXTA.
        try {
            // Create, and start the default jxta NetPeerGroup.
            rootGroup = PeerGroupFactory.newNetPeerGroup();
        } catch (PeerGroupException e) {
            // Very bad trouble: bail out.
            System.out.println("fatal error : group creation failure");
            e.printStackTrace();
            System.exit(-1);
        }

        // If we've been given a Rendezvous listener, add it to the 
        // NetPeerGroup.
        rootGroup.getRendezVousService().addListener(rdvListener);
        System.out.println("rec.. here 1");
        joinedGroups = new Vector(20);
        joinedGroups.add(rootGroup);
        System.out.println("rec .. here 2");
    }


    /** Return my own peer name. */
    public String getMyPeerName() {
        return rootGroup.getPeerAdvertisement().getName();
    }


    /** Return advertisement for the default (initial) peer group. */
    public PeerGroupAdvertisement getDefaultAdv() {
        return rootGroup.getPeerGroupAdvertisement();
    }


    /** Return advertisement for my peer. */
    public PeerAdvertisement getMyPeerAdv() {
        return rootGroup.getPeerAdvertisement();
    }


    /** Return the default (initial) peer group. */
    public PeerGroup getDefaultGroup() {
        return rootGroup;
    }


    public void discoverGroups(String targetPeerId, DiscoveryListener discoListener) {

        DiscoveryService disco = rootGroup.getDiscoveryService();
        DiscoThread thread = new DiscoThread(disco,
                                             targetPeerId,
                                             DiscoveryService.GROUP,
                                             null,
                                             null,
                                             discoListener);
        thread.start();
    }


    public void discoverPeers(String targetPeerId,
                              PeerGroupAdvertisement group,
                              DiscoveryListener discoListener) {

        // Find the PeerGroup for this adv.  If we haven't joined the group,
        // we can't do the discovery.  (We get the DiscoveryService object from the
        // PeerGroup.)
        //
        PeerGroup pg = findJoinedGroup(group);
        if (pg == null)
            return;

        DiscoveryService disco = pg.getDiscoveryService();
        DiscoThread thread = new DiscoThread(disco,
                                             targetPeerId,
                                             DiscoveryService.PEER,
                                             null,
                                             null,
                                             discoListener);
        thread.start();
    }


    public void discoverAdvertisements(String targetPeerId,
                                       PeerGroupAdvertisement group,
                                       DiscoveryListener discoListener,
                                       String attribute,
                                       String value) {

        // Find the PeerGroup for this adv.  If we haven't joined the group,
        // we can't do the discovery.  (We get the DiscoveryService object from the
        // PeerGroup.)
        //
        PeerGroup pg = findJoinedGroup(group);
        if (pg == null)
            return;

        DiscoveryService disco = pg.getDiscoveryService();
        DiscoThread thread = new DiscoThread(disco,
                                             targetPeerId,
                                             DiscoveryService.ADV,
                                             attribute,
                                             value,
                                             discoListener);
        thread.start();
    }


    public Enumeration getKnownGroups() {

        Enumeration en = null;
        DiscoveryService disco = rootGroup.getDiscoveryService();

        try {
            en = disco.getLocalAdvertisements(DiscoveryService.GROUP, null, null);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return en;
    }


    public Enumeration getKnownPeers(PeerGroupAdvertisement group) {

        // Find the PeerGroup for this adv.  If we haven't joined the group,
        // we can't do the discovery.  (We get the DiscoveryService object from the
        // PeerGroup.
        //
        PeerGroup pg = findJoinedGroup(group);
        if (pg == null)
            return null;

        Enumeration en = null;
        DiscoveryService disco = pg.getDiscoveryService();

        try {
            en = disco.getLocalAdvertisements(DiscoveryService.PEER, null, null);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return en;
    }


    public Enumeration getKnownAdvertisements(PeerGroupAdvertisement group,
                                              String attribute,
                                              String value) {

        // Find the PeerGroup for this adv.  If we haven't joined the group,
        // we can't do the discovery.  (We get the DiscoveryService object from the
        // PeerGroup.
        //
        PeerGroup pg = findJoinedGroup(group);
        if (pg == null)
            return null;

        Enumeration en = null;
        DiscoveryService disco = pg.getDiscoveryService();

        try {
            en = disco.getLocalAdvertisements(DiscoveryService.ADV, attribute, value);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return en;
    }


    public synchronized PeerGroup joinPeerGroup(PeerGroupAdvertisement groupAdv,
                                                boolean beRendezvous) {

        // See if it's a group we've already joined.
        PeerGroup newGroup = findJoinedGroup(groupAdv);
        if (newGroup != null)
            return newGroup;

        // Join the group.  This is done by creating a PeerGroup object for
        // the group and initializing it with the group advertisement.
        //
        try {
            newGroup = rootGroup.newGroup(groupAdv);
        } catch (PeerGroupException e) {
            System.out.println(e.toString());
            JOptionPane.showMessageDialog(null, "Unable to join group.\n" +
                                          e.toString(), "Error",
                                          JOptionPane.ERROR_MESSAGE);
            return null;
        }

        authenticateMembership(newGroup);

        // We'll be a rendezvous in the new group, if requested.
        if (beRendezvous)
            newGroup.getRendezVousService().startRendezVous();

        // Advertise that we've joined this group.
        DiscoveryService disco = newGroup.getDiscoveryService();
        try {
            // Publish our advertisements.  Is all of this really needed?
            disco.publish(newGroup.getPeerGroupAdvertisement());
        } catch (IOException e) {
            System.out.println(e.toString());
        }

        // Add the new group to our list of joined groups.
        joinedGroups.add(newGroup);

        return newGroup;
    }


    public PeerGroup createNewGroup(String groupName, String description,
                                    boolean beRendezvous) {

        PeerGroup pg;               // new peer group
        PeerGroupAdvertisement adv; // advertisement for the new peer group
        
        try {
            // Create a new all purpose peergroup.
            ModuleImplAdvertisement implAdv =
                rootGroup.getAllPurposePeerGroupImplAdvertisement();
            
            pg = rootGroup.newGroup(null,         // Assign new group ID
                                    implAdv,      // The implem. adv
                                    groupName,    // The name
                                    description); // Helpful descr.
        }
        catch (Exception e) {
            System.out.println("Group creation failed with " + e.toString());
            JOptionPane.showMessageDialog(null, "Unable to create group.\n" +
                                          e.toString(), "Error",
                                          JOptionPane.ERROR_MESSAGE);
            return null;
        }

        authenticateMembership(pg);

        // We'll be a rendezvous in the new group, if requested.
        if (beRendezvous)
            pg.getRendezVousService().startRendezVous();

        // Advertise that we've created this group.
        DiscoveryService disco = rootGroup.getDiscoveryService();
        try {
            // Not sure how much of this is needed; this might be overkill.
            disco.publish(pg.getPeerGroupAdvertisement());
        } catch (IOException e) {
            System.out.println(e.toString());
        }

        // Add the new group to our list of joined groups.
        joinedGroups.add(pg);

        return pg;
    }


    public boolean authenticateMembership(PeerGroup group) {

        StructuredDocument creds = null;
        try {
            // Generate the credentials for the Peer Group.
            AuthenticationCredential authCred = 
                new AuthenticationCredential(group, null, creds);

            // Get the MembershipService from the peer group.
            MembershipService membership = group.getMembershipService();

            // Get the Authenticator from the Authentication creds.
            Authenticator auth = membership.apply(authCred);

            // Check if everything is okay to join the group.
            if (auth.isReadyForJoin()) {
                Credential myCred = membership.join(auth);
            }
            else {
                System.out.println("Failure: unable to join group.  " +
                                   "Authenticator not ready.");
                JOptionPane.showMessageDialog(null, "Unable to join group.\n" +
                                              "Authenticator not ready.", "Error",
                                              JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        catch (Exception e) {
            System.out.println("Failure in authentication.");
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Unable to authenticate group membership.\n" +
                                          e.toString(), "Error",
                                          JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }


    /** Is the indicated peer a rendezvous? */
    public boolean isRendezvous(PeerAdvertisement peerAdv) {

        // Find the PeerGroup object for this group.
        PeerGroup group = findJoinedGroup(peerAdv.getPeerGroupID());
        if (group == null)
            return false;

        // Are we checking for our own peer?  If so, we can just ask the
        // PeerGroup object if we are a rendezvous.
        if (peerAdv.getPeerID().equals(rootGroup.getPeerAdvertisement().getPeerID()))
            return group.isRendezvous();

        // Get the RendezVousService from the PeerGroup.
        RendezVousService rdv = (RendezVousService)group.getRendezVousService();
        if (rdv == null)
            return false;

        // Get a list of the connected rendezvous peers for this group, and
        // search it for the requested peer.
        //
        PeerID peerID = null;
        Enumeration rdvs = null;
        rdvs = rdv.getConnectedRendezVous();
        while (rdvs.hasMoreElements()) {
            try {
                peerID = (PeerID)rdvs.nextElement();
                if (peerID.equals(peerAdv.getPeerID()))
                    return true;
            } catch (Exception e) {}
        }

        // We'll search the disconnected rendezvous, also.
        rdvs = rdv.getDisconnectedRendezVous();
        while (rdvs.hasMoreElements()) {
            try {
                peerID = (PeerID)rdvs.nextElement();
                if (peerID.equals(peerAdv.getPeerID()))
                    return true;
            } catch (Exception e) {}
        }

        // Didn't find it, the peer isn't a rendezvous.
        return false;
    }


    protected PeerGroup findJoinedGroup(PeerGroupAdvertisement groupAdv) {
        return findJoinedGroup(groupAdv.getPeerGroupID());
    }


    protected PeerGroup findJoinedGroup(PeerGroupID groupID) {

        PeerGroup group = null;

        // Step thru the groups we've created, looking for one that has the
        // same peergroup ID as the requested group.
        //
        Enumeration myGroups = joinedGroups.elements();
        while (myGroups.hasMoreElements()) {
            group = (PeerGroup)myGroups.nextElement();

            // If these match, we found it.
            if (group.getPeerGroupID().equals(groupID))
                return group;
        }

        // Didn't find it.
        return null;
    }


    /** Thread to perform the discovery process.
     *  A separate thread is needed because we don't want to hold up
     *  the main thread's UI & animation.  The JXTA remote discovery call
     *  can take several seconds.
     */
    class DiscoThread extends Thread {

        DiscoveryService disco;
        int type;
        String targetPeerId;
        String attribute;
        String value;
        DiscoveryListener discoListener;

        public DiscoThread(DiscoveryService disco,
                           String targetPeerId,
                           int type,
                           String attribute,
                           String value,
                           DiscoveryListener discoListener) {
            this.disco = disco;
            this.type = type;
            this.discoListener = discoListener;

            if (targetPeerId != null)
                this.targetPeerId = new String(targetPeerId);
            if (attribute != null)
                this.attribute = new String(attribute);
            if (value != null)
                this.value = new String(value);
        }

        public void run() {
            disco.getRemoteAdvertisements(targetPeerId, type, attribute,
                                          value, 10, discoListener);
        }
    }
}
