package fr.inrialpes.exmo.elster.picster.markup;

/*
 * @author Arun Sharma (INRIA, RWTH)
 *
*/

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Timer;
import java.util.TreeSet;
import java.util.Vector;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JDialog;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.ButtonModel;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.DefaultListModel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;

import java.awt.event.KeyAdapter;

import org.mindswap.markup.media.MediaMarkupModel;
import fr.inrialpes.exmo.elster.picster.Picster;
import fr.inrialpes.exmo.elster.picster.PicsterQueryProcessor;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSetFormatter;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;
import java.net.URL;
import org.mindswap.markup.media.MarkupImageModel;
import java.io.File;
import fr.inrialpes.exmo.queryprocessor.impl.ResultImpl;
import fr.inrialpes.exmo.queryprocessor.*;

public class P2PQueryPanel extends javax.swing.JPanel implements ActionListener {
    
    /** Creates new form P2PQueryPanel */
    private Font tahoma = new Font("Tahoma", Font.PLAIN, 11);
    private Razor mApp;
    private Picster ps;
    private MediaMarkupModel mModel;
    private static HashMap results = new HashMap();

    private javax.swing.JRadioButton allPeersBtn;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JButton changePeersBtn;
    private javax.swing.JCheckBox checkBox;
    private javax.swing.JButton classLookup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton keywordClear;
    private javax.swing.JPanel keywordPanel;
    private javax.swing.JButton keywordSearch;
    private javax.swing.JTextField keywordText;
    private javax.swing.JRadioButton localBtn;
    private javax.swing.JPanel ontoPanel;
    private javax.swing.JTextField ontoText;
    private javax.swing.JProgressBar progressBar;
    private static javax.swing.JList resultList;
    private static DefaultListModel listModel;
    private javax.swing.JPanel resultPanel;
    private javax.swing.JPanel scopePanel;
    private javax.swing.JRadioButton selectedPeersBtn;
    private javax.swing.JButton showAllBtn;
    private javax.swing.JButton showSelectedBtn;
    private javax.swing.JButton sparqlClear;
    private javax.swing.JPanel sparqlPanel;
    private javax.swing.JButton sparqlSearch;
    private javax.swing.JTextArea sparqlTextArea;


    private static final String CMD_CHANGE_PEERS = "CMD_CHANGE_PEERS";
    private static final String CMD_KEYWORD_SEARCH = "CMD_KEYWORD_SEARCH";
    private static final String CMD_STOP_K_SEARCH = "CMD_STOP_K_SEARCH";
    private static final String CMD_KEY_CLEAR = "CMD_KEY_CLEAR";
    
    private static final String CMD_SPARQL_SEARCH = "CMD_SPARQL_SEARCH";
    private static final String CMD_STOP_SPARQL_SEARCH = "CMD_STOP_SPARQL_SEARCH";
    private static final String CMD_SPARQL_CLEAR = "CMD_SPARQL_CLEAR";
    private static final String CMD_SHOW_ALL = "CMD_SHOW_ALL";
    private static final String CMD_SHOW_SELECTED = "CMD_SHOW_SELECTED";    
    private static final String CMD_LOOKUP = "CMD_LOOKUP";

    public Hashtable selectedPeers = new Hashtable(50);
    
   public P2PQueryPanel(Razor theApp, Picster picS)  {
                System.out.println("P2PQuery COnstructor");
		mApp = theApp;
		ps = picS;
		mModel = mApp.getMarkupModel();		
		initGUI();
	}
    
   
    private void initGUI() {
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        scopePanel = new javax.swing.JPanel();
        localBtn = new javax.swing.JRadioButton();
        allPeersBtn = new javax.swing.JRadioButton();
        selectedPeersBtn = new javax.swing.JRadioButton();
        changePeersBtn = new javax.swing.JButton();
        keywordPanel = new javax.swing.JPanel();
        keywordText = new javax.swing.JTextField();
        keywordSearch = new javax.swing.JButton();
        keywordClear = new javax.swing.JButton();
        checkBox = new javax.swing.JCheckBox();
        sparqlPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        sparqlTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        sparqlSearch = new javax.swing.JButton();
        sparqlClear = new javax.swing.JButton();
        ontoPanel = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        ontoText = new javax.swing.JTextField();
        classLookup = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        resultPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listModel = new DefaultListModel();
        resultList = new javax.swing.JList(listModel);
        showSelectedBtn = new javax.swing.JButton();
        showAllBtn = new javax.swing.JButton();

        scopePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Scope"));
        buttonGroup1.add(localBtn);
        localBtn.setSelected(true);
        localBtn.setText("Local");
        localBtn.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        localBtn.setMargin(new java.awt.Insets(0, 0, 0, 0));

        buttonGroup1.add(allPeersBtn);
        allPeersBtn.setText("All");
        allPeersBtn.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        allPeersBtn.setMargin(new java.awt.Insets(0, 0, 0, 0));

        buttonGroup1.add(selectedPeersBtn);
        selectedPeersBtn.setText("Selected");
        selectedPeersBtn.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        selectedPeersBtn.setMargin(new java.awt.Insets(0, 0, 0, 0));

	localBtn.addActionListener(this);
        allPeersBtn.addActionListener(this);
        selectedPeersBtn.addActionListener(this);

        changePeersBtn.setText("Change Peers");
	changePeersBtn.setActionCommand(CMD_CHANGE_PEERS);
	changePeersBtn.addActionListener(this);
        org.jdesktop.layout.GroupLayout scopePanelLayout = new org.jdesktop.layout.GroupLayout(scopePanel);
        scopePanel.setLayout(scopePanelLayout);
        scopePanelLayout.setHorizontalGroup(
            scopePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scopePanelLayout.createSequentialGroup()
                .add(scopePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(scopePanelLayout.createSequentialGroup()
                        .add(selectedPeersBtn)
                        .add(51, 51, 51)
                        .add(changePeersBtn))
                    .add(allPeersBtn)
                    .add(localBtn))
                .addContainerGap(62, Short.MAX_VALUE))
        );
        scopePanelLayout.setVerticalGroup(
            scopePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(scopePanelLayout.createSequentialGroup()
                .add(localBtn)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(allPeersBtn)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(scopePanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(selectedPeersBtn)
                    .add(changePeersBtn))
                .addContainerGap())
        );

        keywordPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Keyword Search"));

        keywordSearch.setText("Search");

        keywordClear.setText("Clear");

	keywordSearch.setActionCommand(CMD_KEYWORD_SEARCH);
	//keywordStopBtn.setActionCommand(CMD_STOP_K_SEARCH);
	keywordClear.setActionCommand(CMD_KEY_CLEAR);
	
        keywordSearch.addActionListener(this);
    //    keywordStopBtn.addActionListener(this);
        keywordClear.addActionListener(this);

        checkBox.setText("Match Case");
        checkBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        checkBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        org.jdesktop.layout.GroupLayout keywordPanelLayout = new org.jdesktop.layout.GroupLayout(keywordPanel);
        keywordPanel.setLayout(keywordPanelLayout);
        keywordPanelLayout.setHorizontalGroup(
            keywordPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(keywordPanelLayout.createSequentialGroup()
                .add(keywordPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(checkBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 83, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(keywordPanelLayout.createSequentialGroup()
                        .add(keywordSearch)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 149, Short.MAX_VALUE)
                        .add(keywordClear))
                    .add(keywordText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 225, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        keywordPanelLayout.setVerticalGroup(
            keywordPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(keywordPanelLayout.createSequentialGroup()
                .add(keywordText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(keywordPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(keywordSearch)
                    .add(keywordClear))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(checkBox))
        );

	keywordText.addKeyListener(new KeyAdapter() {
                public void keyReleased(KeyEvent theEvent) {
                if (theEvent.getKeyCode() == KeyEvent.VK_ENTER)
                    keywordSearch.doClick();
                }
                });

        sparqlPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("SPARQL Query"));
        sparqlTextArea.setColumns(20);
        sparqlTextArea.setRows(4);
        sparqlTextArea.setText("select ?uri\nwhere ?uri <predicate> <object>.");
        jScrollPane1.setViewportView(sparqlTextArea);

        jLabel1.setText("Enter Sparql Text Query");

        sparqlSearch.setText("Search");

        sparqlClear.setText("Clear");

	sparqlSearch.setActionCommand(CMD_SPARQL_SEARCH);
	//sparqlStopBtn.setActionCommand(CMD_STOP_SPARQL_SEARCH);
	sparqlClear.setActionCommand(CMD_SPARQL_CLEAR);
	
        sparqlSearch.addActionListener(this);
       // sparqlStopBtn.addActionListener(this);
        sparqlClear.addActionListener(this);

        org.jdesktop.layout.GroupLayout sparqlPanelLayout = new org.jdesktop.layout.GroupLayout(sparqlPanel);
        sparqlPanel.setLayout(sparqlPanelLayout);
        sparqlPanelLayout.setHorizontalGroup(
            sparqlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLabel1)
            .add(sparqlPanelLayout.createSequentialGroup()
                .add(sparqlSearch)
                .add(159, 159, 159)
                .add(sparqlClear))
            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 273, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
        );
        sparqlPanelLayout.setVerticalGroup(
            sparqlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(sparqlPanelLayout.createSequentialGroup()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(sparqlPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(sparqlSearch)
                    .add(sparqlClear)))
        );

        ontoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Ontology Based Search"));
        jLabel2.setText("Enter Class");

        classLookup.setText("Lookup");

	classLookup.setActionCommand(CMD_LOOKUP);
	classLookup.addActionListener(this);

        org.jdesktop.layout.GroupLayout ontoPanelLayout = new org.jdesktop.layout.GroupLayout(ontoPanel);
        ontoPanel.setLayout(ontoPanelLayout);
        ontoPanelLayout.setHorizontalGroup(
            ontoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ontoPanelLayout.createSequentialGroup()
                .add(ontoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(ontoPanelLayout.createSequentialGroup()
                        .add(ontoText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(classLookup)))
                .addContainerGap(6, Short.MAX_VALUE))
        );
        ontoPanelLayout.setVerticalGroup(
            ontoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(ontoPanelLayout.createSequentialGroup()
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(ontoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(ontoText, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(classLookup))
                .addContainerGap())
        );

        resultPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Results"));
        jScrollPane3.setViewportView(resultList);

        showSelectedBtn.setText("Show Selected");
	showSelectedBtn.setActionCommand(CMD_SHOW_SELECTED);
	showSelectedBtn.addActionListener(this);

        showAllBtn.setText("Show All");
	showAllBtn.setActionCommand(CMD_SHOW_ALL);
	showAllBtn.addActionListener(this);
        org.jdesktop.layout.GroupLayout resultPanelLayout = new org.jdesktop.layout.GroupLayout(resultPanel);
        resultPanel.setLayout(resultPanelLayout);
        resultPanelLayout.setHorizontalGroup(
            resultPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(resultPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(resultPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(resultPanelLayout.createSequentialGroup()
                        .add(showSelectedBtn)
                        .add(24, 24, 24)
                        .add(showAllBtn))
                    .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE))
                .addContainerGap())
        );
        resultPanelLayout.setVerticalGroup(
            resultPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, resultPanelLayout.createSequentialGroup()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 314, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 17, Short.MAX_VALUE)
                .add(resultPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(showSelectedBtn)
                    .add(showAllBtn)))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(21, 21, 21)
                        .add(progressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(scopePanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(keywordPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(sparqlPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(ontoPanel, 0, 297, Short.MAX_VALUE))
                .add(20, 20, 20)
                .add(resultPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(scopePanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(keywordPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(sparqlPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(ontoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 73, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(progressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(resultPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>                        
    
    public static void addResults(HashMap res) {
        results.putAll(res);
        updateResList();
    }
    
    public static void updateResList()  {
        Set uris = results.keySet();
        Object[] obs = uris.toArray();
        String[] str = new String[obs.length];
        for(int i = 0; i < obs.length; i++) {
            str[i] = (String)obs[i];
            String peer = (String)results.get(str[i]);
            String uri = "";
            int k = str[i].indexOf('h');
            uri = str[i].substring(k, str[i].length());
            if(listModel.indexOf(peer + uri) == -1) {
                if(!uri.equals("")) {
                    if(str[i].indexOf("instances") == -1)  {
                     listModel.addElement(peer + str[i]);
                     System.out.println("\n\n\nAdded intance" + str[i]);
                    }
                }
            }
        }
       // resultList.setListData(str);
       // resultList.updateUI();
    }
    

    public void actionPerformed(ActionEvent e) {
		String aCommand = e.getActionCommand();
                if (aCommand.equals(CMD_CHANGE_PEERS))  {
                    System.out.println("Changing peers...");
		    changePeerCommand();		    
                }
                else if (aCommand.equals(CMD_KEYWORD_SEARCH))  {
                    System.out.println("key search...");
                    listModel.clear();
		    String keyword = keywordText.getText();
                    //System.out.println("field 1 = " + keyword);
                    //System.out.println("field 2 = " + jTextField2.getText());
                    String aQuery = "";
                    aQuery = "select ?uri where { ";
                    aQuery += " ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?aType . ";
                    aQuery += " OPTIONAL  { ?uri <http://www.w3.org/2000/01/rdf-schema#label> ?label . } ";
                    aQuery += " FILTER (regex(str(?uri), \""+keyword+"\", \"i\") || regex(?label, \""+keyword+"\", \"i\") ) . ";
                    aQuery += " FILTER (!isBlank(?uri)) . ";
                    aQuery += "}";
                    
                    if(localBtn.isSelected())  {
                        //search locally
                      //  selectedPeers = null;
                        try {
                            Set aResults = new HashSet();
                            PicsterQueryProcessor pqp = new PicsterQueryProcessor(mApp.getMarkupModel().getCurrentStore().asModel());
                            //ResultImpl res = (ResultImpl) pqp.query(aQuery);
                            //ResultSet aResultSet = pqp.performQuery();
                            //aResults = pqp.getResourceUrisAsSet(aResultSet);
                            aResults =  pqp.query(aQuery, true);
                            Vector imgUrls = new Vector();
                            Iterator itr = aResults.iterator();
                            while(itr.hasNext())  {
                                String uri = (String) itr.next();
                                if(uri.indexOf("instances") == -1)
                                    listModel.addElement(uri);                                
                            }
                        } catch(Exception exp )  {
                            exp.printStackTrace();
                        }
                    }
                    
                    else if (allPeersBtn.isSelected())  {
                        //search all known peers
                        setSelectedPeers(ps.getKnownPeers());
                        ps.performSearch(aQuery, selectedPeers);
                    }

                    else if (selectedPeersBtn.isSelected()){
                        //select peers
                      //  changePeerCommand();
                        ps.performSearch(aQuery, selectedPeers);
                    }
                    
                    //ps.performSearch(aQuery, selectedPeers);

                }
                else if (aCommand.equals(CMD_STOP_K_SEARCH))  {
                    System.out.println("key stop work");
                }                
                else if (aCommand.equals(CMD_KEY_CLEAR))  {
                    keywordText.setText("");
                }                
                else if (aCommand.equals(CMD_SPARQL_SEARCH))  {
                    System.out.println("sparql search ...");
                    listModel.clear();
		    String aQuery = sparqlTextArea.getText();
		    if(localBtn.isSelected())  {
                        //search locally
                       // selectedPeers = null;
                        try {
                        Set aResults = new HashSet();
		        PicsterQueryProcessor pqp = new PicsterQueryProcessor(mApp.getMarkupModel().getCurrentStore().asModel());
                        //ResultImpl res = (ResultImpl) pqp.query(aQuery);
                        //ResultSet aResultSet = pqp.performQuery();
                        //aResults = pqp.getResourceUrisAsSet(aResultSet);
                        aResults = pqp.query(aQuery, true);
                        Vector imgUrls = new Vector();
                            Iterator itr = aResults.iterator();
                            while(itr.hasNext())  {
                                String uri = (String) itr.next();
                                if(uri.indexOf("instances") == -1)
                                    listModel.addElement(uri);                                
                            }
                	////pqp.showImages(aResults, mApp);
                        } catch(Exception ex )  {
                            ex.printStackTrace();
                        }
                    }
                    
                    else if (allPeersBtn.isSelected())  {
                        //search all known peers
                        setSelectedPeers(ps.getKnownPeers());
                        ps.performSearch(aQuery, selectedPeers);                        
                    }

                    else if (selectedPeersBtn.isSelected()){
                        //select peers
                        //changePeerCommand();
                        ps.performSearch(aQuery, selectedPeers);
                    }
                    
                    ps.performSearch(aQuery, selectedPeers);
                }
                else if (aCommand.equals(CMD_STOP_SPARQL_SEARCH))  {
                  //  System.out.println("sparql stop work");
                }
                else if (aCommand.equals(CMD_SPARQL_CLEAR))  {                    
                    sparqlTextArea.setText("select ?uri\nwhere ?uri <predicate> <object>.");
                }
                else if (aCommand.equals(CMD_LOOKUP))  {
                    System.out.println("Lookup work");
                }
                
                else if(aCommand.equals(CMD_SHOW_ALL))  {
                    Object[] obj = listModel.toArray();
                    String[] urls = new String[obj.length];
                    for(int i = 0; i < obj.length; i++)  {
                       String str = (String)obj[i];
                       //urls[i] = str.substring(str.indexOf(':') + 1, str.length()).trim();
                       urls[i] = str;
                    }
                    if(!localBtn.isSelected())  {
                        for(int j = 0; j < urls.length; j++)  {
                            showUrl(urls[j]);
                        }
                    }
                    else  {
                        for(int k = 0; k < urls.length; k++)  {
                            String imageLocation = (String)MarkupImageModel.getUriMap().get(urls[k]);
                            if(imageLocation == null)
                                imageLocation = (String)MarkupImageModel.getRegUriMap().get(urls[k]);
                            try {
                                System.out.println("URL = " + imageLocation);
                                URL aUrl = new URL(imageLocation);
                                mApp.loadURL(aUrl);
                            } catch(Exception er)  {
                                er.printStackTrace();
                            }
                        }
                    }
                }
                
                else if(aCommand.equals(CMD_SHOW_SELECTED))  {
                    Object[] obj = resultList.getSelectedValues();
                    String[] urls = new String[obj.length];
                    for(int i = 0; i < obj.length; i++)  {
                       String str = (String)obj[i];
                     //  urls[i] = str.substring(str.indexOf(':') + 1, str.length()).trim();
                       urls[i] = str;
                    }
                    if(!localBtn.isSelected())  {
                        for(int j = 0; j < urls.length; j++)  {
                            showUrl(urls[j]);
                        }
                    }
                    else  {
                        for(int k = 0; k < urls.length; k++)  {
                            String imageLocation = (String)MarkupImageModel.getUriMap().get(urls[k]);
                            if(imageLocation == null)
                                imageLocation = (String)MarkupImageModel.getRegUriMap().get(urls[k]);
                                System.out.println("URL = " + imageLocation);
                            try {
                                URL aUrl = new URL(imageLocation);
                                mApp.loadURL(aUrl);
                            } catch(Exception er)  {
                                er.printStackTrace();
                            }
                        }
                    }
                }
	}

        
        public void showUrl(String url)  {
            URL imgUrl = null;
            int i = url.lastIndexOf('/');
            String imgName = url.substring(i+1, url.length());
            String baseUri = mApp.getMarkupModel().getUriBase();
            String localImageUri = baseUri + "/elster/images/" + imgName;
            File f = new File("images/" + imgName);
            try {
            if(f.exists()) {
                imgUrl = f.toURL();
                mApp.loadURL(imgUrl);
            }
            else  {
                String localImg = (String)MarkupImageModel.getUriMap().get(localImageUri);
                File f1 = new File(localImg);
                if(f1.exists())  {
                    imgUrl = f1.toURL();
                    mApp.loadURL(imgUrl);
                }
                else
                    System.err.println("Image does not exit");
            }
            } catch(Exception e)  {
                e.printStackTrace();
            }
        }
	public void changePeerCommand()  {
		Hashtable knownPeers = ps.getKnownPeers();
		Hashtable revKnownPeers = ps.getRevKnownPeers();
		Hashtable queryPeersList = new Hashtable();
		Collection names = knownPeers.values();		
		Vector peerNames = new Vector(names);
		sort(peerNames);

		//now create the GUI..

		JList list = new JList(peerNames);
       		JScrollPane scrollPane = new JScrollPane(list);
	        list.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
 
        	// Build an options dialog to display the list.
//	        ImageIcon icon = new ImageIcon(getClass().getResource("peers.gif"));
	        JOptionPane grpDlgPane = new JOptionPane(scrollPane,
	            JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
	        JDialog grpDlg = grpDlgPane.createDialog(this, "Select peers");
	        grpDlg.show();
	
	        // Bail out if user hit Cancel, or closed dialog without choosing yes/no.
	        if (grpDlgPane.getValue() == null)
	            return;
	        if (grpDlgPane.getValue() instanceof Integer) {
	            if (((Integer)grpDlgPane.getValue()).intValue() == JOptionPane.OK_OPTION) {
                        Object[] values = list.getSelectedValues();
			String[] selNames = new String[values.length];
                        for(int counter = 0; counter < values.length; counter++)  {
                            selNames[counter] = (String)values[counter];
                        }
			for(int i = 0; i < selNames.length; i++)  {
				String selName = (String)selNames[i];
		                if (selName != null) {
		                    String peerId = (String)revKnownPeers.get(selName);
				    queryPeersList.put(peerId, selName);
	                	}
			}
	            }
	        }
		
		setSelectedPeers(queryPeersList);	
	}	

	public void setSelectedPeers(Hashtable peers)  {
		selectedPeers = peers;
	}

	public static void sort(Vector vec) {

        synchronized (vec) {

            // Ye olde bubble sort.
            Object a;
            Object b;
            String aStr;
            String bStr;
            for (int i = 0; i < vec.size() - 1; i++) {
                for (int j = i + 1; j < vec.size(); j++) {

                    a = vec.elementAt(i);
                    b = vec.elementAt(j);
                    aStr = a.toString().toLowerCase();
                    bStr = b.toString().toLowerCase();
                    if (aStr.compareTo(bStr) > 0) {
                        vec.setElementAt(b, i);
                        vec.setElementAt(a, j);
                    }
                }
            }
        }
    }
   
    
}
