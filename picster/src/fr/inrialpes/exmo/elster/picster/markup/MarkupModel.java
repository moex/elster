package fr.inrialpes.exmo.elster.picster.markup;

/*
 * @author Arun Sharma (INRIA, RWTH)
 * @author Michael Grove (MINDSWAP)
*/

import org.mindswap.utils.JenaUtils;
import org.mindswap.utils.BasicUtils;
import org.mindswap.markup.*;

import org.mindswap.markup.utils.ResourceComparator;
//import org.mindswap.markup.SwingWorker;
import org.mindswap.component.OntologyTreeView;

import java.util.LinkedHashSet;
import java.util.TreeSet;
import java.util.Vector;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;
import java.util.Arrays;
import java.util.List;

import java.net.URI;
import java.net.URL;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileInputStream;

import org.mindswap.markup.plugin.OntoEditPlugIn;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntClass;

import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.rdf.model.Property;

import com.hp.hpl.jena.vocabulary.RDFS;
import com.hp.hpl.jena.vocabulary.RDF;

import com.hp.hpl.jena.util.ResourceUtils;

import java.util.prefs.Preferences;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import java.io.Serializable;

import org.mindswap.markup.media.MediaRegion;
import org.mindswap.markup.media.MediaModel;
import org.mindswap.markup.media.MarkupImageModel;
import org.mindswap.markup.media.MediaFilter;
import org.mindswap.markup.media.MediaFilterImpl;

import org.mindswap.store.Store;

import org.mindswap.store.impl.LocalMarkupStore;

import org.mindswap.store.loader.StoreLoader;

import org.mindswap.multimedia.ModelProfile;
import org.mindswap.multimedia.Image;

import org.mindswap.multimedia.impl.MindswapMultimediaModelProfile;

import org.mindswap.datasource.exception.QueryException;

public class MarkupModel implements Serializable {
    public static boolean DEBUG = false;

    private static final String DUMMY_BASE_URL = "http://www.example.org/#";
    private static final String DEFAULT_IMAGE_SERVER_URL = "http://www.mindswap.org/dav/images/";

    public static final ModelProfile PROFILE = new MindswapMultimediaModelProfile();

    public static final String PREF_STORE_NODE = "sites";
    private static final String PREF_LOCAL = "LOCAL";
    private static final String PREF_TYPE = "type";
    private static final String PREF_TYPE_SITE = "site";
    private static final String PREF_TYPE_METASTORE = "metastore";
    private static final String PREF_SUBMIT_URL = "submitURL";
    private static final String PREF_INSTANCE_URL = "instanceURL";
    private static final String PREF_URI = "uri";
    private static final String PREF_IMPL = "IMPL";

    public static boolean USE_LABELS = false;
    public static boolean FILTER_ANON = false;
    public static boolean USE_PELLET = true;
    public static boolean USE_JENA_REASONER = false;

    private HashSet mStores = null;

    private StoreLoader mLoader;
    
    //by arun
    private String id;
    private String uriBase;

    // do we need to skip depiction/depicts as well?
    private List SKIP = Arrays.asList(new Property[] { RDF.type, PROFILE.depiction(),
                                      // this "type" predicate is getting dumped from sites...
                                      ModelFactory.createDefaultModel().getProperty("http://owl.mindswap.org/2003/ont/owlweb.rdf#type")
    } );

    public MarkupModel() {
        try {
//            getPrefs().clear();
//            getPrefs().node("sites").removeNode();
//            getPrefs().node("bookmarks").removeNode();
        }
        catch(Exception ex) {ex.printStackTrace();}

        mLoader = new StoreLoader();
        //mLoader.setCache(new PrefsStoreCacheImpl(mLoader));
        mLoader.getRegistry().register("http://www.mindswap.org/PhotoStuff#MarkupModel",this);

        if (mStores == null)
        {
            mStores = new HashSet();
            refreshStoresFromPrefs();
        }

        mLocalStore = new LocalMarkupStore(this);

        addStore(mLocalStore);

        setCurrentStore(mLocalStore);
    }

    public static final OntModel createOntModel()
    {
        OntModel model = null;
        if (USE_JENA_REASONER)
            model = ModelFactory.createOntologyModel();
        else model = ModelFactory.createOntologyModel(com.hp.hpl.jena.ontology.OntModelSpec.OWL_MEM, null);
        model.setStrictMode(true);
        return model;
    }

    public Resource createInstance(String id, String theTypeURI)
    {
        OntModel tempModel = createOntModel();
        Resource typeRes = getMasterOntologyModel().getResource(theTypeURI);
        if (id == null)
            return tempModel.createIndividual(typeRes);
        else return tempModel.createIndividual(id,typeRes);
    }

    /**
     * Adds a new store to the markup model
     * @param theStore Store the new store
     */
    public void addStore(Store theStore)
    {
        if (!mStores.contains(theStore))
        {
            mStores.add(theStore);
            modelChanged(MarkupModelEvent.PREFS_CHANGED);
        }
    }

    public boolean addNewStore(String theURI)
    {
        try {
            Store aStore = mLoader.getStore(URI.create(theURI));

            if (aStore == null)
                return false;

            Preferences sites = getPrefs().node(PREF_STORE_NODE);
            Preferences storeNode = sites.node(aStore.getName());
            storeNode.put(PREF_TYPE,PREF_TYPE_METASTORE);
            storeNode.put(PREF_IMPL,theURI);

            addStore(aStore);

            return true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

//    private LiveStore mConnectedStore = null;
//    /**
//     * Sets the currently connected store
//     * @param theStore LiveStore the store to connect to
//     */
//    public void setConnectedStore(LiveStore theStore)
//    {
//        if (mConnectedStore != null)
//            mConnectedStore.disconnect();
//
//        // if the parameter is null, we're disconnecting from the site
//        // otherwise it points to the new site we want to connect to
//        mConnectedStore = theStore;
//
//        if (mConnectedStore != null)
//            mConnectedStore.connect();
//
//        fireMindswapEvent(MarkupModelEvent.CONNECTION_STATUS_CHANGED);
//    }

    /**
     * Return the current connected store
     * @return Store the store currently connected to, or null if no current connection
     */
//    public Store getConnectedStore() {
//        return mConnectedStore;
//    }

    /**
     * Remove the specified store from the model
     * @param theStore Store the store to remove
     */
    public void removeStore(Store theStore)
    {
        if (getCurrentStore() == theStore)
            setCurrentStore(mLocalStore);

        try {
            Preferences sites = getPrefs().node(PREF_STORE_NODE);

            if (sites.nodeExists(theStore.getName()))
            {
                sites.node(theStore.getName()).removeNode();

                // TODO: if this is a kowari db, we should probably remove the table/model
            }

            mStores.remove(theStore);

            fireMindswapEvent(MarkupModelEvent.PREFS_CHANGED);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void refreshStoresFromPrefs()
    {
        mStores.clear();

        try {
            Preferences sites = getPrefs().node(PREF_STORE_NODE);

            String[] children = sites.childrenNames();
            for (int i = 0; i < children.length; i++)
            {
                String siteName = children[i];
                Preferences siteNode = sites.node(siteName);

                String type = siteNode.get(PREF_TYPE,"");

//                if (type.equals("") || type.equals(PREF_TYPE_SITE))
//                {
//                    String submitURL = siteNode.get(PREF_SUBMIT_URL,"");
//                    String instanceURL = siteNode.get(PREF_INSTANCE_URL,"");
//
//                    mStores.add(new SiteStore(siteName,this,instanceURL,submitURL));
//                }
//                else
                if (type.equals(PREF_TYPE_METASTORE))
                {
                    try {
                        String implURI = siteNode.get(PREF_IMPL,"");

                        if (!implURI.equals(""))
                        {
                            Store aStore = mLoader.getStore(URI.create(implURI));
                            addStore(aStore);
                        }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                        sites.remove(siteName);
                    }
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Returns a list of all the stores in the model
     * @return Vector
     */
    public Vector getStores() {
        return new Vector(mStores);
    }

    /**
     * Add a new markup model listener
     * @param ml MarkupModelListener the listener
     */
    public void addMarkupModelListener( MarkupModelListener ml) {
        getMarkupModelListenerSupport().addMarkupModelListener( ml );
    }

    /**
     * Remove a markup model listner
     * @param ml MarkupModelListener the listener
     */
    public void removeMarkupModelListener( MarkupModelListener ml ) {
        getMarkupModelListenerSupport().removeMarkupModelListener( ml );
    }

    /**
     * Notifies the model that is has changed
     */
    public void modelChanged() {
        modelChanged(MarkupModelEvent.MODEL_CHANGED);
    }

    /**
     * Notifies the model of the specified change event
     * @param modelEventID int the event id
     */
    public void modelChanged(int modelEventID) {
        MarkupModelEvent e = new MarkupModelEvent(this, modelEventID);
        getMarkupModelListenerSupport().fireMarkupModelEvent(e);
    }

    private MarkupModelListenerSupport mmls;
    private MarkupModelListenerSupport getMarkupModelListenerSupport() {
        if (mmls==null) {
            mmls = new MarkupModelListenerSupport();
        }
        return mmls;
    }

    private OntModel masterOntologyModel;
    /**
     * Returns a model that comprises all the loaded ontologies. Equivalent to
     * the sum of all the models returned by getOntologyModels()
     *
     * @return OntModel
     */
    public OntModel getMasterOntologyModel() {

        if (masterOntologyModel == null)
        {
            OntModel aMasterOntologyModel = createOntModel();

            Iterator iter = getOntologyModels().keySet().iterator();
            while (iter.hasNext())
            {
                Object key = iter.next();
                OntModel aModel = (OntModel)mOntologyModels.get(key);
                aMasterOntologyModel.add(aModel);
            }

            masterOntologyModel = aMasterOntologyModel;
        }
        return masterOntologyModel;
    }

    private LinkedHashMap mOntologyModels;
    /**
     * Returns a collection of all the ontology models currently loaded into the program.
     * @return LinkedHashMap
     */
    public LinkedHashMap getOntologyModels() {
        if (mOntologyModels==null) {
            mOntologyModels = new LinkedHashMap();
        }
        return mOntologyModels;
    }

    /**
     * Adjusts the url as needed. In particular, when it is a local
     * url change it to the corresponding remote url (where the local
     * file got uploaded to).
     */
    public URL adjustURL(URL url, URL remoteURL) {
    	//by arun
    	//System.out.println("ADJUST URL CALLED");
        if (url != null && BasicUtils.isFileURL(url.toExternalForm()) && remoteURL != null) {
            try {
                // TODO: use image server from connected server?
                url = new URL(fileURLToRemoteURL(url.toExternalForm(),remoteURL.toString()));
            } // try
            catch (Exception ex) { ex.printStackTrace(); }
        } // if
        return url;
    }

    /**
     * Returns a model that contains the RDF representation of the data in the model
     * @return OntModel
     */
    public OntModel getWorkingModel(String theResolveURL) {
        OntModel workingModel = createOntModel();

        workingModel.createOntology(null).addImport(workingModel.getResource(PROFILE.URI().toString()));

        LinkedHashMap models = getMediaModels();
        Set entries = models.entrySet();
        Iterator iter = entries.iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            MediaModel aMediaModel = (MediaModel) (entry.getValue());

            // this is our degenerate model, just ignore it
            if (aMediaModel.getMediaURL() == null)
                continue;

            URL aOldURL = aMediaModel.getMediaURL();

            // if we're adjusting urls, lets temporarily set the media model url
            // for when it serializes
            if (theResolveURL != null) {
                try {
                    String aRemoteURL = adjustURL(aOldURL,new URL(theResolveURL)).toString();
                    aMediaModel.setMediaURL(new URL(aRemoteURL));
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            Model m = aMediaModel.asModel();
            workingModel.add(m);

            if (theResolveURL != null)
            {
                try {
                    // now we'll set it back to what it originally was
                    aMediaModel.setMediaURL(aOldURL);

                    String aRemoteURL = adjustURL(aOldURL,new URL(theResolveURL)).toString();
                    Resource aRes = workingModel.getResource(aOldURL.toString());
                    aRes = ResourceUtils.renameResource(aRes,aRemoteURL);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (mInstances != null) {
            Set aSet = mInstances.entrySet();
            Iterator aIter = aSet.iterator();
            while (aIter.hasNext()) {
                Map.Entry entry = (Map.Entry)aIter.next();
                Resource inst = (Resource) (entry.getValue());
                workingModel.add(inst.listProperties());
            }
        }

        return workingModel;
    }

    /**
     * Returns the RDF/XML representation of the data model
     * @return String
     */
    public String getRDF() {
        java.io.StringWriter sw = new java.io.StringWriter();
        Model working = getWorkingModel(mCurrentStore.getImageURL());
        working.write(sw, null, getBaseURL());
        return sw.toString();
    }

    private boolean submitMediaToRemoteSite(Store theStore, MediaModel theModel)
    {
        // TODO: have a better way to devining the media type
        // or at least a better abstracted way of getting to the media file type object thing
        if (theModel instanceof MarkupImageModel)
        {
            try {
                modelChanged(MarkupModelEvent.START_WAIT_EVENT);
                boolean val = saveImageRemotely(adjustURL(theModel.getMediaURL(),new URL(theStore.getImageURL())).toExternalForm(),(ImageIcon)((MarkupImageModel)theModel).getIcon());
                modelChanged(MarkupModelEvent.END_WAIT_EVENT);

                return val;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        }
        else return false;
    }

    /**
     * Submits the RDF generated from the working model to a remote site
     *
     * @param theDB Store - store to submit the RDF to
     * @throws IOException
     * @throws Exception
     */
    public void submitRDF(final Store theDB) {
        boolean success = true;
        Iterator iter = getMediaModels().values().iterator();
        boolean localSave = false;

        Store aPrevStore = mCurrentStore;
        mCurrentStore = theDB;

        Model aSubmitModel = getWorkingModel(theDB.getImageURL());

        removeModel(aSubmitModel,mImportModel);

        while (iter.hasNext() && success) {
            MediaModel aModel = (MediaModel)iter.next();

            // this is our degenerate model
            // it does not have an image, lets just skip it
            if (aModel.getMediaURL() == null)
                continue;

            // TODO: should use the image server url specified by what we're submitting to
            if (aModel.getMediaURL().toExternalForm().startsWith("file:/") && theDB.getImageURL() != null) {
                try {
                    success = submitMediaToRemoteSite(theDB, aModel);

                    localSave = true;
                } catch (Exception e) {
                    success = false;
                    System.out.println("Unable to save image remotely. TODO: handle this exception... alert user with panel?");
                }
                finally {
                    //modelChanged(MarkupModelEvent.END_WAIT_EVENT);
                }
            }
        }

        if (success)
        {
            if (localSave)
                JOptionPane.showMessageDialog(null,"Local images saved to remote server", "Local Image Save", JOptionPane.INFORMATION_MESSAGE);
                // TODO: what happens when the images fail to be sent to the server???

            boolean didSubmitWork = false;

            try {
                modelChanged(MarkupModelEvent.START_WAIT_EVENT);
                long start = System.currentTimeMillis();
                didSubmitWork = theDB.add(aSubmitModel);
                long end = System.currentTimeMillis();
                modelChanged(MarkupModelEvent.END_WAIT_EVENT);
                System.err.println("total submit time: "+(end-start));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            if (didSubmitWork)
            {
                StmtIterator changeIter = mChangeTracker.getChanges();
                long start = System.currentTimeMillis();
                while (changeIter.hasNext())
                {
                    Statement aChange = changeIter.nextStatement();

                    try {
                        theDB.delete(aChange);
                    }
                    catch (Exception ex) {
                        // remove failed
                        //ex.printStackTrace();
                        System.err.println("remove failed: "+ex.getMessage());
                    }
                }
                long end = System.currentTimeMillis();
                System.err.println("total remove time: "+(end-start));
            }

            if (didSubmitWork) {
                submitClear();
                modelChanged(MarkupModelEvent.STORE_CHANGED);
                JOptionPane.showMessageDialog(null, "Your metadata submission was successful.", "Submit Successful!", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                JOptionPane.showMessageDialog(null, "Your metadata submission was NOT successful.", "Submit Failed!", JOptionPane.ERROR_MESSAGE);
                mCurrentStore = aPrevStore;
            }
        } // if
        else {
            mCurrentStore = aPrevStore;
            JOptionPane.showMessageDialog(null,"Failed to save images, cannot complete save", "Submit Error!", JOptionPane.ERROR_MESSAGE);
        }

        mImportModel.removeAll();
    }

    /**
     * Save the specified image to a remote server corresponding to the url parameter
     * @param url String the url of the remote image server
     * @param ii ImageIcon the image to save
     * @return boolean true if the image was successfully saved remotely, false otherwise
     */
    private boolean saveImageRemotely(String url, ImageIcon ii) {
        boolean saved = false;
        try
        {
            int exists = org.mindswap.utils.DavUtil.doesExist(url);
            if (exists == 1) // image already exists remotely
            {
                saved = true;
            } // if
            else if (exists == 0) { // image does not exist remotely
              java.awt.image.BufferedImage bi = new java.awt.image.BufferedImage(ii.getIconWidth(),ii.getIconHeight(),java.awt.image.BufferedImage.TYPE_INT_ARGB);
              ii.paintIcon(null,bi.getGraphics(),0,0);

              java.io.File imgFile = java.io.File.createTempFile("tempImg","png",null);

              javax.imageio.ImageIO.write(bi, "png", imgFile);

              saved = org.mindswap.utils.DavUtil.saveFile(imgFile,url);

              imgFile.deleteOnExit();
            }
            else return false; // user canceled save
        } // try
        catch (Exception ex)
        {
          ex.printStackTrace();
          saved = false;
        } // catch
        return saved;
    }
    
    // arun
    
    public int getNoOfOntologies()  {
    	return this.getOntologyModels().size();
    }


    private LinkedHashMap revOntModels = new LinkedHashMap();

    public LinkedHashMap getRevOntoModels()  {
	    return revOntModels;
	}

    /**
     * Adds a new ontology to the model
     * @param input URL
     */
    public void addOntology(URL input) {
        //OntModel masterModel = getMasterOntologyModel();
    	//System.out.println("WITHIN AddOntology the URL is " + input.toString());
        LinkedHashMap models = getOntologyModels();
        //System.out.println("In add size = " + models.size());

        if (input != null && !input.equals("")) {
            if (models.containsKey(input)==false) {
                		
		OntModel m = createOntModel();

                m.setStrictMode(true);
                m.read(input.toString());
                if(models.containsKey(input)) {
                	System.out.println("REMOVING IN ADDONTOLOGY*******");
                	models.remove(input);
                }
                models.put(input, m);
		        revOntModels.put(m, input);
                //masterModel.add(m);
                masterOntologyModel = null;
                getReasoner().addModel(m);

		//by arun
		if (input.toString().startsWith("http"))  {
            		// this is some global ontology
            		//TODO:
            		//TODO: save the global URI in some file. Why??
			try  {
			String uri = input.toString();
			String localFile = uri.substring(uri.lastIndexOf('/'), uri.length());
			File ontDir = new File("Ontologies");
			if(!ontDir.exists()) ontDir.mkdir();
			OutputStream fileStream = new FileOutputStream(new File("Ontologies/" + localFile));
			m.write(fileStream, "RDF/XML-ABBREV");
			} catch(Exception e)  {
				System.err.println("Error in storing the ontology to local cache");
				e.printStackTrace();
			}
          }
		// arun ends

                modelChanged(MarkupModelEvent.ONTOLOGY_LOADED);
            }
        }
    }

    /**
     * Quits the application
     */
    public void exit() {
        Iterator aIter = mStores.iterator();
        while (aIter.hasNext())
        {
            Store aStore = (Store)aIter.next();
            aStore.disconnect();
        }

        System.exit(0);
    }

    /**
     * Returns a dummy base url used for absolutizing relative URLs
     * (and re: relativizing them). Workaround to support relative
     * URLs.
     * @return String
     */
    public String getBaseURL() {
    //commented by arun    return DUMMY_BASE_URL;
    	//by arun
    	return getUriBase()  + "/elster/";
    }
    
    public void setUserId(String i)  {
    	id = i;
    }
    
    public String getUserId()  {
    	return id;
    }
    
    public void setUriBase(String ur)  {
    	uriBase = ur;
    }
    
    public String getUriBase()  {
    	return uriBase;
    }

    /**
     * Returns the url of the server images are saved to
     * @return String
     */
//    public String getImageServerURL() {
//        return getPrefs().get("image server url", DEFAULT_IMAGE_SERVER_URL);
//    }
//
//    /**
//     * Sets the url of the server to which local images are saved
//     * @param url String
//     */
//    public void setImageServerURL(String url) {
//        getPrefs().put("image server url", url);
//        modelChanged(MarkupModelEvent.PREFS_CHANGED);
//    }

    private static Preferences prefs;
    private static Preferences getPrefs() {
        if (prefs == null) {
            prefs = Preferences.userNodeForPackage(MarkupModel.class).node("PS");
        }
        return prefs;
    }

    /**
     * Returns the N3 representation of the data model
     * @return String
     */
    public String getN3() {
        java.io.StringWriter sw = new java.io.StringWriter();
        Model working = getWorkingModel(mCurrentStore.getImageURL()).getBaseModel();
        working.getWriter("N3-PP").write(working,sw,null);
        return sw.toString();
    }

    /**
     * Returns the list of all classes (non-anonymous, non-RDF/OWL) in the master model
     * @return Vector
     */
    public Set getAllClasses() {
        Set s = getReasoner().listClasses();
        TreeSet sorted = new TreeSet(new ResourceComparator());
        sorted.addAll(s);

        return sorted;
    }

    private Map mInstances;
    public Map getInstances() {
        if (mInstances==null) {
            mInstances = new HashMap();
        }
        return mInstances;
    }

    public void setInstances(Map theInstances) {
        mInstances = theInstances;
    }

//    public Iterator listInstances() {
//        return getInstances().values().iterator();
//    }

    public Resource getInstance(String url) {
        Resource instance = (Resource)getInstances().get(url);
        if (instance == null && getCurrentStore() != null) {
            try {
                instance = getCurrentStore().getResource(url);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }

            if (instance != null && (!instance.listProperties().hasNext() || !JenaUtils.isIndividual(instance)))
                instance = null;
        }

        if (instance == null)
        {
            // no instance by that id in the local kb
            // and we're not connected to a specific store
            // lets first check any live stores for the instance
            // if its not found, then we'll check the imported instances
//            Iterator iter = getStores().iterator();
//            while (iter.hasNext())
//            {
//                Store aStore = (Store)iter.next();
//                if (aStore instanceof LiveStore && getAlwaysUseLiveStores())
//                {
//                    Resource aInst = ((LiveStore)aStore).getResource(url);
//                    if (aInst != null)
//                    {
//                        instance = aInst;
//                        break;
//                    }
//                }
//            }


            instance = getLoadedInstances().getResource(url);

            // if it has no props, then there's no information about it in this model
            // which means it doesnt exist...the OntModel just created a placeholder
            if (!instance.listProperties().hasNext() || !JenaUtils.isIndividual(instance))
                instance = null;
        }

        return instance;
    }

    public void addInstanceToList(Resource theInstance)
    {
        if (getInstances().containsKey(theInstance.getURI()))
        {
            Resource aExisting = (Resource)getInstances().get(theInstance.getURI());
            StmtIterator sIter = aExisting.listProperties();
            while (sIter.hasNext())
            {
                Statement stmt = sIter.nextStatement();
                theInstance.addProperty(stmt.getPredicate(),stmt.getObject());
            }
        }

        getInstances().put(theInstance.getURI(),theInstance);
    } 

    //method added by arun
    
    public void loadInstances()  {
	OntModel m = createOntModel();
	try {
	File instDir = new File("instances/");
	if(instDir.isDirectory())  {                	
          String[] files = instDir.list();
          //System.out.println("NO OF FILES = " + files.length);
          for(int i = 0; i<files.length; i++)  {
          	//System.out.println(files[i]);
          	if(!(new File("instances/" + files[i])).isDirectory()) {
          		FileInputStream in = new FileInputStream(new File("instances/" + files[i]));
          	    if (in == null) {
          	    throw new IllegalArgumentException(
          	                                 "File: " + "instances/" + files[i] + " not found");
          	    }
          		m.read(in, "");

			//ExtendedIterator ex = m.listIndividuals();
			SimpleReasoner reasoner = new SimpleReasoner(this);
			reasoner.addModel(m);

		    Iterator ex = reasoner.listIndividuals().iterator();

		    while (ex.hasNext())
		    {
		      Resource r = (Resource)ex.next();

		      //r.isAnon() ||
		          // if (Utils.getInstanceType(r) == null || Utils.isLanguageTerm(r))
		             //   continue;


		    //  if (!getLoadedInstances().containsResource(r))
		     // {
		    	  addInstanceToList(r);
		     // }
		    }
	        modelChanged();
          	}
		}
      }
	} catch (Exception e)  {
		System.err.println("Error in loading instances");
		e.printStackTrace();
	}
   }
    
    //method added by arun
   





public void loadInstances(String ins)  {
	OntModel m = createOntModel();
	try {
	File instDir = new File(".jxta/instances/");
	if(instDir.isDirectory())  {                	         
          	//System.out.println(files[i]);
          	if(!(new File("instances/" + ins)).isDirectory()) {
          		FileInputStream in = new FileInputStream(new File(".jxta/instances/" + ins));
          	    if (in == null) {
          	    throw new IllegalArgumentException(
          	                                 "File: " + ".jxta/instances/" + ins + " not found");
          	    }
          		m.read(in, "");

			//ExtendedIterator ex = m.listIndividuals();
			SimpleReasoner reasoner = new SimpleReasoner(this);
			reasoner.addModel(m);
                        System.out.println("Instances loaded");

		    Iterator ex = reasoner.listIndividuals().iterator();

		    while (ex.hasNext())
		    {
		      Resource r = (Resource)ex.next();

		      //r.isAnon() ||
		          // if (Utils.getInstanceType(r) == null || Utils.isLanguageTerm(r))
		             //   continue;


		    //  if (!getLoadedInstances().containsResource(r))
		     // {
		    	  addInstanceToList(r);
		     // }
		    }
	        modelChanged();
          	}
		}      
	} catch (Exception e)  {
		System.err.println("Error in loading instances");
		e.printStackTrace();
	}
   }

   
    public void loadAnnotations()  {
	OntModel m = createOntModel();
	try {
	File instDir = new File("annotations/");
	if(!instDir.exists()) instDir.mkdir();
	if(instDir.isDirectory())  {                	
          String[] files = instDir.list();
         // System.out.println("NO OF FILES = " + files.length);
          for(int i = 0; i<files.length; i++)  {
          	//System.out.println(files[i]);
          	if(!(new File("annotations/" + files[i])).isDirectory()) {
          		FileInputStream in = new FileInputStream(new File("annotations/" + files[i]));
          	    if (in == null) {
          	    throw new IllegalArgumentException(
          	                                 "File: " + "annotations/" + files[i] + " not found");
          	    }
          		m.read(in, "");

			//ExtendedIterator ex = m.listIndividuals();
			SimpleReasoner reasoner = new SimpleReasoner(this);
			reasoner.addModel(m);

		    Iterator ex = reasoner.listIndividuals().iterator();

		    while (ex.hasNext())
		    {
		      Resource r = (Resource)ex.next();

		      //r.isAnon() ||
		          // if (Utils.getInstanceType(r) == null || Utils.isLanguageTerm(r))
		             //   continue;


		      if (!getLoadedInstances().containsResource(r))
		      {
		    	  addInstanceToList(r);
		      }
		    }
	        modelChanged();
          	}
		}
      }
	else System.err.println("No annotations available");
	
	} catch (Exception e)  {
		System.err.println("Error in loading instances");
		e.printStackTrace();
	}
   }



 public void loadAnnotations(String file)  {
	OntModel m = createOntModel();
	try {
	File instDir = new File(".jxta/annotations/");
	if(!instDir.exists()) instDir.mkdir();
	if(instDir.isDirectory())  {                	         
            if(!(new File(".jxta/annotations/" + file)).isDirectory()) {
               FileInputStream in = new FileInputStream(new File(".jxta/annotations/" + file));
               if (in == null) {
                throw new IllegalArgumentException(
                              "File: " + ".jxta/annotations/" + file + " not found");
                }
             m.read(in, "");
	     SimpleReasoner reasoner = new SimpleReasoner(this);
	     reasoner.addModel(m);
             System.out.println("Annotations loaded");
              Iterator ex = reasoner.listIndividuals().iterator();
	    while (ex.hasNext())
	    {
              Resource r = (Resource)ex.next();
		      if (!getLoadedInstances().containsResource(r))
		      {
		    	  addInstanceToList(r);
		      }
	     }
	        modelChanged();
            }
        }
        else System.err.println("No annotations available");
	
	} catch (Exception e)  {
		System.err.println("Error in loading instances");
		e.printStackTrace();
	}
   }





   //by arun
    public void addOnt(String k, OntModel ao)  {
    	LinkedHashMap models = getOntologyModels();
    	try  {
    		URL key = new URL(k);
    		models.put(key, ao);	
    	} catch(Exception e)  {
    		e.printStackTrace();
    	}    	
    }
    
    public String removeOnt(OntModel ro)  {
    	
    	LinkedHashMap models = getOntologyModels();
    	String ontKey = "";    
    	for (Iterator it=models.entrySet().iterator(); it.hasNext(); ) {
            	Map.Entry entry = (Map.Entry)it.next();
    	        URL key = (URL)entry.getKey();   
    	        System.out.println("Current Key is " + key.toString());
            	OntModel value = (OntModel)entry.getValue();            	
            	if(value.equals(ro)) {	
            		ontKey = key.toString();            		
            		models.remove(key);            		
            	    mReasoner.removeModel(ro);
            	    System.out.println("Removed Key is " + ontKey);
            	}
            	else  {
            		continue;            		
            	}
    	}    	
    	modelChanged();    	
    	return ontKey;
    }
    
    public boolean saveOntology(OntModel om)  {
	File saveFile = null;
	String location = "";	
	LinkedHashMap models = getOntologyModels();		
	for (Iterator it=models.entrySet().iterator(); it.hasNext(); ) {
        	Map.Entry entry = (Map.Entry)it.next();
	        URL key = (URL)entry.getKey();
        	OntModel value = (OntModel)entry.getValue();
	        if(value.equals(om)) 
			location = key.toString();
		else
			continue;
    	}	
	try  {
		//System.out.println("IN SAVE Location is " + location);
		String saveFileName = "";
	    String ontName = location.substring(location.lastIndexOf('/') + 1, location.length());
	    if(ontName.indexOf('_') > -1)
	    	saveFileName = ontName;
	    else
        saveFileName = this.getUserId() + "_" + ontName;
        File ontDir = new File("ontologies");
		if(!ontDir.exists()) ontDir.mkdir();
		saveFile = new File("ontologies/" + saveFileName);   
	    
    	OutputStream fileStream = new FileOutputStream(saveFile);
    	om.write(fileStream, "RDF/XML-ABBREV");
    	return true;
		} catch (Exception e)  {
			return false;
		//	e.printStackTrace();
		}
    }
    //arun ends

    public void saveInstance(Resource toSave, boolean clearForm)
    {
        // this method makes sure the instances edited in the form
        // are saved back into the list and everything is up to date
        addInstanceToList(toSave);
        Property p = null;
        RDFNode n = null;
        SimpleSelector selector = new SimpleSelector(toSave, p, n);
                
        //by arun
        
        Model workingm = ModelFactory.createDefaultModel();
        OntModel omodel = createOntModel();
        omodel = getWorkingModel(mCurrentStore.getImageURL());
        workingm = getWorkingModel(mCurrentStore.getImageURL());
        Property prop = workingm.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "type");
        Iterator itr = getInstancesWithType(prop);
        //if(workingm.containsResource(toSave)) System.out.println("RESOURCE PRESENT");
        StmtIterator allStatements = workingm.listStatements();
        StmtIterator save = workingm.listStatements(selector);
       /* while (allStatements.hasNext())  {
        	Statement stmt = allStatements.nextStatement();
        	Resource currentResource = stmt.getResource();
        	if (currentResource.equals((Resource)toSave)) continue;
        	else workingm.remove(stmt);
        	//if(!allStatements.nextStatement().getResource().equals((Resource)toSave)) {System.out.println("REMOVING"); workingm.remove(allStatements.nextStatement());}
        	 //else System.out.println("KEEPING");
        }*/
        //save to file
        Resource aRes = workingm.getResource(toSave.getURI());
        Model mTemp = ModelFactory.createDefaultModel();
        Model mTemp2 = ModelFactory.createDefaultModel();
        StmtIterator sIter = aRes.listProperties();
        mTemp.add(sIter);
        while(itr.hasNext())  {
        	Resource currRes = (Resource)itr.next();
        	StmtIterator curr = currRes.listProperties();
        	mTemp.add(curr);
        }
      //  Property prop = workingm.getProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "type");
        Statement typeSt = aRes.getProperty(prop);
        RDFNode obj = typeSt.getObject();
        ExtendedIterator ei = omodel.listIndividuals((Resource)obj);
        while(ei.hasNext())  {
        	Resource current = (Resource) ei.next();
        	StmtIterator itre = current.listProperties();
            mTemp2.add(itre);            
        }
        String type = obj.toString();
        //System.out.println("THIS IS CLASS" + type);
        String instanceDirName = type.substring(type.lastIndexOf('/'), type.length());
        int in = instanceDirName.lastIndexOf('#');
        instanceDirName = instanceDirName.substring(in+1, instanceDirName.length()) + ".rdf";
        if(instanceDirName.equals("")) instanceDirName = type.substring(type.lastIndexOf('#'), type.length()) + ".rdf";
        
        Map ns = workingm.getNsPrefixMap();
        mTemp.setNsPrefixes(ns);
        if(workingm.samePrefixMappingAs(mTemp)) System.out.println("TRUE");
        Model diff = workingm.difference(mTemp);
        diff = workingm.difference(diff);
      //  if (workingm.containsAll(mTemp)) System.out.println("MODEL CONTAINED");
        //System.out.println("INS URI ...." + toSave.getURI());
        String instanceFileName = toSave.getLocalName() + ".rdf";
	File instDir = new File("instances");
	if(!instDir.exists()) instDir.mkdir();
        ////File instanceFile = new File("Instances/" + instanceFileName);
	File instanceFile = new File("instances/" + instanceDirName);
        try  {
        OutputStream fileStream = new FileOutputStream(instanceFile);
        //mTemp.write(fileStream, "RDF/XML-ABBREV");
        RDFWriter writer = mTemp2.getWriter();
        writer.write(mTemp2, fileStream, null);
        } catch(Exception e)  {
        	System.err.println("Error in writing instance file");
			e.printStackTrace();
        }
        //arun ends

        // no label specified, lets add one
        if (!toSave.hasProperty(RDFS.label))
            toSave.addProperty(RDFS.label,toSave.getLocalName());

//        mSaveSet = new HashSet();
//        saveInstanceR(toSave);

        if (clearForm)
            setCurrentInstance(null);

        modelChanged();
    }

    // used to prevent infinite loops in the saveInstanceR function
    // tracks what things have already been processed
//    private HashSet mSaveSet = new HashSet();
//    private void saveInstanceR(Resource inst) {
//        //if (DEBUG)
//            System.err.println("MarkupModel.saveInstanceR("+inst+")");
//
//        mSaveSet.add(inst);
//        StmtIterator sIter = inst.listProperties();
//        while (sIter.hasNext()) {
//            Statement stmt = sIter.nextStatement();
//
//            if (SKIP.contains(stmt.getPredicate()))
//                continue;
//            else if (JenaUtils.isLanguageTerm(inst))
//                break;
//
//            // this is an object property
//            if (stmt.getObject() instanceof Resource)
//            {
//                Resource aInst = (Resource)stmt.getObject();
//
//                if (aInst.equals(inst) || mSaveSet.contains(aInst) || JenaUtils.isLanguageTerm(aInst))
//                    continue;
//System.err.println(stmt.getPredicate());
//                // no label specified, lets add one
//                if (!aInst.hasProperty(RDFS.label) && aInst.getLocalName() != null)
//                    aInst.addProperty(RDFS.label,aInst.getLocalName());
//
//                addInstanceToList(aInst);
//                saveInstanceR(aInst);
//            }
//        }
//        sIter.close();
//    }

//    public void deleteInstance(Resource toDelete, Resource parent, String prop) {
//        Property aProp = null;
//        if (prop != null)
//            parent.getModel().getProperty(prop);
//
//        // delete the specified reference to this object
//        if (parent != null && aProp != null) {
//            deleteProperty(parent,aProp,toDelete);
//            getInstances().put(parent.getURI(),parent);
//            if (parent.equals(getCurrentInstance()))
//                setCurrentInstance(parent);
//        }
//
//        // now lets see if the instance to delete is used elsewhere
//        boolean hasReference = false;
//
//        Map list = getInstances();
//        Set keys = list.keySet();
//        Iterator keyIter = keys.iterator();
//        while (keyIter.hasNext()) {
//            String key = (String) keyIter.next();
//            Resource inst = (Resource) list.get(key);
//            if (inst.hasProperty(null,toDelete))
//                hasReference = true;
//        }
//
//        // if used no where else, lets delete it altogether
//        if (!hasReference) {
//            getInstances().remove(toDelete.getURI());
//            getMediaModel(getMediaURL()).removeDepicts(toDelete);
//            Iterator iter = getMediaModel(getMediaURL()).getRegionDepictions(toDelete);
//            while (iter.hasNext()) {
//                MediaRegion region = (MediaRegion)iter.next();
//                getMediaModel(getMediaURL()).removeRegionDepicts(region,toDelete);
//            }
//        }
//
//        // deleteing the instance we are editing, we ought to reset the form
//        if (toDelete.equals(getCurrentInstance())) {
//            setCurrentInstance(null);
//        }
//
//        modelChanged();
//    }

    /**
     * Performs a clear all on the model, clearing all image models, removes all instances,
     * clears the form, and removes all loaded ontologies
     */
    public void clearAll() {
        LinkedHashMap imgModels = getMediaModels();
        Iterator i = imgModels.values().iterator();
        while (i.hasNext()) {
            MediaModel m = (MediaModel) i.next();
            m.clearAll();
        }

        getInstances().clear();
        setCurrentInstance(null);
        setMediaURL(null);
        setMediaModels(null);
        masterOntologyModel = null;
        mOntologyModels = null;
        cancelPendingChanges();

        mReasoner=null;
        modelChanged();
        modelChanged(MarkupModelEvent.CLEAR_ALL);
        formChanged();
    }

    private void submitClear() {
        LinkedHashMap imgModels = getMediaModels();
        Iterator i = imgModels.values().iterator();
        while (i.hasNext()) {
            MediaModel m = (MediaModel) i.next();
            m.clearAll();
        }

        getInstances().clear();
        setCurrentInstance(null);
        setMediaURL(null);
        setMediaModels(null);
        cancelPendingChanges();

        modelChanged();
        formChanged();
    }

    /**
     * Clear this model, removes all depictions, instances, and clears the form
     */
    public void clear() {
        LinkedHashMap aModels = getMediaModels();
        Iterator i = aModels.values().iterator();
        while (i.hasNext()) {
            MediaModel m = (MediaModel) i.next();
            m.clearDepictions();
        }

        getInstances().clear();
        cancelPendingChanges();
        setCurrentInstance(null);
        modelChanged();
        formChanged();
    }

//    public void loadInstances(Store theStore) {
//        // this forces the store obj to get the instances from the store
//        theStore.connect();
//
//        modelChanged(MarkupModelEvent.INSTANCES_LOADED);
//    }

    public OntModel getLoadedInstances() {

        OntModel aModel = createOntModel();

//        Iterator iter = getStores().iterator();
//        while (iter.hasNext())
//        {
//            Store aStore = (Store)iter.next();
//
//            if (aStore instanceof LiveStore && !getAlwaysUseLiveStores())
//                continue;
//
//            Model aStoreModel = aStore.getInstances();
//            if (aStoreModel != null) {
//                aModel.add(aStoreModel);
//            }
//        }

        try {
            if (getCurrentStore() != null)
                aModel.add(getCurrentStore().asModel());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return aModel;
    }

    private MarkupReasoner mReasoner;
    /**
     * Returns the reasoner associated with this markup model
     * @return MarkupReasoner the reasoner for the model
     */
    public MarkupReasoner getReasoner() {
    	//System.out.println("get reasoner called");
    	if (mReasoner == null)
        {
    		//System.out.println("mReasoner was null");
            if (USE_PELLET)  {
                //System.out.println("USE PALLET TRUE");
            	mReasoner = new PelletReasoner(this);
            }
            else mReasoner = new SimpleReasoner(this);
        }
        return mReasoner;
    }

    /**
     * Notifies the model that the form has changed
     */
    public void formChanged() {
        fireMindswapEvent(MarkupModelEvent.FORM_CHANGED);
    }

    /**
     * Notifies the model that the form was selected
     */
    public void formSelected() {
        fireMindswapEvent(MarkupModelEvent.FORM_SELECTED);
    }

    /**
     * Notifies the model that a region was selected
     */
    public void regionSelected() {
        fireMindswapEvent(MarkupModelEvent.REGION_SELECTED);
    }

    /**
     * Notifies the model that an image was selected
     */
    public void mediaSelected() {
        fireMindswapEvent(MarkupModelEvent.MEDIA_SELECTED);
    }

    private void fireMindswapEvent(int id) {
        MarkupModelEvent e = new MarkupModelEvent(this,id);
        getMarkupModelListenerSupport().fireMarkupModelEvent(e);
    }

    /**
     * Takes a url to the local file system (for an image) and turns it into
     * a remote url on the image server
     * @param url String
     * @return String
     */
    private String fileURLToRemoteURL(String url, String imgServerURL) {
        String fn = url.substring(url.lastIndexOf("/")+1);
        fn = fn.replaceAll("%20"," ");
        if (!imgServerURL.endsWith("/"))
            imgServerURL += "/";
        return imgServerURL+fn;
    }

    /**
     * Returns a list of all instances in the model, both created and loaded instances
     * @return Iterator
     */
//    public Iterator listAllInstances() {
//        Vector filter = new Vector();
//
//        if (getCurrentStore() != null)
//        {
//            Model store = getCurrentStore().getInstances();
//            Iterator iter = JenaUtils.listIndividuals(store);
//            while (iter.hasNext())
//            {
//                Resource aInd = (Resource)iter.next();
//                if (aInd.isAnon() && FILTER_ANON)
//                    continue;
//                if (!filter.contains(aInd))
//                    filter.add(aInd);
//            }
//        }
//
//        OntModel loaded = getLoadedInstances();
//        Iterator eIter = JenaUtils.listIndividuals(loaded);
//        while (eIter.hasNext())
//        {
//            Resource aInd = (Resource)eIter.next();
//            if (aInd.isAnon() && FILTER_ANON)
//                continue;
//            if (!filter.contains(aInd))
//                filter.add(aInd);
//        }
//
//        Set keys = getInstances().keySet();
//        Iterator iter = keys.iterator();
//        while (iter.hasNext()) {
//            Object key = iter.next();
//            Resource inst = (Resource) getInstances().get(key);
//            if (!filter.contains(inst))
//                filter.add(inst);
//        }
//
//        java.util.Collections.sort(filter,new org.mindswap.markup.utils.ResourceComparator());
//
//        return filter.iterator();
//    }

    /**
     * Returns whether or not Qnames should be used when building the user interface
     * @return boolean - true if QNames should be used, false otherwise
     */
    public static boolean getQNames() {
        String s = getPrefs().get("qnames", "true");
        Boolean b = new Boolean(s);
        return b.booleanValue();
    }

    /**
     * Sets whether or not Qnames should be used when building the user interface
     * @param value boolean - true if Qnames should be used, false otherwise
     */
    public static void setQNames(boolean value) {
        Boolean b = new Boolean(value);
        getPrefs().put("qnames", b.toString());
    }

    /**
     * Add a new bookmark to the model
     * @param label String the label of the bookmark, used for UI display
     * @param url String the url of the bookmark
     * @param type int the type of the bookmark
     */
    public void addBookmark(String label, String url, int type)
    {
        try {
            Preferences bookmarks = getPrefs().node("bookmarks");

            Preferences typeNode = bookmarks.node(String.valueOf(type));
            if (!typeNode.nodeExists(url))
                typeNode.put(url,label);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Remove a bookmark with the specified url
     * @param url String the url of the bookmark to remove
     */
    public void removeBookmark(String url)
    {
        try {
            Preferences bookmarks = getPrefs().node("bookmarks");
            String[] children = bookmarks.childrenNames();

            // there's no test to see if a node has a value
            // so we'll just iterate over the entire set and
            // do the remove from each type node and hope we actually
            // remove it =)
            for (int i = 0; i < children.length; i++)
            {
                Preferences typeNode = bookmarks.node(children[i]);
                typeNode.remove(url);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * List all the bookmarks in the model
     * @return Set a list of all bookmarks
     */
    public Set listBookmarks()
    {
        Set s = new HashSet();
        try {
            Preferences bookmarks = getPrefs().node("bookmarks");
            String[] children = bookmarks.childrenNames();
            for (int i = 0; i < children.length; i++)
            {
                Preferences typeNode = bookmarks.node(children[i]);
                int type = new Integer(typeNode.name()).intValue();
                String[] keys = typeNode.keys();
                for (int j = 0; j < keys.length; j++)
                    s.add(new org.mindswap.utils.Bookmark(type,typeNode.get(keys[j], ""),keys[j]));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        Iterator aIter = mStores.iterator();
        while (aIter.hasNext())
        {
            Store aStore = (Store)aIter.next();
            if (aStore.getBookmarks() != null && !aStore.getBookmarks().isEmpty())
                s.addAll(aStore.getBookmarks());
        }

        return s;
    }

    /**
     * Returns the most specific type for an instance
     * @param inst Instance the instance
     * @return Resource the resource corresponding to the most specific type of the instance
     */
//    public Resource getMostSpecificType(Resource inst)
//    {
//        Set typeSet = JenaUtils.getTypes(inst,true);
//
//        Vector types = new Vector(typeSet);
//
//        if (types.isEmpty())
//        {
//            typeSet = getReasoner().getTypes(inst);
//            types = new Vector(typeSet);
//            //return null;
//        }
//
//        if (types.isEmpty())
//            types = new Vector(JenaUtils.getTypes(inst,false));
//
//        if (types.isEmpty())
//            return null;
//
//        java.util.Collections.sort(types, new SubClassResourceSortComparator(this));
//        String typeName = types.lastElement().toString();
//
//        return getMasterOntologyModel().getResource(typeName);
//    }

//    public boolean getUploadLocalImages() {
//        String s = getPrefs().get("upload_local_images", "false");
//        Boolean b = new Boolean(s);
//        return b.booleanValue();
//    }
//
//    public void setUploadLocalImages(boolean b) {
//        Boolean aVal = new Boolean(b);
//        getPrefs().put("upload_local_images",aVal.toString());
//    }

    public boolean getAutoLoadImageMetadata() {
        String s = getPrefs().get("autoload_imagemetadata", "false");
        Boolean b = new Boolean(s);
        return b.booleanValue();
    }

    public void setAutoLoadImageMetadata(boolean b) {
        Boolean aVal = new Boolean(b);
        getPrefs().put("autoload_imagemetadata",aVal.toString());
    }

//    public boolean getAutoLoadSubmissions() {
//        String s = getPrefs().get("autoload_submissions", "false");
//        Boolean b = new Boolean(s);
//        return b.booleanValue();
//    }
//
//    public void setAutoLoadSubmissions(boolean b) {
//        Boolean aVal = new Boolean(b);
//        getPrefs().put("autoload_submissions",aVal.toString());
//    }

    /**
     * Adds a pending depiction to the current image model
     * @param inst Instance
     */
    public void addPendingDepiction(Resource inst) {
System.err.println("adding pending depiction");
        //getMediaModel(getMediaURL()).addPendingDepiction(inst);
        Iterator aIter = getMediaURLs().iterator();
        while (aIter.hasNext())
        {
            URL aURL = (URL)aIter.next();
System.err.println("pending depict for url: "+aURL);
            getMediaModel(aURL).addPendingDepiction(inst);
        }

        clearPendingDepiction();
    }

    /**
     * Clears the pending depiction in the current image model
     */
    public void clearPendingDepiction() {
        //getMediaModel(getMediaURL()).clearPendingDepiction();
        Iterator aIter = getMediaURLs().iterator();
        while (aIter.hasNext())
        {
            URL aURL = (URL)aIter.next();
            getMediaModel(aURL).clearPendingDepiction();
        }
    }

    /**
     * Returns a list of all the instances the model knows about that are the same type as the specfied instance
     * @param theType Resource the type
     * @return Iterator the list of matching instances
     */
    public Iterator getInstancesWithType(Resource theType)
    {
        if (DEBUG)
            System.err.println("start MarkupModel::getInstancesWithType(Resource)");

        HashSet instances = new HashSet();

        if (DEBUG)
            System.err.println("start reasoner get instances");
        long aReasonerTypesStartTime = System.currentTimeMillis();

        instances.addAll(getReasoner().getInstancesWithType(theType));

        long aReasonerTypesEndTime = System.currentTimeMillis();
        if (DEBUG)
            System.err.println("end reasoner get instances ("+(aReasonerTypesEndTime-aReasonerTypesStartTime)+" ms)");

        // if we have a site we're connected to, lets query that for the instance list
        // otherwise we'll rely on the instances we've imported
        if (getCurrentStore() != null)
        {
            Set aInstList = new HashSet();

//            Set types = JenaUtils.getAllSubClassesOf(theType,getMasterOntologyModel());
//            if (!types.contains(theType))
//                types.add(theType);
//
//            Iterator aValIter = getInstances().values().iterator();
//            while (aValIter.hasNext()) {
//                Resource inst = (Resource)aValIter.next();
//                if (BasicUtils.containsAny(getReasoner().getTypes(inst), types) && !aInstList.contains(inst))
//                    aInstList.add(inst);
//            }

            if (theType != null)
            {
                if (DEBUG)
                    System.err.println("start connected store get instances");
                long aStoreTypesStartTime = System.currentTimeMillis();

                //Iterator aReturnVal = getCurrentStore().getInstancesOfType(theType.getURI());

                try {
                    Set types = JenaUtils.getAllSubClassesOf(theType, true);
                    types.add(theType);

                    Iterator iter = JenaUtils.listIndividuals(getCurrentStore().asModel());
                    while (iter.hasNext()) {
                        Resource inst = (Resource)iter.next();

                        if (inst.isAnon() && MarkupModel.FILTER_ANON)
                            continue;

                        if (BasicUtils.containsAny(getReasoner().getTypes(inst), types) && !aInstList.contains(inst))
                            aInstList.add(inst);
                    }
                }
                catch (QueryException ex) {
                    ex.printStackTrace();
                }


                long aStoreTypesEndTime = System.currentTimeMillis();
                if (DEBUG)
                    System.err.println("end connected store get instances ("+(aStoreTypesEndTime-aStoreTypesStartTime)+" ms)");

                //aInstList.addAll(BasicUtils.collectElements(aReturnVal));
            }
            else {
            }

            return aInstList.iterator();
        }

        return instances.iterator();
    }

    public void deleteProperty(Resource theIndividual, Property theProperty, RDFNode theObject)
    {
        // for now we'll pull the model from the resource we're manipulating
        // in the future we'll want to use the markup model, likely the one
        // derived from the db we're connected to
        Model aModel = theIndividual.getModel();
        Statement aStmt = aModel.createStatement(theIndividual,theProperty,theObject);
        aModel.remove(aStmt);

        // should track the statements deleted so the db can be updated properly
    }

    ChangeTracker mChangeTracker = new ChangeTracker();
    public void propertyChanged(Resource theInstance, Property theProperty, Object theOldValue)
    {
        mChangeTracker.propertyChanged(theInstance,theProperty,theOldValue,true);
    }

    public void cancelPendingChangesFor(Resource inst) {
        mChangeTracker.cancelPendingChangesFor(inst);
    }

    public void commitPendingChangesFor(Resource inst) {
        mChangeTracker.commitPendingChangesFor(inst);
    }

    public void cancelPendingChanges() {
        mChangeTracker.cancelPendingChanges();
    }

    public void commitPendingChanges() {
        mChangeTracker.commitPendingChanges();
    }

    public void mediaRegionDepictionRemoved(URL theMediaURL, Resource theDepiction, MediaRegion theRegion)
    {
        // TODO: implement me!
    }

    public void mediaRegionDepictionAdded(URL theMediaURL, Resource theDepiction, MediaRegion theRegion)
    {
        // TODO: implement me!
    }

    public void mediaDepictionRemoved(URL theMediaURL, Resource theDepiction)
    {
        if (theMediaURL == null || theDepiction == null)
            return;

        Model aModel = ModelFactory.createDefaultModel();
        Resource aRes = aModel.createResource(theMediaURL.toExternalForm());

        mChangeTracker.propertyChanged(aRes,PROFILE.depicts(),theDepiction,false);
    }

    public void mediaDepictionAdded(URL theMediaURL, Resource theDepiction)
    {
        if (theMediaURL == null || theDepiction == null)
            return;

        Model aModel = ModelFactory.createDefaultModel();
        Resource aRes = aModel.createResource(theMediaURL.toExternalForm());

        if (mChangeTracker.containsChange(aRes,PROFILE.depicts(),theDepiction))
            mChangeTracker.removeChange(aRes,PROFILE.depicts(),theDepiction);
    }

//    public boolean getAlwaysUseLiveStores() {
//        String s = getPrefs().get("alwaysUseLiveStores", "true");
//        Boolean b = new Boolean(s);
//        return b.booleanValue();
//    }
//
//    public void setAlwaysUseLiveStores(boolean b) {
//        Boolean aVal = new Boolean(b);
//        getPrefs().put("alwaysUseLiveStores",aVal.toString());
//    }

    ////////////////////////////////////////////////////////////////////
    ///////////////////// Refactored Code //////////////////////////////
    ////////////////////////////////////////////////////////////////////
    private static final int FILTER_VERSION = 1;
    //private URL mMediaURL;
    private LinkedHashSet mMediaURLs = new LinkedHashSet();
    private MediaRegion mRegion;
    private LinkedHashMap mMediaModels;
    private Resource mCurrInstance;
    private Store mLocalStore;
    protected Model mImportModel = ModelFactory.createDefaultModel();

    private Store mCurrentStore;

    private HashSet mFilters;

    public static boolean isDigitalMedia(Resource theRes)
    {
        // TODO: check against more types, or a supertype...
        return theRes.canAs(Image.class);// || JenaUtils.isType(theRes,PROFILE.Image());
    }

    public Iterator getFilters() {
        if (mFilters == null)
            loadFilters();

        return mFilters.iterator();
    }

    private void loadFilters() {
        mFilters = new HashSet();

        switch (FILTER_VERSION)
        {
            case 1:
            Preferences filters = getPrefs().node("filters-"+FILTER_VERSION);
            try {
                String[] aChildren = filters.childrenNames();
                for (int i = 0; i < aChildren.length; i++)
                {
                    String aName = aChildren[i];
                    Preferences aFilterNode = filters.node(aName);
                    String aQuery = aFilterNode.get("query","");
                    if (!aQuery.equals(""))
                    {
                        MediaFilter aFilter = new MediaFilterImpl(this,aQuery);
                        aFilter.setName(aName);
                        mFilters.add(aFilter);
                    }
                }
            }
            catch (Exception ex) {
                System.err.println("error loading filters");
            }
            break;
        }
    }

    public void removeFilter(MediaFilter theFilter) {
        mFilters.remove(theFilter);

        switch (FILTER_VERSION)
        {
            case 1:
            Preferences filters = getPrefs().node("filters-"+FILTER_VERSION);
            try {
                String[] aChildren = filters.childrenNames();
                for (int i = 0; i < aChildren.length; i++)
                {
                    String aName = aChildren[i];
                    if (aName.equals(theFilter.getName())) {
                        filters.node(aName).removeNode();
                        break;
                    }
                }
            }
            catch (Exception ex) {
                System.err.println("error removing filter");
            }
            break;
        }
    }

    public void addFilter(MediaFilter theFilter) {
        switch (FILTER_VERSION)
        {
            case 1:
            Preferences filters = getPrefs().node("filters-"+FILTER_VERSION);
            try {
                Preferences aFilterNode = filters.node(theFilter.getName());
                aFilterNode.put("query",theFilter.getQuery());
            }
            catch (Exception ex) {
                System.err.println("error adding filter");
            }
            break;
        }
        mFilters.add(theFilter);
    }

    /**
     * Returns the current media model
     * @return MediaModel the current image model
     */
//    public MediaModel getMediaModel() {
//        return getMediaModel(getMediaURL());
//    }

    public boolean hasMediaModel(URL theURL) {
        return getMediaModels().containsKey(theURL);
    }

    public void removeMediaModel(URL theURL) {
        getMediaModels().remove(theURL);
    }

    public MediaModel getMediaModel(URL url) {
        LinkedHashMap models = getMediaModels();
        MediaModel mediaModel = (MediaModel)models.get(url);
        if (mediaModel==null) {
            mediaModel = MediaModel.create(this,url);
            models.put(url, mediaModel);
        }
        return mediaModel;
    }

    public URL getMediaURL() {
        if (getMediaURLs().isEmpty())
            return null;
        return (URL)getMediaURLs().iterator().next();
    }

    private Set getMediaURLs() {
        return mMediaURLs;
    }

    public void addMediaURL(URL theURL) {
        getMediaURLs().add(theURL);
    }

    public void removeMediaURL(URL theURL) {
        getMediaURLs().remove(theURL);
    }

    public void setMediaURL(URL theURL) {
        getMediaModel(getMediaURL()).deselectAllRegions();

        mMediaURLs.clear();
        if (theURL != null)
            mMediaURLs.add(theURL);

        setCurrentRegion(null);

        // TODO: this used to fire an image changed event...should we still fire that?
        modelChanged(MarkupModelEvent.MEDIA_SELECTED);
    }

    public void setCurrentRegion(MediaRegion theRegion) {
        mRegion = theRegion;

//        Iterator aIter = getMediaURLs().iterator();
//        while (aIter.hasNext())
//        {
//            URL aURL = (URL)aIter.next();
//            if (theRegion == null) {
//                System.err.println("Resetting image region in setcurrRegion");
//                getMediaModel(aURL).deselectAllRegions();
//            }
//            else if (getMediaModel(aURL).containsRegion(theRegion))
//            {
//                System.err.println("Resetting a region in setcurrRegion");
//                getMediaModel(aURL).deselectAllRegions();
//                getMediaModel(aURL).setCurrentRegion(theRegion);
//                theRegion.setSelected(true);
//                break;
//            }
//        }

        MediaModel aModel = getMediaModel(getMediaURL());
        aModel.deselectAllRegions();
        aModel.setCurrentRegion(theRegion);
        if (theRegion != null)
            theRegion.setSelected(true);

        regionSelected();
    }

    public MediaRegion getCurrentRegion() {
        return mRegion;
        //return getMediaModel(getMediaURL()).getCurrentRegion();
    }

    /**
     * Returns the list of media models in the model
     * @return LinkedHashMap the list of media models
     */
    public LinkedHashMap getMediaModels() {
        if (mMediaModels == null) {
            mMediaModels = new LinkedHashMap();
        }
        return mMediaModels;
    }

    /**
     * Sets the list of media models
     * @param models LinkedHashMap the media model list
     */
    public void setMediaModels(LinkedHashMap models) {
        mMediaModels = models;
    }

    /**
     * Returns the current instance open for editing
     * @return Instance
     */
    public Resource getCurrentInstance() {
        return mCurrInstance;
    }

    /**
     * Sets the current instance of the model
     * @param inst Resource the new current instance
     */
    public void setCurrentInstance(Resource inst) {
        mCurrInstance = inst;

//        if (inst == null)
//            modelChanged(MarkupModelEvent.MEDIA_SELECTED);
    }

    public Store getCurrentStore() {
        return mCurrentStore;
    }

    public void setCurrentStore(Store theStore) {
        setMediaURL(null);

        mCurrentStore = theStore;

        // TODO: check if connection already exists
        mCurrentStore.connect();

        modelChanged(MarkupModelEvent.STORE_CHANGED);
    }

    public Store getLocalStore() {
        return mLocalStore;
    }

    private static Map ID_PROP_MAP = new HashMap();
    static {
        ID_PROP_MAP.put(MarkupModel.PROFILE.Region(),RDFS.label);
    }

    public void removeModel(Model theModel, Model theRemoveModel)
    {
        ResIterator rIter = theRemoveModel.listSubjects();
        while (rIter.hasNext())
        {
            Resource aRes = (Resource)rIter.next();

            if (aRes.isAnon())
            {
                Property idProp = null;
                Set aTypes = JenaUtils.getTypes(aRes,false);
                Iterator aIter = aTypes.iterator();
                while (aIter.hasNext()) {
                    Resource aType = (Resource)aIter.next();
                    if (ID_PROP_MAP.containsKey(aType)) {
                        idProp = (Property)ID_PROP_MAP.get(aType);
                        break;
                    }
                }

                if (idProp != null)
                {
                    RDFNode aIdNode = aRes.getProperty(idProp).getObject();

                    StmtIterator sIter = theModel.listStatements(null,idProp,aIdNode);
                    if (sIter.hasNext())
                    {
                        // should only be one hit
                        Statement aStmt = sIter.nextStatement();
                        sIter.close();

                        Resource aNode = aStmt.getSubject();

                        sIter = aRes.listProperties();
                        while (sIter.hasNext()) {
                            Statement temp = sIter.nextStatement();
                            theModel.remove(theModel.createStatement(aNode,temp.getPredicate(),temp.getObject()));
                        }
                        sIter.close();

                        sIter = theRemoveModel.listStatements(null,null,aRes);
                        while (sIter.hasNext()) {
                            Statement temp = sIter.nextStatement();
                            theModel.remove(theModel.createStatement(temp.getSubject(),temp.getPredicate(),aNode));
                        }
                        sIter.close();
                    }
                }
            }
            else {
                theModel.remove(aRes.listProperties());
            }
        }
    }
 
    public void useSwingWorker(MarkupModelEvent event, OntologyTreeView mTree, OntModel o, OntModel p, Resource r)  {
    	final MarkupModel model = this;
    	final MarkupModelEvent eventF = event;
    	final OntModel oF = o;
    	final OntModel previousModel = p;
    	final OntModel masterModel = model.getMasterOntologyModel();
    	final OntologyTreeView mTreeF = mTree;
    	final Resource resF = r;
    	
    	
//    	 create SwingWorker to process ontology using reasoner
		SwingWorker worker = new SwingWorker() {
			//boolean fail = false;
			public Object construct() {
				try {
					//System.out.println(" IN CONSTRUCT");
					model.getReasoner().addModel(oF);
					model.modelChanged(MarkupModelEvent.ONTOLOGY_LOADED);
				} 
				catch (Exception ex) {
					ex.printStackTrace();
				}	
				return null;
			}
			
			public void finished() {
				//System.out.println(" IN FINISHED");
				if(eventF.getID() == MarkupModelEvent.ADDED_ENTITY)  {
				//	model.removeModel(masterModel, previousModel);
				}
				if(resF instanceof OntClass)  {
					//System.out.println(" I AM ONT CLASS");
					mTreeF.getManager().setSelectedResource(resF);
				}
				//model.modelChanged(eventF.getID());
				//System.out.println(" CALLING MODEL CHANGED");
				model.modelChanged(MarkupModelEvent.ADDED_ENTITY);
				//System.out.println(" MODEL CHANGED");
			}
		};
		worker.start();		
    }
}
