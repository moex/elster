package fr.inrialpes.exmo.elster.picster.markup;

/*
 * @author Arun Sharma (INRIA, RWTH)
 *
*/

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import org.mindswap.pellet.output.TableData;
import org.mindswap.pellet.output.OutputFormatter;
import org.mindswap.pellet.output.ATermAbstractSyntaxRenderer;
import org.mindswap.pellet.output.ATermRenderer;

import com.hp.hpl.jena.query.ARQ;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.ResultSetFormatter;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.graph.Node;

import org.mindswap.markup.media.MarkupImageModel;
import fr.inrialpes.exmo.elster.picster.Picster;

import aterm.ATermAppl;

public class QueryInterface extends JSplitPane implements ActionListener {
	static public JFrame frame;	
    
	Razor mHandler;
	MarkupModel mModel;
	
	//Font tahoma = new Font("Tahoma", Font.PLAIN, 11);
    Font tahoma = new Font("SansSerif", Font.PLAIN, 11);
	
	// main panels of splitPane
	JPanel topPanel;
	JPanel bottomPanel;
	
	// top panel
	//JComboBox ontologies;
	// inside tabbedPane
	public JTextArea sparqlText;
	JButton sparqlRun;
	
	//bottom panel
	JEditorPane resultsPane;
        Picster ps;
	
	public QueryInterface(Razor mHandler, MarkupModel mModel, Picster p) {
		this.mHandler = mHandler;
		this.mModel = mModel;
                this.ps = p;
		this.setupUI();
	}
	
	public void setupUI() {
		this.setOrientation( JSplitPane.VERTICAL_SPLIT );
		
		topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );		
		bottomPanel = new JPanel();
		bottomPanel.setLayout( new BorderLayout() );		
		sparqlText = new JTextArea();
		sparqlText.setText( createInitialsparql() );
		sparqlText.setCaretPosition( 0 );
		
		JPanel sparqlPanel = new JPanel( new BorderLayout() );
		JLabel sparqlLabel = new JLabel( "Enter sparql Query Below:" );
		sparqlLabel.setFont(tahoma);
		
		sparqlPanel.setLayout( new BorderLayout() );
		JScrollPane sparqlScroll = new JScrollPane( sparqlText );
		
		JPanel botsparql = new JPanel();
		botsparql.setLayout( new BoxLayout( botsparql, BoxLayout.LINE_AXIS ) );
		botsparql.setComponentOrientation( ComponentOrientation.RIGHT_TO_LEFT );
		botsparql.setBorder( BorderFactory.createEmptyBorder( 5, 0, 0, 0 ) );
		
		sparqlRun = new JButton( "Run Query" );
		sparqlRun.setFont(tahoma);
		sparqlRun.addActionListener( this );
		botsparql.add( sparqlRun );
		
		sparqlPanel.add( sparqlLabel, BorderLayout.NORTH );
		sparqlPanel.add( sparqlScroll, BorderLayout.CENTER );
		sparqlPanel.add( botsparql, BorderLayout.SOUTH );
		sparqlPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
		
		JTabbedPane queryTabs = new JTabbedPane();
		queryTabs.setFont(tahoma);
		queryTabs.add( sparqlPanel, "sparql Query" );
		
		JPanel resultsPanel = new JPanel( new BorderLayout() );
		JLabel resultsLabel = new JLabel( "Query Results:" );
		resultsLabel.setFont(tahoma);
		resultsPane = new JEditorPane();
		resultsPane.setEditable( false );
		resultsPane.setContentType( "text/html" );
	//	resultsPane.addHyperlinkListener(mHandler.termDisplay);
		JScrollPane resultScroll = new JScrollPane( resultsPane );
		
		bottomPanel.add( resultsLabel, BorderLayout.NORTH );
		bottomPanel.add( resultScroll, BorderLayout.CENTER );
		bottomPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
		
		JPanel top = new JPanel();
		top.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );
		top.setLayout( new BoxLayout( top, BoxLayout.X_AXIS ) );
		topPanel.add( top, BorderLayout.NORTH );
		topPanel.add( queryTabs, BorderLayout.CENTER );		
		this.setTopComponent( topPanel );
		this.setBottomComponent( bottomPanel );
		this.setDividerLocation( 250 );
	}
	

    private String createInitialsparql() {
        String sparql = 
            	"SELECT *\n" +
        		"WHERE (?x, rdf:type, owl:Thing)\n";
        
    //    QNameShortFormProvider qnames = new QNameShortFormProvider();
        
     //   if(mModel.getShortForms() instanceof QNameShortFormProvider)
       //     qnames = (QNameShortFormProvider) mModel.getShortForms();

      //  Map map = new TreeMap();
       // addPrefix(RDFVocabularyAdapter.RDF, qnames, map);
        //addPrefix(RDFSVocabularyAdapter.RDFS, qnames, map);
        //addPrefix(OWLVocabularyAdapter.OWL, qnames, map);
       // try {
         //   OWLOntology sel = (OWLOntology) ontologies.getSelectedItem();
           // Set onts = OntologyHelper.importClosure(sel);
           // for(Iterator i = onts.iterator(); i.hasNext();) {
             //   OWLOntology ont = (OWLOntology) i.next();
               // URI uri = ont.getURI();
               // addPrefix(uri.toString(), qnames, map);
            //}
        //} catch(Exception e) {
           // e.printStackTrace();
        //} 
        
      //  sparql += "USING\n";
       // for(Iterator i = map.entrySet().iterator(); i.hasNext(); ) {
         //   Map.Entry entry = (Map.Entry) i.next();
          //  String prefix = (String) entry.getKey();
           // String uri = (String) entry.getValue();
            
          //  sparql += "   " + prefix + " FOR <" + uri + ">";
           // if(i.hasNext())
             //    sparql += ",";
           // sparql += "\n";
        //}    
        
        return sparql;
    }
    
    public void showImages(Set mResults)  {
        System.out.println("Reached shoeImages");
        Vector imgUrls = new Vector();
        Iterator itr = mResults.iterator();
        while(itr.hasNext())  {
            String uri = (String) itr.next();
            String imageLocation = (String)MarkupImageModel.getUriMap().get(uri);
            if(imageLocation == null)
                imageLocation = (String)MarkupImageModel.getRegUriMap().get(uri);
            imgUrls.add(imageLocation);
        }   
       
        
        for(int j = 0; j < imgUrls.size(); j++)  {
            try  {
                String url = (String) imgUrls.get(j);
                System.out.println("Image Location = " + url);
                URL aUrl = new URL(url);
                mHandler.loadURL(aUrl);
            } catch(Exception e)  {
                System.out.println("Error in image location");
            }
        }
    }
    
	public void actionPerformed(ActionEvent arg0) {
	      try {     	
                Model aModel = mHandler.getMarkupModel().getCurrentStore().asModel();
                System.out.println( "Running Query..." );
                Set aResults = new HashSet();
		String aQuery = sparqlText.getText();
              ////  ps.setQuery(aQuery);
                
                //JxtaCast jxtaCast = 
                
                ////PicsterQueryProcessor pqp = new PicsterQueryProcessor(aQuery, aModel);
                ////ResultSet aResultSet = pqp.performQuery();
                ////aResults = pqp.getResourceUrisAsSet(aResultSet);
                ////pqp.showImages(aResults, mHandler);
                
                /*System.out.println("I GOT THIS\n" + aQuery);
                      
		Query query = QueryFactory.create(aQuery);
		QueryExecution qe = QueryExecutionFactory.create(query, aModel);		
                System.err.println("Executing  query:   " + aQuery);               
        	ResultSet aResultSet = qe.execSelect();                
                while (aResultSet.hasNext())
                {
                    QuerySolution aSolution = aResultSet.nextSolution();
                    Resource aRes = (Resource)aSolution.get("uri");
                    Property p = aModel.getProperty("http://www.mindswap.org/~glapizco/technical.owl#", "depiction");
                    StmtIterator stmtr = aRes.listProperties(p);
                    while(stmtr.hasNext())  {
                        Statement st = stmtr.nextStatement();
                        String imguri = st.getObject().toString();
                        System.out.println("Depiction URI = " + imguri);
                        aResults.add(imguri);
                    }
                }
                qe.close();
        	
                showImages(aResults);*/
                
                
        } catch(Exception e) {
    		resultsPane.setText( "Failed to run the query!" );
    		e.printStackTrace();
        }		
        
	}
}
