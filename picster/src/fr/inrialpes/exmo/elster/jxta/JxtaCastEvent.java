package fr.inrialpes.exmo.elster.jxta;

/**
 * 
 * @author JXTA.org
 * @author Arun Sharma (INRIA, RWTH)
 *
 */
public class JxtaCastEvent {

    public final static int SEND = 0;  // We are sending this file.
    public final static int RECV = 1;  // We are receiving this file.

    public int    transType;           // SEND or RECV.
    public String filename;            // File name.
    public String filepath;            // Path to file.
    public String sender;              // Name of sending peer.
    public String senderId;            // ID of sending peer.
    public String destId;              // destinationId
    public String caption;             // File description.
    public float  percentDone;         // 0.0 to 100.0
}


