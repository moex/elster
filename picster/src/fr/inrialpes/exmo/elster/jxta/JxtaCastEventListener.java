package fr.inrialpes.exmo.elster.jxta;


public interface JxtaCastEventListener {

    public void jxtaCastProgress(JxtaCastEvent e);
}
