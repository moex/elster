/*
 * QueryFromPeer.java
 *
 * Created on May 5, 2006, 1:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package fr.inrialpes.exmo.elster;

/**
 *
 * @author Arun Sharma
 */
public class QueryFromPeer {
    private String peerId;
    private String peerName;
    private String query;
    public QueryFromPeer(String id, String name, String text)  {
        this.peerId = id;
        this.peerName = name;
        this.query = text;    
    }

    public String getSenderId()  {
	    return peerId;
    }
    public String getQuery()  {
	    return query;
    }
    public String getSenderName()  {
        return peerName;
    }
}